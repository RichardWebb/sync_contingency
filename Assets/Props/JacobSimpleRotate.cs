﻿using UnityEngine;
using System.Collections;

public class JacobSimpleRotate : MonoBehaviour {
	public int MyAxis;
	public float speed;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (MyAxis == 1) {
			transform.Rotate (speed, 0, 0);
		}
		if (MyAxis == 2) {
			transform.Rotate (0, speed, 0);
		}
		if (MyAxis == 3) {
			transform.Rotate (0, 0, speed);
		}
	}
}
