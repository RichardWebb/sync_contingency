﻿using UnityEngine;
using System.Collections;

public class ConveyorBeltLerp : MonoBehaviour {
	public Renderer rend;
	public float offset;
	public float offsetOffset;
	public Material BeltMat;
	public float MyTime;
	// Use this for initialization
	void Start () {
		//MyMaterials = rend.GetComponents<Material>();
	}
	
	// Update is called once per frame
	void Update () {
		offset = Time.time * MyTime;

		BeltMat.SetTextureOffset("_MainTex", new Vector2(0, offset + offsetOffset));

	}
}
