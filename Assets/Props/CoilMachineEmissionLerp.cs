﻿using UnityEngine;
using System.Collections;

public class CoilMachineEmissionLerp : MonoBehaviour {
	public Color MyColour;
	public Color colorA;
	public Color colorB;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		MyColour = Color.Lerp(colorA, colorB, Mathf.PingPong(Time.time * 2, 1));
		GetComponent<Renderer>().material.SetColor ("_EmissionColor", MyColour);
	}
}
