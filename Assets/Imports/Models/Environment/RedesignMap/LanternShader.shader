// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.25 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.25;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:0,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.1470588,fgcg:0.1470588,fgcb:0.1470588,fgca:1,fgde:0,fgrn:0,fgrf:400,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:9525,x:33085,y:32670,varname:node_9525,prsc:2|custl-5124-OUT;n:type:ShaderForge.SFN_NormalVector,id:4565,x:31716,y:32541,prsc:2,pt:False;n:type:ShaderForge.SFN_LightVector,id:7378,x:31732,y:32776,varname:node_7378,prsc:2;n:type:ShaderForge.SFN_ViewVector,id:8928,x:31719,y:32932,varname:node_8928,prsc:2;n:type:ShaderForge.SFN_Dot,id:1944,x:31937,y:32584,varname:node_1944,prsc:2,dt:0|A-4565-OUT,B-7378-OUT;n:type:ShaderForge.SFN_Dot,id:8292,x:31939,y:32865,varname:node_8292,prsc:2,dt:0|A-4565-OUT,B-8928-OUT;n:type:ShaderForge.SFN_Multiply,id:2464,x:32132,y:32597,varname:node_2464,prsc:2|A-1944-OUT,B-3201-OUT;n:type:ShaderForge.SFN_Multiply,id:9013,x:32121,y:32852,varname:node_9013,prsc:2|A-8292-OUT,B-567-OUT;n:type:ShaderForge.SFN_Vector1,id:567,x:31939,y:33031,varname:node_567,prsc:2,v1:0.8;n:type:ShaderForge.SFN_Vector1,id:3201,x:32033,y:32757,varname:node_3201,prsc:2,v1:0.3;n:type:ShaderForge.SFN_Add,id:9825,x:32330,y:32617,varname:node_9825,prsc:2|A-2464-OUT,B-2174-OUT;n:type:ShaderForge.SFN_Vector1,id:2174,x:32250,y:32757,varname:node_2174,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Append,id:9586,x:32490,y:32802,varname:node_9586,prsc:2|A-9013-OUT,B-9825-OUT;n:type:ShaderForge.SFN_Tex2d,id:1168,x:32732,y:32785,varname:node_1168,prsc:2,tex:507afa2f62128394ba2669c5baac61a5,ntxv:0,isnm:False|UVIN-9586-OUT,TEX-6400-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:6400,x:32464,y:32987,ptovrint:False,ptlb:node_6400,ptin:_node_6400,varname:node_6400,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:507afa2f62128394ba2669c5baac61a5,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:5124,x:32910,y:32810,varname:node_5124,prsc:2|A-1168-RGB,B-6946-RGB,C-2845-OUT;n:type:ShaderForge.SFN_LightColor,id:6946,x:32716,y:32971,varname:node_6946,prsc:2;n:type:ShaderForge.SFN_LightAttenuation,id:2845,x:32693,y:33148,varname:node_2845,prsc:2;proporder:6400;pass:END;sub:END;*/

Shader "Custom/LanternShader" {
    Properties {
        _node_6400 ("node_6400", 2D) = "white" {}
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma target 3.0
            uniform sampler2D _node_6400; uniform float4 _node_6400_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                LIGHTING_COORDS(2,3)
                UNITY_FOG_COORDS(4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float2 node_9586 = float2((dot(i.normalDir,viewDirection)*0.8),((dot(i.normalDir,lightDirection)*0.3)+0.5));
                float4 node_1168 = tex2D(_node_6400,node_9586);
                float3 finalColor = (node_1168.rgb*_LightColor0.rgb*attenuation);
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma target 3.0
            uniform sampler2D _node_6400; uniform float4 _node_6400_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                LIGHTING_COORDS(2,3)
                UNITY_FOG_COORDS(4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float2 node_9586 = float2((dot(i.normalDir,viewDirection)*0.8),((dot(i.normalDir,lightDirection)*0.3)+0.5));
                float4 node_1168 = tex2D(_node_6400,node_9586);
                float3 finalColor = (node_1168.rgb*_LightColor0.rgb*attenuation);
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
