﻿using UnityEngine;
using System.Collections;

public class PondLift : MonoBehaviour {

	public bool UpDown;
	public float MoveFloat;
	public Vector3 Max;
	public Vector3 Min;

	public float t;


	public GameObject MyPondLiftObject;

	void OnTriggerStay (Collider other)
	{
		t = 0;
		if (other.tag == "Player") {
			UpDown = true;
		}
	}
	void OnTriggerExit (Collider other)
	{
		t = 0;
			UpDown = false;

	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		t = t + Time.deltaTime;



		if (UpDown == true) {
			
			MyPondLiftObject.transform.position = Vector3.Lerp(MyPondLiftObject.transform.position, Max, t * 2);
		}

		if (UpDown == false) {

			MyPondLiftObject.transform.position = Vector3.Lerp(MyPondLiftObject.transform.position, Min, t / 2);
		}




	}
}
