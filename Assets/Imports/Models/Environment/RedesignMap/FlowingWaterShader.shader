// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.25 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.25;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:3,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:1,bsrc:0,bdst:1,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:4013,x:34104,y:31685,varname:node_4013,prsc:2|diff-4588-OUT,spec-6562-OUT,gloss-7094-OUT,normal-9751-OUT,alpha-4461-OUT,refract-6107-OUT,voffset-4585-OUT;n:type:ShaderForge.SFN_Slider,id:6562,x:33435,y:32361,ptovrint:False,ptlb:Metallic,ptin:_Metallic,varname:node_6562,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Slider,id:7094,x:33435,y:32461,ptovrint:False,ptlb:Smoothness,ptin:_Smoothness,varname:_Metallic_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.8205138,max:1;n:type:ShaderForge.SFN_Slider,id:4461,x:33435,y:32576,ptovrint:False,ptlb:Opacity,ptin:_Opacity,varname:_Smoothness_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.1282062,max:1;n:type:ShaderForge.SFN_ValueProperty,id:536,x:32293,y:32537,ptovrint:False,ptlb:Detail Speed U,ptin:_DetailSpeedU,varname:node_536,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_ValueProperty,id:6381,x:32293,y:32650,ptovrint:False,ptlb:Detail Speed V,ptin:_DetailSpeedV,varname:_DetailSpeed_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.5;n:type:ShaderForge.SFN_Append,id:8189,x:32459,y:32555,varname:node_8189,prsc:2|A-536-OUT,B-6381-OUT;n:type:ShaderForge.SFN_Time,id:8517,x:32293,y:32796,varname:node_8517,prsc:2;n:type:ShaderForge.SFN_Multiply,id:8780,x:32470,y:32762,varname:node_8780,prsc:2|A-8189-OUT,B-8517-T;n:type:ShaderForge.SFN_Add,id:164,x:32671,y:32511,varname:node_164,prsc:2|A-2432-UVOUT,B-8780-OUT;n:type:ShaderForge.SFN_TexCoord,id:2432,x:32432,y:32343,varname:node_2432,prsc:2,uv:0;n:type:ShaderForge.SFN_Tex2d,id:9523,x:32914,y:32416,ptovrint:False,ptlb:Albedo,ptin:_Albedo,varname:node_9523,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:28c7aad1372ff114b90d330f8a2dd938,ntxv:2,isnm:False|UVIN-164-OUT;n:type:ShaderForge.SFN_RemapRange,id:2744,x:32921,y:32201,varname:node_2744,prsc:2,frmn:0,frmx:1,tomn:0.1,tomx:0.8|IN-9523-RGB;n:type:ShaderForge.SFN_Color,id:8296,x:32921,y:32039,ptovrint:False,ptlb:Colour,ptin:_Colour,varname:node_8296,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.7132353,c2:0.9525355,c3:1,c4:1;n:type:ShaderForge.SFN_Blend,id:4588,x:33112,y:32105,varname:node_4588,prsc:2,blmd:12,clmp:True|SRC-8296-RGB,DST-2744-OUT;n:type:ShaderForge.SFN_Tex2d,id:9158,x:32933,y:32622,ptovrint:False,ptlb:node_9158,ptin:_node_9158,varname:node_9158,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:fc7aa2f8825f9264baeddde6063bc8b6,ntxv:3,isnm:True|UVIN-164-OUT;n:type:ShaderForge.SFN_Append,id:5452,x:33137,y:32670,varname:node_5452,prsc:2|A-9158-R,B-9158-G;n:type:ShaderForge.SFN_Slider,id:2887,x:32954,y:32813,ptovrint:False,ptlb:Refraction Amount,ptin:_RefractionAmount,varname:node_2887,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.1025644,max:0.5;n:type:ShaderForge.SFN_Multiply,id:6107,x:33319,y:32718,varname:node_6107,prsc:2|A-5452-OUT,B-2887-OUT;n:type:ShaderForge.SFN_Multiply,id:4585,x:33743,y:32954,varname:node_4585,prsc:2|A-3601-OUT,B-3449-OUT;n:type:ShaderForge.SFN_NormalVector,id:3449,x:33743,y:33107,prsc:2,pt:False;n:type:ShaderForge.SFN_Clamp01,id:3601,x:33474,y:33025,varname:node_3601,prsc:2|IN-5603-OUT;n:type:ShaderForge.SFN_Add,id:5603,x:33289,y:33025,varname:node_5603,prsc:2|A-1465-OUT,B-8510-OUT;n:type:ShaderForge.SFN_Multiply,id:1465,x:33511,y:32856,varname:node_1465,prsc:2|A-9523-RGB,B-1848-OUT;n:type:ShaderForge.SFN_Slider,id:1848,x:32933,y:32945,ptovrint:False,ptlb:Vertex Offset,ptin:_VertexOffset,varname:node_1848,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.02614926,max:0.25;n:type:ShaderForge.SFN_Multiply,id:8510,x:33462,y:33169,varname:node_8510,prsc:2|A-6755-OUT,B-9109-OUT;n:type:ShaderForge.SFN_Multiply,id:6755,x:33252,y:33169,varname:node_6755,prsc:2|A-5148-OUT,B-3546-OUT;n:type:ShaderForge.SFN_Slider,id:9109,x:33189,y:33440,ptovrint:False,ptlb:Wave Offset,ptin:_WaveOffset,varname:node_9109,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.4140792,max:0.5;n:type:ShaderForge.SFN_Vector1,id:2311,x:32649,y:33323,varname:node_2311,prsc:2,v1:0.5;n:type:ShaderForge.SFN_Power,id:3546,x:32937,y:33267,varname:node_3546,prsc:2|VAL-2455-OUT,EXP-2311-OUT;n:type:ShaderForge.SFN_Power,id:5148,x:32927,y:33140,varname:node_5148,prsc:2|VAL-3898-OUT,EXP-2311-OUT;n:type:ShaderForge.SFN_OneMinus,id:2455,x:32649,y:33107,varname:node_2455,prsc:2|IN-3898-OUT;n:type:ShaderForge.SFN_Add,id:3898,x:32634,y:32956,varname:node_3898,prsc:2|A-6343-OUT,B-5718-OUT;n:type:ShaderForge.SFN_Power,id:5718,x:32385,y:33099,varname:node_5718,prsc:2|VAL-6921-OUT,EXP-3613-OUT;n:type:ShaderForge.SFN_Power,id:6343,x:32385,y:32956,varname:node_6343,prsc:2|VAL-1527-OUT,EXP-3613-OUT;n:type:ShaderForge.SFN_OneMinus,id:1527,x:32154,y:32956,varname:node_1527,prsc:2|IN-6921-OUT;n:type:ShaderForge.SFN_Frac,id:6921,x:32154,y:33099,varname:node_6921,prsc:2|IN-8558-OUT;n:type:ShaderForge.SFN_Vector1,id:3613,x:32195,y:33288,varname:node_3613,prsc:2,v1:5;n:type:ShaderForge.SFN_ComponentMask,id:8558,x:31710,y:32958,varname:node_8558,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-7388-OUT;n:type:ShaderForge.SFN_Time,id:3362,x:31560,y:33388,varname:node_3362,prsc:2;n:type:ShaderForge.SFN_Vector1,id:6744,x:31431,y:33154,varname:node_6744,prsc:2,v1:0;n:type:ShaderForge.SFN_ValueProperty,id:9686,x:31422,y:33230,ptovrint:False,ptlb:Wave1,ptin:_Wave1,varname:node_9686,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Append,id:5262,x:31604,y:33186,varname:node_5262,prsc:2|A-6744-OUT,B-9686-OUT;n:type:ShaderForge.SFN_Multiply,id:1837,x:31756,y:33371,varname:node_1837,prsc:2|A-5262-OUT,B-3362-T;n:type:ShaderForge.SFN_Add,id:7388,x:31930,y:33274,varname:node_7388,prsc:2|A-5663-UVOUT,B-1837-OUT;n:type:ShaderForge.SFN_TexCoord,id:5663,x:31756,y:33227,varname:node_5663,prsc:2,uv:0;n:type:ShaderForge.SFN_ComponentMask,id:7371,x:33187,y:32266,varname:node_7371,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-9158-RGB;n:type:ShaderForge.SFN_Slider,id:5606,x:33255,y:32172,ptovrint:False,ptlb:NormalStrength,ptin:_NormalStrength,varname:node_5606,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1.156658,max:10;n:type:ShaderForge.SFN_Vector1,id:4520,x:33187,y:32413,varname:node_4520,prsc:2,v1:1;n:type:ShaderForge.SFN_Append,id:9751,x:33711,y:32038,varname:node_9751,prsc:2|A-2404-OUT,B-4520-OUT;n:type:ShaderForge.SFN_Multiply,id:2404,x:33633,y:32211,varname:node_2404,prsc:2|A-7371-OUT,B-5606-OUT;proporder:6562-4461-7094-536-6381-9523-8296-9158-2887-1848-9109-9686-5606;pass:END;sub:END;*/

Shader "Custom/FlowingWaterShader" {
    Properties {
        _Metallic ("Metallic", Range(0, 1)) = 1
        _Opacity ("Opacity", Range(0, 1)) = 0.1282062
        _Smoothness ("Smoothness", Range(0, 1)) = 0.8205138
        _DetailSpeedU ("Detail Speed U", Float ) = 0
        _DetailSpeedV ("Detail Speed V", Float ) = 0.5
        _Albedo ("Albedo", 2D) = "black" {}
        _Colour ("Colour", Color) = (0.7132353,0.9525355,1,1)
        _node_9158 ("node_9158", 2D) = "bump" {}
        _RefractionAmount ("Refraction Amount", Range(0, 0.5)) = 0.1025644
        _VertexOffset ("Vertex Offset", Range(0, 0.25)) = 0.02614926
        _WaveOffset ("Wave Offset", Range(0, 0.5)) = 0.4140792
        _Wave1 ("Wave1", Float ) = 1
        _NormalStrength ("NormalStrength", Range(0, 10)) = 1.156658
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        GrabPass{ }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Front
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            #pragma glsl
            uniform sampler2D _GrabTexture;
            uniform float4 _TimeEditor;
            uniform float _Metallic;
            uniform float _Smoothness;
            uniform float _Opacity;
            uniform float _DetailSpeedU;
            uniform float _DetailSpeedV;
            uniform sampler2D _Albedo; uniform float4 _Albedo_ST;
            uniform float4 _Colour;
            uniform sampler2D _node_9158; uniform float4 _node_9158_ST;
            uniform float _RefractionAmount;
            uniform float _VertexOffset;
            uniform float _WaveOffset;
            uniform float _Wave1;
            uniform float _NormalStrength;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                float4 screenPos : TEXCOORD5;
                UNITY_FOG_COORDS(6)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(-v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                float4 node_8517 = _Time + _TimeEditor;
                float2 node_164 = (o.uv0+(float2(_DetailSpeedU,_DetailSpeedV)*node_8517.g));
                float4 _Albedo_var = tex2Dlod(_Albedo,float4(TRANSFORM_TEX(node_164, _Albedo),0.0,0));
                float4 node_3362 = _Time + _TimeEditor;
                float node_6921 = frac((o.uv0+(float2(0.0,_Wave1)*node_3362.g)).r);
                float node_3613 = 5.0;
                float node_3898 = (pow((1.0 - node_6921),node_3613)+pow(node_6921,node_3613));
                float node_2311 = 0.5;
                v.vertex.xyz += (saturate(((_Albedo_var.rgb*_VertexOffset)+((pow(node_3898,node_2311)*pow((1.0 - node_3898),node_2311))*_WaveOffset)))*v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.screenPos = o.pos;
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float4 node_8517 = _Time + _TimeEditor;
                float2 node_164 = (i.uv0+(float2(_DetailSpeedU,_DetailSpeedV)*node_8517.g));
                float3 _node_9158_var = UnpackNormal(tex2D(_node_9158,TRANSFORM_TEX(node_164, _node_9158)));
                float3 normalLocal = float3((_node_9158_var.rgb.rg*_NormalStrength),1.0);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5 + (float2(_node_9158_var.r,_node_9158_var.g)*_RefractionAmount);
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = 1;
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float gloss = _Smoothness;
                float specPow = exp2( gloss * 10.0+1.0);
/////// GI Data:
                UnityLight light;
                #ifdef LIGHTMAP_OFF
                    light.color = lightColor;
                    light.dir = lightDirection;
                    light.ndotl = LambertTerm (normalDirection, light.dir);
                #else
                    light.color = half3(0.f, 0.f, 0.f);
                    light.ndotl = 0.0f;
                    light.dir = half3(0.f, 0.f, 0.f);
                #endif
                UnityGIInput d;
                d.light = light;
                d.worldPos = i.posWorld.xyz;
                d.worldViewDir = viewDirection;
                d.atten = attenuation;
                Unity_GlossyEnvironmentData ugls_en_data;
                ugls_en_data.roughness = 1.0 - gloss;
                ugls_en_data.reflUVW = viewReflectDirection;
                UnityGI gi = UnityGlobalIllumination(d, 1, normalDirection, ugls_en_data );
                lightDirection = gi.light.dir;
                lightColor = gi.light.color;
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float LdotH = max(0.0,dot(lightDirection, halfDirection));
                float4 _Albedo_var = tex2D(_Albedo,TRANSFORM_TEX(node_164, _Albedo));
                float3 diffuseColor = saturate((_Colour.rgb > 0.5 ?  (1.0-(1.0-2.0*(_Colour.rgb-0.5))*(1.0-(_Albedo_var.rgb*0.7+0.1))) : (2.0*_Colour.rgb*(_Albedo_var.rgb*0.7+0.1))) ); // Need this for specular when using metallic
                float specularMonochrome;
                float3 specularColor;
                diffuseColor = DiffuseAndSpecularFromMetallic( diffuseColor, _Metallic, specularColor, specularMonochrome );
                specularMonochrome = 1-specularMonochrome;
                float NdotV = max(0.0,dot( normalDirection, viewDirection ));
                float NdotH = max(0.0,dot( normalDirection, halfDirection ));
                float VdotH = max(0.0,dot( viewDirection, halfDirection ));
                float visTerm = SmithBeckmannVisibilityTerm( NdotL, NdotV, 1.0-gloss );
                float normTerm = max(0.0, NDFBlinnPhongNormalizedTerm(NdotH, RoughnessToSpecPower(1.0-gloss)));
                float specularPBL = max(0, (NdotL*visTerm*normTerm) * unity_LightGammaCorrectionConsts_PIDiv4 );
                float3 directSpecular = (floor(attenuation) * _LightColor0.xyz) * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularPBL*lightColor*FresnelTerm(specularColor, LdotH);
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                float3 directDiffuse = ((1 +(fd90 - 1)*pow((1.00001-NdotL), 5)) * (1 + (fd90 - 1)*pow((1.00001-NdotV), 5)) * NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(lerp(sceneColor.rgb, finalColor,_Opacity),1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            Cull Front
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdadd
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            #pragma glsl
            uniform sampler2D _GrabTexture;
            uniform float4 _TimeEditor;
            uniform float _Metallic;
            uniform float _Smoothness;
            uniform float _Opacity;
            uniform float _DetailSpeedU;
            uniform float _DetailSpeedV;
            uniform sampler2D _Albedo; uniform float4 _Albedo_ST;
            uniform float4 _Colour;
            uniform sampler2D _node_9158; uniform float4 _node_9158_ST;
            uniform float _RefractionAmount;
            uniform float _VertexOffset;
            uniform float _WaveOffset;
            uniform float _Wave1;
            uniform float _NormalStrength;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                float3 tangentDir : TEXCOORD3;
                float3 bitangentDir : TEXCOORD4;
                float4 screenPos : TEXCOORD5;
                LIGHTING_COORDS(6,7)
                UNITY_FOG_COORDS(8)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(-v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                float4 node_8517 = _Time + _TimeEditor;
                float2 node_164 = (o.uv0+(float2(_DetailSpeedU,_DetailSpeedV)*node_8517.g));
                float4 _Albedo_var = tex2Dlod(_Albedo,float4(TRANSFORM_TEX(node_164, _Albedo),0.0,0));
                float4 node_3362 = _Time + _TimeEditor;
                float node_6921 = frac((o.uv0+(float2(0.0,_Wave1)*node_3362.g)).r);
                float node_3613 = 5.0;
                float node_3898 = (pow((1.0 - node_6921),node_3613)+pow(node_6921,node_3613));
                float node_2311 = 0.5;
                v.vertex.xyz += (saturate(((_Albedo_var.rgb*_VertexOffset)+((pow(node_3898,node_2311)*pow((1.0 - node_3898),node_2311))*_WaveOffset)))*v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                o.screenPos = o.pos;
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                #if UNITY_UV_STARTS_AT_TOP
                    float grabSign = -_ProjectionParams.x;
                #else
                    float grabSign = _ProjectionParams.x;
                #endif
                i.normalDir = normalize(i.normalDir);
                i.screenPos = float4( i.screenPos.xy / i.screenPos.w, 0, 0 );
                i.screenPos.y *= _ProjectionParams.x;
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float4 node_8517 = _Time + _TimeEditor;
                float2 node_164 = (i.uv0+(float2(_DetailSpeedU,_DetailSpeedV)*node_8517.g));
                float3 _node_9158_var = UnpackNormal(tex2D(_node_9158,TRANSFORM_TEX(node_164, _node_9158)));
                float3 normalLocal = float3((_node_9158_var.rgb.rg*_NormalStrength),1.0);
                float3 normalDirection = normalize(mul( normalLocal, tangentTransform )); // Perturbed normals
                float2 sceneUVs = float2(1,grabSign)*i.screenPos.xy*0.5+0.5 + (float2(_node_9158_var.r,_node_9158_var.g)*_RefractionAmount);
                float4 sceneColor = tex2D(_GrabTexture, sceneUVs);
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float gloss = _Smoothness;
                float specPow = exp2( gloss * 10.0+1.0);
////// Specular:
                float NdotL = max(0, dot( normalDirection, lightDirection ));
                float LdotH = max(0.0,dot(lightDirection, halfDirection));
                float4 _Albedo_var = tex2D(_Albedo,TRANSFORM_TEX(node_164, _Albedo));
                float3 diffuseColor = saturate((_Colour.rgb > 0.5 ?  (1.0-(1.0-2.0*(_Colour.rgb-0.5))*(1.0-(_Albedo_var.rgb*0.7+0.1))) : (2.0*_Colour.rgb*(_Albedo_var.rgb*0.7+0.1))) ); // Need this for specular when using metallic
                float specularMonochrome;
                float3 specularColor;
                diffuseColor = DiffuseAndSpecularFromMetallic( diffuseColor, _Metallic, specularColor, specularMonochrome );
                specularMonochrome = 1-specularMonochrome;
                float NdotV = max(0.0,dot( normalDirection, viewDirection ));
                float NdotH = max(0.0,dot( normalDirection, halfDirection ));
                float VdotH = max(0.0,dot( viewDirection, halfDirection ));
                float visTerm = SmithBeckmannVisibilityTerm( NdotL, NdotV, 1.0-gloss );
                float normTerm = max(0.0, NDFBlinnPhongNormalizedTerm(NdotH, RoughnessToSpecPower(1.0-gloss)));
                float specularPBL = max(0, (NdotL*visTerm*normTerm) * unity_LightGammaCorrectionConsts_PIDiv4 );
                float3 directSpecular = attenColor * pow(max(0,dot(halfDirection,normalDirection)),specPow)*specularPBL*lightColor*FresnelTerm(specularColor, LdotH);
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                float3 directDiffuse = ((1 +(fd90 - 1)*pow((1.00001-NdotL), 5)) * (1 + (fd90 - 1)*pow((1.00001-NdotV), 5)) * NdotL) * attenColor;
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(finalColor * _Opacity,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            #pragma glsl
            uniform float4 _TimeEditor;
            uniform float _DetailSpeedU;
            uniform float _DetailSpeedV;
            uniform sampler2D _Albedo; uniform float4 _Albedo_ST;
            uniform float _VertexOffset;
            uniform float _WaveOffset;
            uniform float _Wave1;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(-v.normal);
                float4 node_8517 = _Time + _TimeEditor;
                float2 node_164 = (o.uv0+(float2(_DetailSpeedU,_DetailSpeedV)*node_8517.g));
                float4 _Albedo_var = tex2Dlod(_Albedo,float4(TRANSFORM_TEX(node_164, _Albedo),0.0,0));
                float4 node_3362 = _Time + _TimeEditor;
                float node_6921 = frac((o.uv0+(float2(0.0,_Wave1)*node_3362.g)).r);
                float node_3613 = 5.0;
                float node_3898 = (pow((1.0 - node_6921),node_3613)+pow(node_6921,node_3613));
                float node_2311 = 0.5;
                v.vertex.xyz += (saturate(((_Albedo_var.rgb*_VertexOffset)+((pow(node_3898,node_2311)*pow((1.0 - node_3898),node_2311))*_WaveOffset)))*v.normal);
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
