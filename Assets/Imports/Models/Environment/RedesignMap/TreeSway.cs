﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
public class TreeSway : MonoBehaviour {

	public float rotateSpeed;
	public float SwayLimit;
	// Use this for initialization
	void Start () {
		transform.rotation = Quaternion.Euler( Vector3.zero);
		//transform.DOLocalRotate (new Vector3(transform.localRotation.x, transform.localRotation.y, 55), 1.1f).SetEase (Ease.Linear).SetLoops (-1, LoopType.Yoyo);
		//transform.DORotate(new Vector3(transform.rotation.x, transform.rotation.y, 1.5f), 1.1f).SetEase (Ease.Linear).SetLoops (-1, LoopType.Yoyo );
		transform.DOLocalRotate(new Vector3(0, 0, SwayLimit), rotateSpeed).SetEase (Ease.InOutSine).SetLoops (-1, LoopType.Yoyo );


	}
	
	// Update is called once per frame
	void Update () {
		//Quaternion r = Quaternion.Euler (transform.localRotation.x, transform.localRotation.y, transform.localRotation.z);//Mathf.PingPong(Time.time * rotateSpeed, SwayLimit));
		//Debug.Log (r);
		//transform.localRotation = r ;
	}

}
