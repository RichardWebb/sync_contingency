﻿using UnityEngine;
using System.Collections;

public class GateMind : MonoBehaviour {

	public GameObject BlueGate;
	public GameObject OrangeGate;
	public float BlueGateDest;
	public float OrangeGateDest;

	float Bluet;
	float Oranget;

	public IEnumerator BlueGateRaise()
	{
		Bluet = 0;
		while (Bluet < 30) {
			yield return new WaitForSeconds (0.01f);
			BlueGate.transform.position = Vector3.Lerp (BlueGate.transform.position, new Vector3 (BlueGate.transform.position.x, BlueGateDest, BlueGate.transform.position.z), Bluet);
		}
	}

	public IEnumerator OrangeGateRaise()
	{
		Oranget = 0;
		while (Oranget < 30) {
			yield return new WaitForSeconds (0.01f);
			OrangeGate.transform.position = Vector3.Lerp (OrangeGate.transform.position, new Vector3 (OrangeGate.transform.position.x, OrangeGateDest, OrangeGate.transform.position.z), Oranget);
		}
	}



	void OpenOrange()
	{
		StartCoroutine (OrangeGateRaise ());
	}



	void OpenBlue()
	{
		StartCoroutine (BlueGateRaise ());
	}



	// Update is called once per frame
	void Update () {
	
		Bluet = Bluet + Time.deltaTime / 16;
		Oranget = Oranget + Time.deltaTime / 16;


		if (Input.GetKeyDown (KeyCode.Q)) {


		//	StartCoroutine (BlueGateRaise ());


		}
		if (Input.GetKeyDown (KeyCode.Y)) {


		


		}

	}
}
