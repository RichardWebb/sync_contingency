﻿/*
 * Animation Offset View
 * This class handles the delayed startup of animations
 * Created by Daniel O'Neill, s1303529 
 */

using UnityEngine;
using System.Collections;

public class AnimationOffsetView : MonoBehaviour {

    Animator _anim;

	// Use this for initialization
	void Start () {
        _anim = GetComponent<Animator>();
        Invoke("StartAnim", Random.Range(0f, 2f));
	}
	

    //Starts up the animator after the random invoke delay
	void StartAnim()
    {
        _anim.enabled = true;
        Destroy(this);
    }
}
