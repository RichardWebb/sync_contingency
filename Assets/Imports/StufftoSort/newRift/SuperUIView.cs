using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using pulsar.view.characters.super;
using Robotlegs.Bender.Platforms.Unity.Extensions.Mediation.Impl;
using pulsar.view.interfaces;
using System;

[ExecuteInEditMode]
public class SuperUIView : EventView, ISuperListener
{

	[SerializeField]
	RectTransform[] _hex = new RectTransform[6];
	private Vector2[] _starts = new Vector2[6];

	[SerializeField]
	RectTransform _rootHex;

	[Range(0.0f, 100.0f)]
	public float _charge;
    [SerializeField]
    private UltimateView _ulti;

	private float _scaler;
	private bool _synced;

	[SerializeField]
	private GameObject _syncImage;
    
    [SerializeField]
    private CanvasGroup _chargedTextCanvasGroup;

    private Vector3 startingScale;

    //16.6

    // Use this for initialization
    protected override void Start ()
    {
        startingScale = _rootHex.localScale;
        base.Start();
        for (int i = 0; i < _hex.Length; i++)
        {
            _starts[i] = _hex[i].anchoredPosition;
        }

        StartCoroutine(DOTimer());
	}

    private IEnumerator DOTimer()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.5f);

            if (!_synced)
            {
                _charge = (_ulti.timeUntilSuper / _ulti.superCooldown) * 100;
                //  _charge = 100 - ((timer / 120)) * 100;//((_ulti.timeUntilSuper/_ulti.superCooldown)*100);
                // _charge = 100 - ((_charge / 120)) * 100;//((_ulti.timeUntilSuper/_ulti.superCooldown)*100);

                if (_charge >= 0 && _charge <= 16.6f)
                {
                    _scaler = (_charge / 16.6f);
                    _hex[0].DOScale(_scaler, 0.1f).SetEase(Ease.Linear);

                    //_hex[0].localScale = new Vector3(_scaler, _scaler, _scaler);
                }
                if (_charge > 16.6f && _charge <= 33.2f)
                {
                    _scaler = ((_charge - 16.6f) / 16.6f);
                    TweenHex(0);
                  //  _hex[0].localScale = Vector3.one;
                  //  _hex[1].localScale = new Vector3(_scaler, _scaler, _scaler);
                }
                if (_charge > 33.2f && _charge <= 50f)
                {
                    _scaler = ((_charge - 33.2f) / 16.6f);
                     _hex[0].localScale = Vector3.one;

                    TweenHex(1);
                   // _hex[1].localScale = Vector3.one;
                   // _hex[2].localScale = new Vector3(_scaler, _scaler, _scaler);
                }
                if (_charge > 50 && _charge <= 66.6f)
                {
                    _scaler = ((_charge - 50) / 16.6f);

                      _hex[0].localScale = Vector3.one;
                      _hex[1].localScale = Vector3.one;


                    TweenHex(2);

                   // _hex[2].localScale = Vector3.one;
                   // _hex[3].localScale = new Vector3(_scaler, _scaler, _scaler);
                }
                if (_charge > 66.6f && _charge <= 83.2f)
                {
                    _scaler = ((_charge - 66.6f) / 16.6f);
                    _hex[0].localScale = Vector3.one;
                    _hex[1].localScale = Vector3.one;
                    _hex[2].localScale = Vector3.one;


                    TweenHex(3);


                    //_hex[3].localScale = Vector3.one;
                  //  _hex[4].localScale = new Vector3(_scaler, _scaler, _scaler);
                }
                if (_charge > 83.2f && _charge <= 100)
                {
                    _scaler = ((_charge - 83.2f) / 16.6f);

                    _hex[0].localScale = Vector3.one;
                    _hex[1].localScale = Vector3.one;
                    _hex[2].localScale = Vector3.one;
                    _hex[3].localScale = Vector3.one;

                    TweenHex(4);

                    //_hex [4].localScale = Vector3.one;
                    //   _hex[5].DOScale(_scaler, 0.1f).SetEase(Ease.Linear);
                    //_hex [5].localScale = new Vector3 (_scaler, _scaler, _scaler);
                }

                if (_charge == 100f && !_synced)
                {
                    Activated();
                    _synced = true;
                }
            }
        }
    }

    // Update is called once per frame
    void Update () {
	
	}


    private void TweenHex(int index)
    {
        _hex[index].DOScale(1f, 0.1f).SetEase(Ease.Linear);
        _hex[index+1].DOScale(_scaler, 0.1f).SetEase(Ease.Linear);
    }

	void Activated(){
        _chargedTextCanvasGroup.DOFade(1f, 0.25f).OnComplete(() =>
        {
            _chargedTextCanvasGroup.DOFade(0f, 0.25f).SetDelay(0.5f);
        });

		_rootHex.DOScale (0, 0.5f).SetEase(Ease.Linear).OnComplete (() => {
			_syncImage.SetActive (true);
			_rootHex.gameObject.SetActive(false);
		});

	}

	public void resetCooldown(){
        _charge = 0;
		_synced = false;
		_scaler = 0;
		_syncImage.SetActive (false);
		_rootHex.gameObject.SetActive(true);
        _rootHex.localScale = startingScale;
		//_rootHex.localScale = new Vector3 (2, 2, 2);
		foreach (RectTransform hex in _hex) {
			hex.localScale = Vector3.zero;
		}
	}

    public void ExecuteSuper()
    {
        if (_ulti.isLocalPlayer)
            resetCooldown();
    }
}
