﻿using UnityEngine;
using System.Collections;

public class Spinner : MonoBehaviour {

    [SerializeField]
    Vector3 _axis;
	
	// Update is called once per frame
	void Update () {
		transform.Rotate (_axis);
	}
}
