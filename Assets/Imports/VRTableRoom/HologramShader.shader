// Shader created with Shader Forge v1.25 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.25;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:True,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:1,x:33725,y:32450,varname:node_1,prsc:2|emission-3344-OUT,alpha-2444-OUT;n:type:ShaderForge.SFN_Tex2d,id:8083,x:32919,y:32439,ptovrint:False,ptlb:node_8083,ptin:_node_8083,varname:node_8083,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:b5ab21d4c43bd5044869af5a6744492e,ntxv:0,isnm:False|UVIN-7094-OUT;n:type:ShaderForge.SFN_Color,id:9701,x:32787,y:32679,ptovrint:False,ptlb:node_9701,ptin:_node_9701,varname:node_9701,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0.7931037,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:3344,x:33215,y:32441,varname:node_3344,prsc:2|A-8083-RGB,B-9701-RGB;n:type:ShaderForge.SFN_TexCoord,id:3284,x:32282,y:32390,varname:node_3284,prsc:2,uv:0;n:type:ShaderForge.SFN_Multiply,id:7094,x:32475,y:32250,varname:node_7094,prsc:2|A-3284-UVOUT,B-9233-OUT;n:type:ShaderForge.SFN_Vector1,id:9233,x:32502,y:32625,varname:node_9233,prsc:2,v1:2;n:type:ShaderForge.SFN_Panner,id:900,x:32685,y:32284,varname:node_900,prsc:2,spu:0.1,spv:0|UVIN-7094-OUT;n:type:ShaderForge.SFN_TexCoord,id:5935,x:32992,y:32679,varname:node_5935,prsc:2,uv:0;n:type:ShaderForge.SFN_Panner,id:2261,x:32992,y:32867,varname:node_2261,prsc:2,spu:0.1,spv:0|UVIN-5935-UVOUT,DIST-3898-OUT;n:type:ShaderForge.SFN_Tex2d,id:7403,x:33254,y:32893,ptovrint:False,ptlb:node_8083_copy,ptin:_node_8083_copy,varname:_node_8083_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:02f125dec21f8f44d86246359019ed7f,ntxv:0,isnm:False|UVIN-2261-UVOUT;n:type:ShaderForge.SFN_Multiply,id:2444,x:33505,y:32812,varname:node_2444,prsc:2|A-8083-A,B-5995-OUT;n:type:ShaderForge.SFN_Vector1,id:673,x:32672,y:32991,varname:node_673,prsc:2,v1:4;n:type:ShaderForge.SFN_Time,id:8383,x:32708,y:33064,varname:node_8383,prsc:2;n:type:ShaderForge.SFN_Multiply,id:3898,x:32954,y:33035,varname:node_3898,prsc:2|A-8383-T,B-673-OUT;n:type:ShaderForge.SFN_OneMinus,id:5995,x:33435,y:32972,varname:node_5995,prsc:2|IN-7403-A;proporder:8083-9701-7403;pass:END;sub:END;*/

Shader "Custom/HologramShader" {
    Properties {
        _node_8083 ("node_8083", 2D) = "white" {}
        _node_9701 ("node_9701", Color) = (0,0.7931037,1,1)
        _node_8083_copy ("node_8083_copy", 2D) = "white" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _node_8083; uniform float4 _node_8083_ST;
            uniform float4 _node_9701;
            uniform sampler2D _node_8083_copy; uniform float4 _node_8083_copy_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float2 node_7094 = (i.uv0*2.0);
                float4 _node_8083_var = tex2D(_node_8083,TRANSFORM_TEX(node_7094, _node_8083));
                float3 emissive = (_node_8083_var.rgb*_node_9701.rgb);
                float3 finalColor = emissive;
                float4 node_8383 = _Time + _TimeEditor;
                float2 node_2261 = (i.uv0+(node_8383.g*4.0)*float2(0.1,0));
                float4 _node_8083_copy_var = tex2D(_node_8083_copy,TRANSFORM_TEX(node_2261, _node_8083_copy));
                fixed4 finalRGBA = fixed4(finalColor,(_node_8083_var.a*(1.0 - _node_8083_copy_var.a)));
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "Meta"
            Tags {
                "LightMode"="Meta"
            }
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_META 1
            #include "UnityCG.cginc"
            #include "UnityMetaPass.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma exclude_renderers xbox360 ps3 
            #pragma target 3.0
            uniform sampler2D _node_8083; uniform float4 _node_8083_ST;
            uniform float4 _node_9701;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
                return o;
            }
            float4 frag(VertexOutput i) : SV_Target {
                UnityMetaInput o;
                UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
                
                float2 node_7094 = (i.uv0*2.0);
                float4 _node_8083_var = tex2D(_node_8083,TRANSFORM_TEX(node_7094, _node_8083));
                o.Emission = (_node_8083_var.rgb*_node_9701.rgb);
                
                float3 diffColor = float3(0,0,0);
                o.Albedo = diffColor;
                
                return UnityMetaFragment( o );
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
