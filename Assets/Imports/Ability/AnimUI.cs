﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class AnimUI : MonoBehaviour {

    public Sprite[] SequenceW;
    public Sprite[] SequenceL;
    Image _UI;
    [SerializeField]
    float _speed = 0.01f;
    int counter = 0;

	// Use this for initialization
	void Start () {
        _UI = GetComponent<Image>();

	}


    public void WinAnim()
    {
        counter = 0;
        InvokeRepeating("LooperWin", 0, _speed);
    }

    void LooperWin()
    {
        if(counter != SequenceW.Length -1)
        {
            _UI.sprite = SequenceW[counter + 1];
            counter++;
        }
        else
        {
            CancelInvoke("LooperWin");
        }
    }

    public void LossAnim()
    {
        counter = 0;
        InvokeRepeating("LooperLoss", 0, _speed);
    }

    void LooperLoss()
    {
        if (counter != SequenceL.Length - 1)
        {
            _UI.sprite = SequenceL[counter + 1];
            counter++;
        }
        else
        {
            CancelInvoke("LooperLoss");
        }
    }


}
