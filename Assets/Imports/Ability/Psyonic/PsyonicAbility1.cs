﻿/*
 * Psyonic ability, 
 * This class handles psyonic's anchor throw, hooking the player where the hook lands,
 * Created by Dan O'Neill, Edited by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using pulsar.networksync;
using pulsar.view.characters.attack;
using DG.Tweening;
using pulsar.view;
using System;
using pulsar.view.interfaces;
using Robotlegs.Bender.Extensions.Mediation.API;
using Robotlegs.Bender.Extensions.EventManagement.API;
using Robotlegs.Bender.Extensions.EventManagement.Impl;
using Robotlegs.Bender.Extensions.ViewManagement.API;
using UnityEngine.UI;

public class PsyonicAbility1 : NetworkBehaviour, IGameListener
{
    [SyncVar(hook = "OnEndPointChange")]
    private Vector3 endPoint;

    private float _cooldown = 0;

    [SerializeField]
    GameObject _anchor;

    [SerializeField]
    bool _active = false;

    [SerializeField]
    private float maxDistance = 30f;

    [SerializeField]
    private float maxTime = 0.75f;

    [SerializeField]
    private Camera myCamera;

    private Transform anchorParent;
    private Vector3 anchorStartPos;

    private Player_Health playerHealth;
    private PlayerAttackManagerView attackManger;

    private float cooldown = 7.5f;
    private bool isMoving;
    private Tweener currentTween;

    [SerializeField]
    private LayerMask layerMask;

    [SerializeField]
    private Image anchorImage;

    [SerializeField]
    private float manaCost;

    //RobotLegs setup
    public event Action<IView> RemoveView;
    private IEventDispatcher _dispatcher = new EventDispatcher();
    public IEventDispatcher dispatcher
    {
        get
        {
            return _dispatcher;
        }
        set
        {
            _dispatcher = value;
        }
    }

    private bool isReady = true;

    // Use this for initialization
    void Start ()
    {
        ViewNotifier.RegisterView(this);

        playerHealth = GetComponent<Player_Health>();
        attackManger = GetComponent<PlayerAttackManagerView>();
        anchorParent = _anchor.transform.parent;
        anchorStartPos = _anchor.transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
        if (!isLocalPlayer) return;
        if (attackManger.isReloading) return;
        if (attackManger.usingSuper) return;
        if (playerHealth.isDead) return;
        if (attackManger.isScoped) return;
        if (attackManger.holdingRift) return;
        if (!isReady) return;
       // if (attackManger.busy) return;

        if(_cooldown >= 0 && !_active)
        {
            _cooldown -= Time.deltaTime;
        }

        anchorImage.fillAmount = _cooldown / cooldown;

        if (_cooldown <= 0)
        {
            if (!isMoving)
            {
                //if (Input.GetKeyDown(KeyCode.Alpha1))
                if (Input.GetButtonDown("Ability1"))
                {
                    if (attackManger.SpendMana(manaCost))
                    {
                        var _endPoint = GetTarget();

                        if (_endPoint == Vector3.zero)
                        {
                            isMoving = true;

                            var eP = myCamera.transform.position + myCamera.transform.forward * maxDistance;
                            _anchor.SetActive(true);
                            _anchor.transform.SetParent(null);
                            currentTween = _anchor.transform.DOMove(eP, maxTime).SetEase(Ease.Linear).OnComplete(() =>
                            {
                                isMoving = false;
                                _cooldown = 0;
                                _anchor.transform.DOMove(anchorParent.transform.position, maxTime * 0.85f).SetEase(Ease.Linear).OnComplete(() => 
                                {
                                    _anchor.SetActive(false);
                                    _anchor.transform.SetParent(anchorParent);
                                    _anchor.transform.localPosition = anchorStartPos;
                                
                                });
                            });

                            CmdUpdateEndPoint(eP);
                            attackManger.RefundMana(manaCost);
                        }
                        else
                        {
                            isMoving = true;
                        //    _active = true;
                            _cooldown = cooldown;
                            ThrowAnchorToEndPoint(_endPoint);
                            CmdUpdateEndPoint(_endPoint);
                        }
                    }
                }
            }
        }
	}

    

    private Vector3 GetTarget()
    {
        Vector3 _endPoint = Vector3.zero;// myCamera.transform.position + myCamera.transform.forward * maxDistance;
        RaycastHit hit;
        if (Physics.Raycast(myCamera.transform.position, myCamera.transform.forward, out hit, maxDistance, layerMask))
        {
            float dis = Vector3.Distance(transform.position, hit.point);
            _endPoint = myCamera.transform.position + myCamera.transform.forward * (dis * 0.8f);
        }

        /*
        var _endPoint = myCamera.transform.position + myCamera.transform.forward * maxDistance;

        var hits = Physics.RaycastAll(myCamera.transform.position, myCamera.transform.forward, maxDistance, layerMask);

        float closestDis = 500;
        foreach (RaycastHit hit in hits)
        {
            string _tag = hit.collider.gameObject.tag;

            if (_tag == "Wall" || _tag == "Floor")
            {
                float dis = Vector3.Distance(hit.point, myCamera.transform.position);
                if (dis < closestDis)
                {
                    closestDis = dis;

                    if (dis < maxDistance)
                    {
                        _endPoint = myCamera.transform.position + myCamera.transform.forward * (dis * 0.95f);
                    }
                }

                break;
            }
        }
        */
        return _endPoint;
    }

    private void ThrowAnchorToEndPoint(Vector3 p)
    {
        float duration = GetDuration(p);
        _anchor.SetActive(true);
        _anchor.transform.SetParent(null);
        currentTween = _anchor.transform.DOMove(p, duration).SetEase(Ease.Linear).OnComplete(()=> 
        {
            if (isLocalPlayer)
            {
                isMoving = false;
                MoveLocalPlayerToPoint(p, duration);
            }
            else
            {
                StartCoroutine(DisableAnchor(duration));
            }
        }).OnUpdate(()=> 
        {
            if (isLocalPlayer)
            {
                if(Input.GetKeyDown(KeyCode.Alpha1))
                {
                    isMoving = false;
                    currentTween.Kill();
                    var pos = _anchor.transform.position;
                    MoveLocalPlayerToPoint(pos, GetDuration(pos));
                }
            }
        });
    }

    private IEnumerator DisableAnchor(float t)
    {
        yield return new WaitForSeconds(t);
        _anchor.SetActive(false);
        _anchor.transform.SetParent(anchorParent);
        _anchor.transform.localPosition = anchorStartPos;
        _active = false;
    }

    private void MoveLocalPlayerToPoint(Vector3 p, float duration)
    {

        transform.DOMove(p, duration).SetDelay(0.05f).SetEase(Ease.Linear).OnComplete(() =>
        {
            _anchor.SetActive(false);
            _anchor.transform.SetParent(anchorParent);
            _anchor.transform.localPosition = anchorStartPos;
            _active = false;
        });
    }

    //Server side - update end point
    [Command]
    private void CmdUpdateEndPoint(Vector3 _endPoint)
    {
        endPoint = _endPoint;
    }

    private float GetDuration(Vector3 endPoint)
    {
        var dis = Vector3.Distance(endPoint, transform.position);
        return (dis / maxDistance) * maxTime;
    }


    //Client update - for end point;
    private void OnEndPointChange(Vector3 p)
    {
        if (!isLocalPlayer)
        {
            endPoint = p;
            ThrowAnchorToEndPoint(p);
        }
    }

    public void HandleGameStart()
    {
     //   isReady = true;
    }

    public void HandleGameFinished()
    {
        isReady = false;
    }

    /*
    void resetAb()
    {
        _active = false;
        _cooldown = 5f;
        _anch = null;
    }
    */
}
