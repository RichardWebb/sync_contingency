﻿using UnityEngine;
using System.Collections;

public class JacobSpriteSheetPlay : MonoBehaviour {
	public float stepX;
	public float stepY;

	public float OriginalOffsetX;
	public float OriginalOffsetY;


	public float ReverseOffsetX;
	public float ReverseOffsetY;

	public bool Reverse;
	public int XCounter;
	public int YCounter;
	public int MyXCounter;
	public int MyYCounter;

	public float timeStep;
	public Renderer rend;
	// Use this for initialization
	void Start () {
		rend.material.SetTextureOffset("_MainTex", new Vector2(OriginalOffsetX, OriginalOffsetY));

		StartCoroutine (Mover ());
	}

	IEnumerator Mover()
	{
		while (true) {
			yield return new WaitForSeconds (timeStep);

			if (MyXCounter != XCounter) {
				MyXCounter = MyXCounter + 1;


				if (Reverse == true) {
					rend.material.SetTextureOffset ("_MainTex", new Vector2 (rend.material.mainTextureOffset.x - stepX, rend.material.mainTextureOffset.y));
				}


				if (Reverse == false) {
					rend.material.SetTextureOffset ("_MainTex", new Vector2 (rend.material.mainTextureOffset.x + stepX, rend.material.mainTextureOffset.y));
				}
			}

			if (MyXCounter == XCounter) {
				MyYCounter = MyYCounter + 1;
				MyXCounter = 0;


				if (Reverse == true) {
					rend.material.SetTextureOffset ("_MainTex", new Vector2 (ReverseOffsetX, rend.material.mainTextureOffset.y - stepY));
				}
				if (Reverse == false) {
					rend.material.SetTextureOffset ("_MainTex", new Vector2 (OriginalOffsetX, rend.material.mainTextureOffset.y + stepY));
				}
			}

			if (Reverse == true) {
				if (MyYCounter == YCounter -1) {
					Reverse = !Reverse;


				
		
					MyYCounter = 0;
					//	MyXCounter = 0;
			
					rend.material.SetTextureOffset ("_MainTex", new Vector2 (OriginalOffsetX, OriginalOffsetY));
				}
			}



				if (Reverse == false) {
				if (MyYCounter == YCounter ) {
					Reverse = !Reverse;
					MyYCounter = 0;

					rend.material.SetTextureOffset ("_MainTex", new Vector2 (ReverseOffsetX, ReverseOffsetY));
				}
			}





		//	rend.material.SetTextureOffset ("_MainTex", new Vector2 (rend.material.mainTextureOffset.x, rend.material.mainTextureOffset.y + stepY));

		}
	}
	// Update is called once per frame
	void Update () {
		
	}
}
