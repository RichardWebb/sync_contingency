﻿using UnityEngine;
using System.Collections;

public class BonshoBellMind : MonoBehaviour {

	public AudioSource BellHigh;
	public AudioSource BellMedium;
	public AudioSource BellLow;

	public float HighY;
	public float MediumY;
	public float LowY;

	public bool ToggleHigh;
	public bool ToggleLow;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (transform.localPosition.y > HighY) {
			if (ToggleHigh == true) {
				BellHigh.Play ();
				ToggleHigh = false;
			}
		}

	

		if (transform.localPosition.y < HighY) {
			ToggleHigh = true;


		}



		if (transform.localPosition.y > LowY) {
			if (ToggleLow == true) {
				BellLow.Play ();
				ToggleLow = false;
			}
		}



		if (transform.localPosition.y < LowY) {
			ToggleLow = true;


		}
	}
}
