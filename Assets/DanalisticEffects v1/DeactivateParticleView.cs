﻿/*
 * Deactivate Particle View
 * This class handles the fade out and destruction of particle effects
 * Created by Daniel O'Neill, s1303529 
 */
using UnityEngine;
using System.Collections;

public class DeactivateParticleView : MonoBehaviour {

    [SerializeField]
    float _delay;

    private ParticleSystem[] _particles;
	// Use this for initialization
	void Start () {
        _particles = GetComponentsInChildren<ParticleSystem>();
        StartCoroutine(DoDetach());
	}


    IEnumerator DoDetach()
    {
        yield return new WaitForSeconds(_delay);
        transform.parent = null;
        StartCoroutine(DoDestruct());
    }

    IEnumerator DoDestruct()
    {
        foreach (ParticleSystem p in _particles)
        {
            p.Stop();
            ParticleSystem.EmissionModule e = p.emission;
            e.enabled = false;
        }
        yield return new WaitForSeconds(2);       
        Destroy(this.gameObject);
        

    }
}
