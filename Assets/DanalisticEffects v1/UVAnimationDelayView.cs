﻿/*
 * UV Animation Delay View
 * This class handles delaying a UV animation Component
 * Created by Daniel O'Neill, s1303529 
 */

using UnityEngine;
using System.Collections;

public class UVAnimationDelayView : MonoBehaviour {

	[SerializeField]
	private float _delay;

	private UVTextureAnimator _anim;

	// Use this for initialization
	void Start () {
		_anim = GetComponent<UVTextureAnimator> ();
        _anim.enabled = false;
		StartCoroutine (StartAnim ());
	}
	
	IEnumerator StartAnim() {
		yield return new WaitForSeconds (_delay);
		_anim.enabled = true;
	}
}
