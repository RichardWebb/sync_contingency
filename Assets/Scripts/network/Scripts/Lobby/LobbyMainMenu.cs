using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using pulsar.view;
using UnityStandardAssets.Network;

//namespace UnityStandardAssets.Network
//{
//Main menu, mainly only a bunch of callback called by the UI (setup throught the Inspector)

public class LobbyMainMenu : MonoBehaviour 
    {
        public UnityStandardAssets.Network.LobbyManager lobbyManager;

        public RectTransform lobbyServerList;
        public RectTransform lobbyPanel;

        public InputField ipInput;
        public InputField matchNameInput;

        public void OnEnable()
        {
         //  lobbyManager.topPanel.ToggleVisibility(true);

            ipInput.onEndEdit.RemoveAllListeners();
            ipInput.onEndEdit.AddListener(onEndEditIP);

            matchNameInput.onEndEdit.RemoveAllListeners();
            matchNameInput.onEndEdit.AddListener(onEndEditGameName);
        }

        public void OnClickHost()
        {
   
            lobbyManager.StartHost();
        }

        public void OnClickJoin()
        {
            LobbyView view = GetComponent<LobbyView>();
            if(view.IsServer)
            {
                lobbyManager.maxConnections = 50;
                lobbyManager.StartHost();
            }
            else
            {
                lobbyManager.ChangeTo(lobbyPanel);
                lobbyManager.networkAddress = LobbyManager.s_Singleton.targetIP;//"192.168.0.3";// ipInput.text;

                lobbyManager.StartClient();

                lobbyManager.backDelegate = lobbyManager.StopClientClbk;
                lobbyManager.DisplayIsConnecting();

                lobbyManager.SetServerInfo("Connecting...", lobbyManager.networkAddress);

            }
        }

        public void OnClickDedicated()
        {
           lobbyManager.ChangeTo(null);
            lobbyManager.StartServer();

            lobbyManager.backDelegate = lobbyManager.StopServerClbk;

            lobbyManager.SetServerInfo("Dedicated Server", lobbyManager.networkAddress);
        }

        public void OnClickCreateMatchmakingGame()
        {
            lobbyManager.StartMatchMaker();
            lobbyManager.matchMaker.CreateMatch(
                matchNameInput.text,
                (uint)lobbyManager.maxPlayers,
                true,
                "", "", "", 0, 0,
                lobbyManager.OnMatchCreate);

            lobbyManager.backDelegate = lobbyManager.StopHost;
            lobbyManager._isMatchmaking = true;
            lobbyManager.DisplayIsConnecting();

            lobbyManager.SetServerInfo("Matchmaker Host", lobbyManager.matchHost);

        }

        public void OnClickOpenServerList()
        {
            lobbyManager.StartMatchMaker();
            lobbyManager.backDelegate = lobbyManager.SimpleBackClbk;
            lobbyManager.ChangeTo(lobbyServerList);

        }

        void onEndEditIP(string text)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                OnClickJoin();
            }
        }

        void onEndEditGameName(string text)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                OnClickCreateMatchmakingGame();
            }
        }

    }

//}
