﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

namespace network.scripts.lobby
{
    public class LobbyExtraData : NetworkBehaviour
    {
        public bool firstLoad = true;

        [SerializeField]
        public GameObject[] players;

        [SyncVar(hook = "OnIndexChanged")]
        public int characterIndex = 0;

        [SyncVar(hook = "OnStateChange")]
        public bool doReplace = false;

        void Start()
        {
         //   DontDestroyOnLoad(this.gameObject);
        }

        public void OnIndexChanged(int index)
        {
            characterIndex = index;
        }

        [Command]
        public void CmdUpdateIndex(int index)
        {
            characterIndex = index;
        }

        [Client]
        public void UpdateIndex(int value)
        {
            CmdUpdateIndex(value);
        }

        public void OnStateChange(bool s)
        {
            doReplace = s;
        }

        [Command]
        public void CmdUpdateState(bool s)
        {
            doReplace = s;
        }

        [Client]
        public void UpdateState(bool s)
        {
            doReplace = s;
        }
    }
}
