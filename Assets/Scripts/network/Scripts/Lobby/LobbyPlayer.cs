using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using pulsar.view;

using System;

using pulsar.events;
using Robotlegs.Bender.Extensions.Mediation.API;
using Robotlegs.Bender.Extensions.EventManagement.API;
using Robotlegs.Bender.Extensions.ViewManagement.API;
using Robotlegs.Bender.Extensions.EventManagement.Impl;
using pulsar.view.characterselection;
using DG.Tweening;
using pulsar.model;
using System.IO;

namespace UnityStandardAssets.Network
{
    //Player entry in the lobby. Handle selecting color/setting name & getting ready for the game
    //Any LobbyHook can then grab it and pass those value to the game player prefab (see the Pong Example in the Samples Scenes)
    public class LobbyPlayer : NetworkLobbyPlayer, ILobbyPlayer
    {
        static Color[] Colors = new Color[] { Color.magenta, Color.red, Color.cyan, Color.blue, Color.green, Color.yellow };
        //used on server to avoid assigning the same color to two player
        static List<int> _colorInUse = new List<int>();

        [SerializeField]
        private Sprite readyImage;

        [SerializeField]
        private Image readyIcon;

        public Button colorButton;
        public InputField nameInput;
        public Button readyButton;
        public Button waitingPlayerButton;
        public Button removePlayerButton;

        public GameObject localIcone;
        public GameObject remoteIcone;
        private bool isAdded;

        [SerializeField]
        private Sprite[] panelSprites;

        [SerializeField]
        private Sprite[] localPlayerPanelSprites;


        [SyncVar(hook = "OnMyTeam")]
        public bool isTeamOne;

        //OnMyName function will be invoked on clients when server change the value of playerName
        [SyncVar(hook = "OnMyName")]
        public string playerName = "";
        [SyncVar(hook = "OnMyColor")]
        public Color playerColor = Color.white;

        [SyncVar(hook = "OnCharacterName")]
        public string characterName;

        [SyncVar(hook = "OnIsReady")]
        public bool isReady = false;

        public event Action<IView> RemoveView;

        private IEventDispatcher _dispatcher = new EventDispatcher();
        public IEventDispatcher dispatcher
        {
            get
            {
                return _dispatcher;
            }
            set
            {
                _dispatcher = value;
            }
        }


        private CharacterSelectionView selectionView;
        private string knownData;

        void Start()
        {
            ViewNotifier.RegisterView(this);
            GetComponent<CanvasGroup>().DOFade(1f, 1f).SetDelay(1f);

            GetComponent<RectTransform>().localPosition = Vector3.zero;
            if (isLocalPlayer || isServer)
            {
                dispatcher.Dispatch(new UpdateDetailsEvent(UpdateDetailsEvent.Type.GET_USER_DETAILS));
            }
        }

        [Command]
        public void CmdUpdateIndex()
        {
            LobbyPlayerList._instance.UpdatePlayerIndex(this);
        }

        //static Color OddRowColor = new Color(250.0f / 255.0f, 250.0f / 255.0f, 250.0f / 255.0f, 1.0f);
        //static Color EvenRowColor = new Color(180.0f / 255.0f, 180.0f / 255.0f, 180.0f / 255.0f, 1.0f);


        public override void OnClientEnterLobby()
        {
          //  if (isLocalPlayer)
           //     return;


            base.OnClientEnterLobby();


            if (LobbyManager.s_Singleton != null) LobbyManager.s_Singleton.OnPlayersNumberModified(1);

            LobbyPlayerList._instance.AddPlayer(this);
            LobbyPlayerList._instance.DisplayDirectServerWarning(isServer && LobbyManager.s_Singleton.matchMaker == null);

            if (isLocalPlayer)
            {
                SetupLocalPlayer();
            }
            else
            {
                SetupOtherPlayer();
            }

            //setup the player data on UI. The value are SyncVar so the player
            //will be created with the right value currently on server
            OnMyName(playerName);
            OnMyColor(playerColor);
        //    OnMyTeam(isTeamOne);
            if (isLocalPlayer)
            {
       //         CmdUpdateIndex();
            //    CmdSettingTeam(isTeamOne);
            }
        }

        public override void OnStartAuthority()
        {
            base.OnStartAuthority();

            //if we return from a game, color of text can still be the one for "Ready"
            readyButton.transform.GetChild(0).GetComponent<Text>().color = Color.white;

           SetupLocalPlayer();
        }

        public override void OnStartLocalPlayer()
        {
            base.OnStartLocalPlayer();
            //Re-positioning the join button 
            readyButton.transform.SetParent(LobbyPlayerList._instance.gameObject.transform.parent);
            readyButton.transform.SetAsLastSibling();
            var rect = readyButton.GetComponent<RectTransform>();

            rect.localScale = Vector3.one;
            rect.anchoredPosition3D = new Vector3(-914.6f, -440f, 0);

            selectionView = GameObject.FindObjectOfType<CharacterSelectionView>();
            if (selectionView != null)
            {
                selectionView.MenuToggled += HandleSelectionViewToggled;
            }
            GetComponent<Image>().sprite = isTeamOne ? localPlayerPanelSprites[0] : localPlayerPanelSprites[1];
        }

        private void HandleSelectionViewToggled()
        {
            if (!string.IsNullOrEmpty(characterName))
            {
                readyButton.gameObject.SetActive(selectionView.displaying);
            }
               
        }

        void Update()
        {
            if (isLocalPlayer)
            {
                if (readyButton == null) return;
                if (selectionView == null) return;
                if (selectionView.displaying)
                {
                    readyButton.gameObject.SetActive(false);
                }
                else
                {
                    if (!string.IsNullOrEmpty(characterName))
                    {
                        int i = 0;
                        foreach (NetworkLobbyPlayer p in LobbyManager.s_Singleton.lobbySlots)
                        {
                            if (p != null) i++;
                        }
                        readyButton.gameObject.SetActive(i > 1);
                    }
                }
            }
        }

        void ChangeReadyButtonColor(Color c)
        {/*
            ColorBlock b = readyButton.colors;
            b.normalColor = c;
            b.pressedColor = c;
            b.highlightedColor = c;
            b.disabledColor = c;
            readyButton.colors = b;
            */
        }

        void SetupOtherPlayer()
        {
            nameInput.interactable = false;
            removePlayerButton.interactable = NetworkServer.active;

         //   ChangeReadyButtonColor(NotReadyColor);

            readyButton.transform.GetChild(0).GetComponent<Text>().text = "...";
            readyButton.interactable = false;

            OnClientReady(false);
        }

        void SetupLocalPlayer()
        {
           // nameInput.interactable = true;

           // remoteIcone.gameObject.SetActive(false);
           // localIcone.gameObject.SetActive(true);

            CheckRemoveButton();
   //         Debug.Log("player controller is for this client is : " + playerControllerId  +":::::"+ this.netId + ":::::" + this.connectionToServer);
            if (playerColor == Color.white)
                CmdColorChange();

          //  ChangeReadyButtonColor(JoinColor);

            readyButton.transform.GetChild(0).GetComponent<Text>().text = "JOIN";
            readyButton.interactable = true;

            //have to use child count of player prefab already setup as "this.slot" is not set yet
            if (playerName == "")
            {
                CmdNameChanged("Player" + (isTeamOne ? LobbyPlayerList._instance.team1LayoutGroup.childCount-1 : LobbyPlayerList._instance.team2LayoutGroup.childCount - 1));
            }

            //we switch from simple name display to name input
            colorButton.interactable = true;
            //nameInput.interactable = true;

            nameInput.onEndEdit.RemoveAllListeners();
            nameInput.onEndEdit.AddListener(OnNameChanged);

            colorButton.onClick.RemoveAllListeners();
            colorButton.onClick.AddListener(OnColorClicked);

            readyButton.onClick.RemoveAllListeners();
            readyButton.onClick.AddListener(OnReadyClicked);

            //when OnClientEnterLobby is called, the loval PlayerController is not yet created, so we need to redo that here to disable
            //the add button if we reach maxLocalPlayer. We pass 0, as it was already counted on OnClientEnterLobby
            if (LobbyManager.s_Singleton != null) LobbyManager.s_Singleton.OnPlayersNumberModified(0);
        }

        //This enable/disable the remove button depending on if that is the only local player or not
        public void CheckRemoveButton()
        {
            if (!isLocalPlayer)
                return;

            int localPlayerCount = 0;
            foreach (PlayerController p in ClientScene.localPlayers)
                localPlayerCount += (p == null || p.playerControllerId == -1) ? 0 : 1;

            removePlayerButton.interactable = localPlayerCount > 1;
        }

        public override void OnClientReady(bool readyState)
        {
            if (readyState)
            {
                //         ChangeReadyButtonColor(TransparentColor);
                Destroy(readyButton.gameObject);
                /*
                Text textComponent = readyButton.transform.GetChild(0).GetComponent<Text>();
                textComponent.text = "READY";
              //  textComponent.color = ReadyColor;
                readyButton.interactable = false;
                colorButton.interactable = false;
                nameInput.interactable = false;
                */
            }
            else
            {
                // ChangeReadyButtonColor(isLocalPlayer ? JoinColor : NotReadyColor);

                if (readyButton != null)
                {
                    Text textComponent = readyButton.transform.GetChild(0).GetComponent<Text>();
                    textComponent.text = isLocalPlayer ? "JOIN" : "...";
                    //textComponent.color = Color.white;
                    readyButton.interactable = isLocalPlayer;
                    colorButton.interactable = isLocalPlayer;
                }
               // nameInput.interactable = isLocalPlayer;
            }
        }

        public void OnPlayerListChanged(int idx)
        { 
    //        GetComponent<Image>().color = (idx % 2 == 0) ? EvenRowColor : OddRowColor;
        }

        public void UpdateCharacterForPlayer(string name)
        {
           
            CmdUpdateCharacterPrefabName(name);
            
        }

        [Command]
        public void CmdUpdateCharacterPrefabName(string name)
        {
            characterName = name;
        }

        ///===== callback from sync var
        ///
        public void OnCharacterName(string newcharacterName)
        {
            characterName = newcharacterName;
            LobbyPlayer[] players = GameObject.FindObjectsOfType<LobbyPlayer>();
            if (players.Length >= LobbyManager.s_Singleton.minPlayers)
            {
                ToggleJoinButton(true);
            }
        }

        public void OnMyName(string newName)
        {
            playerName = newName;
            nameInput.text = playerName;
        }

        public void OnMyTeam(bool teamState)
        {
            //if (isLocalPlayer)
           // {
                isTeamOne = teamState;
              /*  if (isLocalPlayer)
                {
                    GetComponent<Image>().sprite = isTeamOne ? localPlayerPanelSprites[0] : localPlayerPanelSprites[1];
                }
                else 
                    GetComponent<Image>().sprite = isTeamOne ? panelSprites[0] : panelSprites[1];
                    */
                LobbyPlayerList._instance.UpdatePlayerIndex(this);
             //   CmdUpdateIndex();
             //  }
        }

        public void OnMyColor(Color newColor)
        {
            playerColor = newColor;
            colorButton.GetComponent<Image>().color = newColor;
        }

        //===== UI Handler

        //Note that those handler use Command function, as we need to change the value on the server not locally
        //so that all client get the new value throught syncvar
        public void OnColorClicked()
        {
            CmdColorChange();
        }

        public void OnReadyClicked()
        {
            SendReadyToBeginMessage();
            CmdClientReady();
            CharacterSelectionView selectionView = GameObject.FindObjectOfType<CharacterSelectionView>();

            if (selectionView != null)
                Destroy(selectionView.gameObject);
        }

        [Command]
        public void CmdClientReady()
        {
            isReady = true;
           // GetComponent<Image>().sprite = readyImage;
        }

        [Client]
        public void OnIsReady(bool value)
        {
            isReady = value;
          //  GetComponent<Image>().sprite = readyImage;
          if (readyIcon != null)
            readyIcon.gameObject.SetActive(true);
        }


        public void OnNameChanged(string str)
        {
            CmdNameChanged(str);
        }

        public void OnRemovePlayerClick()
        {
            if (isLocalPlayer)
            {
                RemovePlayer();
            }
            else if (isServer)
                LobbyManager.s_Singleton.KickPlayer(connectionToClient);
                
        }

        public void ToggleJoinButton(bool enabled)
        {
            if (readyButton != null)
            {
                if (characterName.Length > 0)
                {
                    readyButton.gameObject.SetActive(enabled);
                }
                else
                {
                    readyButton.gameObject.SetActive(false);
                }
            }

            //readyButton.gameObject.SetActive(enabled && characterName.Length > 0 );
            waitingPlayerButton.gameObject.SetActive(!enabled);
        }

        [ClientRpc]
        public void RpcUpdateCountdown(int countdown, bool level1)
        {
            //    LobbyManager.s_Singleton.countdownPanel.UIText.text = "Match Starting in " + countdown;
            //TODO change this image
            var cam = GameObject.Find("SceneCam");
            if (cam != null)
            {
                Destroy(cam);
            }
            LobbyManager.s_Singleton.countdownPanel.gameObject.SetActive(true);
            LobbyManager.s_Singleton.levelImages[level1 ? 0 : 1].gameObject.SetActive(true);
        }



        [ClientRpc(channel = 1)]
        public void RpcUpdatePlayerData(string data, bool save)
        {
            knownData += data;
            if (save)
            {
                knownData = "<xml>" + knownData + "</xml>";
                File.WriteAllText(Application.streamingAssetsPath + "/GameSettings/PlayerSettings.xml", knownData);
            }
        }

        [ClientRpc]
        public void RpcUpdateScreenSize()
        {
            StartCoroutine(DoRes());
        }

        private IEnumerator DoRes()
        {
            Screen.fullScreen = !Screen.fullScreen;
            Screen.SetResolution(1920, 1080, true);
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
        }

        [ClientRpc]
        public void RpcUpdateRemoveButton()
        {
            CheckRemoveButton();
        }

        [ClientRpc]
        public void RpcCheckYourTeam()
        {
            CmdSettingTeam(isTeamOne);
        }

     

        [ClientRpc]
        public void RpcUpdateJoinButton(bool enoughPlayers, string _name)
        {
            Debug.Log("RpcUpdateJoinButton " + enoughPlayers + " " + _name);
            if (_name.Length > 0)
            {
                readyButton.gameObject.SetActive(enoughPlayers);
            }
            else
            {
                readyButton.gameObject.SetActive(false);
            }

            waitingPlayerButton.gameObject.SetActive(!enoughPlayers);
        }

        //====== Server Command

        [Command]
        public void CmdColorChange()
        {
            /*
            int idx = System.Array.IndexOf(Colors, playerColor);

            int inUseIdx = _colorInUse.IndexOf(idx);

            if (idx < 0) idx = 0;

            idx = (idx + 1) % Colors.Length;

            bool alreadyInUse = false;
            /*
            do
            {
                alreadyInUse = false;
                for (int i = 0; i < _colorInUse.Count; ++i)
                {
                    if (_colorInUse[i] == idx)
                    {//that color is already in use
                        alreadyInUse = true;
                        idx = (idx + 1) % Colors.Length;
                    }
                }
            }
            while (alreadyInUse);*/
            /*
            if (inUseIdx >= 0)
            {//if we already add an entry in the colorTabs, we change it
                _colorInUse[inUseIdx] = idx;
            }
            else
            {//else we add it
                _colorInUse.Add(idx);
            }

            playerColor = Colors[idx];*/
        }

        [ClientRpc]
        public void RpcSetGameMode(GameSettings.GameTypes gameType)
        {
            GameSettings.GameType = gameType;
            LobbyPlayerList._instance.UpdateGameTitleText();
        }

        [Command]
        public void CmdNameChanged(string name)
        {
            playerName = name;
        }
        
        [Command]
        public void CmdSettingTeam(bool state)
        {           
            isTeamOne = state;
           // CmdUpdateIndex();
        }
        /*
        private IEnumerator CheckTeam()
        {
            while(true)
            {
                yield return new WaitForSeconds(1);
                if (isLocalPlayer)
                    CmdSettingTeam(isTeamOne);

                LobbyPlayerList._instance.UpdatePlayerIndex(this);
            }
        }*/



        //Cleanup thing when get destroy (which happen when client kick or disconnect)
        public void OnDestroy()
        {
            int idx = System.Array.IndexOf(Colors, playerColor);

            if (idx < 0)
                return;

            for (int i = 0; i < _colorInUse.Count; ++i)
            {
                if (_colorInUse[i] == idx)
                {//that color is already in use
                    _colorInUse.RemoveAt(i);
                    break;
                }
            }

            LobbyPlayerList._instance.RemovePlayer(this);
            if (LobbyManager.s_Singleton != null) LobbyManager.s_Singleton.OnPlayersNumberModified(-1);
        }

        public void HandleDetails(UpdateDetailsEvent evt)
        {          
            if (isLocalPlayer)
            {
                playerName = evt.userName;
               
                isTeamOne = evt.isTeamOne;

                CmdNameChanged(playerName);
                CmdSettingTeam(isTeamOne);

                if (playerName == "VR_Sync")
                {
                    UpdateCharacterForPlayer("VR_Character");
                    OnReadyClicked();
                }

            }

        }


        public void SetTeamColorPanel(bool teamOne)
        {
            if (isLocalPlayer)
            {
                GetComponent<Image>().sprite = teamOne ? localPlayerPanelSprites[0] : localPlayerPanelSprites[1];
            }
            else
            {
                GetComponent<Image>().sprite = teamOne ? panelSprites[0] : panelSprites[1];
            }

        }
    }
}
