using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.Network;
using pulsar.model;
using DG.Tweening;

namespace UnityStandardAssets.Network
{
//List of players in the lobby
    public class LobbyPlayerList : MonoBehaviour
    {
        public static LobbyPlayerList _instance = null;

        public RectTransform team1LayoutGroup;
        public RectTransform team2LayoutGroup;
        public GameObject warningDirectPlayServer;
        public Transform team1AddButtonRow;
        public Transform team2AddButtonRow;

        [SerializeField]
        private Text gameTitleText;

        
        protected VerticalLayoutGroup _layout;
        protected List<LobbyPlayer> _players = new List<LobbyPlayer>();

      
        public void OnEnable()
        {
            _instance = this;
            _layout = team1LayoutGroup.GetComponent<VerticalLayoutGroup>();

            StartCoroutine(SortOutLayout());

           
        }

        public IEnumerator SortOutLayout()
        {
            yield return new WaitForSeconds(0.5f);

      //      RectTransform team1Rect = team1LayoutGroup.gameObject.transform.parent.gameObject.GetComponent<RectTransform>();
         //   RectTransform team2Rect = team2LayoutGroup.gameObject.transform.parent.gameObject.GetComponent<RectTransform>();
            /*
            if (UserDataModel._IsTeamOne)
            {
                //    team2Rect.anchoredPosition = new Vector2(0, -2000);
                team1Rect.DOLocalMoveX(0f, 0.5f);
            }
            else
            {
                team2Rect.DOLocalMoveX(0f, 0.5f);

                //   team2Rect.anchoredPosition = team1Rect.anchoredPosition;
                //     team1Rect.anchoredPosition = new Vector2(0, -2000);
            }
            */
        }

        public void DisplayDirectServerWarning(bool enabled)
        {
            if(warningDirectPlayServer != null)
                warningDirectPlayServer.SetActive(enabled);
        }

        void Update()
        {
            //this dirty the layout to force it to recompute evryframe (a sync problem between client/server
            //sometime to child being assigned before layout was enabled/init, leading to broken layouting)
            
           // if(_layout)
             //   _layout.childAlignment = Time.frameCount%2 == 0 ? TextAnchor.UpperCenter : TextAnchor.UpperLeft;
        }

        public void UpdatePlayerIndex(LobbyPlayer player)
        {
            if (player.isTeamOne)
            {
                player.transform.SetParent(team1LayoutGroup);
                player.SetTeamColorPanel(true);
                team1AddButtonRow.transform.SetAsLastSibling();
            }
            else
            {
                player.transform.SetParent(team2LayoutGroup);
                player.SetTeamColorPanel(false);
                team2AddButtonRow.transform.SetAsLastSibling();
            }
        }

        public void UpdateGameTitleText()
        {
            string result = "";
            switch (GameSettings.GameType)
            {
                case GameSettings.GameTypes.DEATH_MATCH:
                    result = "Death Match";
                    break;

                case GameSettings.GameTypes.RIFT:
                    result = "Rift Mode";
                    break;
            }

            gameTitleText.text = result;
        }

        public void AddPlayer(LobbyPlayer player)
        {
            _players.Add(player);

            if (player.isTeamOne)
            {
                player.transform.SetParent(team1LayoutGroup);
                player.SetTeamColorPanel(true);
                team1AddButtonRow.transform.SetAsLastSibling();
            }
            else
            {
                player.transform.SetParent(team2LayoutGroup);
                player.SetTeamColorPanel(false);
                team2AddButtonRow.transform.SetAsLastSibling();
            }

            player.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
           // player.transform.SetParent(player.isTeamOne ? team1LayoutGroup : team2LayoutGroup);


            //player.transform.SetParent(team1LayoutGroup, false);
           // addButtonRow.transform.SetAsLastSibling();

            PlayerListModified();
        }

        public void RemovePlayer(LobbyPlayer player)
        {
            _players.Remove(player);
            PlayerListModified();
        }

        public void PlayerListModified()
        {
            
            int i = 0;
            foreach (LobbyPlayer p in _players)
            {
                p.OnPlayerListChanged(i);
                ++i;
            }
        }
    }
}
