using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.Networking.Types;
using UnityEngine.Networking.Match;
using System.Collections;
using network.scripts.lobby;
using System.Collections.Generic;
using pulsar.view.characterselection;
using UnityEngine.SceneManagement;
using pulsar.model;
using System;
using System.IO;

namespace UnityStandardAssets.Network
{
    public class LobbyManager : NetworkLobbyManager
    {
        static short MsgKicked = MsgType.Highest + 1;
        static public LobbyManager s_Singleton;
        public string targetIP = "127.0.0.1";
      
        [Tooltip("Time in second between all players ready & match start")]
        public float prematchCountdown = 5.0f;

        [Space]
        [Header("UI Reference")]
        public LobbyTopPanel topPanel;
        [SerializeField]
        private string[] scenes;

        public Image[] levelImages;
        public RectTransform mainMenuPanel;
        public RectTransform lobbyPanel;

        public LobbyInfoPanel infoPanel;
        public LobbyCountdownPanel countdownPanel;
        public GameObject addPlayerButton;

        protected RectTransform currentPanel;

        public Button backButton;
        public bool isServer;
        public Text statusInfo;
        public Text hostInfo;

        private Dictionary<NetworkConnection, GameObject> connectedPlayers;

        //Client numPlayers from NetworkManager is always 0, so we count (throught connect/destroy in LobbyPlayer) the number
        //of players, so that even client know how many player there is.
        [HideInInspector]
        public int _playerNumber = 0;

        //used to disconnect a client properly when exiting the matchmaker
        [HideInInspector]
        public bool _isMatchmaking = false;

        protected bool _disconnectServer = false;

        protected ulong _currentMatchID;

        protected LobbyHook _lobbyHooks;

    //    private LobbyExtraData lobbyData;

        void Start()
        {
            Canvas can = GetComponent<Canvas>();
            can.worldCamera = Camera.main;

            if (isServer)
            {
                NetworkServer.Spawn((GameObject)Resources.Load("LobbyExtras/LobbyData"));
            }
            s_Singleton = this;
            _lobbyHooks = GetComponent<UnityStandardAssets.Network.LobbyHook>();
            currentPanel = mainMenuPanel;

            //backButton.gameObject.SetActive(false);
            GetComponent<Canvas>().enabled = true;

            DontDestroyOnLoad(gameObject);

            SetServerInfo("Offline", "None");
          //  lobbyData = GetComponent<LobbyExtraData>();
        }

        public override void OnLobbyClientSceneChanged(NetworkConnection conn)
        {
            
            if ( SceneManager.GetActiveScene().name  == lobbyScene)
            {
                if (topPanel.isInGame)
                {
                    ChangeTo(lobbyPanel);
                    if (_isMatchmaking)
                    {
                        if (conn.playerControllers[0].unetView.isServer)
                        {
                            backDelegate = StopHostClbk;
                        }
                        else
                        {
                            backDelegate = StopClientClbk;
                        }
                    }
                    else
                    {
                        if (conn.playerControllers[0].unetView.isClient)
                        {
                            backDelegate = StopHostClbk;
                        }
                        else
                        {
                            backDelegate = StopClientClbk;
                        }
                    }
                }
                else
                {
                    ChangeTo(mainMenuPanel);
                }

                topPanel.ToggleVisibility(true);
                topPanel.isInGame = false;
            }
            else
            {
                ChangeTo(null);

                Destroy(GameObject.Find("MainMenuUI(Clone)"));

                //backDelegate = StopGameClbk;
                if (topPanel != null)
                {
                    topPanel.isInGame = true;
                    topPanel.ToggleVisibility(false);
                }
     
            }
        }

        public void ChangeTo(RectTransform newPanel)
        {
            if (currentPanel != null)
            {
                currentPanel.gameObject.SetActive(false);
            }

            if (newPanel != null)
            {
                newPanel.gameObject.SetActive(true);

            }

            currentPanel = newPanel;

            if (currentPanel != mainMenuPanel)
            {
             //   backButton.gameObject.SetActive(true);
            }
            else
            {
                if (backButton != null)
                    backButton.gameObject.SetActive(false);
                SetServerInfo("Offline", "None");
                _isMatchmaking = false;
            }
        }

        public void DisplayIsConnecting()
        {
           // var _this = this;
           // infoPanel.Display("Connecting...", "Cancel", () => { _this.backDelegate(); });
        }

        public void SetServerInfo(string status, string host)
        {
           // statusInfo.text = status;
           // hostInfo.text = host;
        }

        public void SetGamePlayerAndSpawn(string targetPlayer)
        {
            GameObject player = Resources.Load<GameObject>("PlayableCharacters/" + targetPlayer);
            gamePlayerPrefab = player;
        }

        public delegate void BackButtonDelegate();
        public BackButtonDelegate backDelegate;
        public void GoBackButton()
        {
            backDelegate();
        }

        // ----------------- Server management

        public void AddLocalPlayer()
        {
            TryToAddPlayer();
        }

        public void RemovePlayer(LobbyPlayer player)
        {
            player.RemovePlayer();
        }

        public void SimpleBackClbk()
        {
            ChangeTo(mainMenuPanel);
        }

        public void StopHostClbk()
        {
            if (_isMatchmaking)
            {
                matchMaker.DestroyMatch((NetworkID)_currentMatchID, 0, OnDestroyMatch);
                _disconnectServer = true;
            }
            else
            {
                StopHost();
            }


            ChangeTo(mainMenuPanel);
        }

        public void StopClientClbk()
        {
            StopClient();

            if (_isMatchmaking)
            {
                StopMatchMaker();
            }

            ChangeTo(mainMenuPanel);
        }

        public void StopServerClbk()
        {
            StopServer();
            ChangeTo(mainMenuPanel);
        }

        class KickMsg : MessageBase { }
        public void KickPlayer(NetworkConnection conn)
        {
            conn.Send(MsgKicked, new KickMsg());
        }

        public void KickedMessageHandler(NetworkMessage netMsg)
        {
            infoPanel.Display("Kicked by Server", "Close", null);
            netMsg.conn.Disconnect();
        }

        //===================

        public override void OnStartHost()
        {
            base.OnStartHost();

            ChangeTo(lobbyPanel);
            backDelegate = StopHostClbk;
            SetServerInfo("Hosting", s_Singleton.targetIP);// networkAddress);
            SetGameType();
            LobbyPlayerList._instance.UpdateGameTitleText();
        }


        public override void OnMatchCreate(bool success, string extendedInfo, MatchInfo matchInfo)
        {
            base.OnMatchCreate(success, extendedInfo, matchInfo);
            _currentMatchID = (System.UInt64)matchInfo.networkId;
        }

        public override void OnDestroyMatch(bool success, string extendedInfo)
        {
            base.OnDestroyMatch(success, extendedInfo);
            if (_disconnectServer)
            {
                StopMatchMaker();
                StopHost();
            }
        }

        //allow to handle the (+) button to add/remove player
        public void OnPlayersNumberModified(int count)
        {
            _playerNumber += count;

            int localPlayerCount = 0;
            foreach (PlayerController p in ClientScene.localPlayers)
                localPlayerCount += (p == null || p.playerControllerId == -1) ? 0 : 1;

            addPlayerButton.SetActive(localPlayerCount < maxPlayersPerConnection && _playerNumber < maxPlayers);
        }

        // ----------------- Server callbacks ------------------

        //we want to disable the button JOIN if we don't have enough player
        //But OnLobbyClientConnect isn't called on hosting player. So we override the lobbyPlayer creation
        public override GameObject OnLobbyServerCreateLobbyPlayer(NetworkConnection conn, short playerControllerId)
        {
            GameObject obj = Instantiate(lobbyPlayerPrefab.gameObject) as GameObject;

         //   LobbyPlayer newPlayer = obj.GetComponent<LobbyPlayer>();


            for (int i = 0; i < lobbySlots.Length; ++i)
            {
                LobbyPlayer p = lobbySlots[i] as LobbyPlayer;

                if (p != null)
                {
                    p.RpcUpdateRemoveButton();
                    p.ToggleJoinButton(numPlayers + 1 >= minPlayers);
                    p.RpcCheckYourTeam();

                 //   if (isServer)
                   // {
                        p.RpcSetGameMode(GameSettings.GameType);
                    //}
              //      p.RpcUpdateJoinButton(numPlayers + 1 >= minPlayers);
                }
            }

            return obj;
        }

        public override GameObject OnLobbyServerCreateGamePlayer(NetworkConnection conn, short playerControllerId)
        {
            string name = conn.playerControllers[0].gameObject.GetComponent<LobbyPlayer>().characterName;
            //Spawn player selected by user.
            if (!string.IsNullOrEmpty(name))
            {
                gamePlayerPrefab = (GameObject)Resources.Load("PlayableCharacters/" + name);
            }

            return base.OnLobbyServerCreateGamePlayer(conn, playerControllerId);
        }

        public override void OnLobbyServerPlayerRemoved(NetworkConnection conn, short playerControllerId)
        {
            for (int i = 0; i < lobbySlots.Length; ++i)
            {
                LobbyPlayer p = lobbySlots[i] as LobbyPlayer;

                if (p != null)
                {
                    p.RpcUpdateRemoveButton();
                    p.ToggleJoinButton(numPlayers + 1 >= minPlayers);
                //    p.RpcUpdateJoinButton(numPlayers + 1 >= minPlayers);
                }
            }
        }

        public override void OnLobbyServerDisconnect(NetworkConnection conn)
        {
            for (int i = 0; i < lobbySlots.Length; ++i)
            {
                LobbyPlayer p = lobbySlots[i] as LobbyPlayer;

                if (p != null)
                {
                    p.RpcUpdateRemoveButton();
                    p.ToggleJoinButton(numPlayers >= minPlayers);
                }
            }
        }
 

       // public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
   //     { 
            /*
            GameObject targetCharacter = playerPrefab;
            bool addToList = false;
            bool doReplace = false;
            LobbyExtraData data = null;

            if (connectedPlayers == null)
            {
                connectedPlayers = new Dictionary<NetworkConnection, GameObject>();
                addToList = true;
            }
            else
            {
                if (!connectedPlayers.ContainsKey(conn))
                {
                    addToList = true;
                }
                else
                {
                    GameObject obj = connectedPlayers[conn];
                    if (obj == null)
                    {
                        connectedPlayers.Clear();
                        PlayerHolder[] holders = GameObject.FindObjectsOfType<PlayerHolder>();

                        foreach (PlayerHolder holder in holders)
                        {
                            NetworkConnection connection = holder.connectionToClient;
                            connectedPlayers.Add(connection, holder.gameObject);

                            if (connection.Equals(conn))
                            {
                                data = holder.gameObject.GetComponent<LobbyExtraData>();
                            }
                        }
                    }
                    else
                    {
                        data = obj.GetComponent<LobbyExtraData>();
                    }

                    if (data != null)
                    {
                        targetCharacter = data.players[data.characterIndex];
                        doReplace = data.doReplace;
                    }
                }
            }
            
            base.OnServerAddPlayer(conn, playerControllerId);
            GameObject player = (GameObject)Instantiate(targetCharacter, Vector3.zero, Quaternion.identity);
            if (doReplace)
            {
                NetworkServer.ReplacePlayerForConnection(conn, player, playerControllerId);
            }
            else
            {
                NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
            }

            if (addToList)
            {
               // data = player.GetComponent<LobbyExtraData>();
                connectedPlayers.Add(conn, player);
            }
            */

      //  }

        public override bool OnLobbyServerSceneLoadedForPlayer(GameObject lobbyPlayer, GameObject gamePlayer)
        {
            //This hook allows you to apply state data from the lobby-player to the game-player
            //just subclass "LobbyHook" and add it to the lobby object.

            if (_lobbyHooks)
                _lobbyHooks.OnLobbyServerSceneLoadedForPlayer(this, lobbyPlayer, gamePlayer);

            return true;
        }

        // --- Countdown management

        public override void OnLobbyServerPlayersReady()
        {
            bool allready = true;
            for (int i = 0; i < lobbySlots.Length; ++i)
            {
                if (lobbySlots[i] != null)
                    allready &= lobbySlots[i].readyToBegin;
            }

            if (allready)
                StartCoroutine(ServerCountdownCoroutine());
        }

        private void SetGameType()
        {
            //var gameTypeVals = Enum.GetValues(typeof(GameSettings.GameTypes));
           // GameSettings.GameType = (GameSettings.GameTypes)gameTypeVals[UnityEngine.Random.Range(0, gameTypeVals.Length)];



            int value = UnityEngine.Random.Range(0, 2);
         //   GameSettings.GameType = GameSettings.GameTypes.RIFT;
            GameSettings.GameType = (GameSettings.GameTypes)value;

          //  if (lobbySlots != null && lobbySlots.Length > 0)
          //  {
                for (int i = 0; i < lobbySlots.Length; ++i)
                {
                    if (lobbySlots[i] != null)
                    {//there is maxPlayer slots, so some could be == null, need to test it before accessing!
                        (lobbySlots[i] as LobbyPlayer).RpcSetGameMode(GameSettings.GameType);
                    }
                }
           // }
        }

        public IEnumerator ServerCountdownCoroutine()
        {
            float remainingTime = prematchCountdown;
            int floorTime = Mathf.FloorToInt(remainingTime);

            int r = UnityEngine.Random.Range(0, 100);
            string sceneName = r > 50 ? "UpdatedKyoshiGardens" : "FactoryLevel";

            //Set random game type
            //SetGameType();

            //  while (remainingTime > 0)
            // {
            yield return null;

            remainingTime -= Time.deltaTime;
            int newFloorTime = Mathf.FloorToInt(remainingTime);

            if (newFloorTime != floorTime)
            {//to avoid flooding the network of message, we only send a notice to client when the number of plain seconds change.
                floorTime = newFloorTime;

                for (int i = 0; i < lobbySlots.Length; ++i)
                {
                    if (lobbySlots[i] != null)
                    {//there is maxPlayer slots, so some could be == null, need to test it before accessing!
                        (lobbySlots[i] as LobbyPlayer).RpcUpdateCountdown(floorTime, sceneName == "UpdatedKyoshiGardens");
                    }
                }
            }
           // }

            var path = Application.streamingAssetsPath + "/GameSettings/PlayerSettings.xml";
            var data = "";
            if (File.Exists(path))
            {
                data = File.ReadAllText(path);
            }

            string diablo = GetCharacterData("Diablo", data);
            string psyonic = GetCharacterData("Hawke", data);
            string hawke = GetCharacterData("Psyonic", data);
            string xenith = GetCharacterData("Xenith", data);

            //Debug.Log("data is " + data);
            for (int i = 0; i < lobbySlots.Length; ++i)
            {
                if (lobbySlots[i] != null)
                {
                 //   (lobbySlots[i] as LobbyPlayer).RpcUpdateCountdown(0);
                    if (!string.IsNullOrEmpty(data))
                    {
                        var lPlayer = (lobbySlots[i] as LobbyPlayer);

                        lPlayer.RpcUpdatePlayerData(diablo, false);
                        lPlayer.RpcUpdatePlayerData(psyonic, false);
                        lPlayer.RpcUpdatePlayerData(hawke, false);
                        lPlayer.RpcUpdatePlayerData(xenith, true);

                        //  (lobbySlots[i] as LobbyPlayer).RpcUpdatePlayerData(data);
                    }
                }
            }
            //Go to random scene


            for (int i = 0; i < lobbySlots.Length; ++i)
            {
                if (lobbySlots[i] != null)
                {
                    (lobbySlots[i] as LobbyPlayer).RpcUpdateScreenSize();
                }
            }
            StartCoroutine(LoadLevel(sceneName));

           // Screen.fullScreen = !Screen.fullScreen;
            //ServerChangeScene(sceneName);
            
        }

        private string GetCharacterData(string characterName, string data)
        {
            var startIndex = data.IndexOf("<"+characterName+">");
            var endIndex = data.IndexOf("</" + characterName + ">") + (characterName.Length + 3);

            var s = data.Substring(startIndex, endIndex - startIndex);

            //s = "<xml>" + s + "</xml>";

           // Debug.Log("s : " + s);
            
            return s;
        }

        IEnumerator LoadLevel(string s)
        {
            Screen.SetResolution(1920, 1080, false);

            Screen.fullScreen = !Screen.fullScreen;
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            ServerChangeScene(s);

        }


        public override void OnServerSceneChanged(string sceneName)
        {
            base.OnServerSceneChanged(sceneName);
          //  if (isServer)
           //     NetworkServer.Spawn((GameObject)Resources.Load("Game/GameTimer"));
        }
        // ----------------- Client callbacks ------------------

        public override void OnClientConnect(NetworkConnection conn)
        {
            base.OnClientConnect(conn);

         //   infoPanel.gameObject.SetActive(false);

            conn.RegisterHandler(MsgKicked, KickedMessageHandler);

            if (!NetworkServer.active)
            {//only to do on pure client (not self hosting client)
                ChangeTo(lobbyPanel);
                backDelegate = StopClientClbk;
                SetServerInfo("Client", networkAddress);
            }
        }


        public override void OnClientDisconnect(NetworkConnection conn)
        {
            base.OnClientDisconnect(conn);
            ChangeTo(mainMenuPanel);
        }

        public override void OnClientError(NetworkConnection conn, int errorCode)
        {
            ChangeTo(mainMenuPanel);
            infoPanel.Display("Cient error : " + (errorCode == 6 ? "timeout" : errorCode.ToString()), "Close", null);
        }
    }
}