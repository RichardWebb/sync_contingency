﻿///
/// This is the entrance point of the project and the initial setup of robot-legs framework. 
/// SyncContingencyConfig.cs will be executed once the setup is complete. 
/// Created by Richard Webb, s130833
///
using UnityEngine;
using System.Collections;
using Robotlegs.Bender.Framework.API;

using Robotlegs.Bender.Framework.Impl;
using Rancon.Extensions.BootstrapSettings;
using Rancon.Extensions.CallbackTimer.Platforms.Unity;
using pulsar.config;
using Robotlegs.Bender.Extensions.ContextViews.Impl;
using Robotlegs.Bender.Bundles.MVCS;
using Robotlegs.Bender.Platforms.Unity.Bundles;

namespace pulsar
{
    public class SyncContingencyContext : MonoBehaviour
    {
        public string currentBootstapSettingsPath = "[StreamingAssets]/GameSettings/bootstrap-settings";
        private IContext context;

	    void Start ()
        {
            Screen.SetResolution(1280, 720, false);
            DontDestroyOnLoad(this);
            currentBootstapSettingsPath = currentBootstapSettingsPath.Replace("[StreamingAssets]", Application.streamingAssetsPath);
            context = new Context()
                .Install<UnitySingleContextBundle>()
                .Install<MVCSBundle>()
           //     .Install<UnityFallbackBundle>()
                .Install<UnityCallbackTimerExtension>()
                .Install<BootstrapSettingsExtension>()
                .Configure(new BootstrapSettingsConfig()
                            .WithPath(currentBootstapSettingsPath)
                            .Synchronously()
                            .WithAfterLoad(BoostrapSettingsLoaded))
                .Configure(new ContextView(gameObject.transform))
                 .Configure<SyncContingencyConfig>();

            context.AfterInitializing(PostInit);

        }

        private void PostInit()
        {
        }

        private void BoostrapSettingsLoaded()
        {
        }
    }

}
