﻿///
/// This is a application config, in here we hook up our events to their mediators and views, including commands and models.
/// Follow MainMenu.cs for the next setup in this project.
/// Created by Richard Webb, s1308033
///

using UnityEngine;
using System.Collections;
using pulsar.events;
using pulsar.mediator;
using pulsar.view.characterselection;
using pulsar.command;
using pulsar.model;
using pulsar.view;
using pulsar.view.interfaces;
using Robotlegs.Bender.Framework.API;
using Robotlegs.Bender.Extensions.Mediation.API;
using Robotlegs.Bender.Extensions.EventCommand.API;
using Rancon.Extensions.BootstrapSettings.API;
using Robotlegs.Bender.Extensions.EventManagement.API;
using Rancon.Extensions.BootstrapSettings;

namespace pulsar.config
{
    public class SyncContingencyConfig : IConfig//, IBootstrapSettingsRequester
    {
        [Inject]
        public IMediatorMap mediatorMap;

        [Inject]
        public IEventCommandMap commandMap;

        [Inject]
        public IEventDispatcher dispatcher; 

        [Inject]
        public IInjector injector { get; set; }

        //Hook robotlegs scripts.
        public void Configure()
        {
            mediatorMap.Map<Character>().ToMediator<CharacterDetailsMediator>();
            mediatorMap.Map<MainMenu>().ToMediator<MainMenuMediator>();
            mediatorMap.Map<ILobbyPlayer>().ToMediator<LobbyPlayerMediator>();
            mediatorMap.Map<ISettingsRequester>().ToMediator<SettingsRequesterMediator>();
            mediatorMap.Map<IGameDispatcher>().ToMediator<GameMediator>();
            mediatorMap.Map<IGameListener>().ToMediator<GameListenerMediator>();
            mediatorMap.Map<IActionListener>().ToMediator<PlayerActionMediator>();
            mediatorMap.Map<IScoreDispatcher>().ToMediator<ScoreDispatcherMediator>();
            mediatorMap.Map<IScoreListener>().ToMediator<ScoreListenerMediator>();
            mediatorMap.Map<IScopeListener>().ToMediator<ScopeMediator>();
            mediatorMap.Map<ISuperListener>().ToMediator<SuperMediator>();
            mediatorMap.Map<IPlayerData>().ToMediator<PlayerDataMediator>();

            commandMap.Map(CharacterDetailsEvent.Type.GET_CHARACTER_DETAILS).ToCommand<HandleGetCharacterDetailsCommand>();
            commandMap.Map(UpdateDetailsEvent.Type.UPDATE_USER_DETAILS).ToCommand<HandleUserDetailsCommand>();
            commandMap.Map(UpdateDetailsEvent.Type.GET_USER_DETAILS).ToCommand<HandleUserDetailsCommand>();
            commandMap.Map(PlayerDataEvent.Type.GET_PLAYER_DATA).ToCommand<GetPlayerDataCommand>();
            
            injector.Map<IUserDataModel>().ToSingleton<UserDataModel>();
        }

    }

}
