﻿/*
 * Details command,
 * This command updates the users data - username and team into the user data model.
 * Created by Richard Webb, s1308033
 */
using Robotlegs.Bender.Extensions.CommandCenter.API;
using pulsar.events;
using pulsar.model;
using Robotlegs.Bender.Extensions.EventManagement.API;

public class HandleUserDetailsCommand : ICommand
{
    [Inject]
    public UpdateDetailsEvent evt;

    [Inject]
    public IEventDispatcher dispatcher;

    [Inject]
    public IUserDataModel model;

    public void Execute()
    {
        switch ((UpdateDetailsEvent.Type)evt.type)
        {
            case UpdateDetailsEvent.Type.UPDATE_USER_DETAILS:
                model.PlayerName = evt.userName;
                model.TeamName = evt.teamName;
                UserDataModel._IsTeamOne = model.IsTeamOne;
                break;

            case UpdateDetailsEvent.Type.GET_USER_DETAILS:
                //Send details back to listeners.
                dispatcher.Dispatch(new UpdateDetailsEvent(UpdateDetailsEvent.Type.GOT_USER_DETAILS, model.PlayerName, model.IsTeamOne, model.TeamName));
                break;
        }

    }
}
