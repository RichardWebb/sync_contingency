﻿/*
 * Get get player data command
 * This class handles data gathering of the players stats
 * Created by RIchard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using Robotlegs.Bender.Extensions.CommandCenter.API;
using System;
using pulsar.events;
using System.Xml;
using Robotlegs.Bender.Extensions.EventManagement.API;

namespace pulsar.command
{
    public class GetPlayerDataCommand : ICommand
    {
        [Inject]
        public PlayerDataEvent evt;

        [Inject]
        public IEventDispatcher dispatcher;

        public void Execute()
        {
            switch((PlayerDataEvent.Type)evt.type)
            {
                case PlayerDataEvent.Type.GET_PLAYER_DATA:

                    //Load file and get data
                    var xmlFile = new XmlDocument();
                    xmlFile.Load(Application.streamingAssetsPath + "/GameSettings/PlayerSettings.xml");

                    var player = new PlayerVO();

                    player.characterName = evt.CharacterName;
                    player.health = int.Parse(xmlFile.SelectSingleNode("//" + evt.CharacterName + "/Health").InnerText);
                    player.runSpeed = float.Parse(xmlFile.SelectSingleNode("//" + evt.CharacterName + "/RunSpeed").InnerText);
                    player.sprintSpeed = float.Parse(xmlFile.SelectSingleNode("//" + evt.CharacterName + "/SprintSpeed").InnerText);
                    player.ultCooldown = int.Parse(xmlFile.SelectSingleNode("//" + evt.CharacterName + "/UltCooldown").InnerText);
                    player.grenadeCoolDown = int.Parse(xmlFile.SelectSingleNode("//" + evt.CharacterName + "/GrenadeCooldown").InnerText);
                    player.grenadeDamage = int.Parse(xmlFile.SelectSingleNode("//" + evt.CharacterName + "/GrenadeDamage").InnerText);
                    player.timeBetweenShots = float.Parse(xmlFile.SelectSingleNode("//" + evt.CharacterName + "/TimeBetweenShots").InnerText);
                    player.timeBetweenSecondaryShots = float.Parse(xmlFile.SelectSingleNode("//" + evt.CharacterName + "/TimeBetweenSecondaryShots").InnerText);
                    player.standardAttackDamage = int.Parse(xmlFile.SelectSingleNode("//" + evt.CharacterName + "/AttackDmg").InnerText);
                    player.secondaryAttackDamage = int.Parse(xmlFile.SelectSingleNode("//" + evt.CharacterName + "/SecondaryAttackDmg").InnerText);

                    //Send data off
                    dispatcher.Dispatch(new PlayerDataEvent(PlayerDataEvent.Type.GOT_PLAYER_DATA, player));

                    break;

            }
        }
    }
}