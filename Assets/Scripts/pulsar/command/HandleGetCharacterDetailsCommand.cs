﻿/*
 * Details command,
 * This command handles getting settings from the bootstraps file and storing them within the settings model
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using Robotlegs.Bender.Extensions.CommandCenter.API;
using Rancon.Extensions.BootstrapSettings.API;
using pulsar.events;
using System.Xml;
using Robotlegs.Bender.Extensions.EventManagement.API;

namespace pulsar.command
{
    public class HandleGetCharacterDetailsCommand : ICommand
    {
        [Inject]
        public IBootstrapSettingsModel bootstrapModel;

        [Inject]
        public CharacterDetailsEvent evt;

        [Inject]
        public IEventDispatcher dispatcher;

        public void Execute()
        {
            string path = "//" + evt.currentCharacter + "/";

            string name = bootstrapModel.GetSetting<string>(path + "name");
            string role = bootstrapModel.GetSetting<string>(path + "role");
            string tag = bootstrapModel.GetSetting<string>(path + "tag_line");
            string weapon = bootstrapModel.GetSetting<string>(path + "weapon");
            string grenTitle = bootstrapModel.GetSetting<string>(path + "grenade_title");
            string grenDes = bootstrapModel.GetSetting<string>(path + "grenade_description");
            string primTitle = bootstrapModel.GetSetting<string>(path + "primary_attack_title");
            string primDes = bootstrapModel.GetSetting<string>(path + "primary_description");
            string secTitle = bootstrapModel.GetSetting<string>(path + "secondary_attack_title");
            string secDes = bootstrapModel.GetSetting<string>(path + "secondary_description");
            string ultTitle = bootstrapModel.GetSetting<string>(path + "ultimate_attack_title");
            string ultDes = bootstrapModel.GetSetting<string>(path + "ultimate_description");

            //Send collected details to the model.

            dispatcher.Dispatch(new CharacterDetailsEvent(CharacterDetailsEvent.Type.GOT_CHARACTER_DETAILS, 
                name, role, tag, weapon, grenTitle, grenDes, primTitle, primDes, secTitle, secDes, ultTitle, ultDes)); 
        }

    }
}
