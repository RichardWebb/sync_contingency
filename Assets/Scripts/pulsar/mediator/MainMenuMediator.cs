﻿/*
 * Main menu mediator, used for update user details messages.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using System;
using pulsar.events;

namespace pulsar.mediator
{
    public class MainMenuMediator : Robotlegs.Bender.Bundles.MVCS.Mediator
    {
        public override void Initialize()
        {
            //listen to view.
            AddViewListener(UpdateDetailsEvent.Type.UPDATE_USER_DETAILS, Dispatch);
        }
    }
}
