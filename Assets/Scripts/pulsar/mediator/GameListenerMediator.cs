﻿/*
 * Game listener mediator
 * Listens for start/end of game
 * Created by Richard Webb, s1308033
 */

using UnityEngine;
using System.Collections;
using pulsar.view.interfaces;
using pulsar.events;

namespace pulsar.mediator
{
    public class GameListenerMediator : Robotlegs.Bender.Bundles.MVCS.Mediator
    {
        [Inject]
        public IGameListener view;

        public override void Initialize()
        {
            //listen for events.
            AddContextListener(GameEvent.Type.END_GAME, view.HandleGameFinished);
            AddContextListener(GameEvent.Type.START_GAME, view.HandleGameStart);
        }
    }
}

