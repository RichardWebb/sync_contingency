﻿/*
 * Lobby mediator mediator
 * Listens for user details got and get events.
 * Created by Richard Webb, s1308033
 */

using UnityEngine;
using System.Collections;
using pulsar.events;
using pulsar.view;

namespace pulsar.mediator
{
    public class LobbyPlayerMediator : Robotlegs.Bender.Bundles.MVCS.Mediator
    {
        [Inject]
        public ILobbyPlayer view; 

        public override void Initialize()
        {
            base.Initialize();
            //listen for events
            AddViewListener(UpdateDetailsEvent.Type.GET_USER_DETAILS, Dispatch);
            AddContextListener<UpdateDetailsEvent>(UpdateDetailsEvent.Type.GOT_USER_DETAILS, view.HandleDetails);
          
        }
    }

}
