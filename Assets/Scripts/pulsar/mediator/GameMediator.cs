﻿/*
 * Game start / end mediator
 * Listens for start and end of game play
 * Created by Richard Webb, s1308033
 */

using UnityEngine;
using System.Collections;
using pulsar.view.interfaces;
using pulsar.events;

namespace pulsar.mediator
{
    public class GameMediator : Robotlegs.Bender.Bundles.MVCS.Mediator
    {
        [Inject]
        public IGameDispatcher view;

        public override void Initialize()
        {
            //listen for events
            AddViewListener(GameEvent.Type.END_GAME, Dispatch);
            AddViewListener(GameEvent.Type.START_GAME, Dispatch);
        }
    }
}
