﻿/*
 * Settings request mediator
 * Listens for get and got user details events
 * Created by Richard Webb, s1308033
 */

using UnityEngine;
using System.Collections;
using pulsar.events;
using pulsar.view.interfaces;

namespace pulsar.mediator
{
    public class SettingsRequesterMediator : Robotlegs.Bender.Bundles.MVCS.Mediator
    {
        [Inject]
        public ISettingsRequester view;

        public override void Initialize()
        {
            base.Initialize();
            //listens for events
            AddViewListener(UpdateDetailsEvent.Type.GET_USER_DETAILS, Dispatch);
            AddContextListener<UpdateDetailsEvent>(UpdateDetailsEvent.Type.GOT_USER_DETAILS, view.GotSettings);
        }

    }
}
