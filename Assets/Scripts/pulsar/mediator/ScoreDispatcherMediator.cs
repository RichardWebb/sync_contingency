﻿/*
 * Score dispatcher mediator
 * Listens for team 1 and 2 score updates
 * Created by Richard Webb, s1308033
 */

using UnityEngine;
using System.Collections;
using pulsar.view.interfaces;
using pulsar.events;

namespace pulsar.mediator
{
    public class ScoreDispatcherMediator : Robotlegs.Bender.Bundles.MVCS.Mediator
    {
        [Inject]
        public IScoreDispatcher view;

        public override void Initialize()
        {
            base.Initialize();
            //listen for events.
            AddViewListener(ScoreEvent.Type.UPDATE_TEAM1_SCORE, Dispatch);
            AddViewListener(ScoreEvent.Type.UPDATE_TEAM2_SCORE, Dispatch);
        }

    }
}
