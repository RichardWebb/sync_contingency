﻿/*
 * Character details mediator
 * Listens for character details get and got events.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;

using pulsar.view.characterselection;
using pulsar.events;

namespace pulsar.mediator
{
    public class CharacterDetailsMediator : Robotlegs.Bender.Bundles.MVCS.Mediator
    {
        [Inject]
        public Character view;

        public override void Initialize()
        {
            //listen to events.
            AddViewListener(CharacterDetailsEvent.Type.GET_CHARACTER_DETAILS, Dispatch);
            AddContextListener<CharacterDetailsEvent>(CharacterDetailsEvent.Type.GOT_CHARACTER_DETAILS, view.GotCharacterDetails);
        }
    }

}
