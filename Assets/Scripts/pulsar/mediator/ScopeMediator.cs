﻿/*
 * Scope mediator
 * Listens for scope in and out events
 * Created by Richard Webb, s1308033
 */

using UnityEngine;
using System.Collections;
using pulsar.view.interfaces;
using pulsar.events;

namespace pulsar.mediator
{
    public class ScopeMediator : Robotlegs.Bender.Bundles.MVCS.Mediator
    {
        [Inject]
        public IScopeListener view;

        public override void Initialize()
        {
            base.Initialize();
            //Listen for events
            AddContextListener(ScopeEvent.Type.SCOPED_IN, view.ScopeIn);
            AddContextListener(ScopeEvent.Type.SCOPED_OUT, view.ScopeOut);

            AddViewListener(ScopeEvent.Type.SCOPED_IN, Dispatch);
            AddViewListener(ScopeEvent.Type.SCOPED_OUT, Dispatch);
        }

    }
}

