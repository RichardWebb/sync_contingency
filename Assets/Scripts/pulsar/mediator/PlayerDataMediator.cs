﻿using UnityEngine;
using System.Collections;
using pulsar.view.interfaces;
using Robotlegs.Bender.Bundles.MVCS;
using pulsar.events;
using System;

namespace pulsar.mediator
{
    public class PlayerDataMediator : Mediator
    {
        [Inject]
        public IPlayerData view;

        public override void Initialize()
        {
            base.Initialize();
            AddViewListener(PlayerDataEvent.Type.GET_PLAYER_DATA, Dispatch);

            AddContextListener<PlayerDataEvent>(PlayerDataEvent.Type.GOT_PLAYER_DATA, HandleGotPlayerData);
        }

        private void HandleGotPlayerData(PlayerDataEvent obj)
        {
            view.GotPlayerData(obj.Player);
        }
    }
}