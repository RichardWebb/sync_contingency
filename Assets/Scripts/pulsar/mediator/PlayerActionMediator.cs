﻿/*
 * Player action mediator
 * Listens for player kill and death
 * Created by Richard Webb, s1308033
 */

using UnityEngine;
using System.Collections;
using pulsar.view.interfaces;
using pulsar.events;
using System;

namespace pulsar.mediator
{
    public class PlayerActionMediator : Robotlegs.Bender.Bundles.MVCS.Mediator
    {
        [Inject]
        public IActionListener view;

        public override void Initialize()
        {
            base.Initialize();
            //Listen for events.
            AddContextListener<PlayerActionEvent>(PlayerActionEvent.Type.KILLED_A_PLAYER, HandlePlayerKilled);
            AddContextListener<PlayerActionEvent>(PlayerActionEvent.Type.PLAYER_HAS_DIED, HandlePlayerDied);
            AddViewListener(PlayerActionEvent.Type.PLAYER_HAS_DIED, Dispatch);
            AddViewListener(PlayerActionEvent.Type.KILLED_A_PLAYER, Dispatch);

        }

        private void HandlePlayerDied(PlayerActionEvent obj)
        {
            //Update view that self died
            view.YouDied(obj.PlayerName);
        }

        private void HandlePlayerKilled(PlayerActionEvent obj)
        {
            //update view will player you got killed
            view.KilledPlayer(obj.PlayerName);
        }
    }
}
