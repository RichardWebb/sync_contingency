﻿/*
 * Score mediator
 * Listens for team 1 and 2 score updates
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using pulsar.view.interfaces;
using pulsar.events;

namespace pulsar.mediator
{
    public class ScoreListenerMediator : Robotlegs.Bender.Bundles.MVCS.Mediator
    {
        [Inject]
        public IScoreListener view;

        public override void Initialize()
        {
            base.Initialize();
            AddContextListener<ScoreEvent>(ScoreEvent.Type.UPDATE_TEAM1_SCORE, view.UpdateTeam1Score);
            AddContextListener<ScoreEvent>(ScoreEvent.Type.UPDATE_TEAM2_SCORE, view.UpdateTeam2Score);
        }

    }
}
