﻿/*
 * Super mediator
 * Listens for execute super events.
 * Created by Richard Webb, s1308033
 */

using UnityEngine;
using System.Collections;
using pulsar.events;
using pulsar.view.interfaces;

namespace pulsar.mediator
{ 
    public class SuperMediator : Robotlegs.Bender.Bundles.MVCS.Mediator
    {
        [Inject]
        public ISuperListener view;

        public override void Initialize()
        {
            base.Initialize();
            //Listen for events
            AddViewListener(SuperEvent.Type.EXECUTE_SUPER, Dispatch);
            AddContextListener(SuperEvent.Type.EXECUTE_SUPER, view.ExecuteSuper);
        }

    }
}
