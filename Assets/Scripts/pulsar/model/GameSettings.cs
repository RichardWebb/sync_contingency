﻿/*
 * Game settings object
 * Object for the game types set through the Lobby manager.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;

namespace pulsar.model
{
    public class GameSettings : MonoBehaviour
    {
        //All game types
        public enum GameTypes
        {
            DEATH_MATCH = 0,
            RIFT = 1
        }

        public static GameTypes GameType = GameTypes.DEATH_MATCH;
        public static string PushPath = "";
    }
}