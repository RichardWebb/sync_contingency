﻿/*
 * User data interface
 * Interface used to connect the user data model when getting data.
 * Created by Richard Webb, s1308033
 */

namespace pulsar.model
{
    public interface IUserDataModel 
    {
        string PlayerName { get; set; }
        string TeamName { get; set; }
        bool IsTeamOne { get; }
    }
}
