﻿/*
 * User data model 
 * This model contains the players name and team data.
 * Created by Richard Webb, s1308033
 */

namespace pulsar.model
{
    public class UserDataModel : IUserDataModel
    {
        //Player and team data.
        public string PlayerName { get; set; }
        public string TeamName { get; set; }

        public bool IsTeamOne
        {
            get
            {
                return TeamName.ToLower().Equals("red");
            }
        }

        public static bool _IsTeamOne;

    }

}
