﻿/*
 * Player health view
 * This script handles the player current health and taking damage, hooks to player death script.
 * Created by Richard Webb, s1308033  
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using DG.Tweening;
using System;
using pulsar.view;
using pulsar.view.interfaces;
using Robotlegs.Bender.Extensions.Mediation.API;
using Robotlegs.Bender.Extensions.EventManagement.API;
using Robotlegs.Bender.Extensions.EventManagement.Impl;
using Robotlegs.Bender.Extensions.ViewManagement.API;

namespace pulsar.networksync
{
    public class Player_Health : NetworkBehaviour, IPlayerData
    {
        [SerializeField]
        public int maxHealth = 100;

        //Network variables.
        [SyncVar(hook = "OnHealthChanged")]
        public int health = 100;

        [SyncVar(hook = "OnLastPersonToHitMeChanged")]
        public string lastPersonToHitMe;


        //Configurable variables
        [SerializeField]
        private Image hitHexImage;

        [SerializeField]
        private Color hexFlashColor;

        [SerializeField]
        private Color hexNormalColor;

        private bool shouldDie = false;
        public bool isDead = false;

        //respawn and death delegates.
        public delegate void DieDelegate();
        public event DieDelegate EventDie;

        public delegate void RespawnDelegate();
        public event RespawnDelegate EventRespawn;

        private Coroutine healthRegenRoutine;

        //Min health for regen.
        private int minHealth = 35;

        private PlayerSettings settings;

        [SerializeField]
        private Image healthBarImage;

        [SerializeField]
        private Text healthBarText;

        [SerializeField]
        private CanvasGroup[] hitFlashCanvasGroups;

        private bool showingHealthBar;

        public event Action<IView> RemoveView;
        private IEventDispatcher _dispatcher = new EventDispatcher();

        public IEventDispatcher dispatcher
        {
            get
            {
                return _dispatcher;
            }
            set
            {
                _dispatcher = value;
            }
        }

        private PlayerSettings[] allPlayers;

        void Start()
        {
            ViewNotifier.RegisterView(this);

            settings = GetComponent<PlayerSettings>();
        }

        //On start of local player - set up HUD
        public override void OnStartLocalPlayer()
        {
            healthBarText.text = "Health: 100%";
            healthBarImage.fillAmount = 1;
            hitHexImage.color = hexNormalColor;
        }

        void Update()
        {
            //Check players health condition
            CheckCondition();

            if (showingHealthBar)
            {
                healthBarText.text = health.ToString();
                healthBarImage.fillAmount = health / maxHealth;
            }
        }

        public void ShowHealthBar()
        {
            healthBarText.text = health.ToString();
            healthBarImage.fillAmount = health / maxHealth;

            healthBarImage.enabled = true;
            healthBarText.enabled = true;
            showingHealthBar = true;
        }

        public void HideHealthBar()
        {
            showingHealthBar = false;
            healthBarImage.enabled = false;
            healthBarText.enabled = false;
        }

        private void CheckCondition()
        {
            //Checking if the player should die.
            if (health <= 0 && !shouldDie && !isDead)
            {
                shouldDie = true;
            }

            //if the player should die then run death sequence
            if (health <= 0 && shouldDie)
            {
                if (EventDie != null)
                {
                    EventDie();
                }

                shouldDie = false;
            }

            //Run respawn sequence if health is above 0 and we have died.
            if (health > 0 && isDead)
            {
                if (EventRespawn != null)
                {

                    EventRespawn();
                }

                isDead = false;
            }
        }

        //Update player health text.
        private void SetHealthText(bool hasBeenHit)
        {
            if (isLocalPlayer)
            {
                //Toggle health on/off depending on health above 0

                float percent = (float)health / (float)maxHealth;

                healthBarImage.fillAmount = percent;

                healthBarText.text = "Health: " + (percent * 100).ToString() + "%";

                //Stop regen if dead.
                if (isDead) if (healthRegenRoutine != null) StopCoroutine(healthRegenRoutine);

                //Check if player has been hit and not dead 
                if (hasBeenHit && !isDead)
                {
                    FlashDirectionalIndicator();
                    //Flash display red
                    FlashHitHex();

                    ///Check health is less than the min health - start regen.
                    if (health < minHealth)
                    {
                        if (healthRegenRoutine != null) StopCoroutine(healthRegenRoutine);
                        healthRegenRoutine = StartCoroutine(RegenHealth());
                    }
                }
            }
        }

        private void FlashDirectionalIndicator()
        {
            if (allPlayers == null) allPlayers = FindObjectsOfType<PlayerSettings>();

            GameObject attacker = null;

            foreach (PlayerSettings player in allPlayers)
            {
                if (player.gameObject.name == lastPersonToHitMe)
                {
                    attacker = player.gameObject;
                    break;
                }
            }

            if (attacker != null)
            {
                var attackerPos = attacker.transform.position;
                var myPos = transform.position;
                var myCamera = PlayerSettings.localPlayerSettings.myCamera;
                var screenPos = myCamera.WorldToScreenPoint(attackerPos);

                var width = Screen.width;
                var screenSeg = width * 0.125f;

                if (screenPos.x < screenSeg)
                {
                    FlashHit(hitFlashCanvasGroups[0]);
                    //LEFT
                }
                else if (screenPos.x > 7 * screenSeg)
                {
                    FlashHit(hitFlashCanvasGroups[1]);

                    //RIGHT
                }
                else if (screenPos.z > 0)
                {
                    FlashHit(hitFlashCanvasGroups[2]);

                    //FORWARD
                }
                else if (screenPos.z <= 0)
                {
                    FlashHit(hitFlashCanvasGroups[3]);

                    //BEHIND
                }
            }
        }

        private void FlashHit(CanvasGroup group)
        {
            group.DOFade(1f, 0.1f).SetEase(Ease.Linear).OnComplete(() => 
            {
                group.DOFade(0f, 0.5f).SetEase(Ease.Linear).SetDelay(0.3f);
            });
        }

        //Regen coroutine 
        private IEnumerator RegenHealth()
        {
            //Wait 5 second then regen the health unless the player has died in the mean time.
            yield return new WaitForSeconds(5);
            if (!isDead)
            {
                if (health < minHealth)
                    CmdUpdateHealth(minHealth);
            }
        }

        //Reduce player health by x amount.
        public void DeductHealth(int dmg)
        {
            health -= dmg;
        }

        //Server side - Update the health on the server. 
        [Command]
        public void CmdUpdateHealth(int amount)
        {
            //Update health will trigger  OnHealthChanged on the clients.
            health = amount;
        }

        //Flash HUD red and back when hit.
        private void FlashHitHex()
        {
            hitHexImage.DOColor(hexFlashColor, 0.1f).OnComplete(() =>
            {
                hitHexImage.DOColor(hexNormalColor, 0.5f).SetDelay(0.5f);
            });
        }

        //Client variable method - Updates health and updates HUD.
        void OnHealthChanged(int hlth)
        {
            bool beenHit = hlth < health;
            health = hlth;
            SetHealthText(beenHit);
        }

        //Respawn event - Resets the players health to max.
        public void ResetHealth() //Called Player_Respawn
        {
            health = maxHealth;
        }

        //Server call to update health to max
        public void RefeshHealth()
        {
            CmdUpdateHealth(maxHealth);
        }

        //Checking if the player will die with current health and damage taken.
        public bool CheckIfPlayerWillDie(int damage)
        {
            return (!isDead && (health - damage <= 0));
        }

        //Update record of last person to hit the player.
        public void OnLastPersonToHitMeChanged(string attacker)
        {
            lastPersonToHitMe = attacker;
        }


        //Health pickup method - instant regen of health.
        public void IncreaseHealth(int value)
        {

            if (health + value >= maxHealth)
            {
                value = maxHealth;
            }
            else
            {
                value = health + value;
            }

            //Update server of health.
            CmdUpdateHealth(value);
        }

        public void GotPlayerData(PlayerVO Player)
        {
            if (settings == null)
            {
                settings = GetComponent<PlayerSettings>();
            }

            if (Player.characterName == settings.characterName)
            {
                maxHealth = Player.health;
                health = maxHealth;
            }
        }

    }
}