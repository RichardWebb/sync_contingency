﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using pulsar.view.characterselection;
using pulsar.view;
using System;
using Robotlegs.Bender.Extensions.EventManagement.API;
using Robotlegs.Bender.Extensions.Mediation.API;
using Robotlegs.Bender.Extensions.EventManagement.Impl;
using Robotlegs.Bender.Extensions.ViewManagement.API;
using pulsar.view.interfaces;
using pulsar.events;
using pulsar.view.characters.shooting;
using UnityEngine.UI;
using DG.Tweening;
using pulsar.networksync;

public class Player_Shoot : NetworkBehaviour//, IActionListener, IScoreDispatcher
{
	private int damage = 10;
	private float range = 200;

	[SerializeField]
    private Transform camTransform;

    [SerializeField]
    private Sprite[] recticles;

    [SerializeField]
    private Text playerNameText;

    [SerializeField]
    private Image recticle;

	private RaycastHit hit;

    public bool localPlayer;
    private PlayerSettings settings;
   // private PlayerStatsView playerStats;

    public event Action<IView> RemoveView;
    private IEventDispatcher _dispatcher = new EventDispatcher();
    public IEventDispatcher dispatcher
    {
        get
        {
            return _dispatcher;
        }
        set
        {
            _dispatcher = value;
        }
    }

    private Player_Health[] playersHealth;

    private GameObject hitText;
    private Tweener currentTween;

    void Start()
    {
        ViewNotifier.RegisterView(this);

        hitText = Resources.Load<GameObject>("Game/HitText");
      //  playerStats = GetComponent<PlayerStatsView>();
        settings = GetComponent<PlayerSettings>();

        if (isServer)
        {
            StartCoroutine(GetPlayersHealth());
        }

    }

    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();
        playerNameText.text = "";
      //  StartCoroutine(RayForPlayer());

    }
    
    private IEnumerator RayForPlayer()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.1f);

            if (Physics.Raycast(camTransform.TransformPoint(0, 0, 0.5f), camTransform.forward, out hit, range))
            {
                //Debug.Log(hit.transform.tag);

                if (hit.transform.tag == "Player")
                {
                    PlayerSettings _settings = hit.transform.gameObject.GetComponent<PlayerSettings>();

                    bool isFriendly = _settings.teamName == settings.teamName;
                    playerNameText.color = isFriendly ? Color.green : Color.red;
                    recticle.sprite = isFriendly ? recticles[1] : recticles[2];
                    playerNameText.text = _settings.playerName;

                    currentTween = playerNameText.gameObject.GetComponent<CanvasGroup>().DOFade(1f, 0.1f);
                }
                else
                {
                    if (currentTween != null) currentTween.Kill();
                    playerNameText.gameObject.GetComponent<CanvasGroup>().alpha = 0;
                    recticle.sprite = recticles[0];

                    playerNameText.text = "";
                }
            }
        }
    }

    private IEnumerator GetPlayersHealth()
    {
        yield return new WaitForSeconds(2);
        playersHealth = GameObject.FindObjectsOfType<Player_Health>();
    }

    // Update is called once per frame
    void Update () 
	{
		CheckIfShooting();
	}

	void CheckIfShooting()
	{
		if(!isLocalPlayer)
		{
			return;
		}
        
		if(Input.GetKeyDown(KeyCode.Mouse0))
		{
			Shoot();
		}
	}

	void Shoot()
	{
		if(Physics.Raycast(camTransform.TransformPoint(0, 0, 0.5f), camTransform.forward, out hit, range))
		{
			//Debug.Log(hit.transform.tag);

			if(hit.transform.tag == "Player")
			{
                if (settings.teamName != hit.transform.gameObject.GetComponent<PlayerSettings>().teamName)
                {
				    string uIdentity = hit.transform.name;
                    Player_Health targetHealth = hit.transform.gameObject.GetComponent<Player_Health>();
                    if (targetHealth.health <= 0) return;
                    if (targetHealth.CheckIfPlayerWillDie(damage))
                    {
                        string playerName = hit.transform.gameObject.GetComponent<PlayerSettings>().playerName;

                        if (!string.IsNullOrEmpty(playerName))
                        {
                            dispatcher.Dispatch(new PlayerActionEvent(PlayerActionEvent.Type.KILLED_A_PLAYER, playerName));
                        }

                       // playerStats.UpdateKillCount();
                    }

                    GameObject _hitText = (GameObject)Instantiate(hitText);
                    _hitText.transform.SetParent(settings.hudCanvas.transform);

                    //_hitText.transform.position = hit.point;
                    int randomVal = UnityEngine.Random.Range(0, 4);
                    bool isCrit = randomVal >= 3;
                    int _damage = isCrit ? damage * 2 : damage;


                    _hitText.GetComponent<PlayerHitTextView>().Init(isCrit,_damage, hit.point.z, false);
			        CmdTellServerWhoWasShot(uIdentity, _damage, settings.playerName, gameObject.name);
                }

			}
		}
	}

	[Command]
	void CmdTellServerWhoWasShot (string uniqueID, int dmg, string attacker, string attackerUniID)
	{
        Player_Health _target = null;
        Player_Health _attacker = null;

        foreach (Player_Health player in playersHealth)
        {
            string name = player.gameObject.name;
            if (name == uniqueID)
            {
                _target = player;
            }

            if (name == attackerUniID)
            {
                _attacker = player;
            }
        }


        _target.lastPersonToHitMe = attacker;
        _target.DeductHealth(dmg);

        //rich.GitGud;


        if (_target.health <= 0)
        {
            PlayerSettings settings = _attacker.gameObject.GetComponent<PlayerSettings>();
            if (settings.teamName == "red")
            {
                dispatcher.Dispatch(new ScoreEvent(ScoreEvent.Type.UPDATE_TEAM1_SCORE, 1));
            }
            else
            {
                dispatcher.Dispatch(new ScoreEvent(ScoreEvent.Type.UPDATE_TEAM2_SCORE, 1));
            }

            _attacker.gameObject.GetComponent<PlayerStatsView>().UpdateKillCount(1);
        }
        /*
        foreach (Player_Health player in playersHealth)
        {
            if (player.gameObject.name == uniqueID)
            {
                player.lastPersonToHitMe = attacker;
                player.DeductHealth(dmg);

                if (player.health <= 0)
                {
                    PlayerSettings settings = player.gameObject.GetComponent<PlayerSettings>();
                    if (settings.teamName == "red")
                    {
                        dispatcher.Dispatch(new ScoreEvent(ScoreEvent.Type.UPDATE_TEAM1_SCORE, 1));
                    }
                    else
                    {
                        dispatcher.Dispatch(new ScoreEvent(ScoreEvent.Type.UPDATE_TEAM2_SCORE, 1));
                    }



                }
            }
        }*/
		//GameObject go = GameObject.Find(uniqueID);
		//go.GetComponent<Player_Health>().DeductHealth(dmg);
	}

    public void YouDied(string name)
    {
    }

    public void KilledPlayer(string name)
    {
    }
}
