﻿/*
 * Player ID script
 * This script creates and ID for players when they join a game so they can be picked apart.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

namespace pulsar.networksync
{
    public class Player_ID : NetworkBehaviour
    {

        [SyncVar]
        private string playerUniqueIdentity;
        private NetworkInstanceId playerNetID;
        private Transform myTransform;

        public override void OnStartLocalPlayer()
        {
            GetNetIdentity();
            SetIdentity();
        }

        // Use this for initialization
        void Awake()
        {
            this.transform.name = "Player(Clone)";
            myTransform = transform;
        }

        // Update is called once per frame
        void Update()
        {
            if (myTransform.name == "" || myTransform.name == "Player(Clone)")
            {
                SetIdentity();
            }
          /*  else
            {
                this.enabled = false;
            }*/
        }

        [Client]
        void GetNetIdentity()
        {
            playerNetID = GetComponent<NetworkIdentity>().netId;
            CmdTellServerMyIdentity(MakeUniqueIdentity());
        }

        void SetIdentity()
        {
            	if(!isLocalPlayer)
            	{
                    myTransform.name = playerUniqueIdentity;
            	}
            	else
            	{
            		myTransform.name = MakeUniqueIdentity();
            	}
        }

        string MakeUniqueIdentity()
        {
            string uniqueName = "Player " + playerNetID.ToString();
            return uniqueName;
        }

        [Command]
        void CmdTellServerMyIdentity(string name)
        {
            playerUniqueIdentity = name;
        }

        /*
        //Network variable
        [SyncVar (hook = "OnIDChanged")]
        private string playerUniqueIdentity;

        //On start of local player - Make a new ID.
        public override void OnStartLocalPlayer()
        {
            GetNetIdentity();
        }

        private void Start()
        {
            if (isServer)
            {
                GetNetIdentity();
            }

            gameObject.name = "Player " + GetComponent<NetworkIdentity>().netId.ToString();
        }

        //network call back when variable changes - assigns name to player.
        private void OnIDChanged(string val)
        {
            playerUniqueIdentity = val;
            gameObject.name = playerUniqueIdentity;
        }

        //Client side  - gets new ID based on NetID on server.
        //[Client]
        void GetNetIdentity()
        {
            var id = MakeUniqueIdentity(GetComponent<NetworkIdentity>().netId.ToString());
            CmdTellServerMyIdentity(id);
        }

        //Make the new id and return it
        private string MakeUniqueIdentity(string id)
        {
            string uniqueName = "Player " + id;
            return uniqueName;
        }

        //Sevrer side update players name
        [Command]
        void CmdTellServerMyIdentity(string name)
        {
            playerUniqueIdentity = name;
        }

    */
    }
}