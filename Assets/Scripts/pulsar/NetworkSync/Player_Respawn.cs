﻿/*
 * Player respawn manager
 * This script handles the respawning of the player.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using pulsar.view.characterselection;
using pulsar.view;
using System;
using pulsar.view.mono;
using UnityStandardAssets.Characters.FirstPerson;
using pulsar.networksync;

namespace pulsar.networksync
{
    public class Player_Respawn : NetworkBehaviour
    {
        private Player_Health healthScript;

        private PlayerSettings settings;
        private GameObject exitPoint;

      //  [SerializeField]
       // private CameraDeathView cameraView;

        public override void PreStartClient()
        {
            healthScript = GetComponent<Player_Health>();
            healthScript.EventRespawn += EnablePlayer;
        }

        void Start()
        {
            settings = GetComponent<PlayerSettings>();
        }

 

        //On network end - unhook delegates.
        public override void OnNetworkDestroy()
        {
            //healthScript.EventRespawn -= EnablePlayer;
        }

        public void EnablePlayer()
        {
            //Enable controller and collision.
            GetComponent<CharacterController>().enabled = true;
            GetComponent<CapsuleCollider>().enabled = true;

            //Toggle renderers
            Renderer[] renderers = GetComponentsInChildren<Renderer>();
            foreach (Renderer ren in renderers)
            {
                if (ren.gameObject.name != "NameText")
                    ren.enabled = true;
            }

            //Enable syncing
          // GetComponent<NetworkTransform>().enabled = true;

            if (isLocalPlayer)
            {
                //if local player then position player at respawn point
             /*   var controller = GetComponent<FirstPersonController>();

                controller.isDead = false;
                controller.enabled = true;

                transform.position = settings.GetRespawnPoint();*/

                //enable movement and disbale respawn button.
              //  GetComponent<FirstPersonController>().enabled = true;
               // GetComponent<CharacterController>().enabled = true;

            }
            else
            {
                //if not local player then enable collision colliders
               
                //Resume syncing.
                StartCoroutine(EnableSyncing());
            }


        }

        //Wait some frames then enable syncing.
        private IEnumerator EnableSyncing()
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            GetComponent<CapsuleCollider>().enabled = true;
            GetComponentInChildren<CapsuleCollider>().enabled = true;
            settings.StopSyncing = false;
        }

        //On respawn button pressed  - update server and reset camera.
        public void CommenceRespawn()
        {
         //   cameraView.ResetCamera();
            CmdRespawnOnServer();
        }

        //Server side - reset health back to max
        [Command]
        void CmdRespawnOnServer()
        {
            healthScript.ResetHealth();
        }

    }
}