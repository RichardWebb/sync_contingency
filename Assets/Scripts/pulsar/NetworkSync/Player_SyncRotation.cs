﻿/*
 * Player rotation syncing
 * This script syncs the players rotation of the network.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;
using pulsar.view;
using System.Collections;
using puslar.view.characters.additional;

namespace pulsar.networksync
{
    [NetworkSettings(channel = 1, sendInterval = 0.1f)]

    public class Player_SyncRotation : NetworkBehaviour
    {
        //Network vars - player rotation and camera rotation;
        [SyncVar(hook = "OnPlayerRotSynced")]
        private Vector3 syncPlayerRotation;

        [SyncVar(hook = "OnCamRotSynced")]
        private Vector3 syncCamRotation;

        //The players camera.
        [SerializeField]
        private Transform camTransform;

        [SerializeField]
        private AnimationSyncView animationSyncView;

        private PlayerSettings mySettings;

        // lerp rate settings.
        private float lerpRate = 16;
        private Vector3 lastPlayerRot;
        private Vector3 lastCamRot;

        private float threshold = 0.1f;

        //lag rotation lists for cam and player
        private List<Vector3> syncPlayerRotList = new List<Vector3>();
        private List<Vector3> syncCamRotList = new List<Vector3>();
        private float closeEnough = 0.2f;

        public bool stop = false;

        [SerializeField]
        private bool doLagLerping;

        void Start()
        {
            //Get my settings.
            mySettings = GetComponent<PlayerSettings>();

            if (isLocalPlayer)
            {
                StartCoroutine(SendRotsToServer());
            }
        }

        void Update()
        {
            //if not local player then lerp to known rotation
            LerpToKnownRotation();
        }

        private IEnumerator SendRotsToServer()
        {
            while (true)
            {
                yield return new WaitForSeconds(0.1f);
                SendRotations();
            }
        }

        void FixedUpdate()
        {
            //Client send rotation to server for syncing.
        //    SendRotations();
        }

        private void LerpToKnownRotation()
        {
            if (stop) return;
            //if not local player and no overrides then lerp to known rotations
            if (!isLocalPlayer && !mySettings.StopSyncing && !mySettings.VRHasControl)
            {
                //Do lag lerp or standard lerp.
                if (doLagLerping)
                {
                    DoLagLerpToRotation();
                }
                else
                {
                    DoStandardRotationLerp();
                }
            }
        }

        private void DoLagLerpToRotation()
        {
            /*
            //Check if there are rotations to lerp to.
            if (syncPlayerRotList.Count > 0)
            {
               //Lerp to first rotation.
                LerpPlayerRotation(syncPlayerRotList[0]);

                //Check if rotation val is close enough then move to next
                if (Mathf.Abs(transform.localEulerAngles.y - syncPlayerRotList[0]) < closeEnough)
                {
                    syncPlayerRotList.RemoveAt(0);
                }
            }

            //Do the same as above but for the camera.
            if (syncCamRotList.Count > 0)
            {
                LerpCamRot(syncCamRotList[0]);

                if (Mathf.Abs(camTransform.localEulerAngles.x - syncCamRotList[0]) < closeEnough)
                {
                    syncCamRotList.RemoveAt(0);
                }
            }*/
        }

        private void DoStandardRotationLerp()
        {
            //Lerp player and camera to last known rotation.
            LerpPlayerRotation(syncPlayerRotation);
            LerpCamRot(syncCamRotation);
        }

        //Do the lerping to the rotation.
        private void LerpPlayerRotation(Vector3 rotAngle)
        {
            //Vector3 playerNewRot = new Vector3(0, rotAngle, 0);
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(rotAngle), lerpRate * Time.deltaTime);
        }

        //Do the lerping for the camera.
        private void LerpCamRot(Vector3 rotAngle)
        {
           // Vector3 camNewRot = new Vector3(rotAngle, 0, 0);
            camTransform.localRotation = Quaternion.Lerp(camTransform.localRotation, Quaternion.Euler(rotAngle), lerpRate * Time.deltaTime);
        }

        //Server side - sending rotations to the server.
        [Command]
        void CmdProvideRotationsToServer(Vector3 playerRot, Vector3 camRot)
        {
            syncPlayerRotation = playerRot;
            syncCamRotation = camRot;
        }

        //Client side send rotations to the server.
        //[Client]
        private void SendRotations()
        {
            if (isLocalPlayer)
            {
                //Check if the player has rotated enough - if so then send rotations.
                //if (CheckIfBeyondThreshold(transform.localEulerAngles.y, lastPlayerRot) || CheckIfBeyondThreshold(camTransform.localEulerAngles.x, lastCamRot))
                //{
                    lastPlayerRot = transform.rotation.eulerAngles;
                    lastCamRot = camTransform.transform.localRotation.eulerAngles;
                    CmdProvideRotationsToServer(lastPlayerRot, lastCamRot);
              //  }
            }
        }

        //Threshold check between two rotations.
        private bool CheckIfBeyondThreshold(float rot1, float rot2)
        {
            return Mathf.Abs(rot1 - rot2) > threshold;
            /*
            if (Mathf.Abs(rot1 - rot2) > threshold)
            {
                return true;
            }
            else
            {
                return false;
            }*/
        }

        // Client side update player rotations.
        [Client]
        private void OnPlayerRotSynced(Vector3 latestPlayerRot)
        {
            syncPlayerRotation = latestPlayerRot;
            syncPlayerRotList.Add(syncPlayerRotation);
        }

        //Client side update cam rotations
        [Client]
        void OnCamRotSynced(Vector3 latestCamRot)
        {
            syncCamRotation = latestCamRot;
            syncCamRotList.Add(syncCamRotation);
            if (!isLocalPlayer)
            {
                animationSyncView.UpdateLookDirection(syncCamRotation.x);
            }
        }
    }
}