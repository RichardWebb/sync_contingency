﻿/*
 * Player syncing of position.
 * This script syncs the the players position over the network.
 * Created by Richard Webb, s1308033.
 */
using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;
using pulsar.view;
using System.Collections;

namespace pulsar.networksync
{
    [NetworkSettings(channel = 1, sendInterval = 0.1f)]
    public class Player_SyncPosition : NetworkBehaviour
    {
        //network variable - position over network.
        [SyncVar(hook = "SyncPositionValues")]
        private Vector3 syncPos;

        //Configurable variable - High latency toggle.
        [SerializeField]
        private bool useLagLerp = false;

        //lerping rates for dummies.
        private float lerpRate;
        private float normalLerpRate = 16;
        private float fasterLerpRate = 27;

        //last known position.
        private Vector3 lastPos;
        private float threshold = 0.1f;

        //list of last position for high latency
        private List<Vector3> syncPosList = new List<Vector3>();

        //Min distance check var.
        private float closeEnough = 0.11f;
        private PlayerSettings settings;

        public bool stop = false;

        void Start()
        {
            //Get settings and update lerp rate.
            settings = GetComponent<PlayerSettings>();
            lerpRate = normalLerpRate;

            if (isLocalPlayer)
            {
                StartCoroutine(SendPositionToServer());
            }
        }


        void Update()
        {
           //if object is not active disbale syncing.
            if (!this.gameObject.activeSelf) return;

            //Lerp to known position (is not local player)
            LerpPosition();
        }

        void FixedUpdate()
        {
            //Local player call - update server my known position.
           // SendPosition();
        }

        private void LerpPosition()
        {
            if (stop) return;

            //if not local player and no overrides then lerp to known position.
            if (!isLocalPlayer && !settings.StopSyncing )
            {
                if (settings.VRHasControl) return;

                if (useLagLerp)
                {
                    DoLagLerpToPosition();
                }
                else
                {
                    DoStandardLerpToPosition();
                }
            }
        }

        private IEnumerator SendPositionToServer()
        {
            while (true)
            {
                yield return new WaitForSeconds(0.1f);
                if (Vector3.Distance(transform.position, lastPos) > threshold)
                {
                    CmdProvidePositionToServer(transform.position);
                    lastPos = transform.position;
                }
            }
        }

        //Server side - Update known position.
        [Command]
        private void CmdProvidePositionToServer(Vector3 pos)
        {
            syncPos = pos;
        }

        //Only client side function - sends position to server 
        [ClientCallback]
        private void SendPosition()
        {
            //Check if we have moved enough then send new position of player.
         /*   if (isLocalPlayer && Vector3.Distance(transform.position, lastPos) > threshold)
            {
                CmdProvidePositionToServer(transform.position);
                lastPos = transform.position;
            }*/
        }

        //Client function - updates the synced position.
        [Client]
        private void SyncPositionValues(Vector3 latestPos)
        {
            syncPos = latestPos;
            syncPosList.Add(syncPos);
        }

        //Standard lerp to known position.
        private void DoStandardLerpToPosition()
        {
            transform.position = Vector3.Lerp(transform.position, syncPos, Time.deltaTime * lerpRate);
        }

        //Latency lerping to known position
        private void DoLagLerpToPosition()
        {
            if (syncPosList.Count > 0)
            {
                //lerp to position 1
                transform.position = Vector3.Lerp(transform.position, syncPosList[0], Time.deltaTime * lerpRate);

                //Check distance and remove 'check point'
                if (Vector3.Distance(transform.position, syncPosList[0]) < closeEnough)
                {
                    syncPosList.RemoveAt(0);
                }

                //check if position are stacking up - if so then move the player faster.
                if (syncPosList.Count > 10)
                {
                    lerpRate = fasterLerpRate;
                }
                else
                {
                    lerpRate = normalLerpRate;
                }

            }
        }
    }
}