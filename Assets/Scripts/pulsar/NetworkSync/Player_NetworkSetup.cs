﻿/*
 * Player setup script 
 * This script sets up the players required scripts - i.e movement 
 * Created by Richard Webb, s1308033
 */
using pulsar.view;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets.Characters.FirstPerson;

namespace pulsar.networksync
{
    public class Player_NetworkSetup : NetworkBehaviour
    {
        //Configurable variables.
        [SerializeField]
        Camera FPSCharacterCam;

        [SerializeField]
        AudioListener audioListener;

        // Use this for initialization
        public override void OnStartLocalPlayer()
        {
            //enable the player controller and camera / audio listener.
         //   StartCoroutine(StartCamAndController());

            audioListener.enabled = true;
         //   GetComponent<NetworkAnimator>().SetParameterAutoSend(0, true);
        }

        private IEnumerator StartCamAndController()
        {
            yield return new WaitForSeconds(5);
            //FPSCharacterCam.enabled = true;
          
        }

        //local player setup once cinematic is done
        public void EnableCamera()
        {
            if (isLocalPlayer)
            {
                FPSCharacterCam.enabled = true;
              //  FPSCharacterCam.GetComponent<TonemappingColorGrading>().enabled = true;
                GetComponent<FirstPersonController>().enabled = true;
                PlayerSettings.localPlayerSettings.EnableCanvas();
				PlayerSettings.localPlayerSettings.PlayClip (0);
            }
        }

        public override void PreStartClient()
        {
          //  GetComponent<NetworkAnimator>().SetParameterAutoSend(0, true);
        }

    }
}