﻿/*
 * Player death manager
 * This script handles the death of the player - disabling any inputs and running death animations.
 * Created by Richard Webb, s1308033
 */

using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using pulsar.view;
using System;
using Robotlegs.Bender.Extensions.Mediation.API;
using Robotlegs.Bender.Extensions.EventManagement.API;
using Robotlegs.Bender.Extensions.EventManagement.Impl;
using Robotlegs.Bender.Extensions.ViewManagement.API;
using pulsar.events;
using pulsar.view.interfaces;
using pulsar.view.mono;
using pulsar.view.characters.additional;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;
using DG.Tweening;

namespace pulsar.networksync
{
    public class Player_Death : NetworkBehaviour, IActionListener
    {
      //  [SerializeField]
      //  private CameraDeathView myCamView;

        private Player_Health playerHealth;
        private PlayerSettings settings;
        private PlayerStatsView playerStats;

        //Robotleg setup variables
        public event Action<IView> RemoveView;
        private IEventDispatcher _dispatcher = new EventDispatcher();
        public IEventDispatcher dispatcher
        {
            get
            {
                return _dispatcher;
            }
            set
            {
                _dispatcher = value;
            }
        }

        private PlayerSettings[] allPlayers;
        private bool lookAtKiller = false;
        private GameObject killer;
        private Quaternion prevRotation;
        private Player_Respawn respawnView;

        //----------------------------- Death Screen Images -------------------------------/
        [SerializeField]
        private CanvasGroup deathScreenViewCGroup;

        [SerializeField]
        private int respawnTimer = 5;

        [SerializeField]
        private Text respawnText;

        [SerializeField]
        private Image respawnImage;

        [SerializeField]
        private Text killerText;

        [SerializeField]
        private CanvasGroup hudCanvas;


        [SerializeField]
        private GameObject deathParticle;

        //Before client is setup get all required references
        public override void PreStartClient()
        {
            playerHealth = GetComponent<Player_Health>();
            playerStats = GetComponent<PlayerStatsView>();
            playerHealth.EventDie += DisablePlayer;
            playerHealth.EventRespawn += HandleRespawn;
        }
        void Start()
        {
            //Hook mediators
            ViewNotifier.RegisterView(this);
            //Get player settings.
            settings = GetComponent<PlayerSettings>();
            respawnView = GetComponent<Player_Respawn>();
        }

        private void HandleRespawn()
        {
            if (isLocalPlayer)
            {
                deathScreenViewCGroup.alpha = 0;
                hudCanvas.alpha = 1f;
                lookAtKiller = false;
                killer = null;
                PlayerSettings.localPlayerSettings.myCamera.transform.rotation = prevRotation;
            }
        }

        //On network end - unhook delgate.
        public override void OnNetworkDestroy()
        {
            playerHealth.EventDie -= DisablePlayer;
        }

        private void Update()
        {
            if (!isLocalPlayer) return;
             
            if (lookAtKiller && killer != null)
            {
                PlayerSettings.localPlayerSettings.myCamera.transform.LookAt(killer.transform);
            }
        }

        private void DisablePlayer()
        {
            //Check if VR is holding character - if so do not disable character.
            if (settings.VRHasControl) return;

            //diable character inputs / collision 

            GetComponent<CharacterController>().enabled = false;
            GetComponent<CapsuleCollider>().enabled = false;

            Renderer[] renderers = GetComponentsInChildren<Renderer>();
            foreach (Renderer ren in renderers)
            {
                if (ren.gameObject.name != "NameText")
                    ren.enabled = false;
            }

            playerHealth.isDead = true;
            //GetComponent<NetworkTransform>().enabled = false;
            var _deathParticle = (GameObject)Instantiate(deathParticle, transform.position, Quaternion.identity);
            _deathParticle.GetComponent<Renderer>().material.SetColor("_TintColor", settings.teamName == "red" ? Color.red : new Color(0, 170 / 255, 1, 1));
            Destroy(_deathParticle, 3);

            if (isLocalPlayer)
            {
                hudCanvas.alpha = 0f;
                var markedView = GetComponent<MarkedPlayerView>();

                //Check if player is marked for death - if so then remove the hit.
                if (markedView.PlayerMarked)
                {
                    markedView.ClearMarked();
                }

                //Play camera pan out on death
               // myCamView.PlayDeath();

                if (allPlayers == null)
                {
                    allPlayers = FindObjectsOfType<PlayerSettings>();
                }

                var targetName = playerHealth.lastPersonToHitMe;
                GameObject target = null;

                foreach(PlayerSettings s in allPlayers)
                {
                    if (s.gameObject.name == playerHealth.lastPersonToHitMe)
                    {
                        targetName = s.playerName;
                        target = s.gameObject;
                    }
                }

                killerText.text = targetName;
                ShowDeathScreen();


                prevRotation = PlayerSettings.localPlayerSettings.myCamera.transform.rotation;

                if (target != null)
                {
                    killer = target;
                    lookAtKiller = true;
                }


                //Inform listeners the player has died and who killed the player.
                dispatcher.Dispatch(new PlayerActionEvent(PlayerActionEvent.Type.PLAYER_HAS_DIED, targetName));
                GetComponent<CharacterController>().enabled = false;
                var controller = GetComponent<FirstPersonController>();

                controller.isDead = true;
                controller.enabled = false;
                //GetComponent<FirstPersonController>().enabled = false;
                //Toggle repsawn button


                //Update my stats of my death.
                playerStats.UpdateMyDeath();
            }
            else
            {
                //if not local player then just disable collision stop syncing.
                GetComponent<CapsuleCollider>().enabled = true;
                GetComponentInChildren<CapsuleCollider>().enabled = true;
                transform.position = new Vector3(0, -1000, 0);
                settings.StopSyncing = true;
            }
        }

        private void ShowDeathScreen()
        {
            respawnText.text = respawnTimer.ToString();
            respawnImage.fillAmount = 1;
            StartCoroutine(DoRespawnCountDown());

            deathScreenViewCGroup.DOFade(1f, 0.5f).SetEase(Ease.Linear).OnComplete(()=> 
            {
                float fill = respawnTimer;

                DOTween.To(() => respawnImage.fillAmount, x => respawnImage.fillAmount = x, 0, respawnTimer);
            });
        }

        private IEnumerator DoRespawnCountDown()
        {
            yield return new WaitForSeconds(0.5f);

            for (int i = 0; i < respawnTimer; i++)
            {
                yield return new WaitForSeconds(1f);
                respawnText.text = (respawnTimer - (i+1)).ToString();
            }


            var controller = GetComponent<FirstPersonController>();

            controller.isDead = false;
            controller.enabled = true;

            transform.position = settings.GetRespawnPoint();

            respawnView.CommenceRespawn();
        }    

        public void YouDied(string name)
        {
        }

        public void KilledPlayer(string name)
        {
        }
    }
}