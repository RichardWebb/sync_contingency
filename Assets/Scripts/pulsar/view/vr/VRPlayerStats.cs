﻿/*
 * VR player stats
 * This class handles the overlay of the players details for VR 
 * 
 * Created by Richard Webb, s1308033;
 */
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using pulsar.networksync;

namespace pulsar.view.vr
{
    public class VRPlayerStats : MonoBehaviour
    {
        private bool showStats = false;

        [SerializeField]
        private Text playerName;

        [SerializeField]
        private Text playerHealth;

        private Player_Health myHealth;

        public void ShowStats()
        {
            showStats = true;
            playerName.text = GetComponent<PlayerSettings>().playerName;
            myHealth = GetComponent<Player_Health>();
        }

        // Update is called once per frame
        void Update()
        {
            if (showStats)
            {
                playerHealth.text = GetPercent();

                Vector3 lookTrgt = transform.position + (transform.position - Camera.main.transform.position);
                playerHealth.gameObject.transform.parent.transform.LookAt(lookTrgt);
            }
        }

        private string GetPercent()
        {
            float h = myHealth.health;
            float mH = myHealth.maxHealth;
            return ((h / mH) * 100).ToString() + "%";
        }
    }
}