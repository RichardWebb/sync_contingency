﻿/*
 * Mark player view
 * This class handles the marking of players.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System;
using UnityEngine.UI;
using DG.Tweening;

namespace pulsar.view.vr
{
    public class VRMarkPlayerView : NetworkBehaviour
    {
        //config vars
        [SerializeField]
        private float cooldown = 30;

        private VRSettings mySettings;

        private GameObject currentPlayer;

        [SerializeField]
        private SteamVR_TrackedObject trackedObj;

        [SerializeField]
        private Image[] coolDownImages;

        [SerializeField]
        private LayerMask layerMask;

        [SerializeField]
        private CanvasGroup[] groups;

        //holding and range check vars 
        private bool readyToUse = false;

        //cooldown timer
        private float currentTime = 0;

        [SerializeField]
        private LineRenderer lineRenderer;

        [SerializeField]
        private Transform startPos;

        void Start()
        {
            mySettings = GetComponent<VRSettings>();

            if (isLocalPlayer)
            {
                UpdateImages();
                lineRenderer.SetColors(Color.gray, Color.gray);
                foreach (CanvasGroup g in groups) g.alpha = 1;
            }
        }

        //Get cooldown percent.
        private float GetPercent()
        {
            return currentTime / cooldown;
        }

        private void UpdateImages()
        {
            //Animate HUD
            float percent = GetPercent();
            DOTween.To(() => coolDownImages[0].fillAmount, x => coolDownImages[0].fillAmount = x, percent, 0.95f).SetEase(Ease.Linear);
            DOTween.To(() => coolDownImages[1].fillAmount, x => coolDownImages[1].fillAmount = x, percent, 0.95f).SetEase(Ease.Linear);
            DOTween.To(() => coolDownImages[2].fillAmount, x => coolDownImages[2].fillAmount = x, percent, 0.95f).SetEase(Ease.Linear);
        }

        void Update()
        {
            if (mySettings.locked) return;
            //If ready to use and is local player then check inputs.
            if (!isLocalPlayer) return;

            var device = SteamVR_Controller.Input((int)trackedObj.index);

            if (device == null) return;

            ColorLineRenderer();

            if (!readyToUse)
            {
                currentTime += Time.deltaTime;
                currentTime = Mathf.Clamp(currentTime, 0, cooldown);
                readyToUse = currentTime == cooldown;
                //lineRenderer.SetColors(Color.green, Color.green);
                UpdateImages();
            }
            else
            {
                //In range - input checks
                if (device.GetTouchUp(SteamVR_Controller.ButtonMask.Trigger))
                {
                    RaycastHit hit;
                    if (Physics.Raycast(startPos.position, startPos.forward, out hit, 500, layerMask))
                    {
                        if (hit.collider.transform.tag.Contains("Player"))
                        {
                            var settings = hit.collider.gameObject.GetComponent<PlayerSettings>();

                            if (settings != null)
                            {
                                if (mySettings.teamName != settings.teamName)
                                {
                                    Reset();
                                    mySettings.MarkPlayer(settings.gameObject.name);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void ColorLineRenderer()
        {
            RaycastHit hit;
            lineRenderer.SetPosition(0, startPos.position);

            if (Physics.Raycast(startPos.position, startPos.forward, out hit, 500, layerMask))
            {
                lineRenderer.SetPosition(1, hit.point);

                if (hit.collider.transform.tag.Contains("Player"))
                {
                    var settings = hit.collider.gameObject.GetComponent<PlayerSettings>();

                    if (settings != null)
                    {
                        var targetColor = (mySettings.teamName == settings.teamName) ? Color.red : Color.green;
                        lineRenderer.SetColors(targetColor, targetColor);
                    }
                }
                else
                {
                    lineRenderer.SetColors(Color.gray, Color.gray);
                }
            }
            else
            {
                var endPos = startPos.position + (startPos.forward * 500);
                lineRenderer.SetPosition(1, endPos);
                lineRenderer.SetColors(Color.gray, Color.gray);
            }
        }

        private void Reset()
        {
            currentTime = 0;
            readyToUse = false;
        }
    }
}