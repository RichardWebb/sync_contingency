﻿/*
 * Health Beam View
 * This class handles healing from VR to players
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using DG.Tweening;
using System;
using pulsar.networksync;

namespace pulsar.view.vr
{
    public class VRHealthBeamView : NetworkBehaviour
    {
        private VRSettings mySettings;

        [SerializeField]
        private LineRenderer lineRenderer;

        [SerializeField]
        private SteamVR_TrackedObject trackedObj;

        [SerializeField]
        private Image[] coolDownImages;

        [SerializeField]
        private Transform startPos;

        [SerializeField]
        private LayerMask layerMask;

        //cooldown time
        [SerializeField]
        private float cooldown = 10;
        private float currentTime = 0;
        private bool readyToUse;

        private Player_Health[] playersHealth;

        [SerializeField]
        private CanvasGroup[] groups;

        // Use this for initialization
        void Start()
        {
            mySettings = GetComponent<VRSettings>();

            if (isLocalPlayer)
            {
                lineRenderer.SetColors(Color.gray, Color.gray);
                foreach (CanvasGroup g in groups) g.alpha = 1;

            }
        }

        // Update is called once per frame
        void Update()
        {
            if (mySettings.locked) return;

            if (!isLocalPlayer) return;

            SteamVR_Controller.Device device = SteamVR_Controller.Input((int)trackedObj.index);

            if (device == null) return;

            ColorLineRenderer();          

            if (!readyToUse)
            {
                currentTime += Time.deltaTime;
                currentTime = Mathf.Clamp(currentTime, 0, cooldown);
                readyToUse = currentTime == cooldown;
                lineRenderer.SetColors(Color.green, Color.green);
                UpdateImages();
            }
            else
            {
                if (device.GetTouchUp(SteamVR_Controller.ButtonMask.Trigger))
                {
                    RaycastHit hit;
                    if (Physics.Raycast(startPos.position, startPos.forward, out hit, 500, layerMask))
                    {
                        if (hit.collider.transform.tag.Contains("Player"))
                        {
                            var settings = hit.collider.gameObject.GetComponent<PlayerSettings>();

                            if (settings != null)
                            {
                                if (mySettings.teamName == settings.teamName)
                                {
                                    Reset();
                                    CmdHealPlayer(settings.gameObject.name);
                                }
                            }
                        }
                    }
                }
            }
        }

        private void ColorLineRenderer()
        {
            RaycastHit hit;
            lineRenderer.SetPosition(0, startPos.position);

            if (Physics.Raycast(startPos.position, startPos.forward, out hit, 500, layerMask))
            {
                lineRenderer.SetPosition(1, hit.point);
                if (hit.collider.transform.tag.Contains("Player"))
                {
                    var settings = hit.collider.gameObject.GetComponent<PlayerSettings>();

                    if (settings != null)
                    {
                        var targetColor = (mySettings.teamName == settings.teamName) ? Color.green : Color.red;
                        lineRenderer.SetColors(targetColor, targetColor);
                    }
                }
                else
                {
                    lineRenderer.SetColors(Color.gray, Color.gray);
                }
            }
            else
            {
                var endPos = startPos.position + (startPos.forward * 500);
                lineRenderer.SetPosition(1, endPos);
                lineRenderer.SetColors(Color.gray, Color.gray);
            }
        }

        [Command]
        private void CmdHealPlayer(string name)
        {
            if (playersHealth == null) playersHealth = FindObjectsOfType<Player_Health>();

            foreach (Player_Health health in playersHealth)
            {
                if (health.gameObject.name == name)
                {
                    health.IncreaseHealth(50);
                    break;
                }
            }
        }

        private void Reset()
        {
            currentTime = 0;
            readyToUse = false;
        }


        //Tween HUD 
        private void UpdateImages()
        {
            float percent = GetPercent();

            DOTween.To(() => coolDownImages[0].fillAmount, x => coolDownImages[0].fillAmount = x, percent, 0.95f).SetEase(Ease.Linear);
            DOTween.To(() => coolDownImages[1].fillAmount, x => coolDownImages[1].fillAmount = x, percent, 0.95f).SetEase(Ease.Linear);
            DOTween.To(() => coolDownImages[2].fillAmount, x => coolDownImages[2].fillAmount = x, percent, 0.95f).SetEase(Ease.Linear);
        }

        //Get percent of cooldown left.
        private float GetPercent()
        {
            return currentTime / cooldown;
        }

    }
}