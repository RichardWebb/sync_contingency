﻿/*
 * Vr Pick up player view
 * This class handles the pick up of players in the game.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using DG.Tweening;
using UnityEngine.Networking;

namespace pulsar.view.vr
{
    public class VRPickUpCharacterView : NetworkBehaviour
    {
        //Configurable vars 
        [SerializeField]
        private Transform parent;

        [SerializeField]
        private VRSettings mysettings;

        //[SerializeField]
      //  private Transform holdingObj;

        [SerializeField]
        private SteamVR_TrackedObject trackedObj;

        [SerializeField]
        private CanvasGroup canvasGroup;

        [SerializeField]
        private Image portraitImage;

        [SerializeField]
        private Text nameTxt;

        [SerializeField]
        private Text killsTxt;

        [SerializeField]
        private Text deathTxt;

        [SerializeField]
        private Text killstreakTxt;

        //Current held player.
        private GameObject currentPlayer;
        private VRSettings mySettings;

      //  [SerializeField]
     //   private LineRenderer lineRenderer;

        [SerializeField]
        private Transform startPos;

        [SerializeField]
        private LayerMask layerMask;

        [SerializeField]
        private Image backgroundImage;

        [SerializeField]
        private Sprite[] backgroundSprites;

        void Start()
        {
            mySettings = GetComponent<VRSettings>();

            if (isLocalPlayer)
            {
                canvasGroup.alpha = 0;
               // lineRenderer.SetColors(Color.gray, Color.gray);
            }
        }


        void Update()
        {
            if (mySettings.locked) return;

            //Check if not local player
            if (!isLocalPlayer) return;

            //Get device
            var device = SteamVR_Controller.Input((int)trackedObj.index);

            if (device == null) return;
        //    ColorLineRenderer();

            //if grip and not holding then try to hold.
            if (device.GetTouchDown(SteamVR_Controller.ButtonMask.Grip))
            {
                RaycastHit hit;
                if (Physics.Raycast(startPos.position, startPos.forward, out hit, 500, layerMask))
                {
                    if (hit.collider.transform.tag.Contains("Player"))
                    {
                        var settings = hit.collider.gameObject.GetComponent<PlayerSettings>();
                        currentPlayer = hit.collider.gameObject;
                        if (settings != null)
                        {
                            settings.Toggle3DMarker(false);
                            ControlPlayer(settings);
                            UpdateStatsPanel(settings.playerName, settings.gameObject.GetComponent<PlayerStatsView>());
                         
                        }
                    }
                }

                /*
                if (currentPlayer == null)
                {
                    RaycastHit hit;
                    if (Physics.Raycast(startPos.position, startPos.forward, out hit, 500, layerMask))
                    {
                        if (hit.collider.transform.tag.Contains("Player"))
                        {
                            currentPlayer = hit.collider.transform.gameObject;
                            var settings = currentPlayer.GetComponent<PlayerSettings>();

                            if (settings != null)
                            {
                                //inform player Vr has control
                                ControlPlayer(settings);
                                //Update the setting panel with controlled player
                                UpdateStatsPanel(settings.playerName, settings.gameObject.GetComponent<PlayerStatsView>());
                            }
                        }
                    }
                }*/
            }

            //Check if holding and releasing - release player.
            if (device.GetTouchUp(SteamVR_Controller.ButtonMask.Grip))
            {
                if (currentPlayer != null)
                {
                    var settings = currentPlayer.GetComponent<PlayerSettings>();

                    if (settings != null)
                    {
                        ReleasePlayer(settings);
                        settings.Toggle3DMarker(true);
                    }

                    canvasGroup.alpha = 0;
                    currentPlayer = null;
                }
            }
        }

        private void ColorLineRenderer()
        {
            RaycastHit hit;

            if (Physics.Raycast(startPos.position, startPos.forward, out hit, 500, layerMask))
            {
             //   lineRenderer.SetPosition(1, hit.point);

                if (hit.collider.transform.tag.Contains("Player"))
                {
                    var settings = hit.collider.gameObject.GetComponent<PlayerSettings>();

                    if (settings != null)
                    {
                    //    var targetColor = (mySettings.teamName == settings.teamName) ? Color.red : Color.green;
                   //     lineRenderer.SetColors(targetColor, targetColor);
                    }
                }
                else
                {
               //     lineRenderer.SetColors(Color.gray, Color.gray);
                }
            }
            else
            {
                var endPos = startPos.position + (startPos.forward * 500);
             //   lineRenderer.SetPosition(1, endPos);
             //   lineRenderer.SetColors(Color.gray, Color.gray);
            }
        }

        //Get scale of object for holding.
        private Vector3 GetTargetScale()
        {
            return new Vector3(0.1f,0.1f,0.1f);
        }

        //Updates the stats panel on the remote.
        private void UpdateStatsPanel(string playerName, PlayerStatsView playerStatsView)
        {
            nameTxt.text = playerName;
            if (playerStatsView != null)
            {
                var targetSettings = playerStatsView.gameObject.GetComponent<PlayerSettings>();
                backgroundImage.sprite = backgroundSprites[targetSettings.teamName.ToLower() == "red" ? 0 : 1];
                portraitImage.sprite = targetSettings.portraitSprite;

                killsTxt.text = playerStatsView.myTotalKills.ToString() + " Kills";
                deathTxt.text = playerStatsView.myTotalDeaths.ToString() + " Deaths";
                killstreakTxt.text = playerStatsView.myKillStreak.ToString() + " Kill streak";
            }
            //Fade panel in
            canvasGroup.DOFade(1f, 0.25f);
        }

        //Take control of the player.
        private void ControlPlayer(PlayerSettings _settings)
        {
            _settings.HandleVRTakeOver();

            //Reposition player.
            currentPlayer.transform.SetParent(parent);
            currentPlayer.transform.localPosition = Vector3.zero;
            currentPlayer.transform.localRotation = Quaternion.Euler(-28.642f, -195.474f, 7.5f);
            currentPlayer.transform.localScale = GetTargetScale();
        }


        //Let go of the player.
        private void ReleasePlayer(PlayerSettings _settings)
        {
            if (_settings == null) return;

            currentPlayer.transform.SetParent(null);
            currentPlayer.transform.localScale = new Vector3(1, 1, 1);
            _settings.HandleVRRelease();
        }

    }
}
