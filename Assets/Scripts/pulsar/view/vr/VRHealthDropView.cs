﻿/*
 * VR health drop view
 * This class handles the dropping of health orbs from the Vr character for other players.
 */
using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using DG.Tweening;

namespace pulsar.view.vr
{
    public class VRHealthDropView : MonoBehaviour
    {
        //Configurable vars - vr settings, where to spawn etc.
        [SerializeField]
        private VRSettings settings;

        [SerializeField]
        private Transform spawnPoint;

        [SerializeField]
        private GameObject dummyHealthPickup;

        [SerializeField]
        private SteamVR_TrackedObject trackedObj;

        [SerializeField]
        private Image[] coolDownImages;

        //Remote canvas groups ( fading in and out )
        [SerializeField]
        private CanvasGroup[] groups;

        //cooldown time
        private float cooldown = 30;
        private float currentTime = 0;

        //is holding a health orb or not.
        private bool isHolding = false;
        private bool readyToUse = false;

        //current health orb
        private GameObject currentDummy;


        void Start()
        {
            //if local player then update remote images and fade canvas in.
            if (settings.isLocalPlayer)
            {
                UpdateImages();
                //Run cooldown timer.
                StartCoroutine(DoTimer());
                foreach (CanvasGroup g in groups) g.alpha = 1;
            }
        }

        //Cooldown timer method
        private IEnumerator DoTimer()
        {
            while (true)
            {
                yield return new WaitForSeconds(1);
                if (!readyToUse)
                {
                    currentTime++;
                    currentTime = Mathf.Clamp(currentTime, 0, cooldown);
                    //Work out if ability ready to use.
                    readyToUse = currentTime == cooldown;
                    //Update hud
                    UpdateImages();
                }
            }
        }

        void Update()
        {
            if (!settings.isLocalPlayer) return;

            //Checking if input can be read.
            if (!readyToUse) return;

            //Getting the device
            var device = SteamVR_Controller.Input((int)trackedObj.index);
            if (device == null) return;

            //Create orb and hold it
            if (!isHolding && device.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger))
            {
                isHolding = true;
                CreateDummy(); 
            }

            //Check if holder and we 'let go' of orb - then throw it.
            if (isHolding && device.GetTouchUp(SteamVR_Controller.ButtonMask.Trigger))
            {
                readyToUse = false;
                isHolding = false;
                if (currentDummy != null)
                {
                    currentDummy.transform.SetParent(null);
                    Rigidbody body = currentDummy.GetComponent<Rigidbody>();
                    body.useGravity = true;

                    //Work out velocity for throw.
                    var origin = trackedObj.origin ? trackedObj.origin : trackedObj.transform.parent;
                    if (origin != null)
                    {
                        body.velocity = origin.TransformVector(device.velocity*0.25f);
                        body.angularVelocity = origin.TransformVector(device.angularVelocity*0.25f);
                    }
                    else
                    {
                        body.velocity = device.velocity;
                        body.angularVelocity = device.angularVelocity;
                    }

                    body.maxAngularVelocity = body.angularVelocity.magnitude;
                    currentDummy.GetComponent<DummyHealthObjView>().Launch(DummyLanded);
                }
                currentTime = 0;
            }
        }

        //Get percent of cooldown left.
        private float GetPercent()
        {
            return currentTime / cooldown;
        }

        //Tween HUD 
        private void UpdateImages()
        {
            float percent = GetPercent();

            DOTween.To(() => coolDownImages[0].fillAmount, x => coolDownImages[0].fillAmount = x, percent, 0.95f).SetEase(Ease.Linear);
            DOTween.To(() => coolDownImages[1].fillAmount, x => coolDownImages[1].fillAmount = x, percent, 0.95f).SetEase(Ease.Linear);
            DOTween.To(() => coolDownImages[2].fillAmount, x => coolDownImages[2].fillAmount = x, percent, 0.95f).SetEase(Ease.Linear);
        }

        //Once orb has landed - spawn the health orb at it position for users to interact with.
        private void DummyLanded()
        {
            settings.SpawnHealth(currentDummy.transform.position);
            Destroy(currentDummy);
        }

        //Create dummy obj for Vr character to throw
        private void CreateDummy()
        {
            currentDummy = Instantiate(dummyHealthPickup);

            currentDummy.transform.SetParent(spawnPoint);
            currentDummy.transform.localPosition = Vector3.zero;
			currentDummy.transform.localScale = Vector3.one;
        }
    }
}