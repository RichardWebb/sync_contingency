﻿/*
 * Dummy health obj view
 * This class handles the health orb object and informs the listener when it has landed.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using System;

namespace pulsar.view.vr
{
    public class DummyHealthObjView : MonoBehaviour
    {
        private bool active = false;
        //call back for listner
        private Action currentCallback;

        public void Launch(Action callBack)
        {
            currentCallback = callBack;
            StartCoroutine(SetActive());
        }

        //Toggle active after a few frames.
        private IEnumerator SetActive()
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            active = true;
        }

        //On the trigger enter - orb has landed - inform listener
        void OnTriggerEnter(Collider col)
        {
            if (active)
            {
                active = false;
                if (currentCallback != null)
                    currentCallback();
            }
        }

    }
}