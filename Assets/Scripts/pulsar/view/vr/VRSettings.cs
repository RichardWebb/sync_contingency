﻿/*
 * Vr setting class
 * This class handles the settings and spawning of objects for the vr mode.
 */
using UnityEngine;
using UnityEngine.Networking;
using System;
using pulsar.events;
using Robotlegs.Bender.Extensions.EventManagement.API;
using Robotlegs.Bender.Extensions.Mediation.API;
using Robotlegs.Bender.Extensions.EventManagement.Impl;
using Robotlegs.Bender.Extensions.ViewManagement.API;
using System.IO;
using pulsar.view.interfaces;

namespace pulsar.view.vr
{
    public class VRSettings : NetworkBehaviour, ILobbyPlayer, IGameListener
    {
        //health orb 
        [SerializeField]
        private GameObject healthObj;

        //All players and their settings.
        private PlayerSettings[] players;

        //Robotlegs setup
        private IEventDispatcher _dispatcher = new EventDispatcher();

        public bool locked = true;

        [SyncVar(hook = "OnTeamSet")]
        public string teamName;

        public IEventDispatcher dispatcher
        {
            get
            {
                return _dispatcher;
            }
            set
            {
                _dispatcher = value;
            }
        }

        public event Action<IView> RemoveView;

        void Start()
        {
            if (isLocalPlayer)
            {
                GameObject obj = GameObject.Find("CountdownPanel");
                if (obj != null)
                {
                    obj.SetActive(false);
                }
            }
            //Hook mediators
            ViewNotifier.RegisterView(this);

            //Get details
            dispatcher.Dispatch(new UpdateDetailsEvent(UpdateDetailsEvent.Type.GET_USER_DETAILS));

            if (isLocalPlayer)
            {
                var splineView = GameObject.FindObjectOfType<SplineView>();

                if(splineView != null)
                    splineView.DestroyCutscene();
            }
        }

        public override void OnStartLocalPlayer()
        {
            base.OnStartLocalPlayer();
        }

        //Calls the server and marks a player.
        public void MarkPlayer(string name)
        {
            if (isLocalPlayer)
            {
                CmdMarkPlayer(name);
            }
        }

        //Update team details for vr character.
        public void HandleDetails(UpdateDetailsEvent evt)
        {
            if (isLocalPlayer)
                CmdUpdateTeam(evt.teamName);
        }


        //server side update team name
        [Command]
        private void CmdUpdateTeam(string val)
        {
            teamName = val;
        }

        //Client side update team anem
        private void OnTeamSet(string val)
        {
            teamName = val;
        }

        //server side mark a player.
        [Command]
        public void CmdMarkPlayer(string playerName)
        {
            //Get players if first time.
            if (players == null)
            {
                players = GameObject.FindObjectsOfType<PlayerSettings>();
            }

            //Go through all players and mark the one required
            foreach (PlayerSettings player in players)
            {
                if (player.teamName != teamName)
                {
                    if (player.gameObject.name == playerName)
                    {
                        player.SetMarked();
                    }
                }
            }
        }

        //Calls server to spawn health pack
        public void SpawnHealth(Vector3 position)
        {
            if (isLocalPlayer)
                CmdSpawnHealth(position);
        }

        //server side spawns a health orb
        [Command]
        private void CmdSpawnHealth(Vector3 pos)
        {
            var obj = Instantiate(healthObj);
            NetworkServer.Spawn(obj);

            obj.transform.position = pos;
        }

        void Update()
        {
            if (isLocalPlayer)
            {
                // Debugging  / position config 
                if (Input.GetKeyDown(KeyCode.Equals))
                {
                    transform.localScale = new Vector3(transform.localScale.x + 5, transform.localScale.y + 5, transform.localScale.z + 5);
                }
                if (Input.GetKeyDown(KeyCode.Minus))
                {
                    transform.localScale = new Vector3(transform.localScale.x - 5, transform.localScale.y - 5, transform.localScale.z - 5);
                }

                if (Input.GetKeyDown(KeyCode.DownArrow))
                {
                    var pos = transform.position;
                    transform.position = new Vector3(pos.x, pos.y - 5f, pos.z);
                }

                if (Input.GetKeyDown(KeyCode.UpArrow))
                {
                    var pos = transform.position;
                    transform.position = new Vector3(pos.x, pos.y + 5f, pos.z);
                }

                //Debugging 
                /*
                if (Input.GetKeyDown(KeyCode.L))
                {
                    string path = Application.persistentDataPath + "/pos.txt";

              

                    //var stream = File.Open(path, FileMode.Open);
                    var text = "Pos y" + transform.position.y + " :::: " +
                        "Scale x " + transform.localScale.x + " :::: " +
                        "Scale y " + transform.localScale.y + " :::: " +
                        "Scale z " + transform.localScale.z + " :::: ";

                    File.WriteAllText(path, text);
                    Debug.Log("Logging stats -----------------------------------------------");

                    Debug.Log("Pos y " + transform.position.y);

                    Debug.Log("Scale x " + transform.localScale.x);
                    Debug.Log("Scale y " + transform.localScale.y);
                    Debug.Log("Scale z " + transform.localScale.z);

                    Debug.Log("Finished stats -----------------------------------------------");
                    */
            
                }
            }

        public void HandleGameStart()
        {
            if (isLocalPlayer)
            {
                PlayerSettings[] players = GameObject.FindObjectsOfType<PlayerSettings>();
                foreach (PlayerSettings player in players)
                {
                    player.Toggle3DMarker(true);
                    player.gameObject.GetComponent<VRPlayerStats>().ShowStats();
                }
            }
        }

        public void HandleGameFinished()
        {
            if (isLocalPlayer)
                Application.Quit();
        }
    }
}
