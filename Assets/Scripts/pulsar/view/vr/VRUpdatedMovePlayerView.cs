﻿/*
 * Vr move player view
 * This class handles the updated moving of the vr character
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using Valve.VR;
using DG.Tweening;
using System;

namespace pulsar.view.vr
{
    public class VRUpdatedMovePlayerView : NetworkBehaviour
    {
        [SerializeField]
        private SteamVR_TrackedObject trackedObj;

        [SerializeField]
        private GameObject[] vrDomes;

        [SerializeField]
        private VRSettings mySettings;

        [SerializeField]
        private Renderer[] domeMats;
        private bool domeActive;

        [SerializeField]
        private LineRenderer lPointer;

        [SerializeField]
        private LayerMask mask;

        [SerializeField]
        private Transform startPos;
        private bool locked = false;

        private Transform aboveMapPoint;

        // Use this for initialization
        void Start()
        {
          /*  for(int i = 0; i < vrDomes.Length; i++)
            {
                domeMats[i] = vrDomes[i].GetComponent<Renderer>().material;
            }*/
            domeActive = true;
            aboveMapPoint = GameObject.FindGameObjectWithTag("VRSpawnPoint").transform;
            transform.position = aboveMapPoint.position;
        }

        // Update is called once per frame
        void Update()
        {
            //Check if not local player
            if (!isLocalPlayer) return;

            //Get device
            var device = SteamVR_Controller.Input((int)trackedObj.index);

            if (device == null) return;

            if (!domeActive)
            {
                if (device.GetPressDown(SteamVR_Controller.ButtonMask.Touchpad))
                {
                    var touchpad = device.GetAxis(EVRButtonId.k_EButton_SteamVR_Touchpad);

                    var y = touchpad.y;
                    var x = touchpad.x;

                    if (y > 0.5f)
                    {
                        locked = true;
                        FadeOrb(0f, HandleFadeIn);
                        domeActive = true;
                        mySettings.locked = true;
                        //Spawn orb
                    }
                }
            }
            else
            {
                if (locked) return;
                ColorLineRenderer();

                //ORB up - enable pick spot? 
                if (device.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger))
                {
                    RaycastHit hit;
                    domeActive = false;

                    if (Physics.Raycast(startPos.position, startPos.forward, out hit, 500, mask))
                    {
                        
                        FadeOrb(0f, () => 
                        {
                            transform.position = hit.point + (Vector3.up * 5);

                            FadeOrb(1f, () => 
                            {
                                locked = false;
                                domeActive = false;
                                mySettings.locked = false;
                            }, 0.25f);
                        });
                    }
                }

            }
        }

        private void HandleFadeIn()
        {
            transform.position = aboveMapPoint.position;
            locked = false;
            FadeOrb(1, () => { locked = false; }, 0.25f);
        }

        private void ColorLineRenderer()
        {
            RaycastHit hit;
            lPointer.SetPosition(0, startPos.position);

            if (Physics.Raycast(startPos.position, startPos.forward, out hit, 500, mask))
            {
                lPointer.SetPosition(1, hit.point);               
               
                lPointer.SetColors(Color.red, Color.red);
               
            }
            else
            {
                var endPos = startPos.position + (startPos.forward * 500);
                lPointer.SetPosition(1, endPos);
                lPointer.SetColors(Color.red, Color.red);
            }
        }

        private void FadeOrb(float endVal, Action callBack, float delay = 0)
        {
            float val = domeMats[0].material.GetFloat("_Dissolve");
            DOTween.To(() => val, _x => val = _x, endVal, 1f).SetDelay(delay).SetEase(Ease.Linear).OnUpdate(() =>
            {
                domeMats[0].material.SetFloat("_Dissolve", val);
            });

            float val2 = domeMats[1].material.GetFloat("_Dissolve");
            DOTween.To(() => val2, _x => val2 = _x, endVal, 1f).SetDelay(delay).SetEase(Ease.Linear).OnUpdate(() =>
            {
                domeMats[1].material.SetFloat("_Dissolve", val);
            }).OnComplete(() => 
            {
                if (callBack != null) callBack();
            });


        }

        private IEnumerator HandleCallBack(Action callBack)
        {
            yield return new WaitForSeconds(1);
            if (callBack != null) callBack();
        }
    }
}