﻿/*
 * Vr move player view
 * This class handles moving the player around the map 
 */

 using UnityEngine;
using System.Collections;
using Valve.VR;
using UnityEngine.Networking;

namespace pulsar.view.vr
{
    public class VRMovePlayerView : NetworkBehaviour
    {
        private VRSettings settings;

        [SerializeField]
        private SteamVR_TrackedObject trackedObj;

        private bool ready = false;

        private GameObject sp1;
        private GameObject sp2;
        private GameObject sp3;
        private GameObject sp4;

		private Vector3 defaultSpot = new Vector3(117.5f, -5.41f, -111.7f);

        private bool GetSpawnPoints()
        {
            sp1 = GameObject.Find("VR_SpawnPoint1");
            sp2 = GameObject.Find("VR_SpawnPoint2");
            sp3 = GameObject.Find("VR_SpawnPoint3");
            sp4 = GameObject.Find("VR_SpawnPoint4");

            return sp1 && sp2 && sp3 && sp4; 
        }

        // Use this for initialization
        void Start()
        {
            settings = GetComponent<VRSettings>();

            if (isLocalPlayer)
            {
                transform.position = defaultSpot;

                ready = GetSpawnPoints();
                if (sp1 != null)
                {
                    transform.position = sp1.transform.position;
                }
                else
                {
                    transform.position = defaultSpot;
                }


             //   ready = true;
            }
        }

        public override void OnStartLocalPlayer()
        {
            base.OnStartLocalPlayer();
			//transform.position = defaultSpot;
			//StartCoroutine (GetSpawns ());
            
         //   ready = true;
        }

		private IEnumerator GetSpawns()
		{
			yield return new WaitForSeconds (1);
			 
			GetSpawnPoints();
			if (sp1 != null) {
				transform.position = sp1.transform.position;
			}
			else
			{
				transform.position = defaultSpot;
			}

		}

        // Update is called once per frame
        void FixedUpdate()
        {
            if (!ready) return;

            if (!settings.isLocalPlayer) return;

            var device = SteamVR_Controller.Input((int)trackedObj.index);

            if (device == null) return;

            if (device.GetTouchDown(SteamVR_Controller.ButtonMask.Touchpad))
            {
                var touchpad = device.GetAxis(EVRButtonId.k_EButton_SteamVR_Touchpad);

                var y = touchpad.y;
                var x = touchpad.x;

                if (y < -0.5f)
                {
					if (sp1 != null) {
						transform.position = sp1.transform.position;
					}
					else
					{
						transform.position = defaultSpot;
					}

                   
                }
                else if (y > 0.5f)
                {
					if (sp2 != null) {
						transform.position = sp2.transform.position;
					}
					else
					{
						transform.position = defaultSpot;
					}
                }
                else if (x < -0.5f)
                {
					if (sp3 != null) {
						transform.position = sp3.transform.position;
					}
					else
					{
						transform.position = defaultSpot;
					}
                }
                else if (x > 0.5f)
                {
					if (sp4 != null) {
						transform.position = sp4.transform.position;
					}
					else
					{
						transform.position = defaultSpot;
					}
                }
            }
        }
    }
}