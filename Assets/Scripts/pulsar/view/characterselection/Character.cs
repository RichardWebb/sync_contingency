﻿/*
 * Character class
 * This class is linked with the character selection view, contains details for each selectable character.
 * Created by Richard Webb, s1308033
 */

using UnityEngine;
using UnityEngine.UI;
using pulsar.events;
using Robotlegs.Bender.Platforms.Unity.Extensions.Mediation.Impl;

namespace pulsar.view.characterselection
{
    public class Character : EventView
    {
        [SerializeField]
        private GameObject infoPanel;

        //Exposed vars 
        public GameObject MyPortrait { private get; set; }
        public string CharacterName;
        public string CharacterDescription { get; set; }
        public GameObject characterModel { get; set; }
        public int MyIndex;
        public bool CurrentlySelected { get; set; }
        public CharacterSelectionView characterSelectionView;

        private Button myButton;
        private bool gotDetails = false;

        public void Init()
        {
            base.Start();
            //Get characters details straight away
            dispatcher.Dispatch(new CharacterDetailsEvent(CharacterDetailsEvent.Type.GET_CHARACTER_DETAILS, CharacterName));

            GetComponent<Button>().onClick.AddListener(() => SetCharacterSelected());
        }

        protected override void Start()
        {
        }

        //Set character selection - display details.
        private void SetCharacterSelected()
        {
            //Check if selected
            if (!CurrentlySelected)
            {
                characterSelectionView.SetNewCharacterSelected(MyIndex);
                characterSelectionView.UpdateInfoPanel(infoPanel);
            }
        }

        //Method to get character details
        public void GetMyDetails()
        {
            gotDetails = true;
            //Getting details based on character name
            dispatcher.Dispatch(new CharacterDetailsEvent(CharacterDetailsEvent.Type.GET_CHARACTER_DETAILS, CharacterName));
        }

        //Got details sent back 
        public void GotCharacterDetails(CharacterDetailsEvent evt)
        { 
            //making sure the names match
            if (evt.name.ToLower().Equals(CharacterName.ToLower()))
            {
                //Update details.
                CharacterName = evt.name;
                CharacterDescription = evt.tagLine;
            }
        }
    }

}