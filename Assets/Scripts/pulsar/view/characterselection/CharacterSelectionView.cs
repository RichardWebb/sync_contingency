﻿///
/// This is the character selection view, this class allows the user to select a character, this is linked with the lobby manager and will update on a server when a user select a character.
/// Created by Richard Webb, s1308033
///

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.Networking;
using UnityStandardAssets.Network;
using pulsar.view.mono;

namespace pulsar.view.characterselection
{
    public class CharacterSelectionView : MonoBehaviour
    {
        [SerializeField]
        private Text characterTitle;

        [SerializeField]
        private Text characterDescription;

        [SerializeField]
        private RectTransform characterPortraitGroup;

        [SerializeField]
        private Button selectCharacterBtn;

        [SerializeField]
        private Camera sceneCam;

        [SerializeField]
        private CanvasGroup cGroup;

        [SerializeField]
        private Button chooseACharacterBtn;

        [SerializeField]
        private GameObject[] characters;

        [SerializeField]
        private Character[] charactersList;

        private GameObject currentInfoPanel;
        [SerializeField]
        private AudioSource source;

        private int currentIndex = 0;

        private bool isReady;
        public bool displaying = false;
        private GameObject charactersHolder;

        private LobbyFaderView lobbyFader;
        private Tweener currentTween;
        private Tweener currentCanvasFade;

        public delegate void MenuToggledDelegate();
        public event MenuToggledDelegate MenuToggled;

        /// <summary>
        /// Entry point of the character selection, this method will create all the characters.
        /// </summary>
        public void Init()
        {
            selectCharacterBtn.onClick.AddListener(() => SelectCharacter());
            selectCharacterBtn.enabled = false;
            selectCharacterBtn.gameObject.SetActive(false);
            //Hook button click
            chooseACharacterBtn.onClick.AddListener(() => 
            {
                source.Play();
                chooseACharacterBtn.gameObject.SetActive(false);

                ToggleCharacterSelection(true);
            });

            //Create character selection camera.
            GameObject cam = new GameObject("SceneCam");

            sceneCam = cam.AddComponent<Camera>();
            sceneCam.depth = 2;
            sceneCam.clearFlags = CameraClearFlags.Depth;
            cam.transform.position = new Vector3(0.2f, 0.64f, 0);
            sceneCam.enabled = false;
            SetUpCharacterHolder(); 

            GameObject obj = LobbyManager.s_Singleton.gameObject;

            lobbyFader = obj.transform.FindChild("LobbyPanel").GetComponent<LobbyFaderView>();

            //reset the info text
            characterTitle.text = "";
            characterDescription.text = "Choose a character";
            //sceneCam.enabled = false;
        }

        void Start()
        {
        }

        void Update()
        {
            if (isReady)
            {
                if (!displaying)
                {
                    if (Input.GetKeyDown(KeyCode.H))
                    {
                        chooseACharacterBtn.gameObject.SetActive(false);

                        //  if (!displaying)
                        ToggleCharacterSelection(true);
                    }
                }
            }
        }

        private void ToggleCharacterSelection(bool _displaying)
        {
            if (MenuToggled != null)
            {
                MenuToggled();
            }
            displaying = _displaying;
            cGroup.gameObject.SetActive(_displaying);

            if (currentCanvasFade != null)
            {
                currentCanvasFade.Kill(true);
            }

            //Fade canvas based on displaying
            currentCanvasFade = cGroup.DOFade(displaying ? 1f : 0f, 0.5f).OnComplete(()=> 
            {
                cGroup.interactable = displaying;
                chooseACharacterBtn.enabled = !displaying;
                selectCharacterBtn.enabled = displaying;
            });

            //Toggle lobby based on displaying
            if (displaying)
            {
                lobbyFader.FadeLobby();
            }
            else
            {
                lobbyFader.ShowLobby();

            }

            UpdateCharacterPosition(displaying ? new Vector3(1,-0.35f,2.25f) : new Vector3(0.8f,0.25f,3.15f));
          //  sceneCam.enabled = true;
        }

        //Update character position when in character selection.
        private void UpdateCharacterPosition(Vector3 pos)
        {
            if (charactersHolder != null)
            {
                charactersHolder.transform.DOLocalMove(pos, 0.5f).SetEase(Ease.Linear);
            }
        }


        /// <summary>
        /// Selecting the character, will update the server with the users choice.
        /// </summary>
        private void SelectCharacter()
        {
            source.Play();
            chooseACharacterBtn.gameObject.SetActive(true);

            ToggleCharacterSelection(false);

            string name = charactersList[currentIndex].CharacterName;
            string _name = name;

            LobbyPlayer[] lobbyPlayers = GameObject.FindObjectsOfType<LobbyPlayer>();

            foreach (LobbyPlayer player in lobbyPlayers)
            {
                if (player.isLocalPlayer)
                {
                    switch (player.playerName.ToLower())
                    {
                        case "dev_ric-hard":
                        case "dev_rich":
                        case "dev_roidlaar":
                        case "dev_pughballs":
                        case "dev_djs150":
                        case "dev_fudgebrownie":
                        case "dev_marler":
                        case "dev_jacob":
                        case "dev_ethan":
                        case "dev_cabostock":


                            _name = "DEV_" + _name;
                            break; 
                    }
                    player.UpdateCharacterForPlayer(_name);
                }
            }

            LobbyManager.s_Singleton.gamePlayerPrefab = (GameObject)Resources.Load("PlayableCharacters/" + _name);//name);
    
            chooseACharacterBtn.GetComponentInChildren<Text>().text = "Change character (h)";
            //fade canvas out
            currentTween = cGroup.DOFade(0f, 0.5f).OnComplete(() =>
            {
                cGroup.interactable = false;
                displaying = false;
                currentTween = cGroup.DOFade(displaying ? 1f : 0f, 0.5f);

            });

        }
        /// <summary>
        /// Creating the characters holder and starting the character creating process.
        /// </summary>
        private void SetUpCharacterHolder()
        {
            charactersHolder = new GameObject("Character_Container");
            charactersHolder.transform.SetParent(sceneCam.gameObject.transform);
            charactersHolder.transform.localPosition = new Vector3(0.8f, 0.25f, 3.15f);

            CreateCharacters(charactersHolder.transform);
        }


        /// <summary>
        /// Loading all the characters from resources and getting all data known about those characters to display to the user.
        /// </summary>
        /// <param name="parent"></param>
        private void CreateCharacters(Transform parent)
        {
            int index = 0;
            foreach (GameObject character in characters)
            {
                GameObject currentCharacter = Instantiate<GameObject>(character);
                currentCharacter.transform.SetParent(parent);
                currentCharacter.transform.localPosition = Vector3.zero;
                currentCharacter.SetActive(false);
                charactersList[index].characterModel = currentCharacter;
                index++;
            }

            //Init the characters.
            foreach (Character character in charactersList)
            {
                character.Init();
            }

            characterPortraitGroup.DOLocalMoveY(-333, 0.75f).SetEase(Ease.OutExpo).SetDelay(0.25f);
            charactersList[currentIndex].GetMyDetails();

            isReady = true;

        }

        /// <summary>
        /// Display current selected character.
        /// </summary>
        /// <param name="index"></param>
        public void SetNewCharacterSelected(int index)
        {
            if (!sceneCam.enabled)
                sceneCam.enabled = true;

            selectCharacterBtn.enabled = true;
            selectCharacterBtn.gameObject.SetActive(true);
                 
            Character currentSelectedCharacter = charactersList[currentIndex];
            currentSelectedCharacter.characterModel.SetActive(false);
            currentSelectedCharacter.CurrentlySelected = false;

            Character newCharacter = charactersList[index];

            newCharacter.characterModel.SetActive(true);
            newCharacter.CurrentlySelected = true;

            currentIndex = index;
            characterTitle.text = newCharacter.CharacterName.UpperCaseFirstCharacter();
            characterDescription.text = newCharacter.CharacterDescription;

        }

        public void UpdateInfoPanel(GameObject obj)
        {
            if (currentInfoPanel != null) currentInfoPanel.SetActive(false);
            currentInfoPanel = obj;
            currentInfoPanel.SetActive(true);
        }

    }

}
