﻿/*
 * Player stats view 
 * This script handles the stats of the player from kills, deaths and kill streaks.
 * Created by Richard Webb, s1308033
 */

using UnityEngine.Networking;
using System;
using Robotlegs.Bender.Extensions.Mediation.API;
using Robotlegs.Bender.Extensions.EventManagement.API;
using Robotlegs.Bender.Extensions.EventManagement.Impl;
using Robotlegs.Bender.Extensions.ViewManagement.API;
using pulsar.view.interfaces;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace pulsar.view
{
    public class PlayerStatsView : NetworkBehaviour, IScoreDispatcher
    {
        //Robotlegs setup.
        public event Action<IView> RemoveView;
        private IEventDispatcher _dispatcher = new EventDispatcher();
        public IEventDispatcher dispatcher
        {
            get
            {
                return _dispatcher;
            }
            set
            {
                _dispatcher = value;
            }
        }

        //network vars - kills, deaths, streaks.
        [SyncVar(hook = "OnKillCountChanged")]
        public int myTotalKills = 0;

        [SyncVar(hook = "OnDeathCountChanged")]
        public int myTotalDeaths = 0;

        [SyncVar(hook = "OnHighKillStreakChanged")]
        public int myKillStreak = 0;


        [SerializeField]
        private Text killStreakText;

        void Start()
        {
            //Hook mediators.
            ViewNotifier.RegisterView(this);
        }

        //Update on server the kill count 
        public void UpdateKillCount()
        {
            CmdUpdateKillCount();
        }

        //Update on the server if player has died / increment count.
        public void UpdateMyDeath()
        {
            CmdUpdateDeathCount();
        }

        /// Called on server no need for command calls
        public void UpdateKillCount(int amount)
        {
            myKillStreak += amount;
            myTotalKills += amount;
        }


        //server side update player death count
        [Command]
        private void CmdUpdateDeathCount()
        {
            myTotalDeaths++;
            myKillStreak = 0;
        }

        //Server side update total kills.
        [Command]
        private void CmdUpdateKillCount()
        {
         //   myTotalKills++;      
        }

        [Command]
        private void CmdUpdateKillStreak(int amount)
        {
            myKillStreak = amount;
        }

        //Client side update kil count
        [Client]
        private void OnKillCountChanged(int value)
        {
            
            myTotalKills = value;
		    
            //dispatch kill count updated
        }

        //Client side update death count 
        [Client]
        private void OnDeathCountChanged(int value)
        {
            myTotalDeaths = value;

        }

        //client side update kill streak count
        [Client]
        private void OnHighKillStreakChanged(int value)
        {
            myKillStreak = value;
            if (isLocalPlayer)
            {
                if (myKillStreak > 0)
                    if (myKillStreak % 5 == 0)
                    {
                        killStreakText.text = myKillStreak.ToString() + " kill streak!";
                        StartCoroutine(HideKillStreakFun());
                    }
            }
        }

        private IEnumerator HideKillStreakFun()
        {
            yield return new WaitForSeconds(1.5f);
            killStreakText.text = "";
        }
    }
}
