﻿/*
 * Lobby player interface 
 * This interface is used to hook mediators to the lobby players - passes users details around.
 * Created by Richard Webb, s1308033
 */
using pulsar.events;
using Robotlegs.Bender.Extensions.Mediation.API;

namespace pulsar.view
{
    public interface ILobbyPlayer : IEventView
    {
        void HandleDetails(UpdateDetailsEvent evt);
    }

}
