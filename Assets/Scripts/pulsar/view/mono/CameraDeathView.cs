﻿/*
 * Camera death view
 * This class handles the players death animation
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using DG.Tweening;

namespace pulsar.view.mono
{ 
    public class CameraDeathView : MonoBehaviour
    {
        [SerializeField]
        private Transform target;

        [SerializeField]
        private Vector3 deathPos;

        private Vector3 myPos;
        private Quaternion myRot;

        private bool playingDeath = false;

        [SerializeField]
        private Camera mainCamera;

        [SerializeField]
        private Camera myCamera;

        void Start ()
        {
            myCamera = GetComponent<Camera>();
            //Remember start local position
            myPos = transform.localPosition;
            myRot = transform.localRotation;
	    }
        
        void Update()
        {
            //If running death animation look at corpse
            if (playingDeath)
            {
                myCamera.transform.LookAt(target);
            }
        }

        //Player death animation 
        public void PlayDeath()
        {
            mainCamera.enabled = false;
            myCamera.enabled = true;
            playingDeath = true;
            //Tween camera out
            transform.DOLocalMove(deathPos, 3f).SetEase(Ease.Linear);
        }

        //Resets the camera back to starting point.
        public void ResetCamera()
        {
            myCamera.enabled = false;

            if (mainCamera == null)
            {
                mainCamera = PlayerSettings.localPlayerSettings.myCamera;
            }

            if(mainCamera != null)
                mainCamera.enabled = true;

            playingDeath = false;

            transform.localPosition = myPos;
            //Reset rotation.
            transform.rotation = Quaternion.Euler(Vector3.zero);
        }

    }
}
