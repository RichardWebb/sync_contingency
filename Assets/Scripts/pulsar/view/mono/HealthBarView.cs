﻿/*
 * Health bar view
 * This class handles the visual health bar when a player mouses over a target
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using pulsar.networksync;
using System;

namespace pulsar.view.mono
{
    public class HealthBarView : MonoBehaviour
    {
        [SerializeField]
        private Image[] hexImages;

        [SerializeField]
        private CanvasGroup cGroup;

        [SerializeField]
        private GameObject[] healthBarBackgroundImages;

        private float interval;
        private Player_Health healthView;
        private float healthPercent;

        [SerializeField]
        private Color[] healthColors;

        [SerializeField]
        private Image portraitImage;

        void Start()
        {
        }

        public void UpdateHealthBar(bool isFriendly, Player_Health _healthView)
        {

            healthView = _healthView;
            interval = (healthView.maxHealth / hexImages.Length);

            portraitImage.sprite = healthView.gameObject.GetComponent<PlayerSettings>().portraitSprite;
            healthPercent = (healthView.health / healthView.maxHealth) * 100;

            cGroup.alpha = 1;

            //Change colours
            healthBarBackgroundImages[isFriendly ? 0 : 1].SetActive(true);
            healthBarBackgroundImages[isFriendly ? 1 : 0].SetActive(false);

            foreach(Image i in hexImages)
            {
                i.color = healthColors[isFriendly ? 0 : 1];
            }

            UpdateHealth();        
        }

        private void UpdateHealth()
        {
            var h = healthView.health;

            if (h < 0) h = 0;

            int currentHex = Mathf.Clamp(Mathf.FloorToInt((h/ interval)), 0, 5);
            var hexSize = (h - (currentHex * interval)) / interval;
            hexImages[currentHex].rectTransform.localScale = new Vector3(hexSize, hexSize, hexSize);

            for (int i = 0; i < hexImages.Length; i++)
            {
                if (i > currentHex)
                {
                    hexImages[i].rectTransform.localScale = new Vector3(0, 0, 0);
                }

                else if (i < currentHex)
                {
                    hexImages[i].rectTransform.localScale = new Vector3(1, 1, 1);

                }

                if (h == 100)
                {
                    hexImages[i].rectTransform.localScale = new Vector3(1, 1, 1);
                }
            }
        }

        void Update()
        {
            if (healthView != null)
            {
                UpdateHealth();
            }
        }

        public void DisableView()
        {
            cGroup.alpha = 0;
            healthView = null;
        }

    }
}