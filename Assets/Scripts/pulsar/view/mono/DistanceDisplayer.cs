﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

[ExecuteInEditMode]
public class DistanceDisplayer : MonoBehaviour {

    [SerializeField]
    private float dis = 25f;

	// Use this for initialization
	void Start () {
      //  transform.DOLocalRotate(new Vector3(transform.localRotation.x, transform.localRotation.y, 270), 0.5f).SetEase(Ease.Linear);//.SetLoops(-1, LoopType.Incremental);
    }
	
	// Update is called once per frame
	void Update ()
    {
        Debug.DrawLine(transform.position, transform.position + (transform.forward * dis), Color.red);
        transform.Rotate(new Vector3(0, 0, 5));
	}
}
