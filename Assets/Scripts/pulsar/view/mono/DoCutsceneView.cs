﻿/************************************************************
 * Do Custscene View
 * Class that handles the initialisation of cutscenes within the level
 * 
 * Created By: Daniel O'Neill s1303529 & Richard Webb s1308033
************************************************************/
using pulsar.networksync;
using pulsar.view;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoCutsceneView : MonoBehaviour {

    [SerializeField]
    Camera _CutsceneCamera;

    private bool enableCam = true;

    //used to call cutscenes
	public void DoCutscene(SplineView _spln)
    {
		StartCoroutine(StartCutscene(_spln));

        GameObject obj = GameObject.Find("CountdownPanel");
        if (obj != null)
        {
            obj.SetActive(false);
        }
    }
		
		  
	IEnumerator StartCutscene(SplineView _spln){
		yield return new WaitForEndOfFrame();
		_CutsceneCamera.enabled = true;
		_spln.StartMove(EndCutscene, _CutsceneCamera.gameObject);
    }

	public void EndCutscene(){
        StartCoroutine(DestroyCut());
    }

    IEnumerator DestroyCut()
    {
        yield return new WaitForSeconds(3);
        _CutsceneCamera.enabled = false;

        if (enableCam)
        {
            if (PlayerSettings.localPlayerSettings != null)
                PlayerSettings.localPlayerSettings.gameObject.GetComponent<Player_NetworkSetup>().EnableCamera();
            enableCam = false;
        }
    }

}
