﻿/*
 * Item animation view
 * This class handles a basic animation of an object.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using DG.Tweening;

namespace pulsar.view.mono
{
    public class ItemAnimationView : MonoBehaviour
    {
        [SerializeField]
        private Vector3 targetPosition;

        [SerializeField]
        private float duration = 5;

        [SerializeField]
        private float delay = 0;

        [SerializeField]
        private Ease easing = Ease.Linear;

        void Start ()
        {
            //Tween object to point 
            transform.DOLocalMove(targetPosition, duration).SetEase(easing).SetDelay(delay);
	    }
    }
}
