﻿/*
 * Lobby fader view
 * This class handles the lobby fading from character selection to lobby.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using DG.Tweening;

namespace pulsar.view.mono
{
    public class LobbyFaderView : MonoBehaviour
    {
        [SerializeField]
        private CanvasGroup[] canvasGroups;

        //Fade the lobby out.
        public void FadeLobby()
        {
            FadeCanvas(0f);
        }

        //Fade the lobby back in 
        public void ShowLobby()
        {
            FadeCanvas(1f);
        }

        //Fade the canvas group to an amount.
        private void FadeCanvas(float v)
        {
            foreach (CanvasGroup cGroup in canvasGroups)
            {
                cGroup.DOFade(v, 0.5f).SetEase(Ease.Linear);
            }
        }
    }
}

