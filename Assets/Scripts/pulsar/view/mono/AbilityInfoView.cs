﻿/*
 * Ability Info View
 * This class handles the display of ability information within character select.
 * Created by Daniel O'Neill + Richard  Webb, s1303529 + s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

namespace pulsar.view.mono
{
    public class AbilityInfoView : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {


        [SerializeField]
        private Text infoText;

        [SerializeField]
        private string abilityInfo;

        void Start()
        {
            var split = abilityInfo.Split('#');
            var result = "";
            foreach (string s in split)
            {
                result += s + "\n";
            }

            abilityInfo = result;
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            infoText.text = abilityInfo;
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            infoText.text = "";
        }
    }
}