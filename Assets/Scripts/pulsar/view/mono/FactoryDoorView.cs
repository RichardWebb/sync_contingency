﻿/*
 * Factory door view
 * This class handles the opening and closing of the factory doors
 * Created by Richard Webb,s1308033
 */
using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using DG.Tweening;

namespace pulsar.view.mono
{
    public class FactoryDoorView : MonoBehaviour
    {
        private bool isOpen;
        private List<string> players = new List<string>();

        [SerializeField]
        private GameObject[] doors;

        // Use this for initialization
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        void OnTriggerEnter(Collider col)
        {
            if (col.tag.Contains("Player"))
            {
                if (!players.Contains(col.gameObject.name))
                {
                    players.Add(col.gameObject.name);
                }

                Open();
            }
        }

        private void Open()
        {
            if (!isOpen)
            {
                isOpen = true;
                doors[0].transform.DOLocalMoveX(-2, 0.25f).SetEase(Ease.Linear);
                doors[1].transform.DOLocalMoveX(2, 0.25f).SetEase(Ease.Linear);
                //Open the door
            }
        }

        void OnTriggerExit(Collider col)
        {
            if (col.tag.Contains("Player"))
            {
                if (players.Contains(col.gameObject.name))
                {
                    players.Remove(col.gameObject.name);
                }

                if (players.Count == 0)
                {
                    Close();
                }
            }
        }

        private void Close()
        {
            //Close the door!
            if (isOpen)
            {
                isOpen = false;
                doors[0].transform.DOLocalMoveX(0, 0.25f).SetEase(Ease.Linear);
                doors[1].transform.DOLocalMoveX(0, 0.25f).SetEase(Ease.Linear);

            }

        }
    }
}