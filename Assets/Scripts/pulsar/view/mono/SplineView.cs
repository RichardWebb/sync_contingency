﻿/************************************************************
 * Spline View
 * Class that handles smooth spline movement for cutscene cameras
 * 
 * Created By: Daniel O'Neill s1303529 & Richard Webb s1308033
************************************************************/
using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;
using System;
#if UNITY_EDITOR
using UnityEditor;
#endif


[ExecuteInEditMode]
public class SplineView : MonoBehaviour
{

    public Transform[] _points;

    [SerializeField]
    float _speed;

    [SerializeField]
    bool _lookFocus;

    [SerializeField]
    Transform _camTarget;

    [HideInInspector]
    public GameObject _cutCam;

    float _time;

    int _current = 0;

    bool _moving;
    float t;
    [SerializeField]
    DoCutsceneView _cutscene;
    //Transform _camTargetA;
    private Action callBack;

    [SerializeField]
    private Transform camTargetA;

    private bool _camMov = false;

    void Start() {
    }

    public void DOCutScene()
    {
        _cutscene.DoCutscene(this);
    }

    public void DestroyCutscene()
    {
        Destroy(_cutscene.gameObject);
        Destroy(this.gameObject);
    }

    public void StartMove(Action _callBack, GameObject _cam)
    {
        if (_callBack != null)
            this.callBack = _callBack;

        _cutCam = _cam;
        camTargetA.position = _points[0].position;
        _cutCam.transform.position = _points[0].position;
        _time = ((_points[1].transform.position - _points[0].transform.position).magnitude) / _speed;
        GotoPoint(1);
        _moving = true;
        _camMov = true;
    }


    //_moving object through spline
    void FixedUpdate()
    {

        if (_moving)
        {

            if (_lookFocus)
            {
                _cutCam.transform.LookAt(_camTarget.position);
            }
            else
            {
                _cutCam.transform.LookAt(camTargetA.position);
            }
            _cutCam.transform.position = Vector3.Lerp(transform.position, camTargetA.position, Time.deltaTime * 1.1f);

            if (!_camMov && _cutCam.transform.position == camTargetA.position)
            {
                _moving = false;
           
            }

        }
    }

    private void GotoPoint(int i) {
        camTargetA.DOMove(_points[i].position, _time).SetEase(Ease.Linear).OnComplete(() =>
        {
            i++;
            if (i <= _points.Length - 1) {
                _time = ((_points[i].transform.position - _points[i - 1].transform.position).magnitude) / _speed;
                GotoPoint(i);
            }

            else
            {
                _camMov = false;
                if (callBack != null)
                {
                    callBack();
                }
            }

        });
    }

}
#if UNITY_EDITOR

    [CustomEditor(typeof(SplineView))]
public class DrawLine : Editor
{

     public void OnSceneGUI()
    {
			
        var path = target as SplineView;
        if (path._points.Length >= 2)
        {

            for (int i = 0; i < path._points.Length - 1; i++)
            {
                Handles.color = Color.magenta;
                Handles.DrawLine(path._points[i].position, path._points[i + 1].position);

            }
        }
    }
}

#endif


