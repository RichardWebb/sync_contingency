using UnityEngine;
using System.Collections;

namespace pulsar.view.mono
{
    public class AudioPlayerView : MonoBehaviour
    {
		[SerializeField]
		private AudioSource source;

		public void PlayClip(AudioClip clip, bool use2D = true, float audioVolume = 1f)
		{
            source.spatialBlend = use2D ? 0 : 1;

			source.clip = clip;
			source.Play ();
		}
    }
}