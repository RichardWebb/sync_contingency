/************************************************************************************
Audio Controller View

class handles audio in the scene and can cycle through sets of audio or play set
clip number

created by: Daniel O'Neill 25/04/2017
*************************************************************************************/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace pulsar.view.mono
{
    public class AudioControlView : MonoBehaviour
    {

        [SerializeField]
        private AudioSource[] source;

        [SerializeField]
        private AudioClip[] clips;

        public void playClip(int i)
        {
            foreach (AudioSource a in source)
            {
                a.clip = clips[i];
                a.Play();
            }
        }
    }
}