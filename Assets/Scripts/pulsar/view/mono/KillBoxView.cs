﻿/*
 * Kill Box View
 * THis class handles respawning a player
 * Created by Richard Webb s1308033, Dan O' Neill s1303529
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using pulsar.networksync;
using Robotlegs.Bender.Platforms.Unity.Extensions.Mediation.Impl;
using pulsar.view.interfaces;
using System;

namespace pulsar.view.mono
{
    public class KillBoxView : EventView, IGameListener
    {
        private bool ready;

        public void HandleGameFinished()
        {
            ready = false;
        }

        public void HandleGameStart()
        {
            ready = true;
        }

        protected override void Start()
        {
            base.Start();
        }

        void OnTriggerEnter(Collider col)
        {
            if (!ready) return;
            if (col.gameObject.tag.Contains("Player"))
            {
                var settings = col.gameObject.GetComponent<PlayerSettings>();
                if (settings == null) return;
                 if (settings.isLocalPlayer)
                {
                    var health = col.gameObject.GetComponent<Player_Health>();
                    if (health.health > 0)
                        health.CmdUpdateHealth(0);
                }
            }
        }
    }
}