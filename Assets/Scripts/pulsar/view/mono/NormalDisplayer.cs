﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class NormalDisplayer : MonoBehaviour
{

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        Debug.DrawLine(transform.position, transform.position + -transform.forward * 50f, Color.red);
	}
}
