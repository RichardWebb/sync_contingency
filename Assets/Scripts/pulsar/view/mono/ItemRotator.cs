﻿/*
 * Item rotator class
 * This simple class rotates and item at a speed.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;

namespace pulsar.view.mono
{
    public class ItemRotator : MonoBehaviour
    {
        [SerializeField]
        [Range(-10f, 10f)]
        private float _speed;

        [SerializeField]
        private Vector3 rotation;

        void Start()
        {
            rotation = new Vector3(_speed * rotation.x, _speed * rotation.y, _speed * rotation.z);
        }

        void FixedUpdate()
        {
            //rotate item.
            transform.Rotate(rotation);
        }
    }
}