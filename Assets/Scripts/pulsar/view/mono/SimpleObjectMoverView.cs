﻿/*
 * Simple object mover
 * This script handles the simple movement of an object
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using DG.Tweening;
using System;

namespace pulsar.view.mono
{
    public class SimpleObjectMoverView : MonoBehaviour
    {
        [SerializeField]
        private Transform endPoint;

        [SerializeField]
        private float duration = 5;

        [SerializeField]
        private bool destroyAtEnd = true;

        [SerializeField]
        private Vector3[] targets;

	    // Use this for initialization
	    void Start ()
        {
            transform.DOMove(endPoint.position, duration).OnComplete(() => 
            {
                if (destroyAtEnd) Destroy(this.gameObject);
            });

          //  GoToPoint(0);
	    }
        /*
        private void GoToPoint(int index)
        {
            if(index < targets.Length)
                GoToPoint(GoToPoint, index);
        }

        private void GoToPoint(Action<int> callback, int index)
        {
            transform.DOMove(targets[index], duration).OnComplete(() =>
            {
                if (callback != null)
                {
                    callback(++index);
                }
            }).OnUpdate(()=> 
            {
                //Look at point
            });
        }
        */
    }
}
