﻿/***************************************************
 * Waypoint View
 * 
 * This handles the placement of the UI objective markers in world space
 * Includes looking at camera and scaling based on distance
 * 
 * Created By: Dan O'Neill s1303529 02/03/17
 * ************************************************/

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Robotlegs.Bender.Platforms.Unity.Extensions.Mediation.Impl;
using pulsar.view.interfaces;
using System;
using pulsar.view;

public class WaypointView : EventView, IGameListener
{

	private RectTransform _rect;
	private Camera _main;

	[SerializeField]
	bool _scaleRelative;

	[SerializeField]
	private bool World2UI;
	[SerializeField]
	private GameObject objectTrack;
    private bool started;


    // Use this for initialization
    protected override void Start ()
    {
        base.Start();
	
	}
	
	// Update is called once per frame
	void Update () {
        // if (!started) return;

        if (_main == null)
        {
            _main = Camera.main;
            if (_main == null) return;
        }

		//lets do distance stuff
		if (!World2UI) {

			Vector3 target = new Vector3 (_main.transform.position.x, transform.position.y, _main.transform.position.z);
			transform.LookAt (target);

			if (_scaleRelative) {
				float _dist = (_main.transform.position - transform.position).magnitude;
				if (_dist > 20) {
					float scaleSize = Mathf.Tan (0.05f) * _dist;
					transform.localScale = new Vector3 (scaleSize, scaleSize, scaleSize);
				} else {
					transform.localScale = Vector3.one;
				}
			}
		} 

		else {
			if (_scaleRelative) {
				float _dist = (_main.transform.position - objectTrack.transform.position).magnitude;
				if (_dist > 20) {
					float scaleSize = Mathf.Tan (0.05f) * _dist;
					scaleSize = 2 / scaleSize;
					_rect.localScale = new Vector3 (scaleSize, scaleSize, scaleSize);
				} else {
					transform.localScale = 2 * Vector3.one;
				}
			}

		}

	}

    public void HandleGameStart()
    {
        if (PlayerSettings.localPlayerSettings != null)
            _main = PlayerSettings.localPlayerSettings.myCamera;
        else
        {
            _main = Camera.main;
        }

        if (World2UI)
        {
            _rect = GetComponent<RectTransform>();
        }

        started = true;
    }


    public void HandleGameFinished()
    {
    }
}

