﻿/*
 * XMlParser
 * dfjg
 * Created by Ethan Gill, s1501083
 */
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;

public class PlayerVO
{
    public string characterName;
	public int health;
	public float runSpeed;
	public float sprintSpeed;
	public int ultCooldown;
	public int grenadeCoolDown;
	public int grenadeDamage;
	public float timeBetweenShots;
	public float timeBetweenSecondaryShots;
	public int standardAttackDamage;
	public int secondaryAttackDamage;
}

public class XMLParser : MonoBehaviour
{
	string path = "Assets/playersettings.xml";

	XmlDocument xmlFile = new XmlDocument();

	XmlNodeList[] CharacterSettings = new XmlNodeList[3]; 

	PlayerVO[] CharacterVO = new PlayerVO[3];

	void Start()
	{
		CharacterVO [0] = new PlayerVO ();
		CharacterVO [1] = new PlayerVO ();
		CharacterVO [2] = new PlayerVO ();

		ReadData ();
		//PopulateVOs ();

		Debug.Log ("Diablo: " + CharacterVO [0].health);
		Debug.Log ("Psyonic: " + CharacterVO [1].health);
		Debug.Log ("Hawke: " + CharacterVO [2].health);
	}

	public void ReadData()
	{
		xmlFile.Load (path);

        var diablo = GetCharacter("Diablo");
        var psyonic = GetCharacter("Psyonic");
        var hawke = GetCharacter("Hawke");

	}

    private PlayerVO GetCharacter(string characterName)
    {
        var player = new PlayerVO();

        player.health =                     int.Parse(xmlFile.SelectSingleNode("//" + characterName + "/Health").InnerText);
        player.runSpeed =                   float.Parse(xmlFile.SelectSingleNode("//" + characterName + "/RunSpeed").InnerText);
        player.sprintSpeed =                float.Parse(xmlFile.SelectSingleNode("//" + characterName + "/SprintSpeed").InnerText);
        player.ultCooldown =                int.Parse(xmlFile.SelectSingleNode("//" + characterName + "/UltCooldown").InnerText);
        player.grenadeCoolDown =            int.Parse(xmlFile.SelectSingleNode("//" + characterName + "/GrenadeCooldown").InnerText);
        player.grenadeDamage =              int.Parse(xmlFile.SelectSingleNode("//" + characterName + "/GrenadeDamage").InnerText);
        player.timeBetweenShots =           float.Parse(xmlFile.SelectSingleNode("//" + characterName + "/TimeBetweenShots").InnerText);
        player.timeBetweenSecondaryShots =  float.Parse(xmlFile.SelectSingleNode("//" + characterName + "/TimeBetweenSecondaryShots").InnerText);
        player.standardAttackDamage =       int.Parse(xmlFile.SelectSingleNode("//" + characterName + "/AttackDmg").InnerText);
        player.secondaryAttackDamage =      int.Parse(xmlFile.SelectSingleNode("//" + characterName + "/SecondaryAttackDmg").InnerText);

        return player;
    }


    /*
	public void PopulateVOs()
	{
		for (int i = 0; i < CharacterSettings.Length; i++)
		{
			CharacterVO [i].health = 						int.Parse(CharacterSettings 	[i][0].SelectSingleNode("//Diablo/Health").InnerText);
			CharacterVO [i].runSpeed = 						float.Parse(CharacterSettings 	[i][0].SelectSingleNode("RunSpeed").InnerText);
			CharacterVO [i].sprintSpeed = 					float.Parse(CharacterSettings 	[i][0].SelectSingleNode("SprintSpeed").InnerText);
			CharacterVO [i].ultCooldown = 					int.Parse(CharacterSettings 	[i][0].SelectSingleNode("UltCooldown").InnerText);
			CharacterVO [i].grenadeCoolDown = 				int.Parse(CharacterSettings 	[i][0].SelectSingleNode("GrenadeCooldown").InnerText);
			CharacterVO [i].grenadeDamage =				 	int.Parse(CharacterSettings 	[i][0].SelectSingleNode("GrenadeDamage").InnerText);
			CharacterVO [i].timeBetweenShots = 				float.Parse(CharacterSettings	[i][0].SelectSingleNode("TimeBetweenShots").InnerText);
			CharacterVO [i].timeBetweenSecondaryShots = 	float.Parse(CharacterSettings 	[i][0].SelectSingleNode("TimeBetweenSecondaryShots").InnerText);
			CharacterVO [i].standardAttackDamage = 			int.Parse(CharacterSettings 	[i][0].SelectSingleNode("AttackDmg").InnerText);
			CharacterVO [i].secondaryAttackDamage = 		int.Parse(CharacterSettings 	[i][0].SelectSingleNode("SecondaryAttackDmg").InnerText);

		}

	}
    */

}
