﻿using UnityEngine;
using System.Collections;

public class ShaderQueueView : MonoBehaviour {
    private Material shader;
	// Use this for initialization
	void Start () {
        //shader = GetComponent<Material>();
        shader = GetComponent<Renderer>().material;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.H))
        {


            shader.SetColor("_SilhouetteColor", Color.gray);
            shader.SetFloat("_OutlineSize", 0.03f);
        }
        if (Input.GetKeyDown(KeyCode.G))
        {


            shader.SetColor("_SilhouetteColor", Color.black);
            shader.SetFloat("_OutlineSize", 0f);
        }
    }
}
