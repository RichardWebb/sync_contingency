﻿/*
 * Player HUD View
 * This class handles the floating text - killed a player and killed by popups
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using UnityEngine.Networking;
using pulsar.view.interfaces;
using System;
using UnityEngine.UI;
using DG.Tweening;
using Robotlegs.Bender.Extensions.ViewManagement.API;
using Robotlegs.Bender.Extensions.Mediation.API;
using Robotlegs.Bender.Extensions.EventManagement.API;
using Robotlegs.Bender.Extensions.EventManagement.Impl;

namespace pulsar.view.game.ui
{
    public class PlayerHUDView : NetworkBehaviour, IActionListener
    {
        //robotlegs setup
        public event Action<IView> RemoveView;
        private IEventDispatcher _dispatcher = new EventDispatcher();
        public IEventDispatcher dispatcher
        {
            get
            {
                return _dispatcher;
            }
            set
            {
                _dispatcher = value;
            }
        }

        //Text to be displayed
        [SerializeField]
        private Text deathTxt;

        //Control image displays the players controls.
        [SerializeField]
        private CanvasGroup controlsImage; 


        //Canvas group for fading
        [SerializeField]
        private CanvasGroup deathCGroup;

        //Current tween of the controls images
        private Tweener currentControlsImageTween;

        // Use this for initialization
        void Start ()
        {
            //hook mediators
            ViewNotifier.RegisterView(this);

            if (controlsImage != null)
                controlsImage.alpha = 1;

            //Get canvas group
           // deathCGroup = deathTxt.gameObject.GetComponent<CanvasGroup>();
	    }

        //Called when a player is killed - displays kill text
        public void KilledPlayer(string name)
        {
            if (isLocalPlayer)
                ShowKillTxt("You killed " + name);
        }

        //Called when the player has died - displays killed by text
        public void YouDied(string name)
        {
            if (isLocalPlayer)
                ShowKillTxt("Killed by " + name);
        }

        //Fade in and out kill text with text
        private void ShowKillTxt(string text)
        {
         //   if (deathCGroup == null) deathCGroup = deathTxt.gameObject.AddComponent<CanvasGroup>();

            deathTxt.text = text;
            deathCGroup.DOFade(1f, 0.5f).SetEase(Ease.Linear).OnComplete(() =>
            {
                deathCGroup.DOFade(0f, 0.5f).SetEase(Ease.Linear).SetDelay(1f).OnComplete(() => 
                {
                    //Reset text
                    deathTxt.text = "";
                });
            });
        }


        private void Update()
        {
            //Toggle control window
            if (controlsImage == null) return;
            //if (Input.GetKeyDown(KeyCode.Escape))
            if(Input.GetButtonDown("Exit"))
            {
                if (currentControlsImageTween != null) currentControlsImageTween.Kill();

                currentControlsImageTween = controlsImage.DOFade(controlsImage.alpha == 1 ? 0 : 1, 0.25f).SetEase(Ease.Linear);
            }
        }
	
    }
}
