﻿/*
 * Score panel player
 * This class handles the score overlay screen player - displaying kills, deaths score.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using UnityEngine.UI;
using System;

namespace pulsar.view.game.ui
{
    public class ScorePanelPlayer : MonoBehaviour
    {
        private PlayerSettings mySettings;
        private PlayerStatsView myStats;

        [SerializeField]
        private Button myButton;

        [SerializeField]
        private Text nameText;

        [SerializeField]
        private Text killText;

        [SerializeField]
        private Text deathText;

        [SerializeField]
        private ScorePanelView panelView;

        //Init the player - populating data.
        public void Init(PlayerSettings settings)
        {
            mySettings = settings;
            myStats = mySettings.gameObject.GetComponent<PlayerStatsView>();
            PopulatePlayer();
        }

        //Populate the player with the data.
        public void PopulatePlayer()
        {
            if (mySettings == null || myStats == null) return;
            var settings = GetLatestStats();
            nameText.text = settings.name;
            killText.text = settings.kills;
            deathText.text = settings.deaths;
        }

        //Get the latest stats for the player.
        public ScorePanelPlayerVO GetLatestStats()
        {
            ScorePanelPlayerVO vo = new ScorePanelPlayerVO();

            vo.name = mySettings.playerName;
            vo.kills = myStats.myTotalKills.ToString();
            vo.bestKillStreak = "99";
            vo.deaths = myStats.myTotalDeaths.ToString();
            return vo;
        }
    }

    //Player object
    public class ScorePanelPlayerVO
    {
        public string name;
        public string kills;
        public string deaths;
        public string bestKillStreak;
    }
}
