﻿/*
 * UI camera follow view
 * This class allows any world canvas or object to look at the local player
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using Robotlegs.Bender.Platforms.Unity.Extensions.Mediation.Impl;
using pulsar.view.interfaces;
using System;

namespace pulsar.view.game.ui
{
    public class UICameraFollowView : EventView, IGameListener
    {
        //toggle for object to look at the camera
        private bool startLooking = false;

        //the camera to look at
        private Camera cam;

        protected override void Start ()
        {
            //hook mediators
            base.Start();
	    }

        //Stop unwanted rotation when the game finishes
        public void HandleGameFinished()
        {
            startLooking = false;
        }

        public void HandleGameStart()
        {
            //Get the camera 
            cam = Camera.main;
            if(cam != null)
                startLooking = true;
        }

        private void Update()
        {
            //if we can rotation - then do so
            if (startLooking)
            {
                transform.rotation = Quaternion.LookRotation(cam.transform.forward);
            }
        }

    }
}
