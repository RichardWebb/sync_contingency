﻿/*
 * Score panel view 
 * This class handles the score overlay screen (toggled by tab)
 * Created by Richard Webb, s130803
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using DG.Tweening;
using System;
using Robotlegs.Bender.Platforms.Unity.Extensions.Mediation.Impl;
using pulsar.view.interfaces;
using pulsar.events;

namespace pulsar.view.game.ui
{
    public class ScorePanelView : EventView, IGameListener
    {
        //Config vars
        [SerializeField]
        private ScorePanelPlayer[] team1Players;

        [SerializeField]
        private ScorePanelPlayer[] team2Players;

        [SerializeField]
        private GameObject[] team1Images;

        [SerializeField]
        private GameObject[] team2Images;

        [SerializeField]
        private CanvasGroup scorePanelCGroup;

        [SerializeField]
        private CanvasGroup statsPanelCGroup;

        [SerializeField]
        private Text nameTxt;

        [SerializeField]
        private Text killsTxt;

        [SerializeField]
        private Text deathsTxt;

        [SerializeField]
        private Text killStreakTxt;

        [SerializeField]
        private Text gameTimeText;

        [SerializeField]
        private Text team1ScoreText;

        [SerializeField]
        private Text team2ScoreText;

        private PlayerSettings[] allPlayers;
        private bool started;
        private bool isShowing = false;
        private Coroutine currentRoutine;
        private GraphicRaycaster rayCaster;
        private ScorePanelPlayer currentSelectedPlayer;
        private GameTimer gameTimer;
        private GameScoreManager scoreManager;
        
        protected override void Start ()
        {
            base.Start();
            //Get players are 8 seconds
       //     StartCoroutine(PopulateScoreScreen(8));
            rayCaster = GetComponent<GraphicRaycaster>();
            //Get the game timer
            gameTimer = GameObject.FindObjectOfType<GameTimer>();
            rayCaster.enabled = false;
        }

        //Coroutine for populating the score screen.
        private IEnumerator PopulateScoreScreen(float delay)
        {
            yield return new WaitForSeconds(delay);

            //Get all players.
            allPlayers = GameObject.FindObjectsOfType<PlayerSettings>();
            gameTimer = GameObject.FindObjectOfType<GameTimer>();
            scoreManager = GameObject.FindObjectOfType<GameScoreManager>();
            StartCoroutine(UpdateGameTimer());

            int team1Index = 0;
            int team2Index = 0;

            //Sort players based on teams
            foreach (PlayerSettings player in allPlayers)
            {
                if (player.teamName == "red")
                {
                    team1Players[team1Index].Init(player);
                    team1Index++;
                }
                else
                {
                    team2Players[team2Index].Init(player);
                    team2Index++;
                }
            }
 
           started = true;
        }

        //Routine to update the game timer
        private IEnumerator UpdateGameTimer()
        {
            while (true)
            {
                yield return new WaitForSeconds(1);
                team1ScoreText.text = scoreManager.team1Score.ToString();
                team2ScoreText.text = scoreManager.team2Score.ToString();
                gameTimeText.text = "Time remaining: " + gameTimer.GetTimeLeft();
            }
        }

        //Unused now
        /*
        public void UpdatePanel(ScorePanelPlayerVO vo, ScorePanelPlayer scorePanelPlayer)
        {
         //   currentSelectedPlayer = scorePanelPlayer;
       //     UpdatePanel(vo);
        }


        private void PopulateIndividualPanel(GameObject[] team, int index, PlayerSettings player)
        {
            GameObject obj = team[index];
            ScorePanelPlayer scorePlayer = obj.GetComponent<ScorePanelPlayer>();

            scorePlayer.Init(player);
            if (player.isLocalPlayer)
            {
            //    localPlayer = scorePlayer;
            }

            obj.transform.FindChild("Name").GetComponent<Text>().text = player.playerName;
        }*/
	
	    // Update is called once per frame
	    void Update ()
        {
            if (started)
            {
                //Toggle panel
               // if (Input.GetKeyUp(KeyCode.Tab))
                if (Input.GetButtonUp("Score"))
               {
                    scorePanelCGroup.interactable = isShowing;

                    scorePanelCGroup.DOFade(isShowing ? 0f : 1f, 0.5f);

                    isShowing = !isShowing;
                    rayCaster.enabled = isShowing;
                    if (currentRoutine != null)
                    {
                        StopCoroutine(currentRoutine);
                    }

                    //if showing then update panel
                    if (isShowing)
                    {
                        team1ScoreText.text = scoreManager.team1Score.ToString();
                        team2ScoreText.text = scoreManager.team2Score.ToString();
                        UpdatePlayers();

                        currentRoutine = StartCoroutine(RefreshPlayerScores());
                    }
                    else
                    {
                        //Otherwise fade panel out
                        statsPanelCGroup.alpha = 0;
                    }
                }
            }
	
	    }

        //Update each player stats.
        private void UpdatePlayers()
        {
            foreach (ScorePanelPlayer player in team1Players)
            {
                player.PopulatePlayer();
            }
            foreach (ScorePanelPlayer _player in team2Players)
            {
                _player.PopulatePlayer();
            }
        }

        //Keep players scores up to date.
        private IEnumerator RefreshPlayerScores()
        {
            while (true)
            {
                yield return new WaitForSeconds(0.5f);
                UpdatePlayers();

            }
        }

        public void HandleGameStart()
        {
            StartCoroutine(PopulateScoreScreen(0.1f));
        }

        //On game finished run end game
        public void HandleGameFinished()
        {
            StartCoroutine(EndGame());
        }

        //Display this panel after time when the game ends
        private IEnumerator EndGame()
        {
            yield return new WaitForSeconds(5.75f);
            team1ScoreText.text = scoreManager.team1Score.ToString();
            team2ScoreText.text = scoreManager.team2Score.ToString();
            UpdatePlayers();

            scorePanelCGroup.DOFade(1f, 0.5f);

            yield return new WaitForSeconds(25f);
            //ComX only - close game for next players.
            Application.Quit();
        }
    }
}
