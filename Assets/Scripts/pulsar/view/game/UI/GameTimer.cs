﻿/*
 * Game timer 
 * This class handles the game time
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System;
using UnityEngine.UI;
using Robotlegs.Bender.Extensions.EventManagement.API;
using Robotlegs.Bender.Extensions.Mediation.API;
using Robotlegs.Bender.Extensions.ViewManagement.API;
using Robotlegs.Bender.Extensions.EventManagement.Impl;
using pulsar.view.interfaces;
using pulsar.events;
using System.Collections.Generic;
using System.IO;
using pulsar.model;

namespace pulsar.view.game.ui
{
    public class GameTimer : NetworkBehaviour, IGameDispatcher, IGameListener

    {
        //Text to update
        [SerializeField]
        private Text countDownTxt;
        
        //Game time
        private int gameTimeInSeconds;
        private int totalMinutes = 1;

        //Time before the game starts
        private int timeBeforeStart = 25;

        //Network vars - hooking game time
        [SyncVar(hook = "SyncGameTimeLeft")]
        public int gameTimeLeft;

        [SyncVar(hook = "SyncTimeLeftUntilStart")]
        private int timeLeftUntilStart;

        //Robotlegs setup
        public event Action<IView> RemoveView;

        private IEventDispatcher _dispatcher = new EventDispatcher();
        private Coroutine currentRoutine;
        private GameScoreManager scoreManager;
        private PlayerSettings[] players;
        private bool endFinished = true;

        public IEventDispatcher dispatcher
        {
            get
            {
                return _dispatcher;
            }
            set
            {
                _dispatcher = value;
            }
        }

        // Use this for initialization
        void Start ()
        {
            //hook mediators
            ViewNotifier.RegisterView(this);

            //if server then setup game time
            if (isServer)
            {
                gameTimeInSeconds = 60 * totalMinutes;
                timeLeftUntilStart = timeBeforeStart;
                gameTimeLeft = gameTimeInSeconds;
                StartCoroutine(DoInitialCountdown());
                int gameId = PlayerPrefs.GetInt("GameID", 0);
                gameId++;
                PlayerPrefs.SetInt("GameID", gameId);
            }	
	    }

        //Work out time left and format into a visual form
        public string GetTimeLeft()
        {
            int d = (int)(gameTimeLeft * 100.0f);
            int minutes = d / (60 * 100);
            int seconds = (d % (60 * 100)) / 100;
            //int hundredths = d % 100;
            return String.Format("{0:00}:{1:00}", minutes, seconds );

        }

        //method for the first countdown before the game starts.
        private IEnumerator DoInitialCountdown()
        {
            for (int i = 0; i < timeBeforeStart; i++)
            {
                yield return new WaitForSeconds(1);
                timeLeftUntilStart--;
                countDownTxt.text = timeLeftUntilStart.ToString();
            }

            //Run main game timer
            StartCoroutine(RunGameTimer());
        }

        //Method to run the game timer
        private IEnumerator RunGameTimer()
        {
			StartCoroutine (DoMidGame(gameTimeLeft * 0.75f));
            //Work game time down
            for (int i = 0; i < gameTimeInSeconds; i++)
            {
                yield return new WaitForSeconds(1);
                if (gameTimeLeft <= 0) break;
                gameTimeLeft--;
            }

           
        }

       

        private IEnumerator DoMidGame(float _gameTime)
		{
			yield return new WaitForSeconds (_gameTime);
			if (PlayerSettings.localPlayerSettings != null) 
			{
				PlayerSettings.localPlayerSettings.HandleNearEndOfGame ();
			}
		}

        //Client side syncing time left until start of game
        [Client]
        private void SyncTimeLeftUntilStart(int value)
        {
            timeLeftUntilStart = value;
            countDownTxt.text = timeLeftUntilStart.ToString();

            if (timeLeftUntilStart == 0)
            {
                //Inform application game has started.
                dispatcher.Dispatch(new GameEvent(GameEvent.Type.START_GAME));

                //Get rid of starting text
                Destroy(countDownTxt.gameObject.transform.parent.gameObject);
            }
        }

        //Client side syncing time left 
        [Client]
        public void SyncGameTimeLeft(int value)
        {                        
            gameTimeLeft = value;
            if (gameTimeLeft == 0)
            {
                //Inform application the game has finished.
                dispatcher.Dispatch(new GameEvent(GameEvent.Type.END_GAME));
                Debug.Log("Game finished");
            }            
        }

        public void HandleGameStared()
        {
            PostData();
            currentRoutine = StartCoroutine(GatherData());
        }

        private IEnumerator GatherData()
        {
            while (true)
            {
                if (!endFinished)
                {
                    yield return new WaitForSeconds(2.5f);
                    PostData();
                }
            }
        }

        private void PostData()
        {
            if (isServer)
            {
            //    Debug.Log("In handle game end");
                GameData data = new GameData();

                int gameId = PlayerPrefs.GetInt("GameID", 0);

                data.gameID = gameId.ToString();

                if (scoreManager == null)
                    scoreManager = FindObjectOfType<GameScoreManager>();

                data.team1Score = scoreManager.team1Score;
                data.team2Score = scoreManager.team2Score;
                data.gameType = GameSettings.GameType == GameSettings.GameTypes.DEATH_MATCH ? "Death^Match" : "Rift^Mode";

                if (players == null || players.Length == 0)
                    players = FindObjectsOfType<PlayerSettings>();
                List<CharacterGameData> playerDatas = new List<CharacterGameData>();

                foreach (PlayerSettings _settings in players)
                {
                    CharacterGameData d = new CharacterGameData();
                    d.characterName = _settings.characterName;
                    d.playerName = _settings.playerName;

                    var stats = _settings.gameObject.GetComponent<PlayerStatsView>();

                    d.playerKills = stats.myTotalKills;
                    d.playerDeaths = stats.myTotalKills;
                    d.teamName = _settings.teamName;

                    float kills = stats.myTotalKills;
                    float deaths = stats.myTotalDeaths;

                    d.playerKD = (kills / deaths > 0 ? deaths : 1).ToString();

                    playerDatas.Add(d);
                }

                data.characterData = playerDatas.ToArray();
                string gameJsonData = JsonUtility.ToJson(data);


                File.WriteAllText(GameSettings.PushPath + "/SyncData.txt", gameJsonData);

            }
        }

        public void HandleGameFinshed()
        {
         
        }

        private void HandleEndOfGame()
        {
            
        }

        public void HandleGameStart()
        {
        }

        public void HandleGameFinished()
        {
            endFinished = true;
            if (isServer)
            {
                PostData();
            }
        }
    }

    [Serializable]
    public class GameData
    {
        public string gameID;
        public string gameType;
        public int team1Score;
        public int team2Score;
        public CharacterGameData[] characterData;
    }

    [Serializable]
    public class CharacterGameData
    {
        public string playerName;
        public string characterName;
        public string teamName;
        public int playerKills;
        public int playerDeaths;
        public string playerKD;
    }


}

