﻿/*
 * Rift zone manager
 * This class handles the collision box and 3d marker
 * Created by Richard Webb, s13083033
 */
using UnityEngine;
using System.Collections;

namespace pulsar.view.game.rift
{
    public class RiftZoneManagerView : MonoBehaviour
    {
        [SerializeField]
        private BoxCollider collider;

        [SerializeField]
        private string team;

        [SerializeField]
        private GameObject markerObj;

        public bool isMainRift = false;

        // Use this for initialization
        void Start()
        {
            if (!string.IsNullOrEmpty(team))
            {
                if (PlayerSettings.localPlayerSettings == null)
                {
                    Destroy(collider);
                    Destroy(this);
                    return;
                }
                if (team == PlayerSettings.localPlayerSettings.teamName.ToLower())
                {
                    Destroy(collider);
                    Destroy(this);
                }
                else
                {
                    collider.enabled = false;
                }
            }

            if (isMainRift) collider.enabled = false;
        }

        public void ToggleMarker(bool state)
        {
            ToggleCollider(state);

            markerObj.SetActive(state);
        }

        public void ToggleCollider(bool state)
        {
            if (collider != null)
            {
                collider.enabled = state;
            }
        }


    }
}