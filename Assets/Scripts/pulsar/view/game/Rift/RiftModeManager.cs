﻿/*
 * Rift mode manager
 * This class handles the rift mode.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System;
using UnityEngine.UI;
using pulsar.model;
using pulsar.view.mono;
using pulsar.view.interfaces;
using Robotlegs.Bender.Extensions.Mediation.API;
using Robotlegs.Bender.Extensions.EventManagement.API;
using Robotlegs.Bender.Extensions.EventManagement.Impl;
using Robotlegs.Bender.Extensions.ViewManagement.API;

namespace pulsar.view.game.rift
{
    public class RiftModeManager : NetworkBehaviour, IGameListener
    {
        //network vars - timings and text updates.
        [SyncVar(hook = "OnRiftStateChanged")]
        private bool riftState = false;

        [SyncVar(hook = "OnTimeChanged")]
        private int timeLeftUntilRift;

        [SyncVar(hook = "OnInfoTextChanged")]
        private string infoText;

		private AudioPlayerView audioView;

		[SerializeField]
		private AudioClip[] clips;

        //Property for rift avaiablity 
        public bool IsRiftAvaiable
        {
            get
            {
                return riftState;
            }
            set
            {
                riftState = value;
            }
        }

        private Vector3 blueRift;
        private Vector3 redRift;
        private Vector3 zonePos;
        private GameObject mainRift;
        private int riftDownTime = 15;
        private Text riftInfoText;
		private bool myTeamHasRift;

        public event Action<IView> RemoveView;
        private IEventDispatcher _dispatcher = new EventDispatcher();

        public IEventDispatcher dispatcher
        {
            get
            {
                return _dispatcher;
            }
            set
            {
                _dispatcher = value;
            }
        }

        private RiftZoneManagerView[] riftZones;

        // Use this for initialization
        void Start ()
        {
            ViewNotifier.RegisterView(this);
            audioView = GetComponent<AudioPlayerView>();
            timeLeftUntilRift = riftDownTime;
            if (isServer)
            {
                //if server then get the rift objects and create the zones
                blueRift = GameObject.FindGameObjectWithTag("BlueRift").transform.position;
                redRift = GameObject.FindGameObjectWithTag("RedRift").transform.position;
                zonePos = GameObject.FindGameObjectWithTag("RiftZone").transform.position;

                var obj1 = (GameObject)Instantiate(Resources.Load("Game/Rift/BlueRift"), blueRift, Quaternion.identity);
                var obj2 = (GameObject)Instantiate(Resources.Load("Game/Rift/RedRift"), redRift, Quaternion.identity);
                var obj3 = (GameObject)Instantiate(Resources.Load("Game/Rift/RiftZone"), zonePos, Quaternion.identity);

                //Spawn the objects on the server for all to see.
                NetworkServer.Spawn(obj1);
                NetworkServer.Spawn(obj2);
                NetworkServer.Spawn(obj3);
                //Run rift cooldown.
                StartCoroutine(RunRiftCountDown(15));
            }
            //Get the rift HUD text
         //   StartCoroutine(GetText());
	    }

        private IEnumerator GetText()
        {
            //Get the rift text on the local player.
            yield return new WaitForSeconds(5);
            riftInfoText = PlayerSettings.localPlayerSettings.hudCanvas.gameObject.transform.FindChild("RiftCountDownText").GetComponent<Text>();
        }

        //Run rift countdown
        public void StartCounter()
        {
            StartCoroutine(RunRiftCountDown(3));
        }

        //Rift count down 
        private IEnumerator RunRiftCountDown(float delay)
        {
            yield return new WaitForSeconds(delay);
            timeLeftUntilRift = riftDownTime;
            for (int i = riftDownTime; i >= 0; i--)
            {
                timeLeftUntilRift = i;
                yield return new WaitForSeconds(1);
            }

            riftState = true;
        }

        //Client side called on rift state changes
        [Client]
        private void OnRiftStateChanged(bool value)
        {
            riftState = value;
        }

        //Client side called on rift cooldown time changed.
        [Client]
        private void OnTimeChanged(int value)
        {
            if (riftInfoText != null)
            {
                if (value == 0)
                {
                    riftInfoText.text = "Rift available!";
                    if (riftZones == null || riftZones.Length <= 0) riftZones = FindObjectsOfType<RiftZoneManagerView>();
                    foreach (RiftZoneManagerView v in riftZones)
                    {
                        if (v == null) continue;

                        if (v.isMainRift) v.ToggleMarker(true);
                       // else v.ToggleCollider(true);
                    }
					audioView.PlayClip (clips [0]);
                }
                else
                {
                    riftInfoText.text = "Rift available in " + value.ToString();
                }
            }
        }

        //Server side - updates text who took the rift
        public void UpdateRiftTakenInfo(string name, string team, string uniID)
        {
            infoText = name + '~' + team + '~' + uniID;
        }

        //Update rift info text with value
        public void UpdateInfoText(string text)
        {
            infoText = text;
        }

        //Client side - called when info text changes
        [Client]
        private void OnInfoTextChanged(string val)
        {
            var _settings = PlayerSettings.localPlayerSettings;
            if (_settings != null)
            {
                _settings.GetComponent<RiftInteractionView>().HideInput();
            }

            var split = val.Split('~');
            if (split.Length == 1)
            {
                riftInfoText.text = val;
				if (isLocalPlayer) 
				{              
					if (val.ToLower ().Contains ("captured")) 
					{
						audioView.PlayClip (myTeamHasRift ? clips [3] : clips [4]);
					}
					if (val.ToLower ().Contains ("dropped")) 
					{
						audioView.PlayClip (clips[5]);
					}

                    if (riftZones == null || riftZones.Length <= 0) riftZones = FindObjectsOfType<RiftZoneManagerView>();
                    foreach (RiftZoneManagerView v in riftZones)
                    {
                        if (v == null) continue;

                        if (v.isMainRift) v.ToggleMarker(false);
                         else v.ToggleCollider(false);
                    }
                }
            }
            else
            {
                if (riftZones == null || riftZones.Length <= 0) riftZones = FindObjectsOfType<RiftZoneManagerView>();
                //Work out what to display.
                if (PlayerSettings.localPlayerSettings == null) return;
                if (PlayerSettings.localPlayerSettings.teamName != split[1])
                {
					myTeamHasRift = false;
                    riftInfoText.text = "The enemy " + split[0] + " has the rift!";

                    foreach (RiftZoneManagerView v in riftZones)
                    {
                        if (v == null) continue;

                        if (v.isMainRift) v.ToggleMarker(false);
                    }

                    audioView.PlayClip(clips[1]);
                }
                else
                {
					myTeamHasRift = true;
                    if (PlayerSettings.localPlayerSettings.gameObject.name == split[2])
                    {
                        riftInfoText.text = "You have the rift!";
                        foreach (RiftZoneManagerView v in riftZones)
                        {
                            if (v == null) continue;

                            if (v.isMainRift) v.ToggleMarker(false);
                            else v.ToggleCollider(true);

                        }
                    }
                    else
                    {
                        riftInfoText.text = "Your team has the rift! Help " + split[0];

                        foreach (RiftZoneManagerView v in riftZones)
                        {
                            if (v == null) continue;

                            if (v.isMainRift) v.ToggleMarker(false);
                        }
                    }

					audioView.PlayClip (clips[2]);
                }
            }

        }

        public void HandleGameStart()
        {
            if (PlayerSettings.localPlayerSettings != null)
            {
                riftInfoText = PlayerSettings.localPlayerSettings.riftText;//PlayerSettings.localPlayerSettings.hudCanvas.gameObject.transform.FindChild("RiftCountDownText").GetComponent<Text>();
            }
        }

        public void HandleGameFinished()
        {
        }
    }
}
