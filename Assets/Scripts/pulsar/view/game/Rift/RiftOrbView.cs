﻿/*
 * Rift orb view
 * This class handles the destroying of the rift objects.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

namespace pulsar.view.game.rift
{
    public class RiftOrbView : NetworkBehaviour
    {
        //Client side tell server to destroy the rift orb
        public void DestroyMe()
        {
            CmdDestroy();
        }

        //Server side destroy the orb.
        [Command]
        private void CmdDestroy()
        {
            NetworkServer.Destroy(this.gameObject);
        }

    }
}