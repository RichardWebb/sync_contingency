﻿/*
 * Rift interaction view 
 * This class handle the users interaction with the rift.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using pulsar.model;
using System;
using pulsar.view.interfaces;
using pulsar.events;
using DG.Tweening;
using puslar.view.characters.additional;
using Robotlegs.Bender.Extensions.Mediation.API;
using Robotlegs.Bender.Extensions.EventManagement.API;
using Robotlegs.Bender.Extensions.EventManagement.Impl;
using Robotlegs.Bender.Extensions.ViewManagement.API;
using UnityEngine.UI;
using pulsar.networksync;

namespace pulsar.view.game.rift
{
    public class RiftInteractionView : NetworkBehaviour, IScoreDispatcher
    {
        //network var - user holding the rift 
        [SyncVar(hook = "OnHoldingChange")]
        private bool holdingRift = false;

        //config vars
        [SerializeField]
        private GameObject[] gunObjs;

        [SerializeField]
        private GameObject riftAuraObj;

        [SerializeField]
        private CanvasGroup riftCanvasGroup;

        [SerializeField]
        private Image inputSliderImage;

        [SerializeField]
        private GameObject markerObject;

        private bool isTeamOne;
        private string riftTrigger = "blueRift";

        //Rift vars
        private bool active = false;
        private bool inZoneRange = false;
        private bool inDunkRange = false;
       // private bool isHolding = false;

        private RiftModeManager riftManager;
        private IRiftListener[] listeners;
        private AnimationSyncView animView;
        private Player_Health healthView;

        private float pressTime;
        private float maxPressTime = 1;
        private bool logTime = false;
        private GameObject orb;

        //Robotlegs setup
        public event Action<IView> RemoveView;
        private IEventDispatcher _dispatcher = new EventDispatcher();
        public IEventDispatcher dispatcher
        {
            get
            {
                return _dispatcher;
            }
            set
            {
                _dispatcher = value;
            }
        }


        private void Start()
        {
            //hook mediators
            ViewNotifier.RegisterView(this);

            //Get the rift manager if on server
            if (isServer)
                StartCoroutine(GetData());

            if (isLocalPlayer)
            {
                GetComponent<Player_Health>().EventDie += HandleDeath;
            }

        }


        public override void OnStartLocalPlayer()
        {
            base.OnStartLocalPlayer();
            //On start of the local player - setup based on team and get references
            listeners = GetComponents<IRiftListener>();
            animView = GetComponent<AnimationSyncView>();
            healthView = GetComponent<Player_Health>();
         //   healthView.EventDie += HandleDeath;
            riftTrigger = UserDataModel._IsTeamOne ? "BlueRift" : "RedRift";
            StartCoroutine(GetData());
        //    active = true;
        }

        //Method to get the rift manager
        private IEnumerator GetData()
        {
            yield return new WaitForSeconds(2);
            riftManager = GameObject.FindObjectOfType<RiftModeManager>();
            active = true;
        }

        //method to handle the rift being dropped (player died while holding)
        private void HandleDeath()
        {
            if (isLocalPlayer)
            {
                inDunkRange = false;
                inZoneRange = false;

                riftCanvasGroup.alpha = 0;
                inputSliderImage.fillAmount = 0;

                if (holdingRift)
                {
                 //   isHolding = false;
                    ToggleListners(false);
                    //Update info text
                    CmdUpdateInfoTextSingle("The Rift has been dropped");
                    //Reset cooldown
                    CmdReset();
                }
            }
        }

        //Method to toggle and listeners on / off
        private void ToggleListners(bool state)
        {
            foreach (IRiftListener listener in listeners)
            {
                listener.holdingRift = state;
            }
        }

        void Update()
        {
            if (!isLocalPlayer) return;
            if (!active) return;

            //Checking if in the zone range and checking inputs
            if (inZoneRange)
            {
                if (!holdingRift)
                {
                    if (!riftManager.IsRiftAvaiable) return;
                    CheckInput(GrabOrb);               
                }
            }
            //Checking if in dunk range and checking inputs
            else if (inDunkRange)
            {
                if (holdingRift)
                {
                    CheckInput(Dunk);
                }
            }
        }

        //Checking user inputs.
        private void CheckInput(Action callback)
        {
           // if (Input.GetKeyDown(KeyCode.E))
            if (Input.GetButtonDown("Use"))
            {
                if (!logTime) logTime = true;
            }

            // if (Input.GetButtonUp("Fire1") || Input.GetAxis("Fire1j") <= 0)

            var h = Input.GetAxis("Horizontal");
            var v = Input.GetAxis("Vertical");
            //Cancel interaction if player moves
            //            if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.Space))
            if (h != 0 || v != 0) 
            {
                logTime = false;
                pressTime = 0;
                inputSliderImage.fillAmount = 0;// GetPercent();
            }

            //Pick up interaction
            //if (Input.GetKeyUp(KeyCode.E))
            if (Input.GetButtonUp("Use"))
            {
                logTime = false;

                if (pressTime >= maxPressTime)
                {
                    if (callback != null) callback();
                }

                pressTime = 0;
                inputSliderImage.fillAmount = 0;// GetPercent();
            }

            //Log press time of button
            if (logTime)
            {
                pressTime += Time.deltaTime;
                if (pressTime >= maxPressTime)
                {
                    logTime = false;
                    pressTime = 0;
                    if (callback != null) callback();
                    riftCanvasGroup.alpha = 0;
                }

                inputSliderImage.fillAmount = GetPercent();
            }
        }

        public void HideInput()
        {
            riftCanvasGroup.alpha = 0;
            inputSliderImage.fillAmount = 0;
        }

        //Get press time percent to update HUD
        private float GetPercent()
        {
            return pressTime / maxPressTime;
        }

        //Server side - updates team score when rift if dunked
        [Command]
        private void CmdUpdateScore(bool team1)
        {
			riftManager.UpdateInfoText("Rift has been captured!");

            if (team1)
            {
                dispatcher.Dispatch(new ScoreEvent(ScoreEvent.Type.UPDATE_TEAM1_SCORE, 500));
            }
            else
            {
                dispatcher.Dispatch(new ScoreEvent(ScoreEvent.Type.UPDATE_TEAM2_SCORE, 500));
            }
        }

        //Server side reset the rift cooldown
        [Command]
        private void CmdReset()
        {
            holdingRift = false;
            if (orb != null)
                NetworkServer.Destroy(orb);

            riftManager.StartCounter();
        }

        //Dunk the rift 
        private void Dunk()
        {
            ToggleListners(false);
          //  isHolding = false;
            //Pass team data
            CmdUpdateScore(UserDataModel._IsTeamOne);
            //Reset cooldown
            CmdReset();
        }

        //Pick up the rift orb
        private void GrabOrb()
        {
            if(riftManager.IsRiftAvaiable)
            {
                inZoneRange = false;
                //Tell server orb has been picked up
                CmdTakeOrb();
                //Toggle the listeners - disable shooting etc.
                ToggleListners(true);
               // isHolding = true;

                //Update text 
                CmdUpdateInfoText(PlayerSettings.localPlayerSettings.playerName, 
                                  PlayerSettings.localPlayerSettings.teamName,
                                  gameObject.name);

          //      CmdSpawnOrb(gameObject.name, isServer);
            }
        }

        //Server side updates info text
        [Command]
        private void CmdUpdateInfoTextSingle(string text)
        {
            riftManager.UpdateInfoText(text);
        }

        //Server side updates info text
        [Command]
        private void CmdUpdateInfoText(string name, string team, string uniID)
        {
            riftManager.UpdateRiftTakenInfo(name, team, uniID);
        }

        //Server side spawns the rift orb
        [Command]
        private void CmdSpawnOrb(string gameObj, bool _isServer)
        {
            var parent = GameObject.Find(gameObj).transform;

            //Create orb
            orb = (GameObject)Instantiate(Resources.Load("Game/Rift/Orb"));

            orb.transform.SetParent(parent);
            orb.transform.localPosition = new Vector3(0, -10, 0);
            if (_isServer)
            {
                //if server player says spawn then just spawn
                NetworkServer.Spawn(orb);
            }
            else
            {
                //if non server player says spawn then spawn and give that player authority of the object.
                NetworkServer.SpawnWithClientAuthority(orb, base.connectionToClient);
            }
        }

        //Server side disable rift when taken
        [Command]
        private void CmdTakeOrb()
        {
            holdingRift = true;
            riftManager.IsRiftAvaiable = false;
        }

        //Trigger enter  - used to check zone and dunk zone collisions
        private void OnTriggerEnter(Collider col)
        {
            //Are we next to the zone
            if (col.tag == "RiftZone")
            {
                riftCanvasGroup.alpha = 1;
                pressTime = 0;
                inZoneRange = true;
            }

            if (holdingRift)
            {
                //Are we next to the dunk zone
                if (col.tag == riftTrigger)
                {
                    riftCanvasGroup.alpha = 1;
                    pressTime = 0;
                    inDunkRange = true;
                }
            }
        }

        //Trigger exit - used to check zone and dunk zone collisions.
        private void OnTriggerExit(Collider col)
        {
            //Are we next to the rift zone
            if (col.tag == "RiftZone")
            {
                riftCanvasGroup.alpha = 0;
                pressTime = 0;
                inZoneRange = false;
            }

            //Are we next to the dunk zone
            if (col.tag == riftTrigger)
            {
                riftCanvasGroup.alpha = 0;
                pressTime = 0;
                inDunkRange = false;
            }
        }

        //Client side - called when holding the rift changes.
        //[Client]
        private void OnHoldingChange(bool val)
        {
            if (!isLocalPlayer)
            {
                markerObject.SetActive(val);
            }

            holdingRift = val;
            //isHolding = false; 
            riftAuraObj.SetActive(val);
            foreach(GameObject obj in gunObjs)
                obj.SetActive(!val);
        }
    }
}