﻿/*
 * Game Score Manager
 * This class manages all the scoring for the game for both teams
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using pulsar.view.interfaces;
using System;
using Robotlegs.Bender.Extensions.Mediation.API;
using Robotlegs.Bender.Extensions.EventManagement.API;
using Robotlegs.Bender.Extensions.EventManagement.Impl;
using Robotlegs.Bender.Extensions.ViewManagement.API;
using UnityEngine.UI;
using pulsar.events;
using DG.Tweening;
using pulsar.model;
using pulsar.view.game.ui;

namespace pulsar.view.game
{
    public class GameScoreManager : NetworkBehaviour, IScoreListener, IScopeListener
    {
        //network vars team 1/2 scores.
        [SyncVar(hook = "OnTeam1ScoreChange")]
        public int team1Score = 0;

        [SyncVar(hook = "OnTeam2ScoreChange")]
        public int team2Score = 0;

        [SerializeField]
        private Image team1Image;

        [SerializeField]
        private Image team2Image;

        private int maxScore;

        //HUD configurables
        [SerializeField]
        private CanvasGroup scorePanelCanvasGroup;

        [SerializeField]
        private Text team1Text;

        [SerializeField]
        private Text team2Text;

        //Robotlegs setup
        public event Action<IView> RemoveView;
        private IEventDispatcher _dispatcher = new EventDispatcher();
        public IEventDispatcher dispatcher
        {
            get
            {
                return _dispatcher;
            }
            set
            {
                _dispatcher = value;
            }
        }

        void Start()
        {
            maxScore = GameSettings.GameType == GameSettings.GameTypes.DEATH_MATCH ? 3000 : 7500;
            //maxScore = GameSettings.GameType == GameSettings.GameTypes.DEATH_MATCH ? 200 : 300;
            //Hook mediators
            ViewNotifier.RegisterView(this);
        }


        //Server update team 1 score.
        [Command]
        public void CmdUpdateTeam1Score(int value)
        {
            team1Score += value;
            team1Text.text = team1Score.ToString();
            if (team1Score >= maxScore)
            {
                FindObjectOfType<GameTimer>().gameTimeLeft = 1;

                //end game;
            }
        }

        //Client side - Update team 1 score.
        [Client]
        public void OnTeam1ScoreChange(int value)
        {
            team1Score = value;
            team1Text.text = team1Score.ToString();
            float t1S = team1Score;
            float mS = maxScore;
            team1Image.fillAmount = t1S / mS;
        }

        //Server side update team 2 score
        [Command]
        public void CmdUpdateTeam2Score(int value)
        {
            team2Score += value;
            team2Text.text = team2Score.ToString();

            if (team2Score >= maxScore)
            {
                FindObjectOfType<GameTimer>().gameTimeLeft = 0;
                //end game;
            }
        }

        //Client Side update team 2 score
        [Client]
        public void OnTeam2ScoreChange(int value)
        {
            team2Score = value;
            team2Text.text = team2Score.ToString();
            float t2S = team2Score;
            float mS = maxScore;
            team2Image.fillAmount = t2S / mS;
        }

        //Tell server to update team 1 score.
        public void UpdateTeam1Score(ScoreEvent evt)
        {
            CmdUpdateTeam1Score(evt.scoreAmount);
        }

        //Tell server to update team 2 score.
        public void UpdateTeam2Score(ScoreEvent evt)
        {
            CmdUpdateTeam2Score(evt.scoreAmount);
        }

        //Toggle score panel on scoping action.
        private void FadeScorePanel(float value)
        {
            scorePanelCanvasGroup.DOFade(value, 0.25f).SetEase(Ease.Linear);
        }

        //Fade score panel when player is scoped in
        public void ScopeIn()
        {
            FadeScorePanel(0f);
        }

        //Fade score panel in when player is scoped out
        public void ScopeOut()
        {
            FadeScorePanel(1f);
        }
    }
}
