﻿/*
 * Game Item
 * This class handles required game start animations.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using pulsar.view.interfaces;
using System;
using Robotlegs.Bender.Extensions.ViewManagement.API;
using Robotlegs.Bender.Extensions.Mediation.API;
using Robotlegs.Bender.Extensions.EventManagement.API;
using Robotlegs.Bender.Extensions.EventManagement.Impl;
using DG.Tweening;

namespace pulsar.view.game
{
    public class GameItem : MonoBehaviour, IGameListener
    {
        //Robotlegs setup
        public event Action<IView> RemoveView;

        private IEventDispatcher _dispatcher = new EventDispatcher();
        public IEventDispatcher dispatcher
        {
            get
            {
                return _dispatcher;
            }
            set
            {
                _dispatcher = value;
            }
        }

        //Types of animation
        private enum ItemState
        {
            Animate_Out,
            DOTWeenOut,
            Destroy,
            Rotate_Out
        }

        //Config vars
        [SerializeField]
        private Vector3 doorRotation;

        [SerializeField]
        private ItemState myState;

        [SerializeField]
        private bool destroyOnComplete = false;

        public void HandleGameFinished()
        {
        }

        //On game start - do animation
        public void HandleGameStart()
        {
            switch (myState)
            {
                //Use animator component to animate out
                case ItemState.Animate_Out:
                    Animator anim = GetComponent<Animator>();
                    if( anim != null)
                    anim.SetBool("Animate_Out", true);
                    break;

                //Just delete
                case ItemState.Destroy:
                    Destroy(this.gameObject);
                    break;

                //Tween out
                case ItemState.DOTWeenOut:
                    transform.DOMoveY(-1000, 2f).SetEase(Ease.InExpo).OnComplete(()=> 
                    {
                        if (destroyOnComplete) Destroy(this.gameObject);
                    });
                    break;

                //Rotate out 
                case ItemState.Rotate_Out:
                    transform.DOLocalRotate(doorRotation, 2f).SetEase(Ease.Linear).OnComplete(() =>
                    {
                        if (destroyOnComplete) Destroy(this.gameObject);
                    }); 
                    break;



            }
        }

        void Start ()
        {
            //Hook mediators
            ViewNotifier.RegisterView(this);
        }
    }
}
