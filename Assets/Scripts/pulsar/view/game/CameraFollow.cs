﻿/*
 * Camera follow class,
 * This class allows objects to rotate towards the camera at all times.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;

namespace pulsar.view.game
{
    public class CameraFollow : MonoBehaviour
    {
	    void Update ()
        {
            var cam = Camera.main;

            if (cam != null)
                transform.rotation = Quaternion.LookRotation(Camera.main.transform.forward);
	    }
    }
}
