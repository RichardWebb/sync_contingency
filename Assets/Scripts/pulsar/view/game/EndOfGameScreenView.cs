﻿/*
 * End of game screen class
 * This class handles the end of game sequence
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;

namespace pulsar.view.game
{
    public class EndOfGameScreenView : MonoBehaviour
    {
        //Config vars
        [SerializeField]
        private CanvasGroup canvasGroup;

        [SerializeField]
        private Image endGameImage;

        [SerializeField]
        private Sprite[] endGameSprites;

        [SerializeField]
        private Image backgroundImage;

        [SerializeField]
        private Color[] endGameColors;

        [SerializeField]
        private AnimUI winLossUI;

        //Display the end of game pass result - 0 loss, 1 win, 3 draw
        public void DisplayEndGame(int result)
        {

            //victory, lost, draw
            switch (result)
            {
                case 0:
                    //Play loss animation
                    winLossUI.LossAnim();
                    break;

                case 1:
                    //Play win animation
                    winLossUI.WinAnim();
                    break;

                case 2:
                    backgroundImage.color = endGameColors[2];
                    endGameImage.sprite = endGameSprites[2];
                    break;
            }

            canvasGroup.alpha = 1;
            
            //Do end of game
            StartCoroutine(EndGame());
        }

        private IEnumerator EndGame()
        {
            //Fade end screen out afer time delay - loads score screen after this time period
            yield return new WaitForSeconds(5.75f);
            canvasGroup.DOFade(0f, 0.75f).SetEase(Ease.Linear);
        }
    }
}
