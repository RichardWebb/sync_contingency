﻿/*
 * Game manager
 * This class handles the start of a game.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using UnityEngine.Networking;
using pulsar.model;

namespace pulsar.view.game
{
    public class GameManager : NetworkBehaviour
    {
        public override void OnStartLocalPlayer()
        {
            base.OnStartLocalPlayer();
        }

        //Rift object for rift game mode
        [SerializeField]
        private GameObject riftObj;

        public override void OnStartServer()
        {
            base.OnStartServer();
            //Checking if we are the server 
            if (isServer)
            {
                //Create game timer and score manager
                GameObject timer = Instantiate((GameObject)Resources.Load("Game/GameTimer"));
                NetworkServer.Spawn(timer);
                GameObject scoreManager = Instantiate((GameObject)Resources.Load("Game/ScoreManager"));
                NetworkServer.Spawn(scoreManager);

                //Checking if we are in rift mode - create rift if so
                if (GameSettings.GameType == GameSettings.GameTypes.RIFT)
                    NetworkServer.Spawn((GameObject)Instantiate(Resources.Load("Game/Rift/RiftModeManager")));

            }
            else
            {
             //   Destroy(this.gameObject);
             
            }
        }
    }
}
