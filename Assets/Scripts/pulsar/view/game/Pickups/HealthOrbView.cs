﻿/*
 * Health orb view
 * This class controls the pick up health orbs 
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using pulsar.networksync;

namespace pulsar.view.game.pickups
{
    public class HealthOrbView : NetworkBehaviour
    {
        //Config health value
        [SerializeField]
        private int healthValue;

        private bool locked = false;

        //OnTriggerEnter - give health - remove health orb
        void OnTriggerEnter(Collider col)
        {
            //Check Locked collision
            if (locked) return;

            //Check if collision is a player
            if (col.gameObject.tag.ToLower().Contains("player"))
            {
                //Get health 
                Player_Health health = col.gameObject.GetComponent<Player_Health>();

                if (health == null) health = col.gameObject.GetComponentInParent<Player_Health>();

                if (health == null) return;

                if (health.isLocalPlayer)
                {
                    //lock down, restore some health
                    locked = true;
                    GetComponent<SphereCollider>().enabled = false;
                    health.IncreaseHealth(healthValue);
                    //Desotry orb over network.
                    var _settings = col.gameObject.GetComponent<PlayerSettings>();
                    if (_settings == null) _settings = col.gameObject.GetComponentInParent<PlayerSettings>();

                    _settings.DestroyObj(this.gameObject);
                }
            }
        } 

    }
}
