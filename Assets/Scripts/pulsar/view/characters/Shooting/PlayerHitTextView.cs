﻿/*
 * Player hit text
 * This class handles the hit text for the player hud - spawned when the player hits and enemy
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.UI;

namespace pulsar.view.characters.shooting
{
    public class PlayerHitTextView : MonoBehaviour
    {
        //Config vars
        [SerializeField]
        private Color normalColor = Color.yellow;

        [SerializeField]
        private Color critColor = Color.red;

        private Text hitText;

        //Init the hit text 
        public void Init(bool isCrit, int damage, float zPoint, bool isScoped)
        {
            //Getting the text component
            hitText = GetComponent<Text>();
            //iif crit then set text colour red 
            hitText.color = isCrit ? critColor : normalColor;

            //increase font size if scoped or not
            if (isScoped)
            {
                hitText.fontSize = isCrit ? 22 : 12;
            }
            else
            {
                hitText.fontSize = isCrit ? 32 : 22;
            }

            RectTransform rect = hitText.GetComponent<RectTransform>();

            //Set position and scale of text

            rect.localPosition = new Vector3(0, 0, 0); 
            rect.localRotation = Quaternion.Euler(Vector3.zero);
            rect.localScale = new Vector3(1.5f,1.5f,1.5f);

            hitText.text = damage.ToString();;
            //    float directionX = Random.Range(0, 6) >= 3 ? -750 : 750;
            // float randomYValue = Random.Range(-750, -350); //-750

            rect.DOMoveY(1000, 1f).SetEase(Ease.Linear).OnComplete(() =>
            {
                Destroy(this.gameObject);
            });

            /*
            //Move the text - different movement for crits
            if (isCrit)
            {
                rect.DOMoveY(-20f, 1f).SetEase(Ease.Linear).OnComplete(() =>
                {
                    Destroy(this.gameObject);
                });

                /*
                rect.DOLocalJump(new Vector3(directionX, 70, rect.localPosition.z), 500,1, 1f).OnComplete(() => 
                {
                    hitText.DOColor(new Color(hitText.color.r, hitText.color.g, hitText.color.b, 0), 0.45f).SetDelay(0.2f).OnComplete(() =>
                    {
                        Destroy(this.gameObject);
                    });
                });*/
                /*
            }
            else
            {
                rect.DOLocalJump(new Vector3(directionX, randomYValue, rect.localPosition.z), 500, 1, 1f).OnComplete(() => 
                {
                    Destroy(this.gameObject);
                });
            }

*/
        }

        void Update()
        {
      //      transform.LookAt(Camera.main.transform);
        }
    }
}
