/*
 * Ray for players view
 * This class handles the overlay text when a player 'mouses over' an enemy.
 * Created by Richard Webb, s130833
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using DG.Tweening;
using pulsar.networksync;
using UnityStandardAssets.Characters.FirstPerson;
using pulsar.view.mono;

namespace pulsar.view.characters.shooting
{
    public class RayForPlayersView : NetworkBehaviour
    {
        //Config Vars
        [SerializeField]
        private Transform camTransform;

        [SerializeField]
        private RaycastHit hit;

        protected float range;

        [SerializeField]
        private Text playerNameText;

        [SerializeField]
        private Image recticle;

        [SerializeField]
        private LayerMask layerMask;

        [SerializeField]
        private Image healthBarImage;

        [SerializeField]
        private Text healthBarText;

        private Tweener currentTween;

        //Player settings
        private PlayerSettings settings;

        private Player_Health healthView;
        private FirstPersonController myController;

        private float mouseLookDefaultY;
        private float mouseLookDefaultX;

        [SerializeField]
        private HealthBarView healthBarView;

        public float PlayerRange
        {        
            set
            {
                range = value;
            }
        }

        private GameObject currentTarget;
        private bool isFriendly;

        void Start()
        {
            //Getting settings
            if (isLocalPlayer)
            {
                settings = GetComponent<PlayerSettings>();
                playerNameText.text = "";
                healthView = GetComponent<Player_Health>();

                //Start looking for players
                StartCoroutine(RayForPlayer());
                myController = GetComponent<FirstPersonController>();

                mouseLookDefaultY = myController.m_MouseLook.YSensitivity;
                mouseLookDefaultX = myController.m_MouseLook.XSensitivity;
            }
        }

        public override void OnStartLocalPlayer()
        {
            base.OnStartLocalPlayer();
            
        }

        //Method to keep looking for players
        private IEnumerator RayForPlayer()
        {
            if (!isLocalPlayer) yield break;
            while (true)
            {
                yield return new WaitForSeconds(0.01f);
                if (healthView.isDead)
                {
                    HideData();

                    myController.m_MouseLook.XSensitivity = mouseLookDefaultX;
                    myController.m_MouseLook.YSensitivity = mouseLookDefaultY;
                }
                else 
                { 
                    //Cast a ray in search for a player
                    if (Physics.Raycast(camTransform.position, camTransform.forward, out hit, range, layerMask))
                    {
                        //if hit is a player 
                        if (hit.transform.tag.Contains("Player"))
                        {
                            //get settings from the player
                            PlayerSettings _settings = hit.transform.gameObject.GetComponent<PlayerSettings>();

                            if (_settings == null) _settings = hit.transform.gameObject.GetComponentInParent<PlayerSettings>();

                            currentTarget = hit.transform.gameObject;
                            if (currentTarget.name == this.gameObject.name)
                            {
                                currentTarget = null;
                                HideData();
                                ResetSensitivity();
                            }
                            else
                            {
                                //currentTarget.GetComponent<Player_Health>().ShowHealthBar();

                                isFriendly = _settings.teamName == settings.teamName;

                                if (!isFriendly)
                                {
                                    myController.m_MouseLook.XSensitivity = mouseLookDefaultX * 0.25f;
                                    myController.m_MouseLook.YSensitivity = mouseLookDefaultY * 0.25f;
                                }

                                recticle.color = isFriendly ? Color.green : Color.red;
                                //recticle.sprite = isFriendly ? recticles[1] : recticles[2];
                                //if (isFriendly)
                                //{
                                if (healthBarView != null)
                                    UpdateHealthBar();
                                    playerNameText.color = isFriendly ? Color.green : Color.red;
                                    playerNameText.text = _settings.playerName;

                                    currentTween = playerNameText.gameObject.GetComponent<CanvasGroup>().DOFade(1f, 0.1f);
                                //}
                                //else
                                //{
                                    //currentTarget = null;
                                  //  HideData();
                                //}
                            }
                        }
                        else
                        {
                            //if not a player- hide text
                            HideData();
                            ResetSensitivity();
                        }
                    }
                    else
                    {
                        HideData();
                        ResetSensitivity();
                    }
              }
            }
        }

        private void ResetSensitivity()
        {
            myController.m_MouseLook.XSensitivity = mouseLookDefaultX;
            myController.m_MouseLook.YSensitivity = mouseLookDefaultY;
        }

        private void UpdateHealthBar()
        {
            var health = currentTarget.GetComponent<Player_Health>();

           // healthBarText.text = health.health.ToString();

           // float h = health.health;
           // float mH = health.maxHealth;


          //  float percent = (h / mH)*100;
            healthBarView.UpdateHealthBar(isFriendly, health);

            //healthBarImage.fillAmount = percent;
            //Debug.Log("Health::" + health.health.ToString() + " and max health:: " + health.maxHealth.ToString());
        }

        /*
        private void Update()
        {
            
            if (isLocalPlayer)
            {
                if (currentTarget != null)
                {
                    UpdateHealthBar();
                }
            }
        }*/

        private void HideData()
        {
            if (currentTween != null) currentTween.Kill();

            if (currentTarget != null)
                currentTarget = null;

            // healthBarText.text  = "";
            // healthBarImage.fillAmount = 0;
            if (healthBarView != null)
                healthBarView.DisableView();

            playerNameText.gameObject.GetComponent<CanvasGroup>().alpha = 0;
            recticle.color = Color.white;
            //recticle.sprite = recticles[0];

            playerNameText.text = "";
        }
    }
}