﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System;
using Robotlegs.Bender.Extensions.Mediation.API;
using Robotlegs.Bender.Extensions.EventManagement.API;
using Robotlegs.Bender.Extensions.EventManagement.Impl;
using pulsar.view.interfaces;
using UnityEngine.UI;
using DG.Tweening;

namespace pulsar.view.characters.shooting
{
    public class PlayerShoot : NetworkBehaviour
    {
        public void CmdTellServerWhoWasShot(string uniqueID, int dmg, string attacker, string attackerUniID)
        {
            throw new NotImplementedException();
        }

        public IEnumerator FlashShot()
        {
            throw new NotImplementedException();
        }

        public void HandleDeath()
        {
            throw new NotImplementedException();
        }

        public void HandleGameFinished()
        {
            throw new NotImplementedException();
        }

        public void HandleGameStart()
        {
            throw new NotImplementedException();
        }

        public void KilledPlayer(string name)
        {
            throw new NotImplementedException();
        }

        public IEnumerator Reload()
        {
            throw new NotImplementedException();
        }

        public void ScopeIn()
        {
            throw new NotImplementedException();
        }

        public void ScopeOut()
        {
            throw new NotImplementedException();
        }

        public void Shoot()
        {
            throw new NotImplementedException();
        }

        public void Start()
        {
            throw new NotImplementedException();
        }

        public void Update()
        {
            throw new NotImplementedException();
        }

        public void YouDied(string name)
        {
            throw new NotImplementedException();
        }
    }
}