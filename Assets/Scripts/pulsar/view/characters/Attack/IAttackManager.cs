﻿/*
 
     IAttackManager. 
     Interface for connecting the individual shooting scripts to the shooting manager 
     Created by Richard Webb, s1308033. 01/01/17.

 */

using UnityEngine;
using System.Collections;

namespace pulsar.view.characters.attack
{
    public interface IAttackManager
    {
        void RefundMana(float amount);
        bool SpendMana(float amount);
        float Mana { get; set; }
        int myDamage { get; set; }
        void HitPlayer(RaycastHit hit, bool overrideCrit = false);
        void HitPlayer(PlayerSettings settings, string targetName, int dmg, bool isCrit);
        void UpdateServerWhoWasShot(string uniqueID, int dmg, string attacker, string attackerUniID);
        void SilencePlayer(string name);
        void ResetSuper();
        Vector3[] GetShotPoints(Vector3 shotPoint);
    }
}