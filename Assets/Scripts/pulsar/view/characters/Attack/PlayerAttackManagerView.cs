﻿/*
 
     Player attack manager. 
     This class controls inputs and damage dealing to enemies.
     Created by Richard Webb, s1308033. 28/12/16
 */

using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityStandardAssets.Characters.FirstPerson;
using System;
using Robotlegs.Bender.Extensions.Mediation.API;
using Robotlegs.Bender.Extensions.EventManagement.API;
using Robotlegs.Bender.Extensions.EventManagement.Impl;
using Robotlegs.Bender.Extensions.ViewManagement.API;
using pulsar.view.interfaces;
using UnityEngine.UI;
using pulsar.view.characters.additional;
using pulsar.events;
using System.Collections.Generic;
using puslar.view.characters.additional;
using pulsar.view.characters.special;
using pulsar.view.characters.psyonic;
using DG.Tweening;
using pulsar.view.characters.shooting;
using pulsar.networksync;
using pulsar.view.characters.super;

namespace pulsar.view.characters.attack
{
    public class PlayerAttackManagerView : NetworkBehaviour, IAttackManager, IRiftListener, IScoreDispatcher, IGameListener, ISuperListener, IGrenade, IActionListener, IPlayerData
    {
        //RobotLegs setup
        public event Action<IView> RemoveView;
        private IEventDispatcher _dispatcher = new EventDispatcher();
        public IEventDispatcher dispatcher
        {
            get
            {
                return _dispatcher;
            }
            set
            {
                _dispatcher = value;
            }
        }

        public bool holdingRift
        {
            get; set;
        }

        [SyncVar(hook = "OnSilenceChanged")]
        private bool isSilenced = false;

        //Server only
        private Player_Health[] playersHealth;

        //General
        private IAttackInputListener attackView;
        private bool gameStarted;

        private FirstPersonController controller;
        private Player_Health healthScript;
        private float runSpeed;
        public bool usingSuper;

        //Shooting
        private GameObject hitText;
        private float currentTimeBetweenStandardShot;
        private float currentTimeBetweenSecondaryShot;
        public bool isReloading = false;
        private int currentAmmo = 0;

        //Scope 
        private float scopedRunSpeed;

        [SerializeField]
        private float scopedTimeBetweenShots = 0.5f;

        public bool isScoped = false;

        //Configurable 
        [SerializeField]
        private CanvasGroup reloadWepImage;

        [SerializeField]
        private bool scopingEnabled = true;

        [SerializeField]
        private float timeBetweenShots = 0.25f;

        [SerializeField]
        private int maxAmmo = 8;

        [SerializeField]
        private Text ammoText;

        [SerializeField]
        private bool hasAmmo = true;

        [SerializeField]
        private int ammoPerShot = 1;

        [SerializeField]
        private int _myDamage;

        public int myDamage { get { return _myDamage; } set { _myDamage = value; } }

        [SerializeField]
        private int mySpecialDamage = 15;
        public bool busy;

        [SerializeField]
        private int critModifer = 20;

        [SerializeField]
        private float maxMana;

        [SerializeField]
        private CanvasGroup hitPlayerFlash;

        [SerializeField]
        private Animator anim;

        protected float currentMana;

        public float Mana
        {
            get
            {
                return currentMana;
            }

            set
            {
                currentMana = value;
            }
        }

        [SerializeField]
        private Image manaBar;
        private bool usingRemote;
        private bool fireFree = true;
        private bool firedWithRemote;

        private bool usingRemoteForScoping;
        private bool scopingFree = true;
        private bool scopedWithRemote;

        void Awake()
        {
            //hook mediators
            ViewNotifier.RegisterView(this);
        }

        void Start()
        {
            //myDamage = _myDamage;          

            hitText = Resources.Load<GameObject>("Game/HitText");

            //Getting local attack on the player
            attackView = GetComponent<IAttackInputListener>();

            currentTimeBetweenStandardShot = timeBetweenShots;
            currentTimeBetweenSecondaryShot = scopedTimeBetweenShots;
            if (isLocalPlayer)
            {
                currentAmmo = maxAmmo;
            }
        }

        public override void PreStartClient()
        {
            healthScript = GetComponent<Player_Health>();
            healthScript.EventDie += HandleDeath;
            controller = GetComponent<FirstPersonController>();
            runSpeed = controller.m_WalkSpeed;
            scopedRunSpeed = runSpeed * 0.5f;
            reloadWepImage.alpha = 0;
        }

        /// <summary>
        /// Disable hooks when player has died.
        /// </summary>
        private void HandleDeath()
        {
            if (isLocalPlayer)
            {
                //flashView.disable
                StartCoroutine(Reload());
                isScoped = false;
                ScopeOut();
            }
        }

        /// <summary>
        /// Reload the weapon
        /// </summary>
        /// <returns></returns>
        private IEnumerator Reload()
        {
            if (anim != null)
            {
                anim.SetTrigger("DoReload");
            }

            attackView.ScopeOut();
            isReloading = true;
            reloadWepImage.alpha = 0;

            yield return new WaitForSeconds(1);

            currentAmmo = maxAmmo;
            if (ammoText != null)
                ammoText.text = currentAmmo.ToString() + "/" + maxAmmo.ToString();
            isReloading = false;
        }

        void Update()
        {
            if (!isLocalPlayer) return;
            if (!gameStarted) return;

            RegenMana();

            if (holdingRift) return;
            if (Input.GetKeyUp(KeyCode.V)) return;
            if (usingSuper) return;
            if (healthScript.health <= 0) return;
            if (busy) return;
            if (isSilenced) return;

            currentTimeBetweenStandardShot += Time.deltaTime;
            currentTimeBetweenSecondaryShot += Time.deltaTime;
            

            //Check reload.
            if (!isReloading && currentAmmo < maxAmmo && Input.GetButtonUp("Reload"))//Input.GetKeyUp(KeyCode.R))
            {
                StartCoroutine(Reload());
            }

            if (isReloading) return;

            //Scope handling.
            if (scopingEnabled)
            {
                var scopingAxis = Input.GetAxis("Fire2j");
                if (scopedWithRemote)
                {
                    scopingFree = scopingAxis == 0;
                    if (scopingFree)
                        scopedWithRemote = false;
                }

                usingRemoteForScoping = scopingAxis > 0;
                bool scope = false;

                if (usingRemoteForScoping && scopingFree)
                {
                    scope = scopingAxis == 1;
                }
                else
                {
                    scope = Input.GetButtonDown("Fire2");
                }


                //if (Input.GetKey(KeyCode.LeftShift))
                if (Input.GetButton("Sprint"))
                {
                    float h = Input.GetAxis("Horizontal");
                    float v = Input.GetAxis("Vertical");

                    //if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.S))
                    if (h != 0 || v != 0)
                    {
                        if (isScoped)
                        {
                            ScopeOut();
                        }
                    }
                }

            //    if (Input.GetKeyDown(KeyCode.Mouse1))
                if (scope) 
                {
                    scopedWithRemote = usingRemoteForScoping;
                    if (scopedWithRemote)
                        scopingFree = false;

                    if (!isScoped)
                    {
                        ScopeIn();
                    }
                }

//                if (Input.GetKeyUp(KeyCode.Mouse1))
                if (Input.GetButtonUp("Fire2") || scopedWithRemote && Input.GetAxis("Fire2j") <= 0f)
                {
                    if (isScoped)
                    {
                        ScopeOut();
                    }
                }
            }

            float targetTime = isScoped ? scopedTimeBetweenShots : timeBetweenShots;
            float currentTime = isScoped ? currentTimeBetweenSecondaryShot : currentTimeBetweenStandardShot;
            //Shoot handling.
            //if (Input.GetKeyDown(KeyCode.Mouse0) && currentTime >= targetTime)

            if (firedWithRemote)
            {
                fireFree = Input.GetAxis("Fire1j") == 0;
                if (fireFree)
                    firedWithRemote = false;
            }

            var axis = Input.GetAxis("Fire1j");
            usingRemote = axis > 0;

            bool fire = false;

            if (usingRemote && fireFree)
            {
                fire = axis == 1;
            }
            else
            {
                fire = Input.GetButtonDown("Fire1");
            }

           // if ( (Input.GetButtonDown("Fire1") || axis > 0.75f)  && currentTime >= targetTime)
            if (fire && currentTime >= targetTime)
            {
                firedWithRemote = usingRemote;
                if (firedWithRemote)
                    fireFree = false;
              //  usingRemote = axis > 0;
                if (currentAmmo <= 0)
                {
                    StartCoroutine(Reload());
                }
                else
                {
                    Shoot();
                }
            }
        }

        private void RegenMana()
        {
            if (Mana < maxMana)
            {
                Mana += Time.deltaTime * 8f;
                manaBar.fillAmount = Mana / maxMana;
            }
        }

        public bool SpendMana(float amount)
        {
            bool result = false;

            if (Mana - amount > 0)
            {
                result = true;
                Mana -= amount;
            }

            return result;
        }

        public void RefundMana(float amount)
        {
            Mana += amount;

            Mana = Mathf.Clamp(Mana, 0, maxMana);
        }


        private void ScopeIn()
        {
            isScoped = true;
            controller.m_WalkSpeed = scopedRunSpeed;
            attackView.ScopeIn();
        }

        private void ScopeOut()
        {
            isScoped = false;
            controller.m_WalkSpeed = runSpeed;
            attackView.ScopeOut();
        }

        /// <summary>
        /// Shooting method - will hook to attack input listeners.
        /// </summary>
        private void Shoot()
        {
            //        StartCoroutine(FlashShot());

            if (hasAmmo)
            {
                currentAmmo = isScoped ? currentAmmo - 3 : currentAmmo - ammoPerShot;
            }

            if (ammoText != null)
                ammoText.text = currentAmmo.ToString() + "/" + maxAmmo.ToString();

            if (currentAmmo == 0)
            {
                FadeReload();
            }

            if (isScoped)
            {
                currentTimeBetweenSecondaryShot = 0;
            }
            else
            {
                currentTimeBetweenStandardShot = 0;
            }
            attackView.Shoot(isScoped);
        }

        //Fade the reload image
        private void FadeReload()
        {
            reloadWepImage.DOFade(1, 0.5f).OnComplete(() =>
            {
                reloadWepImage.DOFade(0, 0.5f).OnComplete(() =>
                {
                    if (currentAmmo <= 0)
                    {
                        FadeReload();
                    }
                });

            });
        }

        public void UpdateServerWhoWasShot(string uniqueID, int dmg, string attacker, string attackerUniID)
        {
            CmdTellServerWhoWasShot(uniqueID, dmg, attacker, attackerUniID);
        }

        /// <summary>
        /// Update team score and players kills.
        /// </summary>
        /// <param name="uniqueID"></param>
        /// <param name="dmg"></param>
        /// <param name="attacker"></param>
        /// <param name="attackerUniID"></param>
        [Command]
        void CmdTellServerWhoWasShot(string uniqueID, int dmg, string attacker, string attackerUniID)
        {
            Player_Health _target = null;
            Player_Health _attacker = null;

            foreach (Player_Health player in playersHealth)
            {
                string name = player.gameObject.name;
                if (name == uniqueID)
                {
                    _target = player;
                }

                if (name == attackerUniID)
                {
                    _attacker = player;
                }
            }

            if (_target == null) return;
            if (_attacker == null) return;

            _target.lastPersonToHitMe = attackerUniID;
            _target.DeductHealth(dmg);

            if (_target.health <= 0)
            {
                bool isMarked = _target.GetComponent<MarkedPlayerView>().PlayerMarked;

                PlayerSettings settings = _attacker.gameObject.GetComponent<PlayerSettings>();

                if (isMarked)
                    settings.KilledMarkedPlayer();

                if (settings.teamName == "red")
                {
                    dispatcher.Dispatch(new ScoreEvent(ScoreEvent.Type.UPDATE_TEAM1_SCORE, 100));
                }
                else
                {
                    dispatcher.Dispatch(new ScoreEvent(ScoreEvent.Type.UPDATE_TEAM2_SCORE, 100));
                }

                _attacker.gameObject.GetComponent<PlayerStatsView>().UpdateKillCount(1);
            }
        }

        /// <summary>
        /// Get random shot points basic aim assit
        /// </summary>
        /// <param name="shotPoint"></param>
        /// <returns></returns>
        public Vector3[] GetShotPoints(Vector3 shotPoint)
        {
            List<Vector3> shotPoints = new List<Vector3>();
            shotPoints.Add(shotPoint);
            shotPoints.Add(new Vector3(shotPoint.x - 0.01f, shotPoint.y, shotPoint.z));
            shotPoints.Add(new Vector3(shotPoint.x + 0.01f, shotPoint.y, shotPoint.z));

            //  shotPoints.Add(new Vector3(shotPoint.x, shotPoint.y, shotPoint.z - 0.05f));
            //  shotPoints.Add(new Vector3(shotPoint.x, shotPoint.y, shotPoint.z + 0.05f));
            //shotPoints.Add(new Vector3(shotPoint.x, shotPoint.y - 0.05f, shotPoint.z));
            //shotPoints.Add(new Vector3(shotPoint.x, shotPoint.y + 0.05f, shotPoint.z));

            return shotPoints.ToArray();
        }

        private void FlashHit()
        {
            hitPlayerFlash.DOFade(1f, 0.25f).SetEase(Ease.Linear).OnComplete(() => 
            {
                hitPlayerFlash.DOFade(0f, 0.1f).SetDelay(0.33f).SetEase(Ease.Linear);
            });
        }

        /// <summary>
        /// Hit text spawn  - deals damage to the player aswell.
        /// </summary>
        /// <param name="hit"></param>
        public void HitPlayer(RaycastHit hit, bool overrideCrit = false)
        {
            string uIdentity = hit.transform.name;
            Player_Health targetHealth;

            bool isHeadShot = hit.collider.gameObject.tag.Contains("Head");
          
            if (isHeadShot)
            {
                targetHealth = hit.collider.GetComponentInParent<Player_Health>();
            }
            else
            {
                targetHealth = hit.collider.GetComponent<Player_Health>();
            }

            //Player_Health targetHealth = hit.transform.gameObject.GetComponent<Player_Health>();
            if (targetHealth == null) return;
            if (targetHealth.health <= 0) return;

            if (targetHealth.CheckIfPlayerWillDie(myDamage))
            {
                string playerName = "";
                PlayerSettings targetSettings;

                if (isHeadShot)
                {
                    targetSettings = hit.collider.GetComponentInParent<PlayerSettings>();
                }
                else
                {
                    targetSettings = hit.collider.GetComponent<PlayerSettings>();
                }

                playerName = targetSettings.playerName;

                //hit.transform.gameObject.GetComponent<PlayerSettings>().playerName;

                GetComponent<UltimateView>().ReduceTimer();
                if (!string.IsNullOrEmpty(playerName))
                {
                    dispatcher.Dispatch(new PlayerActionEvent(PlayerActionEvent.Type.KILLED_A_PLAYER, playerName));
                }
            }

            GameObject _hitText = (GameObject)Instantiate(hitText);
            _hitText.transform.SetParent(PlayerSettings.localPlayerSettings.hudCanvas.transform);
            _hitText.GetComponent<RectTransform>().localScale = Vector3.one;
            FlashHit();

            bool isCrit = overrideCrit ? true :  isHeadShot;

            int _dmg = myDamage;

            if (scopingEnabled)
            {
                if (isScoped)
                {
                    _dmg = isHeadShot ? targetHealth.maxHealth : mySpecialDamage;
                }
                else
                {
                    if (isCrit)
                        _dmg += critModifer;
                }
            }
            else
            {
                if (isCrit)
                {
                    _dmg += 20;
                }
            }

            _hitText.GetComponent<PlayerHitTextView>().Init(isCrit, _dmg, hit.point.z, isScoped);
            CmdTellServerWhoWasShot(uIdentity, _dmg, PlayerSettings.localPlayerSettings.playerName, gameObject.name);
        }

        //Override hit player - optimise me!
        public void HitPlayer(PlayerSettings _settings, string targetName, int dmg, bool isCrit)
        {
            string uIdentity = targetName;
            Player_Health targetHealth = _settings.gameObject.GetComponent<Player_Health>();
            if (targetHealth == null) return;
            if (targetHealth.health <= 0) return;

            if (targetHealth.CheckIfPlayerWillDie(myDamage))
            {
                string playerName = _settings.playerName;
                GetComponent<UltimateView>().ReduceTimer();

                if (!string.IsNullOrEmpty(playerName))
                {
                    dispatcher.Dispatch(new PlayerActionEvent(PlayerActionEvent.Type.KILLED_A_PLAYER, playerName));
                }
            }

            FlashHit();

            GameObject _hitText = (GameObject)Instantiate(hitText);
            _hitText.transform.SetParent(PlayerSettings.localPlayerSettings.hudCanvas.transform);
            _hitText.GetComponent<RectTransform>().localScale = Vector3.one;

            _hitText.GetComponent<PlayerHitTextView>().Init(isCrit, dmg, 15, false);
            CmdTellServerWhoWasShot(uIdentity, dmg, PlayerSettings.localPlayerSettings.playerName, gameObject.name);
        }

        public void HandleGameStart()
        {
            gameStarted = true;
            if (isServer)
            {
                playersHealth = GameObject.FindObjectsOfType<Player_Health>();
            }
        }

        public void HandleGameFinished()
        {
            gameStarted = false;
        }

        public void ExecuteSuper()
        {
            if (isLocalPlayer)
            {
                usingSuper = true;
                attackView.ExecuteSuper();
            }
        }

        public void ResetSuper()
        {
            usingSuper = false;
        }

        public void DoGrenade(Action callback)
        {
            if (callback != null) callback();
            attackView.ExecuteGrenade();
        }

        public void YouDied(string name)
        {
        }

        public void KilledPlayer(string name)
        {
        }

        public void SilencePlayer(string _name)
        {
            CmdSilencePlayer(_name);
           
        }

        [Command]
        private void CmdSilencePlayer(string _name)
        {
            foreach (Player_Health player in playersHealth)
            {
                if (player.gameObject.name == _name)
                {
                    player.gameObject.GetComponent<PlayerAttackManagerView>().isSilenced = true;
                }
            }
        }

        private void OnSilenceChanged(bool v)
        {
            if (isLocalPlayer)
            {
                isSilenced = v;
                if (isSilenced)
                {
                    var controller = GetComponent<FirstPersonController>();
                    controller.enabled = false;
                    StartCoroutine(RemoveSilence());
                    transform.DOMove(transform.position + transform.up * 5f, 0.75f).SetEase(Ease.Linear).OnComplete(() =>
                    {
                        if (healthScript.health >= 0) 
                            controller.enabled = true;
                    });
                }
            }
        }

        private IEnumerator RemoveSilence()
        {
            yield return new WaitForSeconds(1.25f);
            isSilenced = false;
        }

        public void GotPlayerData(PlayerVO playerData)
        {
         //   Debug.Log("Heard player attack " + playerData.characterName + " ____ " + playerData.standardAttackDamage.ToString());
            var settings = GetComponent<PlayerSettings>();
           // Debug.Log("Character name is : " +settings.characterName);

            if (settings.characterName.ToLower() == playerData.characterName.ToLower())
            {
                myDamage = playerData.standardAttackDamage;
                mySpecialDamage = playerData.secondaryAttackDamage;
                timeBetweenShots = playerData.timeBetweenShots;
                scopedTimeBetweenShots = playerData.timeBetweenSecondaryShots;
            }
        }
    }
}