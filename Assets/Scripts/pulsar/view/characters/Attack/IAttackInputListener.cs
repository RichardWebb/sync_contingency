﻿/*
 
     IAttackInputListener. 
     Interface for connecting the manager to the individual shooting scripts on players
     Created by Richard Webb, s1308033. 01/01/17.

 */

using UnityEngine;
using System.Collections;

namespace pulsar.view.characters.attack
{
    public interface IAttackInputListener
    {
        Vector3 spawnPoint { get; set; }
        void Shoot(bool isScoped);
        void ScopeIn();
        void ScopeOut();
        void ExecuteSuper();
        void ExecuteGrenade();
    }
}