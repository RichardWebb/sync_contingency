﻿/*
 * Xenith attack view
 * This class handles the xenith's attacks (lazer hands)
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using pulsar.view.characters.attack;
using DG.Tweening;
using pulsar.view.characters.shooting;
using pulsar.view.characters.additional;
using puslar.view.characters.additional;
using System;
using puslar.view.characters.xenith;
using pulsar.view.mono;

namespace pulsar.view.characters.xenith
{
    public class XenithAttackView: NetworkBehaviour, IAttackInputListener
    {
     //   [SyncVar(hook = "OnShotChange")]
    //    private Vector3 targetShot;

        [SyncVar(hook = "ToggleSingle")]
        private bool singleToggle;

        [SyncVar(hook = "ToggleSuper")]
        private bool toggleSuper;

        private IAttackManager attackManager;
        private Camera cam;
        private Tweener fieldOfViewTween;
        private Tweener canvasTween;
        private int currentIndex = 0;

        //Config vars
        [SerializeField]
        private float standardRange = 100;

        [SerializeField]
        private LineRenderer standardShotLineRenderer;

        [SerializeField]
        private GameObject[] projectileObjs;

        [SerializeField]
        private Transform projectileSpawnPoint;

        [SerializeField]
        private GameObject[] myGuns;

        [SerializeField]
        private CanvasGroup playerHolderCGroup;

        [SerializeField]
        private GameObject superProjectileObj;

        [SerializeField]
        private Transform superProjectileSpawnPoint;

        [SerializeField]
        private GameObject grenadeObj;

        [SerializeField]
        private Animator[] anims;

        [SerializeField]
        private LayerMask layerMask;

        [SerializeField]
        private GameObject[] comboShotsUI;

        private PlayerSettings settings;
        private float timeSinceLastShot;
        private float timeOutDuration = 3;
        private int comboCount;

        [SerializeField]
        private AudioClip[] clips;

        [SerializeField]
        private Animator anim;

        private AudioPlayerView audioView;

        public Vector3 spawnPoint
        {
            get
            {
                return projectileSpawnPoint.transform.position;
            }

            set
            {
               
            }
        }

        private string[] animNames = new string[] { "DoRightHandAttack", "DoLeftHandAttack", "DoComboAttack" };

        void Start()
        {
            audioView = GetComponent<AudioPlayerView>();
            GetComponent<RayForPlayersView>().PlayerRange = standardRange;
            settings = GetComponent<PlayerSettings>();
            attackManager = GetComponent<IAttackManager>();
        }

        public void ScopeOut()
        {
        }

        public void ScopeIn()
        {
        }

        public void Shoot(bool isScoped)
        {
            //So normal shot
            DoStandardShot();
        }

        private void DoStandardShot()
        {
            //audioView.PlayClip(clips[0]);
            anim.SetTrigger(animNames[comboCount]);

            //Get multiple points - aim assit
            var points = attackManager.GetShotPoints(Camera.main.transform.forward);
            bool hitSomething = false;
            RaycastHit hit;
            //Cast rays for hits
            foreach (Vector3 point in points)
            {
                if (Physics.Raycast(Camera.main.transform.TransformPoint(0, 0, 0.5f), point, out hit, standardRange, layerMask))
                {
                    if (hit.transform.tag.Contains("Player"))
                    {
                        var isHeadShot = hit.transform.tag.Contains("Head");
                        PlayerSettings targetSettings = isHeadShot ? hit.transform.gameObject.GetComponentInParent<PlayerSettings>() : hit.transform.gameObject.GetComponent<PlayerSettings>();

                        if (PlayerSettings.localPlayerSettings.teamName != targetSettings.teamName)
                        {
                            hitSomething = true;
                            //Spawn projectile
                            CmdSpawnShot();
                            //Hit the player 
                            attackManager.HitPlayer(hit, comboCount == 2);
                            break;
                        }
                    }
                }
            }

            if (!hitSomething)
            {
                //Spawn projectile
                CmdSpawnShot();
            }
        }



        [Command]
        private void CmdSpawnShot()
        {
            singleToggle = !singleToggle;
        }


        public void ToggleSingle(bool v)
        {
            if (settings.VRHasControl) return;

            timeSinceLastShot = 0;

            singleToggle = v;
            if (isLocalPlayer)
            {
                if (anims != null && anims.Length > 0)
                    anims[currentIndex].SetTrigger("Fire");                
            }

            audioView.PlayClip(clips[comboCount > 1 ? 1 : 0], isLocalPlayer ? true : false);

            var _projectile = projectileObjs[comboCount];

            var shot = (GameObject)Instantiate(_projectile, spawnPoint, Quaternion.identity);
            shot.GetComponent<ProjectileMonoView>().Launch(settings.myCamera.transform, standardRange);

            comboCount++;

            if (comboCount - 1 <= comboShotsUI.Length )
                comboShotsUI[comboCount - 1].SetActive(false);

            if (comboCount > 2)
            {
                comboCount = 0;
                StartCoroutine(EnableHudCombo());
            }
        }

        public void PlayReverseTime()
        {
            audioView.PlayClip(clips[3]);
        }

        private IEnumerator EnableHudCombo(float delay = 0.5f)
        {
            yield return new WaitForSeconds(delay);
            foreach (GameObject g in comboShotsUI) g.SetActive(true);
        }

        private void Update()
        {
            timeSinceLastShot += Time.deltaTime;

            if (timeSinceLastShot > timeOutDuration)
            {
                comboCount = 0;
                StartCoroutine(EnableHudCombo(0));
            }
        }



        /// <summary>
        /// Executes super - called from attack manager
        /// </summary>
        public void ExecuteSuper()
        {
            //     GetComponent<DiabloSuperView>().ExecuteSuper();
            audioView.PlayClip(clips[2]);
            GetComponent<XenithSuperView>().ExecuteSuper();
            CmdUpdateSuper();
            StartCoroutine(ResetSuper());
        }

        [Command]
        private void CmdUpdateSuper()
        {
            toggleSuper = !toggleSuper;
        }

        private void ToggleSuper(bool v)
        {
            if (settings.VRHasControl) return;

            if (!isLocalPlayer)
            {
                audioView.PlayClip(clips[2], false);
                GetComponent<XenithSuperView>().ExecuteSuper();
            }
        }

        private IEnumerator ResetSuper()
        {
            yield return new WaitForSeconds(1.5f);
            attackManager.ResetSuper();
        }

        /// <summary>
        /// Call back for super projectile 
        /// </summary>
        /// <param name="playerSettings"></param>
        public void HitPlayersWithSuper(GameObject _player)
        {
            attackManager.UpdateServerWhoWasShot(_player.name, 15, PlayerSettings.localPlayerSettings.playerName, gameObject.name);
            attackManager.SilencePlayer(_player.name);
        }

        //Grenade Management
        /// <summary>
        /// Called from attack manager
        /// </summary>
        public void ExecuteGrenade()
        {
          //  CmdSpawnGrenade(isServer);
        }

        //Server side spawn the grenade
        [Command]
        private void CmdSpawnGrenade(bool _isServer)
        {
            if (grenadeObj == null) return;
            var obj = (GameObject)Instantiate(grenadeObj, transform.position, Quaternion.identity);

            if (_isServer)
            {
                NetworkServer.Spawn(obj);
           //     obj.GetComponent<DiabloGrenade>().ServerInit(attackManager);
            }
            else
            {
                NetworkServer.SpawnWithClientAuthority(obj, base.connectionToClient);
            }
        }

        //Call back to hit players with grenade damage
        public void HitPlayersWithGrenade(PlayerSettings[] playerSettings)
        {
            foreach (PlayerSettings player in playerSettings)
            {
                if (player.teamName != PlayerSettings.localPlayerSettings.teamName)
                {
                    attackManager.UpdateServerWhoWasShot(player.gameObject.name, 100, PlayerSettings.localPlayerSettings.playerName, gameObject.name);
                }
            }
        }
    }
}