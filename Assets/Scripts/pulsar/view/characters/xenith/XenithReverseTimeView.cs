﻿/*
 * Reverse Ability
 * This ability allows the player to traverse their previous movement over a specified period of time.
 * Created by Ethan Gill, s1501083, Editied by Richard Webb, s1308033
 */

using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
using DG.Tweening;
using pulsar.view.characters.attack;
using UnityStandardAssets.Characters.FirstPerson;
using pulsar.networksync;
using System;
using UnityEngine.UI;

namespace pulsar.view.characters.xenith
{
    public class XenithReverseTimeView : NetworkBehaviour
    {
        [SyncVar(hook = "OnEndChange")]
        private Vector3 endTarget;

        [SerializeField]
        private Camera playerCam;

        [SerializeField]
        private int logsPerSecond = 10; //amount of logs per second

        [SerializeField]
        private int maxTraverseTime = 3; //max amount of time to reverse to.

        private float coolDown = 15f;
        private float currenTime = 0;

        private float secondsPerLog;
        private int logsKept;
        private bool isTraversing = false;
        private float traverseDuration;

        private Player_SyncPosition posSync;
        private Player_SyncRotation rotSync;

        private List<Vector3> charPos = new List<Vector3>();
        private List<Vector3> charRot = new List<Vector3>();
        private List<Vector3> camRot = new List<Vector3>();

        private FirstPersonController controller;

        private PlayerAttackManagerView attackManager;
        private PlayerSettings mySettings;
        private XenithAttackView xenithAttackview;

        [SerializeField]
        private GameObject decoy;

        [SerializeField]
        private Image cooldownImage;

        [SerializeField]
        private float manaCost;

        // Use this for initialization
        void Start()
        {
            xenithAttackview = GetComponent<XenithAttackView>();
            mySettings = GetComponent<PlayerSettings>();
            posSync = GetComponent<Player_SyncPosition>();
            rotSync = GetComponent<Player_SyncRotation>();
            secondsPerLog = 1f / (float)logsPerSecond;
            logsKept = maxTraverseTime * logsPerSecond;
            traverseDuration = secondsPerLog * 0.1f;

            if (isLocalPlayer)
            {
                attackManager = GetComponent<PlayerAttackManagerView>();
                controller = GetComponent<FirstPersonController>();
                StartCoroutine(TrackPlayer());
            }

            currenTime = coolDown;
        }

        void Update()
        {
            if (!isLocalPlayer) return;

            if (!isTraversing)
            {
                if (currenTime >= 0)
                {
                    currenTime -= Time.deltaTime;
                    cooldownImage.fillAmount = currenTime / coolDown;
                }

                if (currenTime <= 0)
                {
                    //if (Input.GetKeyDown(KeyCode.Alpha1))
                    if (Input.GetButtonDown("Ability1"))
                    {
                        if (attackManager.SpendMana(manaCost))
                        {
                            isTraversing = true;
                            currenTime = coolDown;
                            controller.enabled = false;
                            attackManager.busy = true;
                            CmdUpdateTarget(charPos[0]);
                            TraversePlayer(charPos.Count - 1);
                        }
                    }
                }
            }
        }

        private IEnumerator TrackPlayer()
        {
            while (true)
            {
                yield return new WaitForSeconds(secondsPerLog);

                if (!isTraversing)
                {
                    if (charPos.Count == 0 || transform.position != charPos[charPos.Count - 1])
                    {
                        charPos.Add(transform.position);
                        charRot.Add(transform.rotation.eulerAngles);
                        camRot.Add(playerCam.transform.rotation.eulerAngles);
                    }

                    if (charPos.Count > logsKept)
                    {
                        charPos.RemoveAt(0);
                        charRot.RemoveAt(0);
                        camRot.RemoveAt(0);
                    }
                }
            }
        }

        private void TraversePlayer(int index)
        {
            if (isLocalPlayer)
            {
                xenithAttackview.PlayReverseTime();
            }

            transform.DORotate(charRot[index], traverseDuration).SetEase(Ease.Linear);
            playerCam.transform.DORotate(camRot[index], traverseDuration).SetEase(Ease.Linear);
            transform.DOMove(charPos[index], traverseDuration).SetEase(Ease.Linear).OnComplete(() =>
            {
                index--;
                if (index >= 0)
                {
                    TraversePlayer(index);
                }
                else
                {
                    controller.enabled = true;
                    attackManager.busy = false;
                    //camera control enable
                    charPos.Clear();
                    charRot.Clear();
                    camRot.Clear();
                    isTraversing = false;
                }
            });
        }

        [Command]
        private void CmdUpdateTarget(Vector3 t)
        {
            endTarget = t;
        }

        private void OnEndChange(Vector3 target)
        {
            if (mySettings.VRHasControl) return;

            var _decoy = (GameObject)Instantiate(decoy, transform.position, Quaternion.LookRotation(transform.forward));
            _decoy.transform.localScale = new Vector3(0.46f, 0.46f, 0.46f);
          //  cube.transform.position = transform.position;

            Destroy(_decoy, (float)maxTraverseTime-0.5f);

            if (!isLocalPlayer)
            {
                posSync.stop = true;
                rotSync.stop = true;

                transform.position = new Vector3(0, -2000f, 0);
                StartCoroutine(EnableSyncing());
            }
        }

        private IEnumerator EnableSyncing()
        {
            yield return new WaitForSeconds(maxTraverseTime);

            posSync.stop = false;
            rotSync.stop = false;
        }
    }
}