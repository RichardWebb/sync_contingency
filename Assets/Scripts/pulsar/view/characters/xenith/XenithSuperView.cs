﻿/*
 * Xenith Super view
 * This class handles the void orbs super - cone effect infront of player, silences players.
 * Created by Richard Webb, s1308033.
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.Networking;
using pulsar.view;
using pulsar.view.characters.xenith;

namespace puslar.view.characters.xenith
{
    public class XenithSuperView : NetworkBehaviour
    {
        [SerializeField]
        private GameObject coneObj;

        [SerializeField]
        private Camera myCamera;

        private List<GameObject> hitList = new List<GameObject>();

        private XenithAttackView attackView;


        private int damage = 15;

        void Start()
        {
            attackView = GetComponent<XenithAttackView>();
        }

        public void ExecuteSuper()
        {
            GameObject obj = (GameObject)Instantiate(coneObj, myCamera.transform.position + myCamera.transform.forward * 3f, Quaternion.LookRotation(myCamera.transform.forward));

            XenithOrbHolderView holderView = obj.GetComponent<XenithOrbHolderView>();

            XenithVoidOrbView[] orbViews = obj.GetComponentsInChildren<XenithVoidOrbView>();

            foreach (XenithVoidOrbView view in orbViews)
            {
                view.Init(PlayerHit, HandleOrbFailed, isLocalPlayer, transform);
            }

            holderView.MoveRows(()=> 
            {
                Destroy(obj);
                hitList.Clear();
            });
        }

        private void PlayerHit(GameObject obj)
        {
            if (!hitList.Contains(obj))
            {
                string myTeam = PlayerSettings.localPlayerSettings.teamName;

                PlayerSettings s = obj.GetComponent<PlayerSettings>();

                if (s == null) return;
                            
                string targetsTeam = s.teamName;

                if (myTeam != targetsTeam)
                {
                    hitList.Add(obj);
                    attackView.HitPlayersWithSuper(obj);
                }

            }
        }

        private void HandleOrbFailed()
        {
            //Debug.Log("Orb failed");
        }

        void Update()
        {
        
        }
    }
}