﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using pulsar.view.interfaces;
using System;
using Robotlegs.Bender.Extensions.Mediation.API;
using Robotlegs.Bender.Extensions.EventManagement.API;
using Robotlegs.Bender.Extensions.EventManagement.Impl;
using pulsar.view.characters.attack;
using pulsar.networksync;
using pulsar.view.characters.additional;
using Robotlegs.Bender.Extensions.ViewManagement.API;
using UnityEngine.UI;

namespace pulsar.view.characters.xenith
{
    [NetworkSettings(channel = 1, sendInterval = 0.1f)]
    public class XenithPortalView : NetworkBehaviour, IGameListener
    {
        [SyncVar(hook = "OnTargetOnePosChange")]
        private Vector3 portal1TargetPos;

        [SyncVar(hook = "OnTargetOneRotChange")]
        private Quaternion portal1TargetRot;

        [SyncVar(hook = "OnTargetTwoPosChange")]
        private Vector3 portal2TargetPos;

        [SyncVar(hook = "OnTargetTwoRotChange")]
        private Quaternion portal2TargetRot;


        [SerializeField]
        private PortalCamView[] portals;

        private int currentPortal = 0;

        private float cooldown = 5;
        private float timeOut = 20;
        private float currentTime = 0;

        public event Action<IView> RemoveView;
        private IEventDispatcher _dispatcher = new EventDispatcher();
        private PlayerAttackManagerView attackManager;
        private Player_Health playerHealth;
        private bool isReady;

        private TargeterView targetView;

        [SerializeField]
        private GameObject targetObject;

        [SerializeField]
        private float maxDistance = 15f;

        [SerializeField]
        private Vector3 groundOffset = Vector3.zero;

        private bool isTargeting = false;
        private Coroutine currentTimeOutRoutine;

        [SerializeField]
        private Image cooldownImage;

        [SerializeField]
        private float manaCost;
        private bool firedWithRemote;

        public IEventDispatcher dispatcher
        {
            get
            {
                return _dispatcher;
            }
            set
            {
                _dispatcher = value;
            }
        }

        // Use this for initialization
        void Start()
        {
            ViewNotifier.RegisterView(this);
            playerHealth = GetComponent<Player_Health>();
            attackManager = GetComponent<PlayerAttackManagerView>();
            targetView = GetComponent<TargeterView>();

            playerHealth.EventDie += HandleDeath;
            currentTime = cooldown;
        }

        private void HandleDeath()
        {
            //Kill portals;
        }

        public void HandleGameFinished()
        {
            isReady = false;
        }

        public void HandleGameStart()
        {
            isReady = true;
        }

        // Update is called once per frame
        void Update()
        {
            if (!isReady) return;
            if (!isLocalPlayer) return;
            if (attackManager.isReloading) return;
            if (attackManager.usingSuper) return;
            if (playerHealth.isDead) return;
            if (attackManager.isScoped) return;
            if (attackManager.holdingRift) return;
           // if (attackManager.busy) return;

            cooldownImage.fillAmount = currentTime / cooldown;

            if (currentPortal < 2)
            {
                if (!isTargeting)
                {
                    if (currentTime > 0)
                    {
                        currentTime -= Time.deltaTime;
                    }
                    else
                    {
                        //if (Input.GetKeyDown(KeyCode.Alpha2))

                        if (Input.GetButtonDown("Ability2"))
                        {
                            attackManager.busy = true;
                            isTargeting = true;
                            targetView.startTarget(targetObject, maxDistance, groundOffset);
                        }
                    }
                }
                else
                {
                    if (Input.GetButtonDown("Fire1") || Input.GetAxis("Fire1j") == 1f)
                    {
                        firedWithRemote = Input.GetAxis("Fire1j") > 0;
                        if (attackManager.SpendMana(manaCost))
                        {
                            attackManager.busy = false;

                            isTargeting = false;

                            if (currentTimeOutRoutine != null) StopCoroutine(currentTimeOutRoutine);
                            currentTimeOutRoutine = StartCoroutine(StartTimeOut());

                            var portal = portals[currentPortal];

                            portal.transform.SetParent(null);

                            var trans = targetView.GetTargetTransform;

                            portal.gameObject.transform.position = trans.Key;
                            portal.gameObject.transform.rotation = trans.Value;

                            CmdUpdatePortalPosition(currentPortal == 0, trans.Key, trans.Value);

                            //portal.transform.localEulerAngles = new Vector3(portal.transform.localEulerAngles.x - 90, portal.transform.localEulerAngles.y, portal.transform.localEulerAngles.z);
                            portal.gameObject.SetActive(true);
                            portal.Activate();

                            currentPortal++;
                        }
                    }
                    else if (Input.GetButtonDown("Fire2") || (firedWithRemote && Input.GetAxis("Fire2j") <= 0f))
                    {
                        targetView.StopTarget();
                        isTargeting = false;
                        attackManager.busy = false;

                    }
                }
            }

        }

        [Command]
        private void CmdUpdatePortalPosition(bool isPortal1, Vector3 _p, Quaternion _r)
        {
            if (isPortal1)
            {
                portal1TargetPos = _p;
                portal1TargetRot = _r;
            }
            else
            {
                portal2TargetPos = _p;
                portal2TargetRot = _r;
            }
        }

        private void OnTargetOnePosChange(Vector3 v)
        {
            if (isLocalPlayer) return;
            var p = portals[0];

            p.transform.SetParent(null);

            p.transform.position = v;
            p.gameObject.SetActive(true);

            p.Activate();
        }

        private void OnTargetOneRotChange(Quaternion r)
        {
            if (!isLocalPlayer)
                portals[0].transform.rotation = r;
        }

        private void OnTargetTwoPosChange(Vector3 v)
        {
            if (isLocalPlayer) return;

            var p = portals[1];

            p.transform.SetParent(null);

            p.transform.position = v;
            p.gameObject.SetActive(true);
            p.Activate();

            StartCoroutine(StartTimeOut());
        }

        private void OnTargetTwoRotChange(Quaternion r)
        {
            if (!isLocalPlayer)
                portals[1].transform.rotation = r;
        }


        private IEnumerator StartTimeOut()
        {
            yield return new WaitForSeconds(timeOut);

            currentTime = cooldown;
            currentPortal = 0;

            foreach (PortalCamView p in portals)
            {
                p.Deactivate();
                p.gameObject.SetActive(false);
                p.transform.SetParent(this.transform);
                p.transform.position = new Vector3(0, -2000, 0);
            }
        }
    }
}