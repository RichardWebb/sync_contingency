﻿/*
 * Xenith orb view
 * This class handles the collision of players - linked back to xenith super view
 * Created by Richard Webb, s1308033.
 */
using UnityEngine;
using System.Collections;
using System;

namespace puslar.view.characters.xenith
{
    public class XenithVoidOrbView : MonoBehaviour
    {
        private Action<GameObject> callBack;

        [SerializeField]
        private LayerMask mask;

        //Setting up the hex lines position and beginning the attack
        public void Init(Action<GameObject> _callBack, Action failedCallBack, bool canHitPeople, Transform myTransform)
        { 
            if (_callBack == null) Destroy(this.gameObject);

            callBack = _callBack;

            RaycastHit hit;
            transform.localRotation = Quaternion.Euler(-90, 0, 0);
            if (Physics.Raycast(transform.position, -transform.forward/*-myTransform.up*/, out hit, 50, mask))
            {
                transform.position = hit.point;
                transform.position = transform.position + -transform.forward * 0.5f;
                transform.rotation = Quaternion.LookRotation(-hit.normal);

                GetComponent<MeshRenderer>().enabled = true;
                if (canHitPeople)
                    GetComponent<BoxCollider>().enabled = true;
            }
            else
            {
                failedCallBack();
            }
        }

        void OnTriggerEnter(Collider col)
        {
            if (col.tag.Contains("Player"))
            {
                if (col.gameObject.tag.Contains("Head"))
                {
                    callBack(col.transform.parent.gameObject);
                }
                else
                    callBack(col.transform.gameObject);
            }    
        }

    }
}