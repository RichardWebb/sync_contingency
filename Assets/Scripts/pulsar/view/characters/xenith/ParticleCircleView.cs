﻿/*
 * Particle Circle View
 * This class handles moving the particle source around a circle with parameters
 * Created by Daniel O'Neill, s1303529 
 */

using UnityEngine;
using System.Collections;

public class ParticleCircleView : MonoBehaviour {

    [SerializeField]
    float _xRadius;
    [SerializeField]
    float _yRadius;
    [SerializeField]
    float _zRadius;

    [SerializeField]
    float _speed;
    [SerializeField]
    float _offset;

    [SerializeField]
    private bool UseX;
    [SerializeField]
    private bool UseY;
    [SerializeField]
    private bool UseZ;

    [SerializeField]
    private float xOffset;
    [SerializeField]
    private float yOffset;
    [SerializeField]
    private float zOffset;

    Vector3 _initial;
    float _xDist;
    float _zDist;
    float _yDist;

	void Start()
    {
        _initial = transform.localPosition;

    }

	// Update is called once per frame
	void Update () {
        //_initial = transform.parent.transform.position;
        //_xDist = _initial.x + (_radius * Mathf.Sin(_speed * ((Time.time * Mathf.PI) + ((_offset/_speed) * Mathf.PI))));
        if (UseX) _xDist = _initial.x + (_xRadius * Mathf.Sin(_speed * ((Time.time * Mathf.PI) + (((_offset / _speed) + (xOffset / _speed)) * Mathf.PI))));
        else _xDist = _initial.x;

        if (UseY)  _yDist = _initial.y + (_yRadius * Mathf.Sin(_speed * ((Time.time * Mathf.PI) + (((_offset / _speed) + (yOffset / _speed)) * Mathf.PI))));
        else _yDist = _initial.y;

        if (UseZ) _zDist = _initial.z + (_zRadius * Mathf.Sin(_speed * ((Time.time * Mathf.PI) + (((_offset/_speed) + (zOffset / _speed)) * Mathf.PI))));
        else _zDist = _initial.z;

        transform.localPosition = new Vector3(_xDist, _yDist, _zDist);
    }
}
