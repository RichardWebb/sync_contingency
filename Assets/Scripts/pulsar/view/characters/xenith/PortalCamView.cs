﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace pulsar.view.characters.xenith
{
    public class PortalCamView : MonoBehaviour
    {
        [SerializeField]
        private GameObject twinCam;

        [SerializeField]
        public PortalCamView pair;

        [SerializeField]
        private GameObject portal;

        private Renderer rend;

        
        private Camera cam;

        private RenderTexture rTex;
        [HideInInspector]
        public bool active = false;
        private FloatView[] floats;

        [SerializeField]
        private Transform exitPoint;

        private Vector3 endPoint;

        public Vector3 ExitPoint
        {
            get
            {
                return endPoint;
            }
        }


        // Use this for initialization
        void Start()
        {
            rend = portal.GetComponent<Renderer>();
            rTex = GetComponentInChildren<Camera>().targetTexture;
            floats = GetComponentsInChildren<FloatView>();
         //   active = true;
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            if (cam == null) return;
            if (pair.active && active)
            {
                if (!portal.activeSelf)
                {
                    portal.SetActive(true);
                    GetComponent<BoxCollider>().enabled = true;
                }

                if (rend.isVisible)
                {
                    Vector3 vec = -(cam.gameObject.transform.position - transform.position);
                    twinCam.transform.LookAt(twinCam.transform.position + vec);
                }
            }
            else
            {
                portal.SetActive(false);

            }
        }

        public void Activate()
        {
            GetComponent<Animation>().Play();
            StartCoroutine(activation());
        }

        IEnumerator activation()
        {
            yield return new WaitForSeconds(2f);
            endPoint = exitPoint.transform.position;
            cam = Camera.main;
            active = true;
            foreach (FloatView f in floats)
            {
                f.doFloat(0.1f, 0.5f);
            }
        }

        public void Deactivate()
        {
            GetComponent<BoxCollider>().enabled = false;

            if (rTex != null)
                rTex.Release();
            active = false;
            foreach (FloatView f in floats)
            {
                f.killFloat();
            }
        }

    }
}