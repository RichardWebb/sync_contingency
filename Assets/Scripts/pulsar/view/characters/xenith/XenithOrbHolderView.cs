﻿/*
 * Xenith orb holder view
 * This class handles the moving of rows from the player
 * Created by Richard Webb, s1308033.
 */

using UnityEngine;
using System.Collections;
using System;
using DG.Tweening;

namespace puslar.view.characters.xenith
{
    public class XenithOrbHolderView : MonoBehaviour
    {
        [SerializeField]
        private GameObject[] rows;

        [SerializeField]
        private AudioSource[] sources;

        //Move the hex rows incrementally infront of the player
        public void MoveRows(Action callBack)
        {
            float duration = 0.15f;
            float interval = 0.1f;
            int current = 1;
            int currentAudio = 0;
            foreach (GameObject row in rows)
            {
                var endPoint = row.transform.position + row.transform.up * 2.5f;

                var delay = current == 1 ? 0 : interval * current;

                //Send row up and down
                row.transform.DOMove(endPoint, duration).SetEase(Ease.Linear).SetDelay(delay).SetLoops(2, LoopType.Yoyo);
                sources[currentAudio].Play();

                current++;
                currentAudio++;
            }

            StartCoroutine(Call(duration + interval * current, callBack));

        }

        private IEnumerator Call(float v, Action callBack)
        {
            yield return new WaitForSeconds(v+0.3f);
            callBack();
        }
    }
}