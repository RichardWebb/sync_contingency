﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FloatView : MonoBehaviour {

    private bool hover;
    private Vector3 _startPos;

    public void doFloat(float amount, float duration)
    {
        _startPos = transform.position;
        transform.DOKill();
        transform.DOMoveY(_startPos.y + amount, duration).SetEase(Ease.InOutQuad).SetDelay(Random.Range(0, 10) * 0.1f).SetLoops(-1, LoopType.Yoyo);
    }

    public void killFloat()
    {
        transform.DOKill();
        transform.position = _startPos;
    }

}
