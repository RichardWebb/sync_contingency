﻿/*
 * Animation syncing view 
 * This class manages the animations for a player and syncing them over the network.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using pulsar.view.interfaces;
using System;
using Robotlegs.Bender.Extensions.Mediation.API;
using Robotlegs.Bender.Extensions.EventManagement.API;
using Robotlegs.Bender.Extensions.EventManagement.Impl;
using Robotlegs.Bender.Extensions.ViewManagement.API;
using pulsar.view.characters.special;
using pulsar.networksync;
using pulsar.view.characters.attack;
using System.Collections.Generic;


namespace puslar.view.characters.additional
{
    [NetworkSettings(channel = 1, sendInterval = 0.1f)]
    public class AnimationSyncView : NetworkBehaviour, ISuperListener, IGrenade
    {
        //network var - anim names change
        [SyncVar(hook = "OnAnimationChange")]
        private string currentAnimation;

        [SyncVar(hook = "OnHChange")]
        private float currentHorizontal;

        [SyncVar(hook = "OnVChange")]
        private float currentVertical;

        [SyncVar(hook = "OnJump")]
        private bool jumpToggle;

        //Config vars
        [SerializeField]
        private Animator anim;

        [SerializeField]
        private bool doAnimation = true;

        [SerializeField]
        private GameObject[] hiddenObjs;

        private Player_Health playerHealth;
        private PlayerAttackManagerView attackManager;

        [SerializeField]
        private GameObject[] myLocalWeps;

        //Robotlegs setup
        public event Action<IView> RemoveView;
        private IEventDispatcher _dispatcher = new EventDispatcher();
        private string lastAnim;
        private bool isSprinting;

        public IEventDispatcher dispatcher
        {
            get
            {
                return _dispatcher;
            }
            set
            {
                _dispatcher = value;
            }
        }

        void Start()
        {

            playerHealth = GetComponent<Player_Health>();
            attackManager = GetComponent<PlayerAttackManagerView > ();

            playerHealth.EventDie += HandleDeath;
            playerHealth.EventRespawn += HandleRespawn;

            //hook mediators
            ViewNotifier.RegisterView(this);

            //if a dummy character then enable character model
            if (!isLocalPlayer)
            {
                ToggleHidden(true);
            }
            else
            {
                foreach (GameObject obj in myLocalWeps)
                {
                    obj.SetActive(true);
                }
                StartCoroutine(SendAxis());
            }
        }

        private IEnumerator SendAxis()
        {
            if (!doAnimation) yield break;
            while (true)
            {
                var horizontal = Input.GetAxis("Horizontal");
                var vectical = Input.GetAxis("Vertical");

                //   KeyValuePair<float, float> inputs = new KeyValuePair<float, float>(horizontal, vectical);

                CmdUpdateBlend(horizontal, vectical);
                yield return new WaitForEndOfFrame();
            }
        }

        private void HandleDeath()
        {
            if (isLocalPlayer)
            {
              //  CmdUpdateAnimation("DoDeath", true);
            }
        }

        private void HandleRespawn()
        {
            if (isLocalPlayer)
            {

            }
        }

        //Toggle un/wanted objects on the player
        public void ToggleHidden(bool state)
        {
            foreach (GameObject obj in hiddenObjs)
            {
                if (obj != null)
                    obj.SetActive(state);
            }
        }


        private void OnHChange(float v)
        {
            if (!isLocalPlayer)
            {
                currentHorizontal = v;

                if (anim != null)
                    anim.SetFloat("StrafeBlend", v);
            }
        }

        private void OnVChange(float v)
        {
            if (!isLocalPlayer)
            {
                currentVertical = v;
                if (anim != null)
                    anim.SetFloat("ForwardBlend", v);
            }
        }


        public void UpdateLookDirection(float rotation)
        {
            if (isLocalPlayer) return;
            if (!doAnimation) return;

            //range -80 -> 80

            if(rotation > 180)
            {
                rotation -= 360;
            }

            var rot = rotation + 80;

            rot = 1 - (rot / 160);

            if (anim != null)
                anim.SetFloat("LookingBlend", rot);
        }

        void Update()
        {
            if (isLocalPlayer)
            {
                if (playerHealth.isDead) return;
                if (attackManager.usingSuper) return;

                if (Input.GetKeyDown(KeyCode.LeftShift) )
                {
                    if (!isSprinting)
                    {
                        isSprinting = true;
                        CmdUpdateSprint("isSprinting", true);
                    }
                }
                if (Input.GetKeyUp(KeyCode.LeftShift))
                {
                    if (isSprinting)
                    {
                        isSprinting = false;
                        CmdUpdateSprint("isSprinting", false);
                    }
                }

                if (Input.GetKeyUp(KeyCode.Space))
                {
                    CmdDoJump();
                }
            }

            /*
            //If local player then check inputs - On input send animation name to server
            if (isLocalPlayer)
            {
                if (Input.GetKey(KeyCode.LeftShift) && Input.GetKeyDown(KeyCode.W))
                {
                    CmdUpdateAnimation("doRun");
                }
                else if (Input.GetKey(KeyCode.W))
                {
                   // OnAnimationChange("doRun");
                    CmdUpdateAnimation("doRun");
                }
                else if (Input.GetKeyDown(KeyCode.Space))
                {
                    CmdUpdateAnimation("doJump");
                }
                else if (Input.GetKey(KeyCode.D))
                {
                    CmdUpdateAnimation("doRun");
                }
                else
                {
                   CmdUpdateAnimation("doIdle");
                }
             }*/
        }

        [Command]
        private void CmdDoJump()
        {
            jumpToggle = !jumpToggle;

        }

        public void OnJump(bool v)
        {
            if (!isLocalPlayer)
            {
                if (anim == null) return;
                if (!doAnimation) return;

                anim.SetTrigger("DoJump");
            }
        }

        [Command]
        private void CmdUpdateBlend(float h, float v)
        {
            if (anim == null) return;

            currentHorizontal = h;
            currentVertical = v;
            //anim.SetFloat("ForwardBlend", v);
            //anim.SetFloat("StrafeBlend", h);
        }

        [Command]
        private void CmdUpdateSprint(string val, bool state)
        {
            if (anim == null) return;
            currentAnimation = val;
          //  anim.SetBool(val, state);
        }

        public void OnAnimationChange(string val)
        {
            if (anim == null) return;
            if (!doAnimation) return;

            if (!isLocalPlayer)
            {
                currentAnimation = val;
                //Play new animation

                if (currentAnimation == "isSprinting")
                {
                    anim.SetBool(currentAnimation, anim.GetBool("isSprinting"));
                }
                else
                {
                   
                    anim.SetTrigger(val);
                }
                lastAnim = currentAnimation;

            }
        }

        //Server side - used to update the animation on the player
        [Command]
        private void CmdUpdateAnimation(string val, bool state)
        {
            if (anim == null) return;

            currentAnimation = val;
            //if (!string.IsNullOrEmpty(lastAnim))
            anim.SetTrigger(lastAnim);

          //  currentAnimation = val;
          //Player new animation
            //anim.SetBool(currentAnimation, state);
            //astAnim = currentAnimation;
        }



        //Execute super called - player super animation
        public void ExecuteSuper()
        {
            if (!doAnimation) return;
            if (isLocalPlayer)
            {
            //    CmdUpdateAnimation("DoUlt", true);
         //       CmdUpdateAnimation("super");
            }
        }

        public void DoGrenade(Action callback)
        {
            if (!doAnimation) return;

            if (isLocalPlayer)
            {
            //    CmdUpdateAnimation("DoGrenade", true);
            }
        }
    }
}