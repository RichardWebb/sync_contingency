﻿/*
 * Projectile view
 * This class handles the shooting projectiles
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System;
using DG.Tweening;
using pulsar.view;
using pulsar.view.characters.attack;

namespace puslar.view.characters.additional
{
    [NetworkSettings(channel = 0, sendInterval = 0.1f)]
    public class ProjectileView : NetworkBehaviour
    {
        //Network var sync pos vector
        [SyncVar(hook = "SyncPositionValues")]
        private Vector3 syncPos;

        [SyncVar(hook = "SyncExplosion")]
        private bool doExplosion = false;

        [SerializeField]
        private GameObject explosionObj;

        [SerializeField]
        private bool lookAtEndPoint;

        [SerializeField]
        private GameObject core;

        private bool _hasAuth = false;
        private bool stop = false;
        private TrailRenderer trail;

        private Vector3 endPoint;
        private bool startLooking = false;

        private void Awake()
        {
            core.SetActive(false);
        }

        private IEnumerator EnableCore()
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            core.SetActive(true);

        }

        private void Start()
        {

            //Checking if we have authority to server
            if (hasAuthority)
            {
                core.SetActive(true);
                IAttackInputListener attackView = PlayerSettings.localPlayerSettings.gameObject.GetComponent<IAttackInputListener>();
                if (attackView != null)
                {
                    transform.position = attackView.spawnPoint;
                }

                //Fire projectile
                Launch();
            }
            else
            {
                core.SetActive(false);
                StartCoroutine(EnableCore());

                trail = GetComponent<TrailRenderer>();
                if (trail != null)
                {
                    trail.enabled = false;
                    StartCoroutine(EnableTrail());
                }
            }
        }

        //Couroutine to enable trail renderer
        private IEnumerator EnableTrail()
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            trail.enabled = true;
        }

        //Start init the launch (server projectile)
        public void Init()
        {
            //Fire projectile
            _hasAuth = true;
            core.SetActive(true);
            Launch();
        }

        //Launch the project forward based on the main camera's forward
        private void Launch()
        {
            RaycastHit hit;
            var cam = PlayerSettings.localPlayerSettings.myCamera;
            float closeDistance = 10f;
            float duration = 0.25f;
            float quickFireDuration = 0.1f;

            

            if (Physics.Raycast(transform.position, cam.transform.forward, out hit, 100))
            {
                //Check if the shot needs to be sped up 
                float dis = Vector3.SqrMagnitude(hit.point - transform.position);
                if (dis < closeDistance) duration = quickFireDuration;

                endPoint = hit.point;
             /*   transform.DOMove(endPoint, duration).SetEase(Ease.Linear).OnComplete(()=> 
                {
                    if (explosionObj != null)
                    {
                        explosionObj.SetActive(true);
                    }
                });*/
            }
            else
            {
                endPoint = cam.transform.position + cam.transform.forward * 100;
              //  transform.DOMove(endPoint, duration).SetEase(Ease.Linear);
            }

            if (lookAtEndPoint) startLooking = true;

            transform.DOMove(endPoint, duration).SetEase(Ease.Linear).OnComplete(() =>
            {
                if (lookAtEndPoint) startLooking = false;

                if (explosionObj != null)
                {
                    explosionObj.SetActive(true);
                    CmdToggleExplosion(true);
                }
            });

            //    GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * 15000);
            StartCoroutine(Kill(duration+0.5f));
        }

        //Destroy the projectile after a time delay
        private IEnumerator Kill(float duration = 0.35f)
        {
            yield return new WaitForSeconds(duration);
            //yield return new WaitForSeconds(0.75f);
            stop = true;
            //Call server to destroy projectile
            CmdDestroyObj();
        }

        //Server side - destroys projectile
        [Command]
        private void CmdDestroyObj()
        {
            NetworkServer.Destroy(this.gameObject);
        }

        [Command]
        private void CmdToggleExplosion(bool state)
        {
            doExplosion = true;
            
        }

        private void Update()
        {
            //If dummy object - lerp to last know position
            if (!hasAuthority && !_hasAuth)
            {
                transform.position = Vector3.Lerp(transform.position, syncPos, Time.deltaTime * 16);
            }

            if (lookAtEndPoint)
            {
                if (startLooking)
                {
                    transform.LookAt(endPoint);
                }
            }
        }

        private void FixedUpdate()
        {
            //Send position until told to stop
            if (!stop)
                SendPosition();
        }

        //Server side update last position
        [Command]
        void CmdProvidePositionToServer(Vector3 pos)
        {
            syncPos = pos;
        }

        //Send the position to the server
        private void SendPosition()
        {
            //!islocalplayer
            if (hasAuthority || _hasAuth)
            {
                CmdProvidePositionToServer(transform.position);
            }
        }


        private void SyncExplosion(bool val)
        {
            doExplosion = val;
            if (!hasAuthority && !_hasAuth)
            {
                if (explosionObj != null)
                {
                    explosionObj.SetActive(true);
                }
            }

        }

        //Client side update last known position
        private void SyncPositionValues(Vector3 lastPos)
        {
            syncPos = lastPos;
        }
    }
}