﻿/*
 * Melee view
 * This class handles the melee attacks of players
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using pulsar.view.interfaces;
using System;
using Robotlegs.Bender.Extensions.Mediation.API;
using Robotlegs.Bender.Extensions.EventManagement.API;
using Robotlegs.Bender.Extensions.EventManagement.Impl;
using pulsar.events;
using pulsar.view.characters.shooting;
using pulsar.networksync;
using pulsar.view.characters.attack;
using Robotlegs.Bender.Extensions.ViewManagement.API;

namespace pulsar.view.characters.additional
{
    public class MeleeView : NetworkBehaviour, ISuperListener, IActionListener, IGameListener
    {
        //Config var - camera
        [SerializeField]
        private Transform camTransform;

        //timings 
        private float timeBetweenAttacks = 2f;
        private int damage = 35;
        private float maxRange = 2.5f;
        private float currentTimeBetweenAttacks = 0;
        private bool meleeAvaiable = false;
        private bool locked;

        private PlayerSettings settings;
        private Player_Health[] playersHealth;
        private GameObject hitText;

        //robotlegs setup
        public event Action<IView> RemoveView;
        private IEventDispatcher _dispatcher = new EventDispatcher();
        private Player_Health healthView;
        private PlayerAttackManagerView attackManager;

        [SerializeField]
        private LayerMask layerMask;

        [SerializeField]
        private Animator anim;

        public IEventDispatcher dispatcher
        {
            get
            {
                return _dispatcher;
            }
            set
            {
                _dispatcher = value;
            }
        }

        void Start()
        {
            ViewNotifier.RegisterView(this);
            //Getting the settings
            settings = GetComponent<PlayerSettings>();
            //getting the hit text to spawn
            hitText = Resources.Load<GameObject>("Game/HitText");
            //Getting the player health
          //  StartCoroutine(GetPlayersHealth());
            attackManager = GetComponent<PlayerAttackManagerView>();
            healthView = GetComponent<Player_Health>();
        }

        public override void OnStartLocalPlayer()
        {
            base.OnStartLocalPlayer();
            //hooking health respawn 
            healthView = GetComponent<Player_Health>();
            healthView.EventRespawn += HandleRespawn;
        }

        private IEnumerator GetPlayersHealth()
        {
            yield return new WaitForSeconds(2);
        }

        void Update()
        {
            if (!isLocalPlayer) return;
            if (locked) return;
            if (healthView.isDead) return;
            if (attackManager.usingSuper) return;

            meleeAvaiable = (currentTimeBetweenAttacks >= timeBetweenAttacks);

            //if local player check melee avaiable 
            if (meleeAvaiable)
            {
                //Check input and do attack
                //if (Input.GetKeyUp(KeyCode.V))
                if (Input.GetButtonUp("Melee"))
                {
                    DoMelee();
                }
            }
            else
            {
                currentTimeBetweenAttacks += Time.deltaTime;
            }
        }

        //Do melee atack 
        private void DoMelee()
        {
            if (anim != null)
            {
                anim.SetTrigger("DoMeleeAttack");
            }

            locked = true;
            currentTimeBetweenAttacks = 0;
            RaycastHit hit;
            //Short ray infront of player
            Debug.DrawRay(camTransform.TransformPoint(0, 0, 0.5f), camTransform.TransformPoint(0, 0, 0.5f + maxRange), Color.red);
            if (Physics.Raycast(camTransform.TransformPoint(0, 0, 0.5f), camTransform.forward, out hit, maxRange, layerMask))
            {
                //if hit a player 
                if (hit.transform.tag.Contains("Player"))
                {
                    //if an enemy
                    var isHeadShot = hit.transform.tag.Contains("Head");
                    PlayerSettings enemySettings = isHeadShot ? hit.transform.gameObject.GetComponentInParent<PlayerSettings>() : hit.transform.gameObject.GetComponent<PlayerSettings>();

                    if (enemySettings != null)
                    {
                        if (settings.teamName != enemySettings.teamName)
                        {
                            string uIdentity = hit.transform.name;
                            Player_Health targetHealth = isHeadShot ? hit.transform.gameObject.GetComponentInParent<Player_Health>() : hit.transform.gameObject.GetComponent<Player_Health>();
                            if (targetHealth.health <= 0) return;

                            //Check if player will die
                            if (targetHealth.CheckIfPlayerWillDie(damage))
                            {
                                string playerName = enemySettings.playerName;

                                if (!string.IsNullOrEmpty(playerName))
                                {
                                    //Inform application player was killed
                                    dispatcher.Dispatch(new PlayerActionEvent(PlayerActionEvent.Type.KILLED_A_PLAYER, playerName));
                                }
                            }

                            //Create hit text - displays on HUD
                            GameObject _hitText = (GameObject)Instantiate(hitText);
                            _hitText.transform.SetParent(settings.hudCanvas.transform);

                            int randomVal = UnityEngine.Random.Range(0, 4);
                            bool isCrit = randomVal >= 3;

                            _hitText.GetComponent<PlayerHitTextView>().Init(isCrit, damage, hit.point.z, false);
                            //Update server who was hit.
                            CmdTellSeverWhoWasHit(uIdentity, damage, settings.playerName, gameObject.name);
                        }

                    }

                }
            }
            locked = false;
            currentTimeBetweenAttacks = 0;
        }

        //Server side  - updates server who was hit
        [Command]
        void CmdTellSeverWhoWasHit(string uniqueID, int dmg, string attacker, string attackerUniID)
        {
            Player_Health _target = null;
            Player_Health _attacker = null;

            if (playersHealth == null || playersHealth.Length < 1) playersHealth = GameObject.FindObjectsOfType<Player_Health>();

            //get the player and target
            foreach (Player_Health player in playersHealth)
            {
                string name = player.gameObject.name;
                if (name == uniqueID)
                {
                    _target = player;
                }

                if (name == attackerUniID)
                {
                    _attacker = player;
                }
            }

            _target.lastPersonToHitMe = attacker;
            _target.DeductHealth(dmg);

            //Update scores if health player dies
            if (_target.health <= 0)
            {
                PlayerSettings settings = _attacker.gameObject.GetComponent<PlayerSettings>();
                if (settings.teamName == "red")
                {
                    dispatcher.Dispatch(new ScoreEvent(ScoreEvent.Type.UPDATE_TEAM1_SCORE, 1));
                }
                else
                {
                    dispatcher.Dispatch(new ScoreEvent(ScoreEvent.Type.UPDATE_TEAM2_SCORE, 1));
                }

                _attacker.gameObject.GetComponent<PlayerStatsView>().UpdateKillCount(1);
            }
        }

        private void HandleRespawn()
        {
            locked = false;
        }

        public void ExecuteSuper()
        {
        }

        //Lock melee if player has died
        public void YouDied(string name)
        {
            if (isLocalPlayer)
                locked = true;
        }

        public void KilledPlayer(string name)
        {
        }

        public void HandleGameStart()
        {
            playersHealth = GameObject.FindObjectsOfType<Player_Health>();
        }

        public void HandleGameFinished()
        {
        }
    }
}