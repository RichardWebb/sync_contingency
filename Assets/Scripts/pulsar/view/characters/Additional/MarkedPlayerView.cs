﻿/*
 * Marked player view
 * This script manages the mark player visual 
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

namespace pulsar.view.characters.additional
{
    public class MarkedPlayerView : NetworkBehaviour
    {
        [SerializeField]
        private GameObject markedRing;

        //Network var - setting the player marked.
        [SyncVar(hook = "OnMarkedChanged")]
        private bool isMarked;

        //How long the player is marked for if they stay alive.
        private float markedDuration = 30;
        private Coroutine currentRoutine;

        //Property for marked player
        public bool PlayerMarked
        {
            get
            {
                return isMarked;
            }
        }

        //Calls the server to mark the player
        public void MarkPlayer()
        {
            CmdSetMarked(true);
        }

        //Server side - set the player marked
        [Command]
        private void CmdSetMarked(bool value)
        {
            isMarked = value;
        }

        //Client side - sets the player marked
        [Client]
        private void OnMarkedChanged(bool value)
        {
            isMarked = value;

            //Toggle visuals
            if (!isLocalPlayer)
                markedRing.SetActive(isMarked);
            else
            {
                if (isMarked)
                    currentRoutine = StartCoroutine(StartResetTimer());
                else
                    Debug.Log("Cleared UI text if  there was any");
            }
        }

        //Timer to reset the marked player
        private IEnumerator StartResetTimer()
        {
            yield return new WaitForSeconds(markedDuration);
            CmdSetMarked(false);
        }

        //Calls the server to clear the marked player
        public void ClearMarked()
        {
            if (currentRoutine != null) StopCoroutine(currentRoutine);

            CmdSetMarked(false);
        }
    }
}