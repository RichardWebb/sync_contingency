﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace pulsar.view.characters.additional
{
    public class TargeterView : MonoBehaviour
    {

        private GameObject dummyObj;
        private float range;
        private Vector3 offset;

        [SerializeField]
        private LayerMask lMask;
        private Vector3 placePos;
        private bool trgting;

        private GameObject cam;
        private Ray ray;
        private RaycastHit hit;

        // Use this for initialization
        void Start()
        {
            cam = GetComponentInChildren<Camera>().gameObject;
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            if (trgting)
            {
                ray = new Ray(cam.transform.position, cam.transform.forward);
                Vector3 dir = cam.transform.forward * range;

                if (Physics.Raycast(ray, out hit, range, lMask))
                {
                    if (hit.collider.tag.Contains("Wall"))
                    {
                        placePos = hit.point - (cam.transform.forward * 2);
                        if (Physics.Raycast(placePos, Vector3.down, out hit, 10f))
                        {
                            if (hit.collider.tag.Contains("Floor"))
                            {
                                placePos = hit.point + offset;
                            }
                        }
                    }
                    else if (hit.collider.tag.Contains("Floor"))
                    {
                        placePos = hit.point + offset;
                    }
                }

                else
                {
                    if (Physics.Raycast(cam.transform.position + dir, Vector3.down, out hit, 10f))
                    {
                        if (hit.collider.tag.Contains("Floor"))
                        {
                            placePos = hit.point + offset;
                        }
                    }
                }

                dummyObj.transform.position = placePos;

                Vector3 target = new Vector3(transform.position.x, dummyObj.transform.position.y, transform.position.z);
                dummyObj.transform.LookAt(target);

            }
        }

        public void startTarget(GameObject go, float rng, Vector3 off)
        {
            placePos = Vector3.zero;
            dummyObj = go;
            dummyObj.SetActive(true);
            dummyObj.transform.SetParent(null);
            range = rng;
            offset = off;
            trgting = true;
        }

        public KeyValuePair<Vector3, Quaternion> GetTargetTransform
        {
            get
            {
                KeyValuePair<Vector3, Quaternion> z = new KeyValuePair<Vector3, Quaternion>(dummyObj.transform.position, dummyObj.transform.rotation);
                StopTarget();
                return z;
            }
        }

        public void StopTarget()
        {
            dummyObj.SetActive(false);
            dummyObj.transform.SetParent(this.transform);
            dummyObj.transform.position = new Vector3(0, -2000, 0);
            trgting = false;
        }
    }
}