﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using pulsar.networksync;
using UnityStandardAssets.Characters.FirstPerson;
using pulsar.view.characters.attack;
using pulsar.view.characters.xenith;
using System;

namespace pulsar.view.characters.additional
{
    public class PortalInteractionView : NetworkBehaviour
    {
        [SyncVar(hook = "OnTargetChange")]
        private Vector3 targetPos;

        private PlayerSettings mySettings;
        private Player_SyncPosition posSync;
        private Player_SyncRotation rotSync;
        private FirstPersonController controller;
        private PlayerAttackManagerView attackManager;

        private bool ableToUsePortal = true;

        private float downTime = 2f;

        // Use this for initialization
        void Start()
        {
            mySettings = GetComponent<PlayerSettings>();
            posSync = GetComponent<Player_SyncPosition>();
            rotSync = GetComponent<Player_SyncRotation>();
            controller = GetComponent<FirstPersonController>();
            attackManager = GetComponent<PlayerAttackManagerView>();
        }

        // Update is called once per frame
        void Update()
        {

        }

        void OnTriggerEnter(Collider col)
        {
            if (!isLocalPlayer) return;
            if (!ableToUsePortal) return;

            if (col.tag == "Portal")
            {
                ableToUsePortal = false;
                PortalCamView portalView = col.gameObject.GetComponent<PortalCamView>();
                Vector3 end = portalView.pair.ExitPoint;

                controller.enabled = false;
                attackManager.busy = true;

                CmdUpdatePosition(end);
            }
        }

        [Command]
        private void CmdUpdatePosition(Vector3 pos)
        {
            targetPos = pos;
        }

        private void OnTargetChange(Vector3 v)
        {
            if (mySettings.VRHasControl) return;

            if (!isLocalPlayer)
            {
                if (!mySettings.VRHasControl)
                {
                    posSync.stop = true;
                    rotSync.stop = true;
                }
            }
            else
            {
                StartCoroutine(ReEnableUseOfPortal());
                controller.enabled = true;
                attackManager.busy = false;
            }

            transform.position = v;

            if (!isLocalPlayer)
            {
                if (!mySettings.VRHasControl)
                {
                    posSync.stop = false;
                    rotSync.stop = false;
                }
            }
        }

        private IEnumerator ReEnableUseOfPortal()
        {
            yield return new WaitForSeconds(downTime);
            ableToUsePortal = true;
        }
    }
}