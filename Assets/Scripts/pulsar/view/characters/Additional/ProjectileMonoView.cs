﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using System;

namespace pulsar.view.characters.additional
{
    public class ProjectileMonoView : MonoBehaviour
    {
        [SerializeField]
        private GameObject explosionObj;

        [SerializeField]
        private float duration = 0.25f;

        [SerializeField]
        private LayerMask layerMask;

        [SerializeField]
        private TrailRenderer trailRenderer;

        public void Launch(Transform camTransform, float range, bool lookAtTarget = false, float destroyDelay = 0.3f, bool trySetParent = false, Action<Vector3> callback = null)
        {
            RaycastHit hit;
            var endPoint = Vector3.zero;

            if (Physics.Raycast(transform.position, camTransform.forward, out hit, range, layerMask) )
            {
                var dis = Vector3.Distance(hit.point, transform.position);
                duration = (dis / range) * duration;

                endPoint = hit.point;

                if (trySetParent)
                {
                    transform.SetParent(hit.transform);
                }
            }
            else
            {
                endPoint = camTransform.position + camTransform.forward * range;
            }

            if (trailRenderer != null) trailRenderer.enabled = true;
            transform.DOMove(endPoint, duration).SetEase(Ease.Linear).OnComplete(() =>
            {
                if (explosionObj != null)
                {
                    explosionObj.SetActive(true);

                }

                if (callback != null)
                {
                    callback(endPoint);
                }

                Destroy(this.gameObject, destroyDelay);


            }).OnUpdate(() =>
            {
                if (lookAtTarget) transform.LookAt(endPoint);
            });
        }
    }
}