﻿/*
 * Ultimate view 
 * This class the input and cooldowns for the ultimate abilities for characters
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System;
using pulsar.view.interfaces;
using Robotlegs.Bender.Extensions.Mediation.API;
using Robotlegs.Bender.Extensions.EventManagement.API;
using Robotlegs.Bender.Extensions.EventManagement.Impl;
using Robotlegs.Bender.Extensions.ViewManagement.API;
using pulsar.events;
using DG.Tweening;
using pulsar.networksync;
using pulsar.view.characters.attack;

namespace pulsar.view.characters.super
{
    public class UltimateView : NetworkBehaviour, ISuperListener, IActionListener, IRiftListener, IGameListener, IPlayerData
    {
        //Network var - syncing super time change
        [SyncVar(hook = "OnSuperTimeChange")] 
        public float timeUntilSuper = 0;

        //Config vars
        [SerializeField]
        public int superCooldown = 120;

        private int killValue = 15;

        [SerializeField]
        private bool hasCameraPan = false;

        [SerializeField]
        private Camera mainCamera;

   //     [SerializeField]
   //     private Camera subcamera;

        [SerializeField]
        private float maxDistance = 5;

        private bool superAvaible = false;

        private bool superInUse = false;


        [SerializeField]
        private float manaCost;

        //robotlegs setup
        public event Action<IView> RemoveView;
        private IEventDispatcher _dispatcher = new EventDispatcher();
        public IEventDispatcher dispatcher
        {
            get
            {
                return _dispatcher;
            }
            set
            {
                _dispatcher = value;
            }
        }
        public bool holdingRift
        {
            get;set;
        }

        private bool isDead;
        private Player_Health healthView;
        private IAttackManager attackManager;

        void Start()
        {
            //Hook mediators
            ViewNotifier.RegisterView(this);
            healthView = GetComponent<Player_Health>();
            attackManager = GetComponent<IAttackManager>();
        }

        //On local player start
        public override void OnStartLocalPlayer()
        {
            base.OnStartLocalPlayer();
            //set timer
            timeUntilSuper = 0;                 

            //Listen for respawn eventsss
            healthView = GetComponent<Player_Health>();
            healthView.EventRespawn += HandleRespawn;
        }

        //Method for cooldown 
        private IEnumerator RunCounter()
        {
            while (true)
            {
                yield return new WaitForSeconds(1);

                if (!superInUse)
                {
                    if (timeUntilSuper < superCooldown)
                    {
                        timeUntilSuper++;
                        superAvaible = timeUntilSuper == superCooldown;
                    }
                }
            }
        }

        void Update()
        {
            //If local player and super is avaiable check input - then do super
            if (holdingRift) return;
            if (healthView.isDead) return;

            if (isLocalPlayer)
            {
                if (!superAvaible) return;

                if (!superInUse && !isDead)
                {
                   // if (Input.GetKeyUp(KeyCode.Q))
                    if (Input.GetButtonUp("Ulti"))
                    {
                        if (attackManager.SpendMana(manaCost))
                            DoSuper();
                    }
                }
            }
        }

        private void DoSuper()
        {
            superInUse = true;
            superAvaible = false;
            timeUntilSuper = 0;

            //Informs application that super has been activated
            dispatcher.Dispatch(new SuperEvent(SuperEvent.Type.EXECUTE_SUPER));
        }

        //Used to reduce timer after a kill TODO hook this to kill events
        public void ReduceTimer()
        {
            if (superInUse) return;
            timeUntilSuper += killValue;
            timeUntilSuper = timeUntilSuper > superCooldown ? superCooldown: timeUntilSuper;
            superAvaible = timeUntilSuper == superCooldown;
            //timeUntilSuper -= killValue;
         //   timeUntilSuper = timeUntilSuper < 0 ? 0 : timeUntilSuper;
          //  superAvaible = timeUntilSuper == 0;
        }


        //Reset cooldown of super
        public void ResetSuper()
        {
            timeUntilSuper = 0;
        }

        //Enable super - mainly used from killing a marked player
        public void RefreshSuper()
        {
            CmdUpdateTimer(0);
        }


        //Client side - called when super time changed
        [Client]
        public void OnSuperTimeChange(float value)
        {
            timeUntilSuper = value;
            superAvaible = timeUntilSuper == superCooldown;
        }

        //Sevrer side - called to update time until super
        [Command]
        private void CmdUpdateTimer(int value)
        {
            timeUntilSuper = value;
        }

        //Called via super event - used to reset timer
        public void ExecuteSuper()
        {
            //Reset timer after delay
            StartCoroutine(RestartSuper());
        }

        //Reset super cooldown
        private IEnumerator RestartSuper()
        {
            yield return new WaitForSeconds(3);
            superInUse = false;
            timeUntilSuper = 0;
        }

        //Disable this view if player has died
        public void YouDied(string name)
        {
            isDead = true;
        }

        //Enable this view if player has respawned
        private void HandleRespawn()
        {
            isDead = false;
        }

        public void KilledPlayer(string name)
        {
        }

        public void HandleGameStart()
        {
            if (isLocalPlayer)
            {
                //If camera pan on super active 
              /*  if (hasCameraPan)
                {
                    //Add camera super view - used for panning
                    var view = gameObject.AddComponent<CameraSuperView>();
                    //init camera movement with cameras
                    view.Init(mainCamera, subcamera, maxDistance, 1.5f);
                }*/

                //Start super cooldown
                StartCoroutine(RunCounter());
            }
        }

        public void HandleGameFinished()
        {
        }

        public void GotPlayerData(PlayerVO playerData)
        {
            if (GetComponent<PlayerSettings>().characterName == playerData.characterName)
            {
                superCooldown = playerData.ultCooldown;
                timeUntilSuper = 0;

            }
        }
    }
}
