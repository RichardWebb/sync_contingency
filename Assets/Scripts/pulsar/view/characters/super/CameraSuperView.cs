﻿/*
 * Camera super class
 * This class handles the camera pan on players super ability use.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using Robotlegs.Bender.Platforms.Unity.Extensions.Mediation.Impl;
using pulsar.view.interfaces;
using System;
using DG.Tweening;
using UnityStandardAssets.Characters.FirstPerson;
using puslar.view.characters.additional;

namespace pulsar.view.characters.super
{
    public class CameraSuperView : EventView, ISuperListener, IActionListener
    {
        private GameObject myTarget;
        private Vector3 myTargetPoint;
        private Vector3 startPoint;
        private Tweener currentRoutine;

        private FirstPersonController controller;

        private Camera mainCamera;
        private Camera movingCamera;
        private float maxDistance;
        private AnimationSyncView syncView;
        private float superDuration;


        protected override void Start ()
        {
            //Hook mediators
            base.Start();
            //Get start point 
            startPoint = transform.localPosition;
	    }

        // Init for the view - used to set up the move points and getting references
        public void Init(Camera mainCamera, Camera movingCamera, float maxDistance, float duration)
        {
            this.mainCamera = mainCamera;
            this.movingCamera = movingCamera;
            this.maxDistance = maxDistance;
            this.superDuration = duration;
            startPoint = movingCamera.transform.localPosition;
            controller = GetComponent<FirstPersonController>();
            syncView = GetComponent<AnimationSyncView>();
        }

        public void ExecuteSuper()
        {
            movingCamera.transform.localPosition = mainCamera.transform.localPosition;
            movingCamera.transform.localRotation = mainCamera.transform.localRotation;
            controller.enabled = false;
            var endPoint = GetEndPoint();
            mainCamera.enabled = false;
            movingCamera.enabled = true;
            syncView.ToggleHidden(true);

            StartCoroutine(ReEnable());
            StartCoroutine(HideBody());
            var returnPoint = movingCamera.transform.localPosition;

            currentRoutine = movingCamera.transform.DOMove(endPoint, superDuration * 0.25f).SetEase(Ease.Linear).OnComplete(() =>
              {
                  movingCamera.transform.DOLocalMove(returnPoint, superDuration * 0.75f).SetEase(Ease.Linear).SetDelay(0.2f).OnUpdate(() =>
                  {
                    // movingCamera.transform.LookAt(mainCamera.transform);
                });
              });

            /*
            //Executing super - perform camera pan in and out.
            if (myTarget != null)
            {
                controller.enabled = false;
                currentRoutine = transform.DOLocalMove(myTargetPoint, 0.25f).SetEase(Ease.Linear).OnComplete(()=> 
                {
                    transform.DOLocalMove(startPoint, 0.25f).SetDelay(1).OnComplete(()=> 
                    {
                        controller.enabled = true;
                    });
                });
            }*/
        }

        private IEnumerator HideBody()
        {
            yield return new WaitForSeconds(superDuration * 0.95f);
            syncView.ToggleHidden(false);
        }

        private IEnumerator ReEnable()
        {
            yield return new WaitForSeconds(superDuration+0.25f);
            movingCamera.enabled = false;
            mainCamera.enabled = true;
            controller.enabled = true;
        }

        private Vector3 GetEndPoint()
        {
            var units = maxDistance;
            // float duration = 0.75f;
            RaycastHit[] hits = Physics.RaycastAll(mainCamera.transform.position, mainCamera.transform.forward, maxDistance);

            float furthestWall = 0;
            foreach (RaycastHit hit in hits)
            {
                string _tag = hit.collider.gameObject.tag;

                if (_tag == "Wall" || _tag == "Floor")
                {
                    float dis = Vector3.Distance(hit.point, mainCamera.transform.position);

                    if (dis > furthestWall)
                    {
                        furthestWall = dis;

                        if (dis < maxDistance)
                        {
                            units = dis * 0.7f;
                         //   endPoint = Camera.main.transform.position + Camera.main.transform.forward * (dis * 0.95f);
                        }
                    }
                    break;
                }
            }

            var endPoint = mainCamera.transform.position + (-mainCamera.transform.forward) * units;
            endPoint = new Vector3(endPoint.x, endPoint.y + 3f, endPoint.z);
            return endPoint;
        }

        //Disbale controllers and any routines
        public void YouDied(string name)
        {
            if (controller != null) controller.enabled = false;
            if (currentRoutine != null)
                currentRoutine.Kill();
        }

        public void KilledPlayer(string name)
        {
        }
    }
}
