﻿/*
 * Psyonic grenade
 * This class handles the Psyonic's grenade ability
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using pulsar.view.characters.special;
using System;
using System.Collections.Generic;
using UnityEngine.Networking;

namespace pulsar.view.characters.psyonic
{
    [NetworkSettings(channel = 0, sendInterval = 0.1f)]
    public class PsyonicGrenade : NetworkBehaviour, IGrenadeObj
    {
        //network var - position syncing
        [SyncVar(hook = "SyncPositionValues")]
        private Vector3 syncPos;

        private Action<PlayerSettings[]> _callback;

        private bool armed = false;
        private bool isEnabled = false;
        private Rigidbody body;
        private PlayerSettings mySettings;
        private bool _hasAuth = false;
        private bool stop = false;
        private BoxCollider triggerCol;
        private Coroutine currentRoutine;
        private float force = 1500;
        private float threshold = 0.1f;
        private Vector3 lastPos;

        private void Awake()
        {
            //get my settings.
            mySettings = PlayerSettings.localPlayerSettings;
            triggerCol = GetComponent<BoxCollider>();
        }

        void Start()
        {
            //Check authority and if pass add force
            if (hasAuthority && !mySettings.isServer)
            {
                GetComponent<Rigidbody>().AddForce(PlayerSettings.localPlayerSettings.myCamera.transform.forward * force);

                body = GetComponent<Rigidbody>();
                body.useGravity = true;
                StartCoroutine(SetReady());
                currentRoutine = StartCoroutine(DestroyMe());
            }
        }

        //Destroy grenade after delay
        private IEnumerator DestroyMe()
        {
            yield return new WaitForSeconds(20);
            CmdDestroyGrenade();
        }

        //Server side destroy grenade object
        [Command]
        private void CmdDestroyGrenade()
        {
            NetworkServer.Destroy(this.gameObject);
        }

        //Init the view adds force - called from server
        public void Init()
        {
            _hasAuth = true;
            GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * force);

            body = GetComponent<Rigidbody>();
            body.useGravity = true;

            StartCoroutine(SetReady());
        }

        //Toggle the grenade ready
        private IEnumerator SetReady()
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();

            triggerCol.enabled = true;
            isEnabled = true;
        }

        //Method to arm the grenade
        private IEnumerator Arm()
        {
            yield return new WaitForSeconds(1.5f);

            triggerCol.size = new Vector3(6, 8, 6);
            armed = true;
        }

        //Clear rigidbody
        private void ClearRigidBody()
        {
            if (body != null)
            {
                body.velocity = Vector3.zero;
                body.angularVelocity = Vector3.zero;
                Destroy(body);
            }

            if (triggerCol != null) triggerCol.enabled = false;
        }

        private void CheckNotArmed(Collider col)
        {
            // if not armed yet 
         //   if (!armed)
         //   {
                //if hit player then stick to x player and kill
                if (col.tag.ToLower().Contains("player"))
                {
                    PlayerSettings _s = col.gameObject.GetComponent<PlayerSettings>();
                    if (_s == null) _s = col.gameObject.GetComponentInParent<PlayerSettings>();

                    if (_s.Equals(mySettings)) return;
                    if (_s != null)
                    {
                        if (mySettings.teamName != _s.teamName)
                        {
                            isEnabled = false;
                            ClearRigidBody();
                            transform.SetParent(col.gameObject.transform);
                            StartCoroutine(DoTimedExplosion());
                            return;
                        }
                    }
                }
                else
                {
                    //Hit object - clear body and set rotation
                    ClearRigidBody();
                    transform.rotation = Quaternion.Euler(col.gameObject.transform.up);
                }

                //Armed the grenade
                StartCoroutine(Arm());
                return;
            //}
        }

        private void CheckArmed(Collider col)
        {
            //if armed
          //  if (armed)
         //   {
                //if hit player
                if (col.tag.ToLower().Contains("player"))
                {
                    isEnabled = false;
                    //Get all hits
                    var _players = DoCast();

                    //if players hit - damage them
                    if (_players != null && _players.Length > 0)
                        mySettings.gameObject.GetComponent<PsyonicAttackView>().HitPlayersWithGrenade(_players);


                    if (currentRoutine != null) StopCoroutine(currentRoutine);
                    stop = true;

                    //Destroy the grenade
                    CmdDestroyGrenade();
                }
           // }
        }

        //Collision check
        void OnTriggerEnter(Collider col)
        {
            if (!isEnabled) return;
            if (!_hasAuth)
            {
                if (!hasAuthority) return;
            }

            if (!armed)
            {
                CheckNotArmed(col);
            }
            else
            {
                CheckArmed(col);
            }
           

       
        }

        private void FixedUpdate()
        {
            //Send position to server
            if (!stop)
                SendPosition();
        }

        private void Update()
        {
            //if dummy then lerp to last known position
            if (!hasAuthority && !_hasAuth)
            {
                transform.position = Vector3.Lerp(transform.position, syncPos, Time.deltaTime * 16);
            }
        }

        //Update server with last position
        [Command]
        void CmdProvidePositionToServer(Vector3 pos)
        {
            syncPos = pos;
        }

        //Sent position to the server
        private void SendPosition()
        {
            if (hasAuthority || _hasAuth)
            {
                if (Vector3.Distance(transform.position, lastPos) > threshold)
                    CmdProvidePositionToServer(transform.position);
            }
        }

        //Client side update last position
        [Client]
        private void SyncPositionValues(Vector3 lastPos)
        {
            syncPos = lastPos;
        }

        //Get all hits around the grenade
        private PlayerSettings[] DoCast()
        {
            RaycastHit[] hits = Physics.SphereCastAll(transform.position, 5f, transform.forward, 25f);

            List<PlayerSettings> settings = new List<PlayerSettings>();

            foreach (RaycastHit hit in hits)
            {
                if (hit.collider.tag.ToLower().Contains("player"))
                {
                    PlayerSettings _settings = hit.collider.gameObject.GetComponent<PlayerSettings>();

                    if (_settings == null) _settings = hit.collider.gameObject.GetComponentInParent<PlayerSettings>();

                    if (_settings != null)
                    {
                        if (!settings.Contains(_settings))
                            settings.Add(_settings);
                    }
                }
            }
            //Return all hit players
            return ( (settings != null) && (settings.Count > 0) ) ? settings.ToArray() : null;
        }

        //Method for timed explosion - when grenade is stuck on a player
        private IEnumerator DoTimedExplosion()
        {
            yield return new WaitForSeconds(1.5f);

            //Get all hits 
            var _players = DoCast();

            //Hit players if there are any
            if (_players != null && _players.Length > 0)
                mySettings.gameObject.GetComponent<PsyonicAttackView>().HitPlayersWithGrenade(_players);

            if (currentRoutine != null) StopCoroutine(currentRoutine);
            stop = true;
            //Destroy Grenade
            CmdDestroyGrenade();
        }

        public void Init(Action<PlayerSettings[]> callback, PlayerSettings settings)
        {
        }
    }
}