﻿/*
 * Psyonic Super Two,
 * This class handles the blast ray of psyonic super - Kills all enemies in a line.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityStandardAssets.Characters.FirstPerson;
using System;
using System.Collections.Generic;

namespace pulsar.view.characters.psyonic
{
    public class PsyonicSuperTwo : NetworkBehaviour
    {
        [SyncVar(hook = "OnEndPointChanged")]
        private Vector3 superEndPoint;

        private FirstPersonController controller;

        [SerializeField]
        private PsyonicLineRendererView lineRendererView;

        [SerializeField]
        private Animator anim;

        [SerializeField]
        private GameObject muzzleObj;

        [SerializeField]
        private Camera myCamera;

        private float maxUnits = 60;

        private PsyonicAttackView attackView;

        [SerializeField]
        private LayerMask layerMask;

        [SerializeField]
        private LayerMask playerLayer;

	    // Use this for initialization
	    void Start ()
        {
            attackView = GetComponent<PsyonicAttackView>();
            controller = GetComponent<FirstPersonController>();
	    }

        public void DoSuper()
        {
            StartCoroutine(DoSequence());
        }
	
        private IEnumerator DoSequence()
        {
            controller.enabled = false;
            muzzleObj.SetActive(true);

            Vector3 target = GetTarget();
            CmdUpdateTargetPos(target);

            lineRendererView.ToggleLine(target);
            yield return new WaitForSeconds(0.1f);
            lineRendererView.DoSuper(target, null, 0f);

            if (isLocalPlayer)
                HitPlayers(target);

            StartCoroutine(DisableMuzzle());
        }

        //Hit player
        private void HitPlayers(Vector3 target)
        {
            float dis = Vector3.Distance(myCamera.transform.position, target);
            List<PlayerSettings> targets = new List<PlayerSettings>();
          

            List<Vector3> shotPoints = new List<Vector3>();

            var shotPoint = myCamera.transform.forward;
            shotPoints.Add(shotPoint);
            shotPoints.Add(new Vector3(shotPoint.x - 1f, shotPoint.y, shotPoint.z));
            shotPoints.Add(new Vector3(shotPoint.x - 1.25f, shotPoint.y, shotPoint.z));
            shotPoints.Add(new Vector3(shotPoint.x + 1.25f, shotPoint.y, shotPoint.z));
            shotPoints.Add(new Vector3(shotPoint.x + 1f, shotPoint.y, shotPoint.z));

            foreach (Vector3 v in shotPoints)
            {
                //Get all points
                RaycastHit[] hits = Physics.RaycastAll(myCamera.transform.position, myCamera.transform.forward, dis, playerLayer);
                foreach (RaycastHit hit in hits)
                {
                    var settings = hit.transform.gameObject.GetComponent<PlayerSettings>();

                    if (settings.teamName != PlayerSettings.localPlayerSettings.teamName)
                    {
                        if (!targets.Contains(settings))
                        {
                            targets.Add(settings);
                        }
                    }
                }
            }

            if (targets.Count > 0)
                HitPlayersWithSuper(targets.ToArray());
        }

        private IEnumerator DisableMuzzle()
        {
            yield return new WaitForSeconds(0.5f);
            muzzleObj.SetActive(false);
            controller.enabled = true;
        }

        private void HitPlayersWithSuper(PlayerSettings[] targets)
        {
            attackView.HitPlayersWithSuper(targets);
        }

        private Vector3 GetTarget()
        {           
            var endPoint = myCamera.transform.position + myCamera.transform.forward * maxUnits;

            RaycastHit hit;

            if (Physics.Raycast(myCamera.transform.position, myCamera.transform.forward, out hit, maxUnits, layerMask))
            {
                var dis = Vector3.Distance(myCamera.transform.position, hit.point) * 0.95f;
                endPoint = myCamera.transform.position + myCamera.transform.forward * dis;
            }
                       

           /* var hits = Physics.RaycastAll(myCamera.transform.position, myCamera.transform.forward, maxUnits, layerMask);

            float closestDis = 500;
            foreach (RaycastHit hit in hits)
            {
                string _tag = hit.collider.gameObject.tag;

                if (_tag == "Wall" || _tag == "Floor")
                {
                    float dis = Vector3.Distance(hit.point, myCamera.transform.position);
                    if (dis < closestDis)
                    {
                        closestDis = dis;

                        if (dis < maxUnits)
                        {
                            endPoint = myCamera.transform.position + myCamera.transform.forward * (dis * 0.99f);
                        }
                    }

                    break;
                }
            }*/

            return endPoint;
        }

        public void OnEndPointChanged(Vector3 pos)
        {
            superEndPoint = pos;

            if (!isLocalPlayer)
            {
                muzzleObj.SetActive(true);
                lineRendererView.ToggleLine(pos);
                StartCoroutine(DoDummyLineRenderer());
                GetComponent<PsyonicAttackView>().PlayDummySuperAudio();
            }
        }

        private IEnumerator DoDummyLineRenderer()
        {
            yield return new WaitForSeconds(0.5f);
            lineRendererView.ShowDummyLineRenderer(superEndPoint, 0f);
            StartCoroutine(DisableMuzzle());
        }

        [Command]
        private void CmdUpdateTargetPos(Vector3 target)
        {
            superEndPoint = target;
        }
    }
}
