﻿/*
 * Scope view
 * This class handles scoping in and out
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using DG.Tweening;
using pulsar.view.game;
using System;
using Robotlegs.Bender.Extensions.Mediation.API;
using Robotlegs.Bender.Extensions.EventManagement.API;
using Robotlegs.Bender.Extensions.EventManagement.Impl;
using Robotlegs.Bender.Extensions.ViewManagement.API;
using pulsar.events;
using pulsar.view.interfaces;

namespace pulsar.view.characters.psyonic
{
    public class ScopeView : MonoBehaviour, IScopeListener, IRiftListener
    {
        //current tweens
        private Tweener scopeAnimation;
        private Tweener superBarAnimation;

        //Config vars
        [SerializeField]
        private CanvasGroup scopeImageCanvasGroup;

        [SerializeField]
        private CanvasGroup superBarCanvasGroup;

        [SerializeField]
        private GameObject gun;

        private GameScoreManager scoreManager;

        //Robotlegs
        public event Action<IView> RemoveView;
        private IEventDispatcher _dispatcher = new EventDispatcher();
        public IEventDispatcher dispatcher
        {
            get
            {
                return _dispatcher;
            }
            set
            {
                _dispatcher = value;
            }
        }

        public bool holdingRift
        {
            get;set;
        }

        void Start()
        {
            //hook mediators
            ViewNotifier.RegisterView(this);

            StartCoroutine(GetScoreManager());
            scopeImageCanvasGroup.alpha = 0f;
       //     superBarCanvasGroup.alpha = 1f;
        }

        //Get score manager
        private IEnumerator GetScoreManager()
        {
            while (scoreManager == null)
            {
                yield return new WaitForSeconds(3f);
                scoreManager = GameObject.FindObjectOfType<GameScoreManager>();
            }
        }

        //handle scoping in
        public void _ScopeIn()
        {
            if (holdingRift) return;
            //inform listeners of scoping in
            dispatcher.Dispatch(new ScopeEvent(ScopeEvent.Type.SCOPED_IN));

            StartCoroutine(DoScopeAnimation(1f, 0f, 0.25f, 0.5f, 0.25f, 0f, false));
        }

        //Handle scoping out
        public void _ScopeOut()
        {
            if (holdingRift) return;
            //inform listeners of scoping out
            dispatcher.Dispatch(new ScopeEvent(ScopeEvent.Type.SCOPED_OUT));
            StartCoroutine(DoScopeAnimation(0f, 1f, 0.1f, 0.5f, 0f, 0.25f, true));
        }

        //Do the scope animation - fades HUD
        private IEnumerator DoScopeAnimation(float scopeEndValue, float hudValue, float scopeDuration, float superDuration, float scopeDelay, float superDelay, bool gunState)
        {
            if (scopeAnimation != null) scopeAnimation.Kill();
            if (superBarAnimation != null) superBarAnimation.Kill();

          
            scopeAnimation = scopeImageCanvasGroup.DOFade(scopeEndValue, scopeDuration).SetDelay(scopeDelay).SetEase(Ease.Linear);
            superBarAnimation = superBarCanvasGroup.DOFade(hudValue, superDuration).SetDelay(superDelay).SetEase(Ease.Linear);

            yield return new WaitForSeconds(0.15f);
            gun.SetActive(gunState);

        }

        public void ScopeIn()
        {
        }

        public void ScopeOut()
        {
        }
    }

}
