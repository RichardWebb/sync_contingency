﻿/*
 * Psyonic projectile grenade view
 * This class handles the launching of psyonic grenade
 * Created by Richard Webb,s1308033
 */
using UnityEngine;
using System.Collections;
using DG.Tweening;
using System;
using System.Collections.Generic;
using UnityEngine.UI;

namespace pulsar.view.characters.psyonic
{
    public class PsyonicProjectileGrenadeView : MonoBehaviour
    {
        private float moveDuration = 0.25f;

        private float maxUnits = 50f;

        [SerializeField]
        private LayerMask layerMask;

        private bool ready = false;

        private Action<PlayerSettings> callback;

        private string myTeamName;

        private bool hittingEnabled = false;
        private Coroutine timeOutRoutine;

        [SerializeField]
        private CapsuleCollider cCollider;

        [SerializeField]
        private GameObject explosionObj;

        [SerializeField]
        private LineRenderer lRenderer;

        [SerializeField]
        private Transform lRendStartPos;

     

        private float lineRendererStartPosY;

        // Use this for initialization
        void Start ()
        {
            lineRendererStartPosY = lRendStartPos.localPosition.y;
	    }

        public void Init(bool canHitPeople, Camera cam, string teamName, Action<PlayerSettings> _callback = null)
        {
            if (_callback != null) callback = _callback;
            timeOutRoutine = StartCoroutine(DoTimeOut());

            hittingEnabled = canHitPeople;

            myTeamName = teamName;
            RaycastHit hit;
            if (Physics.Raycast(transform.position, cam.transform.forward, out hit, maxUnits, layerMask))
            {
                var duration = GetDuration(hit.point);

                transform.DOMove(hit.point, duration).SetEase(Ease.Linear).OnComplete(() =>
                {
                    if (hit.collider.tag.Contains("Player"))
                    {
                        transform.SetParent(hit.collider.transform);
                        transform.LookAt(cam.transform);
                        if (canHitPeople)
                        {
                            StartCoroutine(DoExplosion(1.5f));
                        }
                        else
                        {
                            Destroy(this.gameObject);
                        }
                    }
                    else
                    {
                        transform.rotation = Quaternion.LookRotation(-hit.normal);
                        ActivateTrigger();
                    }
                });
            }
            else
            {
                var endPoint = cam.transform.position + cam.transform.forward * maxUnits;
                if (Physics.Raycast(endPoint, Vector3.down, out hit, 100, layerMask))
                {
                    transform.DOJump(new Vector3(endPoint.x, hit.point.y, endPoint.z), 1, 1, moveDuration).SetEase(Ease.Linear).OnComplete(() =>
                    {
                        ActivateTrigger();
                    });
                }
                else
                {
                    //This should never happen! :D 
                    transform.DOMove(endPoint, moveDuration).SetEase(Ease.Linear).OnComplete(() =>  
                    {
                        ActivateTrigger();
                    });
                }
            }
        }

        private void ActivateTrigger()
        {
            cCollider.enabled = true;

            RaycastHit hit;
            var laserLength = 20f;
            if (Physics.Raycast(lRendStartPos.position, lRendStartPos.right, out hit, laserLength, layerMask))
            {
                laserLength = Vector3.Distance(lRendStartPos.position, hit.point);
            }

            float length = 0;
            DOTween.To(() => length, x => length = x, laserLength, 0.25f).SetEase(Ease.Linear).OnUpdate(() => 
            {
                lRenderer.SetPosition(1, new Vector3(0, 0, -length));
            });

            cCollider.height = laserLength;
            cCollider.center = new Vector3(0, 0, -(laserLength * 0.5f));

            ready = true;
        }

        private IEnumerator DoTimeOut()
        {
            yield return new WaitForSeconds(30);
            Destroy(this.gameObject);
        }

        private IEnumerator DoExplosion(float delay = 0)
        {
            yield return new WaitForSeconds(delay);

            Collider[] hits = Physics.OverlapSphere(transform.position, 10f);

            GetComponent<AudioSource>().Play();

            if (hits != null && hits.Length > 0)
            {               
                List<string> hitList = new List<string>();
                foreach (Collider hit in hits)
                {
                    if (hit.tag.Contains("Player"))
                    {
                        var hitSettings = hit.gameObject.GetComponent<PlayerSettings>();

                        if (hitSettings == null) hitSettings = hit.gameObject.GetComponentInParent<PlayerSettings>();

                        if (hitSettings != null)
                        {
                            if (myTeamName != hitSettings.teamName)
                            {
                                if (Physics.Raycast(transform.position, hit.gameObject.transform.position - transform.position, 10f, layerMask))
                                {
                                    var targetName = hit.gameObject.name;
                                    if(!hitList.Contains(targetName))
                                    {
                                        hitList.Add(targetName);
                                        if (callback != null) callback(hitSettings);
                                    }
                                }
                            }
                        }
                    }
                }

            }


            ToggleExplosion();
        }

        private void OnTriggerEnter(Collider col)
        {
            if (!ready) return;

            if (col.tag.Contains("Player"))
            {
                if (timeOutRoutine != null) StopCoroutine( timeOutRoutine);

                var settings = col.gameObject.GetComponent<PlayerSettings>();

                if (settings == null) settings = col.gameObject.GetComponentInParent<PlayerSettings>();

                if (settings == null) return;

                if (hittingEnabled)
                {

                    if (settings != null)
                    {
                        if (settings.teamName != myTeamName)
                        {
                            ready = false;
                            StartCoroutine(DoExplosion());
                        }
                    }
                }
                else
                {
                    if (settings.teamName != myTeamName)
                    {
                        //Create explosion here
                        ToggleExplosion();
                    }
                }
            }
        }

        private void ToggleExplosion()
        {
            explosionObj.transform.SetParent(null);
            explosionObj.SetActive(true);
            Destroy(this.gameObject);
        }

        private float GetDuration(Vector3 endPoint)
        {
            var dis = Vector3.Distance(endPoint, transform.position);
            return moveDuration * (dis / maxUnits);
        }

    }
}
