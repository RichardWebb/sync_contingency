﻿/*

   Psyonic attack. 
   This class hadnles weapon firing - will talk to the IAttackManager for damage / scores.
   Created by Richard Webb, s1308033. 28/12/16

*/

using UnityEngine;
using System.Collections;
using pulsar.view.characters.attack;
using UnityEngine.Networking;
using System;
using puslar.view.characters.additional;
using DG.Tweening;
using pulsar.view.characters.additional;
using pulsar.view.characters.shooting;
using pulsar.view.mono;

namespace pulsar.view.characters.psyonic
{
    public class PsyonicAttackView : NetworkBehaviour, IAttackInputListener
    {
        [SyncVar(hook = "ToggleSniper")]
        private bool sniperToggle;

        [SyncVar(hook = "ToggleSingle")]
        private bool singleToggle;

        [SyncVar(hook = "ToggleGrenade")]
        private bool grenadeToggle;

        [SerializeField]
        private Transform dummySpawnPoint; 

        private IAttackManager attackManager;
      //  private Camera cam;
        private Tweener fieldOfViewTween;
        private Tweener canvasTween;
        private ScopeView scopeView;

        //Config vars
        [SerializeField]
        private float sniperRange = 500;

        [SerializeField]
        private float standardRange = 100;

        [SerializeField]
        private LineRenderer sniperLineRenderer;

        [SerializeField]
        private GameObject projectileObj;

        [SerializeField]
        private GameObject sniperProjectileObj;

        [SerializeField]
        private Transform projectileSpawnPoint;

        [SerializeField]
        private GameObject myGun;

        [SerializeField]
        private CanvasGroup playerHolderCGroup;

        [SerializeField]
        private GameObject superProjectileObj;

        [SerializeField]
        private Transform superProjectileSpawnPoint;

        [SerializeField]
        private GameObject grenadeObj;

        [SerializeField]
        private Animator anim;

        private PlayerSettings settings;

        private Vector3 startPos;

        public Vector3 spawnPoint
        {
            get
            {
                return superProjectileSpawnPoint.position;
            }

            set
            {
            }
        }

        [SerializeField]
        private AudioPlayerView audioView;

        [SerializeField]
        private AudioClip[] clips;

        [SerializeField]
        private LayerMask layerMask;

        private RayForPlayersView rayForPlayersView;

        [SerializeField]
        private Camera myCamera;

        void Start ()
        {
            settings = GetComponent<PlayerSettings>();
            rayForPlayersView = GetComponent<RayForPlayersView>();
         //   cam = Camera.main;
            scopeView = GetComponent<ScopeView>();
            attackManager = GetComponent<IAttackManager>();

            startPos = myGun.transform.localPosition;
            rayForPlayersView.PlayerRange = standardRange;
	    }

        //Handle scoping out
        public void ScopeOut()
        {
            rayForPlayersView.PlayerRange = standardRange;
            //Tell scope view to scope out
            scopeView._ScopeOut();
            float view = 60f;

            //Vector3 endPos = new Vector3(0.213f, -0.363f, 0.52f);

            if (canvasTween != null) canvasTween.Kill();
            canvasTween = playerHolderCGroup.DOFade(1, 0.01f);
            //Move gun
            myGun.transform.DOLocalMove(startPos, 0.2f).SetEase(Ease.Linear);
            if (fieldOfViewTween != null) fieldOfViewTween.Kill();
            fieldOfViewTween = Camera.main.DOFieldOfView(view, 0.25f);
        }

        //Handle scoping in
        public void ScopeIn()
        {
            if (anim != null)
            {
                anim.SetTrigger("DoScopeIn");
            }

            rayForPlayersView.PlayerRange = sniperRange;
            //Tell scope view to scope in
            scopeView._ScopeIn();
            if (fieldOfViewTween != null) fieldOfViewTween.Kill();
            float view = 22f;

            Vector3 endPos = new Vector3(-0.0604f, startPos.y, 0.6148f);
            if (canvasTween != null) canvasTween.Kill();

            canvasTween = playerHolderCGroup.DOFade(0, 0.01f);

            //Move gun
            myGun.transform.DOLocalMove(endPos, 0.2f).SetEase(Ease.Linear);

            fieldOfViewTween = Camera.main.DOFieldOfView(view, 0.25f);
        }

        //Handle shoot called
        public void Shoot(bool isScoped)
        {
            //if scoped do sniper shot
            if (isScoped)
            {
                DoSniperShot();
            }
            else
            {
                //So hip fire shot otherwise
                //    DoStandardShot();
                StartCoroutine(DoPlasmaShots());
            }
        }

        private IEnumerator DoPlasmaShots()
        {
            if (isLocalPlayer)
            {
                if (anim != null)
                    anim.SetTrigger("DoShot");
                    //anim.SetTrigger("Fire");

                audioView.PlayClip(clips[0], audioVolume:0.5f);
            }
            

            for (int i = 0; i < 3; i++)
            {
                DoStandardShot();
                yield return new WaitForSeconds(0.1f);
                //TODO Add recoil here
            }
        }

        //Do hip fire shot
        private void DoStandardShot()
        {
            var cam = PlayerSettings.localPlayerSettings.myCamera;
            //Get random points to aim for
            var points = attackManager.GetShotPoints(cam.transform.forward);
            bool hitSomething = false;

            RaycastHit hit;
            //Cast rays at multiple points in hope to hit something
            foreach (Vector3 point in points)
            {
                if (Physics.Raycast(cam.transform.TransformPoint(0, 0, 0.5f), point, out hit, standardRange, layerMask))
                {
                    if (hit.transform.tag.Contains("Player"))
                    {
                        PlayerSettings targetSettings = hit.transform.gameObject.GetComponent<PlayerSettings>();//(hit.transform.tag.Contains("Head") ? hit.transform.gameObject.GetComponentInParent<PlayerSettings>() : hit.transform.gameObject.GetComponent<PlayerSettings>());

                        if (PlayerSettings.localPlayerSettings.teamName != targetSettings.teamName)
                        {
                            hitSomething = true;
                            //Tell server to spawn a projectile
                            CmdSpawnShot(false);
                            //CmdSpawnProjectile(isServer, false);
                            //Hit the player
                            attackManager.HitPlayer(hit);
                            break;
                        }
                    }
                }
            }

            if (!hitSomething)
            {
                //Tell server to spawn a projectile
                CmdSpawnShot(false);

                //CmdSpawnProjectile(isServer, false);
            }
        }

        public void PlayDummySuperAudio()
        {
            audioView.PlayClip(clips[2], false);
        }

        //Aim assist sniper shot
        private void DoSniperShot()
        {
            RaycastHit hit;

            audioView.PlayClip(clips[1]);

            var cam = PlayerSettings.localPlayerSettings.myCamera;
            var points = attackManager.GetShotPoints(cam.transform.forward);
            bool hitSomething = false;

            Vector3 _endPoint = cam.transform.position + cam.transform.forward * sniperRange;

            foreach (Vector3 point in points)
            {
                if (Physics.Raycast(cam.transform.TransformPoint(0, 0, 0.5f), point, out hit, sniperRange, layerMask))
                {
                    if (hit.transform.tag.Contains("Player"))
                    {
                        PlayerSettings targetSettings = (hit.transform.tag.Contains("Head") ? hit.transform.gameObject.GetComponentInParent<PlayerSettings>() : hit.transform.gameObject.GetComponent<PlayerSettings>());

                        if (PlayerSettings.localPlayerSettings.teamName != targetSettings.teamName)
                        {
                            _endPoint = hit.point;
                            //Tell server to spawn a projectile
                         
                            CmdSpawnShot(true);
                            //Hit the player
                            attackManager.HitPlayer(hit);
                            hitSomething = true;
                            break;
                        }
                    }                 
                }
            }

            if (!hitSomething)
            {
                //Tell server to spawn a projectile
                //CmdSpawnProjectile(isServer, true);
                CmdSpawnShot(true);
            }

            StartCoroutine(DoSniperShotLineRenderer(_endPoint));
        }

        private IEnumerator DoSniperShotLineRenderer(Vector3 _endPoint)
        {
            var sPos = isLocalPlayer ? myCamera.transform.position + -myCamera.transform.up * 0.02f : dummySpawnPoint.position;

            sniperLineRenderer.SetPosition(0, sPos);
            sniperLineRenderer.SetPosition(1, _endPoint);

            yield return new WaitForSeconds(0.15f);

            sniperLineRenderer.SetPosition(0, sPos);
            sniperLineRenderer.SetPosition(1, sPos);
        }

        [Command]
        private void CmdSpawnShot(bool isSniper)
        {
            if (isSniper)
            {
                sniperToggle = !sniperToggle;
            }
            else
            {
                singleToggle = !singleToggle;
            }
        }

        public void ToggleSniper(bool v)
        {
            if (!isLocalPlayer)
            {
                audioView.PlayClip(clips[1], false);

                RaycastHit hit;
                Vector3 _endPoint = myCamera.transform.position + myCamera.transform.forward * sniperRange;

                if (Physics.Raycast(myCamera.transform.TransformPoint(0, 0, 0.5f), myCamera.transform.forward, out hit, sniperRange, layerMask))
                {
                    _endPoint = hit.point;
                }

                StartCoroutine(DoSniperShotLineRenderer(_endPoint));
            }

            sniperToggle = v;

           // var shot = (GameObject)Instantiate(sniperProjectileObj, projectileSpawnPoint.position, Quaternion.identity);
           // shot.GetComponent<ProjectileMonoView>().Launch(settings.myCamera.transform, sniperRange);
        }
    
        public void ToggleSingle(bool v)
        {
            singleToggle = v;

            var shot = (GameObject)Instantiate(projectileObj, projectileSpawnPoint.position, Quaternion.identity);
            shot.GetComponent<ProjectileMonoView>().Launch(settings.myCamera.transform, standardRange);

            if (!isLocalPlayer)
            {
                audioView.PlayClip(clips[0], false);
            }

        }


        /// <summary>
        /// Psyonic projectile - spawned on the server and projected forward on clients.
        /// </summary>
        /// <param name="_isServer"></param>
        [Command]
        private void CmdSpawnProjectile(bool _isServer, bool isSniper)
        {
            //Create the projectile
            var obj = (GameObject)Instantiate(isSniper ? sniperProjectileObj : projectileObj, projectileSpawnPoint.position, Quaternion.identity);

            if (_isServer)
            {
                obj.GetComponent<ProjectileView>().Init();
                NetworkServer.Spawn(obj);
            }
            else
            {
                NetworkServer.SpawnWithClientAuthority(obj, base.connectionToClient);
            }
        }

        //Super management

        /// <summary>
        /// Super projectile spawning and setting authority.
        /// </summary>
        /// <param name="_isServer"></param>
        [Command]
        private void CmdSpawnSuper(bool _isServer)
        {
            //Create the super object
            var obj = (GameObject)Instantiate(superProjectileObj, superProjectileSpawnPoint.position, Quaternion.identity);

            if (_isServer)
            {
                obj.GetComponent<PsyonicSuperView>().Init(PlayerSettings.localPlayerSettings.gameObject.GetComponent<PsyonicAttackView>(), true);
                NetworkServer.Spawn(obj);
            }
            else
            {
                NetworkServer.SpawnWithClientAuthority(obj, base.connectionToClient);
            }
        }

        /// <summary>
        /// Executes super - called from attack manager
        /// </summary>
        public void ExecuteSuper()
        {
            StartCoroutine(DoPlasmaSuper());
            //Old Super 
          //  StartCoroutine(DoSuper());
        }

        //Gun plasma shot 
        private IEnumerator DoPlasmaSuper()
        {
            GetComponent<PsyonicSuperTwo>().DoSuper();
            audioView.PlayClip(clips[2]);
            yield return new WaitForSeconds(1.5f);
            attackManager.ResetSuper();
        }

        //Old Super
        private IEnumerator DoSuper()
        {
            yield return new WaitForSeconds(0.35f);
            attackManager.ResetSuper();
            CmdSpawnSuper(isServer);
        }

        /// <summary>
        /// Call back for super projectile 
        /// </summary>
        /// <param name="playerSettings"></param>
        public void HitPlayersWithSuper(PlayerSettings[] playerSettings)
        {
            foreach (PlayerSettings player in playerSettings)
            {
                if (player.isLocalPlayer) continue;
                if (player.teamName == PlayerSettings.localPlayerSettings.teamName) continue;

                attackManager.UpdateServerWhoWasShot(player.gameObject.name, 1000, PlayerSettings.localPlayerSettings.playerName, gameObject.name);
            }
        }

        //Grenade Management
        /// <summary>
        /// Called from attack manager
        /// </summary>
        public void ExecuteGrenade()
        {
            if (isLocalPlayer)
            {
                audioView.PlayClip(clips[3]);
                var obj = (GameObject)Instantiate(grenadeObj, projectileSpawnPoint.position, Quaternion.Euler(0,0,90f));
                obj.GetComponent<PsyonicProjectileGrenadeView>().Init(true, settings.myCamera, settings.teamName, HitPlayerWithGrenade);
            }
            CmdToggleGrenade();
          //  CmdSpawnGrenade(isServer);
        }

        [Command]
        private void CmdToggleGrenade()
        {
            grenadeToggle = !grenadeToggle;
        }

        public void ToggleGrenade(bool v)
        {
            if (!isLocalPlayer)
            {
                audioView.PlayClip(clips[3], false);
                var obj = (GameObject)Instantiate(grenadeObj, projectileSpawnPoint.position, Quaternion.Euler(0, 0, 90f));
                obj.GetComponent<PsyonicProjectileGrenadeView>().Init(false, settings.myCamera, settings.teamName);
            }
        }

        /*
        //Server side spawn the grenade
        [Command]
        private void CmdSpawnGrenade(bool _isServer)
        {
            //Create the grenade object
            var obj = (GameObject)Instantiate(grenadeObj, transform.position, Quaternion.identity);
            obj.transform.position = superProjectileSpawnPoint.position;
            if (_isServer)
            {
                NetworkServer.Spawn(obj);
                obj.GetComponent<PsyonicGrenade>().Init();
            }
            else
            {
                NetworkServer.SpawnWithClientAuthority(obj, base.connectionToClient);
            }
        }*/

        public void HitPlayerWithGrenade(PlayerSettings player)
        {
            attackManager.UpdateServerWhoWasShot(player.gameObject.name, 100, PlayerSettings.localPlayerSettings.playerName, gameObject.name);
        }

        //Callback to hit players with grenade
        public void HitPlayersWithGrenade(PlayerSettings[] playerSettings)
        {
            foreach (PlayerSettings player in playerSettings)
            {
                if (player.teamName != PlayerSettings.localPlayerSettings.teamName)
                {
                    attackManager.UpdateServerWhoWasShot(player.gameObject.name, 100, PlayerSettings.localPlayerSettings.playerName, gameObject.name);
                }
            }
        }
    }
}
