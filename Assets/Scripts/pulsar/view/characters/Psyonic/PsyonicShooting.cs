﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using pulsar.view.interfaces;
using UnityEngine.UI;
using Robotlegs.Bender.Extensions.Mediation.API;
using System;
using Robotlegs.Bender.Extensions.EventManagement.API;
using Robotlegs.Bender.Extensions.EventManagement.Impl;
using Robotlegs.Bender.Extensions.ViewManagement.API;
using DG.Tweening;
using pulsar.events;
using pulsar.view.characters.shooting;
using UnityStandardAssets.Characters.FirstPerson;
using System.Collections.Generic;
using pulsar.view.characters.special;
using pulsar.view.characters.additional;
using puslar.view.characters.additional;
using pulsar.networksync;

namespace pulsar.view.characters.psyonic
{
    public class PsyonicShooting : NetworkBehaviour, IActionListener, IScoreDispatcher, IGameListener, ISuperListener, IGrenade, IRiftListener
    {
        private int damage = 10;
        private float range = 200;

        [SerializeField]
        private Transform camTransform;

        [SerializeField]
        private Sprite[] recticles;

        [SerializeField]
        private Text playerNameText;

        [SerializeField]
        private Image recticle;

        [SerializeField]
        private int maxAmmo = 40;

        [SerializeField]
        private Text onGunAmmoLeftText;

        private int currentAmmo = 0;

        [SerializeField]
        private CanvasGroup playerHolderCGroup;

        [SerializeField]
        private Text ammoText;

        private RaycastHit hit;

        private float timeBetweenShots = 0.25f;
        private float scopedTimeBetweenShots = 0.5f;
        private float currentTimeBetweenShot = 0f;

        public bool localPlayer;
        private PlayerSettings settings;
    //    private PlayerStatsView playerStats;
        private Player_Health myHealth;
        private bool clickedReload = false;
        private bool isScoped = false;

        private float runSpeed;
        private float scopedRunSpeed;
        private FirstPersonController controller;

        private Camera myCamera;
        private Tweener canvasTween;

        [SerializeField]
        private Transform myGun;

        [SerializeField]
        private GameObject flashObj;

        [SerializeField]
        private GameObject projectile;

        [SerializeField]
        private Transform projectileSpawnPoint;

        [SerializeField]
        private GameObject superProjectile;

        [SerializeField]
        private Transform superSpawnPoint;

        private ScopeView scopeView;

        public event Action<IView> RemoveView;
        private IEventDispatcher _dispatcher = new EventDispatcher();
        public IEventDispatcher dispatcher
        {
            get
            {
                return _dispatcher;
            }
            set
            {
                _dispatcher = value;
            }
        }

        public bool holdingRift
        {
            get;set;
        }

        private Player_Health[] playersHealth;

        private GameObject hitText;
        private Tweener currentTween;
        private Player_Health healthScript;

        [SerializeField]
        private CanvasGroup reloadWepImage;
        private Tweener fieldOfViewTween;
        private bool usingSuper;

        [SerializeField]
        private GameObject grenadeObj;

        void Start()
        {
        //    ViewNotifier.RegisterView(this);
            
            hitText = Resources.Load<GameObject>("Game/HitText");
       //     playerStats = GetComponent<PlayerStatsView>();
            myHealth = GetComponent<Player_Health>();
            settings = GetComponent<PlayerSettings>();
            myCamera = Camera.main;
            if (isServer)
            {
                StartCoroutine(GetPlayersHealth());
            }

            currentAmmo = maxAmmo;
            scopeView = GetComponent<ScopeView>();

        }


        public override void PreStartClient()
        {
            healthScript = GetComponent<Player_Health>();
            healthScript.EventDie += HandleDeath;
            controller = GetComponent<FirstPersonController>();
            runSpeed = controller.m_WalkSpeed;
            scopedRunSpeed = runSpeed * 0.5f;
            reloadWepImage.alpha = 0;
        }

        private void HandleDeath()
        {
            if (isLocalPlayer)
            {
                return;
                //move to flash view 
                flashObj.SetActive(false);
                StartCoroutine(Reload());
                ScopeOut();
            }
        }

        public override void OnStartLocalPlayer()
        {
            base.OnStartLocalPlayer();
        }
   
        private IEnumerator GetPlayersHealth()
        {
            yield return new WaitForSeconds(2);
            playersHealth = GameObject.FindObjectsOfType<Player_Health>();
        }

        // Update is called once per frame
        void Update()
        {
         //   CheckIfShooting();
        }

        void CheckIfShooting()
        {
            if (!isLocalPlayer)
            {
                return;
            }

            if (holdingRift) return;

            if (Input.GetKeyUp(KeyCode.V)) return;

            currentTimeBetweenShot += Time.deltaTime;

            if (usingSuper) return;

            if (myHealth.health <= 0) return;

            if (!clickedReload && currentAmmo < maxAmmo && Input.GetKeyUp(KeyCode.R))
            {              
                StartCoroutine(Reload());
            }           

            if (clickedReload) return;
            
            if (Input.GetKey(KeyCode.LeftShift))
            {
                if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.S))
                {
                    if (isScoped) ScopeOut();
                }
            }

            if (Input.GetKeyDown(KeyCode.Mouse1))
            {
                if (!isScoped)
                {
                    ScopeIn();
                }
            }

            if (Input.GetKeyUp(KeyCode.Mouse1))
            {
                if (isScoped)
                {
                    ScopeOut();
                }             
            }     

            float targetTime = isScoped ? scopedTimeBetweenShots : timeBetweenShots;

            if (Input.GetKeyDown(KeyCode.Mouse0) && currentTimeBetweenShot >= targetTime)
            {
                if (currentAmmo <= 0)
                {
                    StartCoroutine(Reload());
                }
                else
                {
                    Shoot();
                }
            }      

            onGunAmmoLeftText.text = currentAmmo.ToString();
        }

        private IEnumerator Reload()
        {
            ScopeOut();
            clickedReload = true;
            reloadWepImage.alpha = 0;
            yield return new WaitForSeconds(1);
            currentAmmo = maxAmmo;
            ammoText.text = currentAmmo.ToString() + "/" + maxAmmo.ToString();

            clickedReload = false;
        }

        private void ScopeOut()
        {
            isScoped = false;
            scopeView._ScopeOut();
            float view = 60f;

            controller.m_WalkSpeed = runSpeed;
            //Vector3 endPos = new Vector3(0.23f, -0.381f, 0.6148f);
          //  Vector3 endPos = new Vector3(0.217f, -0.33f, 0.54f);
           // Vector3 endPos = new Vector3(0.22f, -0.33f, 0.51f);
            Vector3 endPos = new Vector3(0.213f, -0.363f, 0.52f);

            //rotation = 2.03f, 10.06f, 0


            if (canvasTween != null) canvasTween.Kill();
            canvasTween = playerHolderCGroup.DOFade(1, 0.01f);

            myGun.DOLocalMove(endPos, 0.2f).SetEase(Ease.Linear);
            onGunAmmoLeftText.GetComponent<RectTransform>().DOLocalRotate(new Vector3(0, 90, 0), 0.2f);
            if (fieldOfViewTween != null) fieldOfViewTween.Kill();
            //   myCamera.fieldOfView = view;
            fieldOfViewTween = myCamera.DOFieldOfView(view, 0.25f);
        }

        private void ScopeIn()
        {
            isScoped = true;
            scopeView._ScopeIn();
            if (fieldOfViewTween != null) fieldOfViewTween.Kill();
            float view = 22f;

            controller.m_WalkSpeed = scopedRunSpeed;
            Vector3 endPos = new Vector3(-0.0604f, -0.316f, 0.6148f);
            if (canvasTween != null) canvasTween.Kill();
            
            canvasTween = playerHolderCGroup.DOFade(0, 0.01f);

            myGun.DOLocalMove(endPos, 0.2f).SetEase(Ease.Linear);
            onGunAmmoLeftText.GetComponent<RectTransform>().DOLocalRotate(new Vector3(0, 0, 0), 0.2f);


            fieldOfViewTween = myCamera.DOFieldOfView(view, 0.25f);
        }


        void Shoot()
        {
            StartCoroutine(FlashShot());
            currentAmmo--;
            ammoText.text = currentAmmo.ToString() + "/" + maxAmmo.ToString();

            if (currentAmmo == 0)
            {
                FadeReload();
            }
            currentTimeBetweenShot = 0;

            if (isScoped)
            {
                DoSingleShot();
            }
            else
            {
                DoSpreadShot();
            }
        }

        private Vector3[] GetShotPoints(Vector3 shotPoint)
        {
            List<Vector3> shotPoints = new List<Vector3>();
            shotPoints.Add(shotPoint);
            shotPoints.Add(new Vector3(shotPoint.x, shotPoint.y, shotPoint.z - 0.05f));
            shotPoints.Add(new Vector3(shotPoint.x, shotPoint.y, shotPoint.z + 0.05f));
            shotPoints.Add(new Vector3(shotPoint.x, shotPoint.y - 0.05f, shotPoint.z));
            shotPoints.Add(new Vector3(shotPoint.x, shotPoint.y + 0.05f, shotPoint.z));
            //shotPoints.Add(new Vector3(shotPoint.x, shotPoint.y, shotPoint.z));

            return shotPoints.ToArray();
        }

        private void HitPlayer(RaycastHit hit)
        {
            if (hit.transform.tag == "Player")
            {
                if (settings.teamName != hit.transform.gameObject.GetComponent<PlayerSettings>().teamName)
                {
                    // SpawnProjectile(hit.transform.position);
                    ShootProjectile();
                    string uIdentity = hit.transform.name;
                    Player_Health targetHealth = hit.transform.gameObject.GetComponent<Player_Health>();
                    if (targetHealth.health <= 0) return;
                    if (targetHealth.CheckIfPlayerWillDie(damage))
                    {
                        string playerName = hit.transform.gameObject.GetComponent<PlayerSettings>().playerName;

                        if (!string.IsNullOrEmpty(playerName))
                        {
                            dispatcher.Dispatch(new PlayerActionEvent(PlayerActionEvent.Type.KILLED_A_PLAYER, playerName));
                        }
                    }

                    GameObject _hitText = (GameObject)Instantiate(hitText);
                    _hitText.transform.SetParent(settings.hudCanvas.transform);
                    _hitText.GetComponent<RectTransform>().localScale = Vector3.one;

                    int randomVal = UnityEngine.Random.Range(0, 4);
                    bool isCrit = randomVal >= 3;
                    if (isScoped) isCrit = true;

                    int _currentDamage = isScoped ? damage + 5 : damage;
                    int _damage = isCrit ? _currentDamage * 2 : _currentDamage;

                    _hitText.GetComponent<PlayerHitTextView>().Init(isCrit, _damage, hit.point.z, isScoped);
                    CmdTellServerWhoWasShot(uIdentity, _damage, settings.playerName, gameObject.name);
                }
            }
            else
            {
                ShootProjectile();
         //       SpawnProjectile(new Vector3(hit.transform.position.x, camTransform.forward.y, hit.transform.position.z));
            }
        }

        private void SpawnProjectile(Vector3 endPoint)
        {
            var _projectile = (GameObject)Instantiate(projectile);
            _projectile.transform.position = projectileSpawnPoint.position;
            _projectile.transform.DOMove(endPoint, 0.25f).SetEase(Ease.Linear).OnComplete(()=> 
            {
                Destroy(_projectile);
            });

        }

        private void SpawnProjectileWithForce()
        {
            var _projectile = (GameObject)Instantiate(projectile);
            _projectile.transform.position = projectileSpawnPoint.position;

            var endPoint = new Vector3(camTransform.forward.x, camTransform.forward.y, camTransform.forward.z + 15);

            _projectile.transform.DOMove(endPoint, 1f).SetEase(Ease.Linear).OnComplete(() =>
            {
                Destroy(_projectile, 2f);
            });

//            _projectile.GetComponent<Rigidbody>().AddForce(transform.forward, ForceMode.Force);
 //           Destroy(_projectile, 2f);
        }

        private void DoSpreadShot()
        {
            var camPoint = camTransform.forward;
            var points = GetShotPoints(camPoint);
            bool hitSomething = false;
            foreach (Vector3 point in points)
            {
                if (Physics.Raycast(camTransform.TransformPoint(0, 0, 0.5f), point, out hit, range))
                {
                    hitSomething = true;
                    HitPlayer(hit);
                    break;
                }
            }

            if (!hitSomething)
            {
                ShootProjectile();
            }
        }

        private void DoSingleShot()
        {
            //  Vector3 debugLine = new Vector3(camTransform.forward.x, camTransform.forward.y, camTransform.forward.z + range);
            //  Debug.DrawRay(camTransform.TransformPoint(0, 0, 0.5f), debugLine,Color.green);
            if (Physics.Raycast(camTransform.TransformPoint(0, 0, 0.5f), camTransform.forward, out hit, range))
            {
                HitPlayer(hit);
            }
            else
            {
                ShootProjectile();
                //DoMissShot();
            }
        }

        private void DoMissShot()
        {
            //var forward = projectileSpawnPoint.forward;
         //   SpawnProjectileWithForce();
        }

        private void ShootProjectile()
        {
            CmdSmawnProjectile(isServer);
        }

        [Command]
        private void CmdSmawnProjectile(bool _isServer)
        {

            var obj = (GameObject)Instantiate(projectile, projectileSpawnPoint.position, Quaternion.identity);

            if (_isServer)
            {
                obj.GetComponent<ProjectileView>().Init();
                NetworkServer.Spawn(obj);
            }
            else
            {
                NetworkServer.SpawnWithClientAuthority(obj, base.connectionToClient);
            }
        }

        private IEnumerator FlashShot()
        {
            if (isScoped)
            {
                flashObj.transform.localPosition = new Vector3(-0.2f, 0.11f, 3.17f);
            }
            else
            {
                flashObj.transform.localPosition = new Vector3(-0.178f, 0, 0.141f);
            }

            flashObj.SetActive(true);
            yield return new WaitForSeconds(0.25f);
            flashObj.SetActive(false);
        }

        private void FadeReload()
        {
            reloadWepImage.DOFade(1, 0.5f).OnComplete(() => 
            {
                reloadWepImage.DOFade(0, 0.5f).OnComplete(() =>
                {
                    if (currentAmmo <= 0)
                    {
                        FadeReload();
                    }
                });

            }); 

        }

        [Command]
        void CmdTellServerWhoWasShot(string uniqueID, int dmg, string attacker, string attackerUniID)
        {
            Player_Health _target = null;
            Player_Health _attacker = null;

            foreach (Player_Health player in playersHealth)
            {
                string name = player.gameObject.name;
                if (name == uniqueID)
                {
                    _target = player;
                }

                if (name == attackerUniID)
                {
                    _attacker = player;
                }
            }


            _target.lastPersonToHitMe = attacker;
            _target.DeductHealth(dmg);

            if (_target.health <= 0)
            {
                bool isMarked = _target.GetComponent<MarkedPlayerView>().PlayerMarked;

                PlayerSettings settings = _attacker.gameObject.GetComponent<PlayerSettings>();

                if (isMarked)
                    settings.KilledMarkedPlayer();

                if (settings.teamName == "red")
                {
                    dispatcher.Dispatch(new ScoreEvent(ScoreEvent.Type.UPDATE_TEAM1_SCORE, 100));
                }
                else
                {
                    dispatcher.Dispatch(new ScoreEvent(ScoreEvent.Type.UPDATE_TEAM2_SCORE, 100));
                }

                _attacker.gameObject.GetComponent<PlayerStatsView>().UpdateKillCount(1);
            }
        }

        public void YouDied(string name){}

        public void KilledPlayer(string name){}

        public void HandleGameStart(){}

        public void HandleGameFinished()
        {
            if (isLocalPlayer)
            {
                this.enabled = false;
            }
        }

        public void ExecuteSuper()
        {
            if (isLocalPlayer)
            {
                //Debug.Log("Using Super");
                usingSuper = true;
                ScopeOut();
                StartCoroutine(DoSuper());
            }
        }

        private IEnumerator DoSuper()
        {
            yield return new WaitForSeconds(0.35f);
            usingSuper = false;
            CmdSpawnSuper(isServer);
          //  var obj = (GameObject)Instantiate(superProjectile);
           // obj.GetComponent<PsyonicSuperView>().Init(this);
           // obj.transform.position = superSpawnPoint.position;
           // obj.GetComponent<Rigidbody>().AddForce(camTransform.forward * 1500);
        }


        [Command]
        private void CmdSpawnSuper(bool _isServer)
        {
            var obj = (GameObject)Instantiate(superProjectile, superSpawnPoint.position, Quaternion.identity);
            //obj.transform.position = superSpawnPoint.position;

            if (_isServer)
            {
           //     obj.GetComponent<PsyonicSuperView>().Init(PlayerSettings.localPlayerSettings.gameObject.GetComponent<PsyonicShooting>(), true);
                NetworkServer.Spawn(obj);
            }
            else
            {
                NetworkServer.SpawnWithClientAuthority(obj, base.connectionToClient);
            }
        }

        public void HitPlayersWithSuper(PlayerSettings[] playerSettings)
        {
            foreach (PlayerSettings player in playerSettings)
            {
                if (player.isLocalPlayer) continue;
                if (player.teamName == settings.teamName) continue;

                CmdTellServerWhoWasShot(player.gameObject.name, 1000, settings.playerName, gameObject.name);
            }
        }


        [Command]
        private void CmdSpawnGrenade(bool _isServer)
        {
            var obj = (GameObject)Instantiate(grenadeObj, transform.position, Quaternion.identity);
            obj.transform.position = superSpawnPoint.position;
            //  NetworkServer.Spawn(obj);
            if (_isServer)
            {
                NetworkServer.Spawn(obj);
                obj.GetComponent<PsyonicGrenade>().Init();
            }
            else NetworkServer.SpawnWithClientAuthority(obj, base.connectionToClient);

         //   obj.GetComponent<IGrenadeObj>().Init(GrenadeHit, settings);
            //obj.GetComponent<Rigidbody>().AddForce(camTransform.forward * 2500);

        }

        public void DoGrenade(Action callback)
        {
            if (callback != null) callback();
            CmdSpawnGrenade(isServer);
        }

        public void GrenadeHit(PlayerSettings[] obj)
        {
            foreach (PlayerSettings player in obj)
            {
                //if (player.isLocalPlayer) continue;
            //    if (player.teamName == settings.teamName) continue;

                if ( player.teamName != settings.teamName)
                    CmdTellServerWhoWasShot(player.gameObject.name, 100, settings.playerName, gameObject.name);
            }
        }
    }
}
