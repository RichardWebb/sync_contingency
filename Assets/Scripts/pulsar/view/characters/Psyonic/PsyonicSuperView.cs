﻿/*
 * Psyonic super view
 * This class handles Psyonic's super ability 
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.Networking;

namespace pulsar.view.characters.psyonic
{
    [NetworkSettings(channel = 0, sendInterval = 0.1f)]
    public class PsyonicSuperView : NetworkBehaviour
    {
        //Network vars - positioning and doing explosion
        [SyncVar(hook = "SyncPositionValues")]
        private Vector3 syncPos;

        [SyncVar(hook = "SyncExplosion")]
        private bool doExplosion;

        //Config vars
        [SerializeField]
        private GameObject explosion;

        [SerializeField]
        private GameObject[] unwantedObjs;

        private PsyonicAttackView shooting;

        private bool active = false;
        private Rigidbody body;
        private SphereCollider triggerCol;
        private PlayerSettings settings;
        private bool _hasAuth = false;
        private Coroutine currentRoutine;
        private float waitTime = 15;

        void Start()
        {
            //Get player settings
            settings = PlayerSettings.localPlayerSettings;

            //Check authority - if pass init the view
            if (hasAuthority && !settings.isServer)
            {
                Init(settings.gameObject.GetComponent<PsyonicAttackView>(), false);
            }
        }

        //Init the view - executes super
        public void Init(PsyonicAttackView psyonicShooting, bool serverAuth)
        {
            _hasAuth = serverAuth;
            body = GetComponent<Rigidbody>();
            shooting = psyonicShooting;
            body.useGravity = true;
            //launch obj
            transform.GetComponent<Rigidbody>().AddForce(Camera.main.transform.forward * 2250);
            StartCoroutine(Activate());

            currentRoutine = StartCoroutine(StartDeactiveTimer());
        }

        //method to deactivate the super projectile
        private IEnumerator StartDeactiveTimer()
        {
            yield return new WaitForSeconds(waitTime);
            //Tell server to destroy projectile
            CmdDestroyProjectile();
        }

        //Activate the super 
        private IEnumerator Activate()
        {
            var colliders = GetComponents<SphereCollider>();
            foreach (SphereCollider sC in colliders)
            {
                if (sC.isTrigger)
                {
                    sC.enabled = true;
                    triggerCol = sC;
                    break;
                }
            }

            yield return new WaitForSeconds(0);

            active = true;
        }

        
        private void FixedUpdate()
        {
            //Send position to the server
            SendPosition();
        }

        private void Update()
        {
            //if dummy then lerp to last known position
            if (!hasAuthority && !_hasAuth)
            {
                transform.position = Vector3.Lerp(transform.position, syncPos, Time.deltaTime * 16);
            }
        }

        //Server side - update position
        [Command]
        void CmdProvidePositionToServer(Vector3 pos)
        {
            syncPos = pos;
        }

        //Send position to the server
        private void SendPosition()
        {
            if (hasAuthority || _hasAuth)
            {
                CmdProvidePositionToServer(transform.position);
            }
        }

        //Client side get last position
        [Client]
        private void SyncPositionValues(Vector3 lastPos)
        {
            syncPos = lastPos;
        }

        //Client side - syncing explosion
        [Client]
        private void SyncExplosion(bool value)
        {
            doExplosion = value;
            // if time to explode toggle objects
            if (doExplosion)
            {
                foreach (GameObject obj in unwantedObjs)
                {
                    obj.SetActive(false);
                }
                explosion.SetActive(true);
            }
        }

        //Server side setting do explosion
        [Command]
        private void CmdSetExplosion(bool value)
        {
            doExplosion = true;
        }

        //Collision check
        void OnTriggerEnter(Collider col)
        {
            if (!active) return;

            if (!_hasAuth)
            {
                if (!hasAuthority) return;
            }

            active = false;
            //Remove rigidbody
            ClearBody();
            //Get all hits
            var hits = Physics.SphereCastAll(transform.position, 5f, transform.forward, 15);

            List<PlayerSettings> settings = new List<PlayerSettings>();
            //go through all hits and look for player
            foreach (RaycastHit hit in hits)
            {
                if (hit.collider.tag.ToLower().Contains("player"))
                {
                    PlayerSettings player = hit.transform.gameObject.GetComponent<PlayerSettings>();

                    if (player == null) player = hit.transform.gameObject.GetComponentInParent<PlayerSettings>();

                    if (!settings.Contains(player) && !player.isLocalPlayer)
                    {
                        //Add to hit list
                        settings.Add(player);
                    }
                }
            }

            //Tell server to explode projectile
            CmdSetExplosion(true);

            //Stop routine
            if (currentRoutine != null)
                StopCoroutine(currentRoutine);

            //Destroy this projectile
            StartCoroutine(DestroyThis());
            if (settings.Count > 0)
            {
                //Inform shooting manager to hit all enemies
                shooting.HitPlayersWithSuper(settings.ToArray());
            }
        }

        //Tells server to destroy projectile
        private IEnumerator DestroyThis()
        {
            yield return new WaitForSeconds(5);
            CmdDestroyProjectile();
        }

        //Clears the rigidbody
        private void ClearBody()
        {
            if (body != null)
            {
                body.velocity = Vector3.zero;
                Destroy(body);
            }

            triggerCol.enabled = false;
        }

        //Server side - destroys projectile.
        [Command]
        private void CmdDestroyProjectile()
        {
            NetworkServer.Destroy(this.gameObject);
        }

    }
}