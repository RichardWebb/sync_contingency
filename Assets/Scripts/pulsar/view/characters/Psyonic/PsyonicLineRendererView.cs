﻿/*
 * Psyonic Line Renderer View
 * This class handles the beam of light from psyonics gun hits players
 * Created by Richard Webb, s130833
 * 
 * Particle direction edits - Dan O'Neill - s1303529
 */
using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using DG.Tweening;

namespace pulsar.view.characters.psyonic
{
    public class PsyonicLineRendererView : MonoBehaviour
    {
        [SerializeField]
        private LineRenderer lineRenderer;

        [SerializeField]
        private GameObject plasmaLine;

        [SerializeField]
        private GameObject plasmaParent;

        [SerializeField]
        private Transform startPointTransform;

        [SerializeField]
        private ParticleSystem disParticleSystem;
        [SerializeField]
        private ParticleSystem beamParticleSystem;

        private Action<PlayerSettings[]> callBack;
        private BoxCollider _collider;

        private List<PlayerSettings> targets;
        private bool lineActive = false;

        private void Start()
        {
           // plasmaParent.transform.position = startPointTransform.position;
        }
        
        public void DoSuper(Vector3 target, Action<PlayerSettings[]> _callBack, float _delay)
        {
           // if (targets != null && targets.Count > 0) targets.Clear();
            //targets = new List<PlayerSettings>();
           // callBack = _callBack;

           // var _dist = target - plasmaParent.transform.position;


          //  _collider = lineRenderer.gameObject.AddComponent<BoxCollider>();
           // _collider.isTrigger = true;
           // transform.LookAt(target);
          //  _collider.center = new Vector3 (0, 0, _dist.magnitude * 0.5f);
           // _collider.size = new Vector3(3, 3, (_dist.magnitude));

            StartCoroutine(SetPlasmaScale(target));
            //Enables shot - visual and damage
            StartCoroutine(EnablePlasma(true, _delay));
        }

        public void ToggleLine(Vector3 target)
        {
            //Sets the visual line 
            lineRenderer.SetPosition(0, startPointTransform.position);
            lineRenderer.SetPosition(1, target);
            lineRenderer.SetWidth(0.01f, 0.01f);
            lineRenderer.enabled = true;
        }
        
        //Updating particle velocity 
        private IEnumerator SetPlasmaScale(Vector3 endPoint)
        {
            /*
            
            var _dist =  endPoint - plasmaParent.transform.position;
            ParticleSystem.VelocityOverLifetimeModule vModule = disParticleSystem.velocityOverLifetime;

            ParticleSystem.MinMaxCurve _x = new ParticleSystem.MinMaxCurve();
            _x.constantMax = _dist.x;

            ParticleSystem.MinMaxCurve _y = new ParticleSystem.MinMaxCurve();
            _y.constantMax = _dist.y;

            ParticleSystem.MinMaxCurve _z = new ParticleSystem.MinMaxCurve();
            _z.constantMax = _dist.y;

            vModule.x = _x;
            vModule.y = _y;
            vModule.z = _z;

            vModule = beamParticleSystem.velocityOverLifetime;
            vModule.x = _x;
            vModule.y = _y;
            vModule.z = _z;*/

            yield return new WaitForEndOfFrame();
        }

        private IEnumerator EnablePlasma(bool enableCollision, float delay)
        {
            yield return new WaitForSeconds(delay);
            //Sets collision active
            lineActive = enableCollision;
            lineRenderer.enabled = true;
            lineRenderer.SetWidth(1.2f, 1.2f);

            //Toggles plasma effect
            plasmaLine.SetActive(true);

            //Removes collision check
            StartCoroutine(DestroyCollider());
        }

        public void ShowDummyLineRenderer(Vector3 target, float delay)
        {
            StartCoroutine(SetPlasmaScale(target));

            StartCoroutine(EnablePlasma(false, delay));
        }

        private IEnumerator DestroyCollider()
        {
            yield return new WaitForSeconds(0.1f);
           // if (_collider != null)
           //     Destroy(_collider);

            plasmaLine.SetActive(false);
            lineRenderer.SetPosition(0, Vector3.zero);
            lineRenderer.SetPosition(1, Vector3.zero);
            lineRenderer.enabled = false;
            /*
            if (callBack != null)
            {
                if (targets != null && targets.Count > 0)
                    callBack(targets.ToArray());
            }*/

        }

        void OnTriggerEnter(Collider col)
        {
            /*
            if (!lineActive) return;

            if (col.tag.Contains("Player"))
            {
                var settings = col.GetComponent<PlayerSettings>();

                if (settings == null) settings = col.GetComponentInParent<PlayerSettings>();

                if (settings.teamName != PlayerSettings.localPlayerSettings.teamName)
                {
                    if (targets.IndexOf(settings) < 0)
                    {
                        targets.Add(settings);
                    }
                }
            }
            */
        }

        private void OnTriggerExit(Collider col)
        {
            /*
            if (!lineActive) return;

            if (col.tag.Contains("Player"))
            {
                var settings = col.GetComponent<PlayerSettings>();

                if (settings == null) settings = col.gameObject.GetComponentInParent<PlayerSettings>();

                if (settings.teamName != PlayerSettings.localPlayerSettings.teamName)
                {
                    var index = targets.IndexOf(settings);
                    if (index >= 0)
                    {
                        targets.RemoveAt(index);
                    }
                }
            }
            */
        }
    }
}
