﻿/*
 * VR character setup 
 * This class handles the setup of the vr character - enabling components 
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using pulsar.view.vr;

namespace pulsar.view.characers.vr
{
    public class VRCharacterSetup : NetworkBehaviour
    {
        [SerializeField]
        private GameObject[] guns;

        //On local player start
        public override void OnStartLocalPlayer()
        {
            base.OnStartLocalPlayer();
            //get the spawn point

            //transform.position = GetSpawnPoint();

            //transform.position = GameObject.Find("VRSpawnPoint").transform.position;

            //Get all components and references
            var myCameras = gameObject.GetComponentsInChildren<Camera>();
            var vrTrackedObjects = gameObject.GetComponentsInChildren<SteamVR_TrackedObject>();
            var vrRenderModel = gameObject.GetComponentsInChildren<SteamVR_RenderModel>();
            var manager = gameObject.GetComponentInChildren<SteamVR_ControllerManager>();
            var meshRenderer = gameObject.GetComponentInChildren<MeshRenderer>();
            var playArea = gameObject.GetComponentInChildren<SteamVR_PlayArea>();
            var vrCam = gameObject.GetComponentInChildren<SteamVR_Camera>();
            var ears = gameObject.GetComponentInChildren<SteamVR_Ears>();
            //var pickUpViews = gameObject.GetComponentsInChildren<VRPickUpCharacterView>();

            //enable my cameras
            foreach (Camera cam in myCameras)
            {
                cam.enabled = true;
            }

            //enable my remotes
            foreach (SteamVR_TrackedObject trackerObj in vrTrackedObjects)
            {
                trackerObj.enabled = true;
            }

            //enable my renderers
            foreach (SteamVR_RenderModel rendererModel in vrRenderModel)
            {
                rendererModel.enabled = true;
            }

            //enable references
            manager.enabled = true;
            meshRenderer.enabled = true;
            playArea.enabled = true;
            vrCam.enabled = true;
            ears.enabled = true;

            GetComponent<VRPickUpCharacterView>().enabled = true;
            GetComponent<VRHealthBeamView>().enabled = true;
            GetComponent<VRMarkPlayerView>().enabled = true;
            GetComponent<VRPickUpCharacterView>().enabled = true;
            //GetComponent<VRMovePlayerView>().enabled = true;
            GetComponent<VRUpdatedMovePlayerView>().enabled = true;

            foreach (GameObject g in guns) g.SetActive(true);

            //Enable all pick up items
            //foreach (VRPickUpCharacterView pickupView in pickUpViews)
            //{
            //    pickupView.enabled = true;
            //}
        }

    }

}
