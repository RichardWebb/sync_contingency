﻿/*
 * Diablo Scythe view
 * This class handles the collision and movment of the scythe
 */
using UnityEngine;
using DG.Tweening;
using System.Collections.Generic;
using pulsar.view.characters.attack;

namespace pulsar.view.characters.diablo
{
    public class DiabloScytheView : MonoBehaviour
    {
        [SerializeField]
        private DiabloSuperView attackView;

        [SerializeField]
        private PlayerAttackManagerView managerView;

        [SerializeField]
        private GameObject scytheObj;

        private bool hasAuth = false;

        private bool inMotion = false;

        private Quaternion startingRotation;

        private Vector3 startingPos = new Vector3(-0.5f, 0, 1f);

        private List<PlayerSettings> targets = new List<PlayerSettings>();
        private float moveDuration = 1.5f;
        private BoxCollider _collider;
        private GameObject _parent;

        private void Start()
        {
            _collider = GetComponent<BoxCollider>();
            _collider.enabled = false;
        }

        public void Init(Vector3 pos)
        {
            if (_collider == null) _collider = GetComponent<BoxCollider>();
            _collider.enabled = true;
            if (targets.Count > 0)
            {
                targets.Clear();
            }
            hasAuth = true;
            GoToPosition(pos);
        }

        public void GoToPosition(Vector3 pos)
        {
            startingRotation = transform.localRotation;
            scytheObj.SetActive(true);
            _parent = transform.parent.gameObject;
            var duration = GetDuration(pos);
            transform.SetParent(null);
            transform.DOMove(pos, duration).SetEase(Ease.Linear).OnComplete(() => 
            {
                transform.DOMove(_parent.transform.position, duration * 0.75f).SetEase(Ease.Linear).OnComplete(() => 
                {
                    inMotion = false;
                    scytheObj.SetActive(false);  
                    _collider.enabled = false;

                    transform.SetParent(_parent.transform);
                   // transform.localRotation = startingRotation;
                    transform.localPosition = startingPos;
                    attackView.ScytheThrowFinished();
                        //attackView.HitPlayersWithSuper(targets.ToArray());
                });
            });
        }

        private float GetDuration(Vector3 endPoint)
        {
            var dis = Vector3.Distance(endPoint, transform.position);
            return (dis / attackView.maxUnits) * moveDuration;
        }


        private void Update()
        {
          //  if (inMotion)
          //  {
           //     transform.Rotate(new Vector3(0, 0, 15));
           // }
        }

        private void OnTriggerEnter(Collider col)
        {
            //transform.DOLocalRotate(new Vector3(1,1,3), 0.5f).SetLoops(-1, LoopType.Yoyo);

            if (hasAuth)
            {
                if (col.gameObject.tag.Contains("Player"))
                {
                    var settings = col.gameObject.GetComponent<PlayerSettings>();

                    if (settings == null) settings = col.gameObject.GetComponentInParent<PlayerSettings>();

                    if (settings.teamName != PlayerSettings.localPlayerSettings.teamName)
                    {
                        if (targets.IndexOf(settings) < 0)
                        {
                            targets.Add(settings);
                            managerView.HitPlayer(settings, settings.gameObject.name, 200, true);
                        }
                    }
                }
            }

        }

    }
}
