﻿/*
 * Diablo shadow walk 
 * This class handles the shadow walk for diablo
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityStandardAssets.Characters.FirstPerson;
using pulsar.view.characters.attack;
using pulsar.networksync;
using UnityEngine.UI;
using System;

namespace pulsar.view.characters.diablo
{
    public class DiabloShadowWalkAbility : NetworkBehaviour
    {
        //network var - toggling shadow walk
        [SyncVar(hook = "OnShadowWalkToggle")]
        private bool shadowWalkActive = false;

        //Config vars
        [SerializeField]
        private GameObject shadowObj;

        [SerializeField]
        private GameObject[] bodyObj;

        [SerializeField]
        private Image abilityImage;

        [SerializeField]
        private FirstPersonController controller;

        [SerializeField]
        private GameObject[] guns;

        private Rigidbody body;

        private float currentTime = 0;

        private float maxUseTime = 4;

        private PlayerAttackManagerView managerView;
        private Player_Health playerHealth;

        [SerializeField]
        private float manaCost;

        [SerializeField]
        private AudioSource source;

        private void Awake()
        {
            source.enabled = false;
        }

        private void Start()
        {
            //get rigidbody
            body = GetComponent<Rigidbody>();
            playerHealth = GetComponent<Player_Health>();
            playerHealth.EventDie += HandleDeath;
            managerView = GetComponent<PlayerAttackManagerView>();
        }

        private void HandleDeath()
        {
            if (isLocalPlayer)
            {
                shadowWalkActive = false;
                controller.autoWalk = false;
                CmdToggleShadowWalk(false);
            }
        }

        public override void OnStartLocalPlayer()
        {
            base.OnStartLocalPlayer();
            foreach (GameObject obj in bodyObj)
            {
                obj.SetActive(false);
            }

        }

        void Update()
        {   
            if (!isLocalPlayer) return;
      //      if (!shadowWalkActive) currentTime += Time.deltaTime;
      //      else currentTime -= Time.deltaTime;

            if (playerHealth.isDead) return;
            if (managerView.usingSuper) return;
            if (managerView.holdingRift) return;

            abilityImage.fillAmount = 1 - (currentTime / maxUseTime);

            if (!shadowWalkActive)
            {
                if (source.enabled)
                {
                    source.enabled = false;
                }

                currentTime += Time.deltaTime;

                currentTime = Mathf.Clamp(currentTime, 0, maxUseTime);

                if (currentTime > 1f)
                {
                   // if (Input.GetKeyDown(KeyCode.Alpha1))
                    if (Input.GetButtonDown("Ability1"))
                    {
                            if (managerView.SpendMana(manaCost))
                        {
                            shadowWalkActive = true;
                            managerView.busy = true;
                            currentTime -= Time.deltaTime;
                            CmdToggleShadowWalk(true);
                            StartCoroutine(SpendMana());
                        }
                    }
                }
            }

            if (shadowWalkActive)
            {
                if (!source.enabled)
                {
                    source.spatialBlend = isLocalPlayer ? 0 : 1;
                    source.enabled = true;
                }
                currentTime -= Time.deltaTime;

                if (currentTime < 0.1f)
                {
                    shadowWalkActive = false;
                    managerView.busy = false;

                    CmdToggleShadowWalk(false);
                }
                else
                {
                    //if (Input.GetKeyUp(KeyCode.Alpha1))
                    if (Input.GetButtonUp("Ability1"))
                    {
                        shadowWalkActive = false;
                        managerView.busy = false;
                        CmdToggleShadowWalk(false);
                    }
                }
            }

           
        
            /*    
            if (currentTime >= timeBetweenAbility)
            {
                //if ready check input - toggle shadow walk
                if (Input.GetKeyDown(KeyCode.Alpha1))
                {
                    shadowWalkActive = true;
                    //Toggle walk on server
                    CmdToggleShadowWalk(true);
                    currentTime = 0;
                }
            }*/

            //if shadowing walking and key up disable walk
          /*  if (shadowWalkActive)
            {
                if (Input.GetKeyUp(KeyCode.Alpha1))
                {
                    currentTime = 0;
                    shadowWalkActive = false;
                    CmdToggleShadowWalk(false);
                }
            }*/
        }

        private IEnumerator SpendMana()
        {
            while (shadowWalkActive)
            {
                yield return new WaitForEndOfFrame();
                yield return new WaitForEndOfFrame();
                yield return new WaitForEndOfFrame();
                yield return new WaitForEndOfFrame();
                yield return new WaitForEndOfFrame();

                managerView.SpendMana(manaCost);
            }
        }



        //Server side toggle shadow walk
        [Command]
        private void CmdToggleShadowWalk(bool val)
        {
            shadowWalkActive = val;
        }

        //Client side - toggle shadow walk
        [Client]
        private void OnShadowWalkToggle(bool enabled)
        {
            if (GetComponent<PlayerSettings>().VRHasControl) return;

            shadowWalkActive = enabled;

            if (!isLocalPlayer)
            {
                foreach (GameObject obj in bodyObj)
                {
                    obj.SetActive(!enabled);
                }
            }

            shadowObj.SetActive(enabled);

            if (isLocalPlayer)
            {

                foreach (GameObject obj in guns) obj.SetActive(!enabled);

                //Enable auto walk on controller
                if (enabled)
                {
                    controller.autoWalk = true;
                }
                else
                {
                    controller.autoWalk = false;
                }
            }
        }
    }
}