﻿/*
 * Diablo super view
 * This class handles the super ability of diablo - scythe
 * Created by Richard Webb, s1308033 
 */

using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using pulsar.view.characters.attack;
using System;
using System.Collections.Generic;
using UnityStandardAssets.Characters.FirstPerson;
using DG.Tweening;

namespace pulsar.view.characters.diablo
{
    [NetworkSettings(channel = 0, sendInterval = 0.1f)]
    public class DiabloSuperView : NetworkBehaviour
    {
        [SerializeField]
        private GameObject[] guns;

     //   [SerializeField]
      //  private GameObject scythe;

        [SerializeField]
        private GameObject shadowFormObj;

        [SerializeField]
        private DiabloScytheView scytheView;

        //network var - syncing the scythe throw point
        [SyncVar(hook = "OnTargetPointChange")]
        private Vector3 targetPoint;
         
        private bool _hasAuth = false;
        private Vector3 scytheStartPos;

        private PlayerSettings mySettings;
        private DiabloAttackView attackView;
        private FirstPersonController controller;
        private CharacterController charController;
        private int speed = 100;
        public float maxUnits = 50;

        [SerializeField]
        private LayerMask layerMask;

        // Use this for initialization
        void Start()
        {
            //Getting settings and references
            mySettings = PlayerSettings.localPlayerSettings;
            controller = GetComponent<FirstPersonController>();
            attackView = GetComponent<DiabloAttackView>();
            charController = GetComponent<CharacterController>();
        }

        void Update()
        {
    
        }

        public void ExecuteSuper()
        {
            if (isLocalPlayer)
            {
                //Enables scythe
                StartCoroutine(ToggleSuperVisuals(true));

                DoScytheAnimation();
                
                StartCoroutine(TurnOffController());

    
                RaycastHit[] hits;
                var cam = Camera.main;
                hits = Physics.RaycastAll(cam.transform.position, cam.transform.forward, maxUnits, layerMask);
                List<PlayerSettings> targets = new List<PlayerSettings>();
                var endPoint = cam.transform.position + cam.transform.forward * maxUnits;
               // float duration = 0.75f;

                float closestDis = 500;
                foreach (RaycastHit hit in hits)
                {
                    string _tag = hit.collider.gameObject.tag;

                    if (_tag == "Wall" || _tag == "Floor")
                    {
                        float dis = Vector3.Distance(hit.point, cam.transform.position);

                        if (dis < closestDis)
                        {
                            closestDis = dis;

                            //if (dis < 25f)
                            //{
                                endPoint = GetEndPoint(dis);
                            //}

                        }
                        
                        break;
                    }
                }

                scytheView.Init(endPoint);
                CmdUpdateTargetPoint(endPoint);
   
            }
        }

    

        public void ScytheThrowFinished()
        {
            StartCoroutine(ToggleSuperVisuals(false));
        }

        [Command]
        private void CmdUpdateTargetPoint(Vector3 _targetPoint)
        {
            targetPoint = _targetPoint;
           // scytheView.GoToPosition(_targetPoint);
        }

        //Sync var call back, will animate the scythe on the dummies.
        public void OnTargetPointChange(Vector3 point)
        {
            targetPoint = point;
            if (!isLocalPlayer)
            {
                StartCoroutine(ToggleSuperVisuals(true));
                scytheView.GoToPosition(point);
                GetComponent<DiabloAttackView>().PlaySuperAudio();
            }
        }

        private Vector3 GetEndPoint(float units)
        {
            return Camera.main.transform.position + Camera.main.transform.forward * (units * 0.95f);
        }

        private IEnumerator TurnOffController()
        {
            yield return new WaitForSeconds(0.75f);
            charController.Move(Vector3.zero);
        }

        //Handles the scythe animation - fade in and out and movement
        private void DoScytheAnimation()
        {
        }

        //Toggles the scythe and guns based on super use.
        private IEnumerator ToggleSuperVisuals(bool state, float delay = 0)
        {
            yield return new WaitForSeconds(delay);
       //     if (isLocalPlayer)
         //       controller.enabled = !state;
            foreach (GameObject gun in guns)
            {
                gun.SetActive(!state);
            }
            shadowFormObj.SetActive(state);
           // scythe.SetActive(state);
        }
    }
}