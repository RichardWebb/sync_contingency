﻿/*
 * Diablo grenade
 * This class handles the grenade of diablo - shadow aura on the floor which stops players movement.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
using System;
using pulsar.networksync;
using pulsar.view.characters.attack;

namespace pulsar.view.characters.diablo
{
    [NetworkSettings(channel = 0, sendInterval = 0.1f)]
    public class DiabloGrenade : NetworkBehaviour
    {
        //network var - position syncing
        [SyncVar(hook = "SyncPositionValues")]
        private Vector3 syncPos;

        [SerializeField]
        private LayerMask mask;

        private bool _hasAuth = false;
        private bool stop = false;

        private List<PlayerSettings> enemies = new List<PlayerSettings>();
        private PlayerSettings[] allPlayers;
        private int damage = 2;
        private PlayerSettings mySettings;
        private IAttackManager attackManager;
        private float duration = 10;
        private float maxDis = 35;

        private void Awake()
        {
            allPlayers = GameObject.FindObjectsOfType<PlayerSettings>();
        }

        // Use this for initialization
        void Start()
        {
            //if clients object then get references and start routines
            if (hasAuthority && !PlayerSettings.localPlayerSettings.isServer)
            {
                DoSetup();
            }
        }


        //Runs setup - gets references and starts routines
        private void DoSetup()
        {
            if (attackManager == null)
                attackManager = PlayerSettings.localPlayerSettings.gameObject.GetComponent<IAttackManager>();

           // RaycastHit hit;

          //  var cam = PlayerSettings.localPlayerSettings.myCamera;
          //  var endPoint = cam.transform.position + cam.transform.forward * maxDis;
            //var endY = 6.89f;

          //  RaycastHit floorHit;
            /*
            if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, maxDis, mask))
            {
                var _tag = hit.collider.gameObject.tag;
              //  if (_tag == "Wall" || _tag == "Floor")
              //  {
               // }
                var dis = Vector3.Distance(hit.point, cam.transform.position);
                dis = dis * 0.95f;
                endPoint = cam.transform.position + cam.transform.forward * dis;
            }
            transform.position = endPoint;

            if (Physics.Raycast(transform.position, -transform.up, out floorHit, 50f, mask))
            {
                transform.position = floorHit.point + (floorHit.transform.up * 0.01f);
            }*/

            _hasAuth = true;
            StartCoroutine(HitPlayers());
            StartCoroutine(DestroyMe());
        }

        //If server players object then get references and start routines
        public void ServerInit(IAttackManager manager)
        {
            attackManager = manager;
            DoSetup();
        }

        private IEnumerator DestroyMe()
        {
            yield return new WaitForSeconds(duration);

            if (enemies.Count > 0)
            {
                foreach (PlayerSettings player in enemies)
                {
                    CmdHookPlayer(player.gameObject.name, false);
                }
            }

            CmdDestroyThis();
        }

        [Command]
        private void CmdDestroyThis()
        {
            NetworkServer.Destroy(this.gameObject);
        }

        private IEnumerator HitPlayers()
        {
            while (true)
            {
                yield return new WaitForSeconds(1);
                if (enemies.Count > 0)
                {
                    foreach (PlayerSettings player in enemies)
                    {
                        attackManager.HitPlayer(player, player.gameObject.name, damage, false);
                    }
                }
            }
        }

        private void FixedUpdate()
        {
            //Send position to server
            if (!stop)
                SendPosition();
        }

        void Update()
        {
            //if dummy then lerp to last known position
            if (!hasAuthority && !_hasAuth)
            {
                transform.position = Vector3.Lerp(transform.position, syncPos, Time.deltaTime * 16);
            }
        }

        //Trigger check for enemies
        void OnTriggerEnter(Collider col)
        {
            //Debug.Log("Trigger entered");
            if (_hasAuth)
            {
                if (col.tag.Contains("Player"))
                {
                    var myTeam = PlayerSettings.localPlayerSettings.teamName;
                    var targetSettings = col.gameObject.GetComponent<PlayerSettings>();

                    if (targetSettings == null)
                    {
                        targetSettings = col.gameObject.GetComponentInParent<PlayerSettings>();
                    }

                    if (targetSettings == null) return;

                    if (myTeam != targetSettings.teamName)
                    {
                        UpdateHitList(col.gameObject.name);
                    }                    
                }
            }
        }

        private void UpdateHitList(string name)
        {
            foreach (PlayerSettings player in allPlayers)
            {
                if (player.gameObject.name == name)
                {
                    if (enemies.Count == 0)
                    {
                        enemies.Add(player);
                        CmdHookPlayer(player.gameObject.name, true);
                    }
                    else
                    {
                        var index = enemies.IndexOf(player);
                        if (index < 0)
                        {
                            enemies.Add(player);
                            CmdHookPlayer(player.gameObject.name, true);
                        }
                    }
                }
            }
        }

        [Command]
        private void CmdHookPlayer(string playerName, bool val)
        {
            foreach (PlayerSettings player in allPlayers)
            {
                if (player.gameObject.name == playerName)
                {
                    player.HookPlayer(val);
                }
            }
        }

        [Command]
        private void CmdUnHookPlayer(string playerName)
        {
            foreach (PlayerSettings player in allPlayers)
            {
                if (player.gameObject.name == playerName)
                {
                    player.HookPlayer(false);
                }
            }
        }

        //Sent position to the server
        private void SendPosition()
        {
            if (hasAuthority || _hasAuth)
            {
                CmdProvidePositionToServer(transform.position);
            }
        }

        //Update server with last position
        [Command]
        void CmdProvidePositionToServer(Vector3 pos)
        {
            syncPos = pos;
        }

        //Client side update last position
        [Client]
        private void SyncPositionValues(Vector3 lastPos)
        {
            syncPos = lastPos;
        }
    }


}