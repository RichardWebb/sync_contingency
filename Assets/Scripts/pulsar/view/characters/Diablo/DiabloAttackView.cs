﻿/*
 * Diablo attack view
 * This class handles diablo's attack - linked with attack managaer 
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using pulsar.view.characters.attack;
using DG.Tweening;
using puslar.view.characters.additional;
using System;
using pulsar.view.characters.additional;
using pulsar.view.characters.shooting;
using pulsar.networksync;
using pulsar.view.mono;

namespace pulsar.view.characters.diablo
{
    public class DiabloAttackView : NetworkBehaviour, IAttackInputListener
    {
        [SyncVar(hook = "ToggleSingle")]
        private bool singleToggle;

        private PlayerAttackManagerView attackManager;
        private Camera cam;
        private Tweener fieldOfViewTween;
        private Tweener canvasTween;
        private int currentIndex = 0;
        private TargeterView targetView;

        //Config vars
        [SerializeField]
        private float standardRange = 100;

        [SerializeField]
        private GameObject projectileObj;

        [SerializeField]
        private Transform[] projectileSpawnPoints;

        [SerializeField]
        private GameObject[] myGuns;

        [SerializeField]
        private CanvasGroup playerHolderCGroup;

        [SerializeField]
        private GameObject superProjectileObj;

        [SerializeField]
        private Transform superProjectileSpawnPoint;

        [SerializeField]
        private GameObject grenadeObj;

        [SerializeField]
        private Animator[] anims;

        [SerializeField]
        private LayerMask layerMask;

        [SerializeField]
        private GameObject targetObject;

        private PlayerSettings settings;
        private bool aimingGrenade;

        [SerializeField]
        private AudioClip[] clips;



        private AudioPlayerView audioPlayerView;


        public Vector3 spawnPoint
        {
            get
            {
                return projectileSpawnPoints[currentIndex].position;
            }

            set
            {
                //DoNothing
            }
        }

        void Start()
        {
            audioPlayerView = GetComponent<AudioPlayerView>();
            targetView = GetComponent<TargeterView>();
            GetComponent<RayForPlayersView>().PlayerRange = standardRange;
            settings = GetComponent<PlayerSettings>();
            attackManager = GetComponent<PlayerAttackManagerView>();
        }

        public void ScopeOut()
        {
        }

        public void ScopeIn()
        {
        }

        public void Shoot(bool isScoped)
        {
            //So normal shot
            DoStandardShot();
        }

        private void DoStandardShot()
        {


            audioPlayerView.PlayClip(clips[0]);
            //Get multiple points - aim assit
            var points = attackManager.GetShotPoints(Camera.main.transform.forward);
            bool hitSomething = false;
            RaycastHit hit;
            //Cast rays for hits
            foreach (Vector3 point in points)
            {
                if (Physics.Raycast(Camera.main.transform.TransformPoint(0, 0, 0.5f), point, out hit, standardRange, layerMask))
                {
                    if (hit.transform.tag.Contains("Player"))
                    {
                        var isHeadShot = hit.transform.tag.Contains("Head");
                        PlayerSettings targetSettings = isHeadShot ? hit.transform.gameObject.GetComponentInParent<PlayerSettings>() : hit.transform.gameObject.GetComponent<PlayerSettings>();

                        if (PlayerSettings.localPlayerSettings.teamName != targetSettings.teamName)
                        {
                            hitSomething = true;
                            //Spawn projectile
                            //CmdSpawnProjectile(isServer);
                            CmdSpawnShot();
                            //Hit the player 
                            attackManager.HitPlayer(hit);
                            break;
                        }
                    }
                }
            }

            if (!hitSomething)
            {
                //Spawn projectile
                CmdSpawnShot();

                //   CmdSpawnProjectile(isServer);
            }
        }

        public void PlaySuperAudio()
        {
            audioPlayerView.PlayClip(clips[1], false);
        }

        //Get projectile spawn point - two guns different spawn points
        private Vector3 GetSpawnPoint()
        {
            currentIndex = currentIndex == 1 ? 0 : 1;
            return projectileSpawnPoints[currentIndex].position;
        }


        [Command]
        private void CmdSpawnShot()
        {
            singleToggle = !singleToggle;
        }


        public void ToggleSingle(bool v)
        {
            if (settings.VRHasControl) return;

            singleToggle = v;
            if (isLocalPlayer)
            {
                // anims[currentIndex].SetTrigger("Fire");
                anims[currentIndex].SetTrigger("DoShot");
            }
            else
            {
                audioPlayerView.PlayClip(clips[0], false);
            }

            var shot = (GameObject)Instantiate(projectileObj, GetSpawnPoint(), Quaternion.identity);
            shot.GetComponent<ProjectileMonoView>().Launch(settings.myCamera.transform, standardRange);

        }

        /// <summary>
        /// Psyonic projectile - spawned on the server and projected forward on clients.
        /// </summary>
        /// <param name="_isServer"></param>
        [Command]
        private void CmdSpawnProjectile(bool _isServer)
        {
            var obj = (GameObject)Instantiate(projectileObj, GetSpawnPoint(), Quaternion.identity);

            if (_isServer)
            {
                obj.GetComponent<ProjectileView>().Init();
                NetworkServer.Spawn(obj);
            }
            else
            {
                NetworkServer.SpawnWithClientAuthority(obj, base.connectionToClient);
            }
        }

        /// <summary>
        /// Executes super - called from attack manager
        /// </summary>
        public void ExecuteSuper()
        {
            /*   foreach (Animator anim in anims)
               {
                   anim.SetTrigger("DoUlt");
               }*/

            foreach (GameObject g in myGuns) g.SetActive(false);

            audioPlayerView.PlayClip(clips[1]);
            GetComponent<DiabloSuperView>().ExecuteSuper();
            StartCoroutine(ResetSuper());
        }

        private IEnumerator ResetSuper()
        {
            yield return new WaitForSeconds(1.5f);
            attackManager.ResetSuper();
            foreach (GameObject g in myGuns) g.SetActive(true);
        }

        /// <summary>
        /// Call back for super projectile 
        /// </summary>
        /// <param name="playerSettings"></param>
        public void HitPlayersWithSuper(PlayerSettings[] playerSettings)
        {
            foreach (PlayerSettings player in playerSettings)
            {
                if (player.isLocalPlayer) continue;
                if (player.teamName == PlayerSettings.localPlayerSettings.teamName) continue;

                attackManager.UpdateServerWhoWasShot(player.gameObject.name, 1000, PlayerSettings.localPlayerSettings.playerName, gameObject.name);
            }
        }

        void Update()
        {
            if (!aimingGrenade) return;
            if (!isLocalPlayer) return;
            if (!attackManager.busy) return;
            if (GetComponent<Player_Health>().isDead)
            {
                aimingGrenade = false;
                targetView.StopTarget();
            }

            //Grenade drop
            if (Input.GetButtonDown("Fire1") || Input.GetAxis("Fire1j") > 0.75f)
            {
                aimingGrenade = false;
                attackManager.busy = false;

                var trans = targetView.GetTargetTransform;
                CmdSpawnGrenade(isServer, trans.Key);
                audioPlayerView.PlayClip(clips[2]);

            }

        }

        //Grenade Management
        /// <summary>
        /// Called from attack manager
        /// </summary>
        public void ExecuteGrenade()
        {
         
            aimingGrenade = true;
            attackManager.busy = true;
            targetView.startTarget(targetObject, 35f, new Vector3(0,0.5f,0));

            //CmdSpawnGrenade(isServer);
        }

        //Server side spawn the grenade
        [Command]
        private void CmdSpawnGrenade(bool _isServer, Vector3 pos )
        {
            if (grenadeObj == null) return;
            var obj = (GameObject)Instantiate(grenadeObj, pos, Quaternion.identity);
       
            //obj.transform.position = superProjectileSpawnPoint.position;
            if (_isServer)
            {
                NetworkServer.Spawn(obj);
                obj.GetComponent<DiabloGrenade>().ServerInit(attackManager);
         //       obj.GetComponent<PsyonicGrenade>().Init();
            }
            else
            {
                NetworkServer.SpawnWithClientAuthority(obj, base.connectionToClient);
            }
        }

        //Call back to hit players with grenade damage
        public void HitPlayersWithGrenade(PlayerSettings[] playerSettings)
        {
            foreach (PlayerSettings player in playerSettings)
            {
                if (player.teamName != PlayerSettings.localPlayerSettings.teamName)
                {
                    attackManager.UpdateServerWhoWasShot(player.gameObject.name, 100, PlayerSettings.localPlayerSettings.playerName, gameObject.name);
                }
            }
        }
    }
}