﻿/*
 * IGrenade Interface
 * This interface allows views to listen for grenade actions
 * Created by Richard Webb, s1308033
 */
using System;

namespace pulsar.view.characters.special
{
    public interface IGrenade 
    {
        void DoGrenade(Action callback);
    }
}