﻿/*
 * Grenade view 
 * This class handles the grenade input and cooldown management
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System;
using UnityEngine.UI;
using pulsar.view.characters.attack;
using pulsar.networksync;
using pulsar.view.interfaces;
using Robotlegs.Bender.Extensions.Mediation.API;
using Robotlegs.Bender.Extensions.EventManagement.API;
using Robotlegs.Bender.Extensions.EventManagement.Impl;
using Robotlegs.Bender.Extensions.ViewManagement.API;

namespace pulsar.view.characters.special
{
    public class GrenadeView : NetworkBehaviour, IPlayerData
    {
        //Network var - on cool down change 
        [SyncVar(hook = "OnCooldownChange")]
        private float coolDownTime = 20;

        private bool grenadeAvaiable = false;

        private float timeUntilGrenade;
        private float grenadeDuration = 4;
        private bool locked;
        private PlayerAttackManagerView attackManager;
        private Player_Health playerHealth;

        [SerializeField]
        private Image cooldownImage;
        private bool usingGrenade;

        [SerializeField]
        private float manaCost;

        //robotlegs setup
        public event Action<IView> RemoveView;
        private IEventDispatcher _dispatcher = new EventDispatcher();
        public IEventDispatcher dispatcher
        {
            get
            {
                return _dispatcher;
            }
            set
            {
                _dispatcher = value;
            }
        }

        public override void OnStartLocalPlayer()
        {
            base.OnStartLocalPlayer();
            //Start timer
          //  StartCoroutine(ReduceTimer());
        }

        void Start()
        {
            ViewNotifier.RegisterView(this);

            attackManager = GetComponent<PlayerAttackManagerView>();
            playerHealth = GetComponent<Player_Health>();
            //Set current time
            timeUntilGrenade = coolDownTime;
        }

        //Method to reduce time until super
        private IEnumerator ReduceTimer()
        {
            while (true)
            {
                yield return new WaitForSeconds(1);
                if (grenadeAvaiable) continue;

                timeUntilGrenade -= 1;

                grenadeAvaiable = timeUntilGrenade <= 0;
            }
        }


        void Update()
        {
            if (!isLocalPlayer) return;
            if (locked) return;
            if (attackManager.busy) return;
            if (attackManager.isReloading) return;
            if (attackManager.usingSuper) return;
            if (attackManager.holdingRift) return;
            if (playerHealth.isDead) return;

            if (timeUntilGrenade > 0)
            {
                timeUntilGrenade -= Time.deltaTime;
            }

            grenadeAvaiable = timeUntilGrenade <= 0.1f;

            if (cooldownImage != null)
                cooldownImage.fillAmount = timeUntilGrenade / coolDownTime;

            //Check grenade avaiable - Check input - Do grenade
            if (grenadeAvaiable)
            {
                //if (Input.GetKeyUp(KeyCode.Alpha2))
                if (Input.GetButtonUp("Ability2"))
                {
                    if (attackManager.SpendMana(manaCost))
                        DoGrenade();
                }
            }
        }

        //Call back when grenade is used - reset cooldowns
        private void GrenadeFinished()
        {
            timeUntilGrenade = coolDownTime;
            locked = false;
            grenadeAvaiable = false;
        }

        //Input confirmed - inform IGrenade listener
        private void DoGrenade()
        {
            locked = true;
            var grenades = GetComponents<IGrenade>();

            foreach (IGrenade g in grenades)
            {
                g.DoGrenade(GrenadeFinished);
            }

            //GetComponent<IGrenade>().DoGrenade(GrenadeFinished);
        }

        //Refresh the grenade cooldown - Called for killing a marked player
        public void RefreshGrenade()
        {
            CmdUpdateCooldown(0);
        }

        //Client Side - On cooldown changed
        [Client]
        private void OnCooldownChange(float value)
        {
            coolDownTime = value;
            grenadeAvaiable = coolDownTime == 0;
        }

        //Server side - On cooldown changed
        [Command]
        private void CmdUpdateCooldown(float value)
        {
            coolDownTime = value;
        }

        public void GotPlayerData(PlayerVO playerData)
        {
            if (GetComponent<PlayerSettings>().characterName == playerData.characterName)
            {
                coolDownTime = playerData.grenadeCoolDown;
            }
        }
    }
}
