﻿/*
 * IGrenadeObj
 * This interface connects the grenade object to the grenade view
 * Created by Richard Webb, s1308033
 */
using System;

namespace pulsar.view.characters.special
{
    public interface IGrenadeObj
    {
        void Init(Action<PlayerSettings[]> callback, PlayerSettings settings);
    }
}