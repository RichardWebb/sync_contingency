﻿/*
 * HawkeDashView 
 * This class handles the dash of hawke
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using pulsar.view.interfaces;
using System;
using UnityStandardAssets.Characters.FirstPerson;
using Robotlegs.Bender.Extensions.Mediation.API;
using Robotlegs.Bender.Extensions.EventManagement.API;
using Robotlegs.Bender.Extensions.EventManagement.Impl;
using Robotlegs.Bender.Extensions.ViewManagement.API;
using pulsar.view.characters.attack;
using pulsar.networksync;
using DG.Tweening;
using UnityEngine.UI;
using pulsar.view.mono;

namespace pulsar.view.characters.hawke
{
    public class HawkeDashView : NetworkBehaviour, IGameListener
    {
        [SerializeField]
        private LayerMask mask;

        [SerializeField]
        private Image coolDownImage;

        [SyncVar(hook = "OnDash")]
        private bool isDashing = false;

        private float dashDistance = 7f;

        private float coolDown = 15;
        private float currentTime = 0;

        private FirstPersonController controller;
        private bool isReady;

        [SerializeField]
        private Camera myCamera;

        [SerializeField]
        private AudioClip clip;

        private AudioPlayerView audioView;

        //RobotLegs setup
        public event Action<IView> RemoveView;
        private IEventDispatcher _dispatcher = new EventDispatcher();
        private PlayerAttackManagerView attackManger;
        private Player_Health playerHealth;

        public IEventDispatcher dispatcher
        {
            get
            {
                return _dispatcher;
            }
            set
            {
                _dispatcher = value;
            }
        }

        [SerializeField]
        private float manaCost;

        void Start()
        {
            ViewNotifier.RegisterView(this);
            audioView = GetComponent<AudioPlayerView>();

            controller = GetComponent<FirstPersonController>();
            attackManger = GetComponent<PlayerAttackManagerView>();
            playerHealth = GetComponent<Player_Health>();
            currentTime = coolDown;
        }

        void Update()
        {            
            if (!isReady) return;
            if (!isLocalPlayer) return;
            if (attackManger.isReloading) return;
            if (attackManger.usingSuper) return;
            if (playerHealth.isDead) return;
            if (attackManger.isScoped) return;
            if (attackManger.holdingRift) return;


            if (currentTime > 0)
            {
                currentTime -= Time.deltaTime;
                coolDownImage.fillAmount = currentTime / coolDown;
            }
            else
            {
                float h = Input.GetAxis("Horizontal");
                float v = Input.GetAxis("Vertical");
                //if (Input.GetKeyDown(KeyCode.Alpha1))
                if (Input.GetButtonDown("Ability1"))
                {
                    if (attackManger.SpendMana(manaCost))
                    {
                        currentTime = coolDown;
                        Dash(h,v);
                    }
                }
            }
        }

        private void Dash(float h, float v)
        {
            audioView.PlayClip(clip);
            CmdDoDash();

            Vector3 direction = (transform.right * h) + (transform.forward * v);
            if (direction == Vector3.zero) direction = transform.forward;

            Vector3 endP = transform.position + (direction * dashDistance);

            RaycastHit hit;
            if (Physics.Raycast(transform.position, direction, out hit, dashDistance, mask))
            {
                float dis = Vector3.Distance(transform.position, hit.point);

                endP = transform.position + (direction * (dis * 0.9f));
            }

            controller.enabled = false;
            attackManger.busy = true;
            endP = new Vector3(endP.x, transform.position.y, endP.z);
            transform.DOMove(endP, 0.25f).SetEase(Ease.Linear).OnComplete(() => 
            {
                controller.enabled = true;
                attackManger.busy = false;
            });
        }

        [Command]
        private void CmdDoDash()
        {
            isDashing = !isDashing;
        }

        private void OnDash(bool v)
        {
            if (!isLocalPlayer)
            {
                audioView.PlayClip(clip, false);

            }
        }

        public void HandleGameFinished()
        {
            isReady = false;
        }

        public void HandleGameStart()
        {
            isReady = true;
        }

    }
}