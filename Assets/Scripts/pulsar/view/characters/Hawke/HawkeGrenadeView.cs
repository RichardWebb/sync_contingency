﻿/*
 * Hawke grenade view.
 * This class handles the detection of enemies and highlights them
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using DG.Tweening;
using System;
using pulsar.networksync;

namespace pulsar.view.characters.hawke
{
    public class HawkeGrenadeView : MonoBehaviour
    {
        private List<GameObject> enemies = new List<GameObject>();
        private bool ready = false;

        [SerializeField]
        private float endScale = 5f;

        [SerializeField]
        private Transform myParent;

        [SerializeField]
        private float scaleDuration = 0.25f;

        [SerializeField]
        private float life = 7.5f;

        [SerializeField]
        private Camera grenadeCam;

        [SerializeField]
        private Player_Health health;

        public void Init(Vector3 targetPoint)
        {
            health.EventDie += HandleDeath;
            transform.SetParent(null);
            transform.localScale = Vector3.zero;
            ready = true;

            transform.position = targetPoint;
            StartCoroutine(Disable());
            grenadeCam.enabled = true;

            transform.DOScale(endScale, scaleDuration);
        }

        private void HandleDeath()
        {
            grenadeCam.enabled = false;
        }

        private IEnumerator Disable()
        {
            yield return new WaitForSeconds(life);
            ready = false;

            transform.DOScale(0, 0.1f).OnComplete(() => 
            {
                transform.SetParent(myParent);
                transform.localScale = Vector3.zero;
                transform.localPosition = Vector3.zero;
            });

            if (enemies.Count > 0)
            {
                foreach (GameObject o in enemies)
                {
                    o.GetComponent<PlayerSettings>().UnHighLightPlayer();
                }
            }

            enemies.Clear();
            grenadeCam.enabled = false;
        }

        private void OnTriggerEnter(Collider col)
        {
            if (!ready) return;

            if (col.tag.Contains("Player"))
            {
                var myTeam = PlayerSettings.localPlayerSettings.teamName;
                var targetSettings = col.gameObject.GetComponent<PlayerSettings>();
                if (targetSettings == null) targetSettings = GetComponentInParent<PlayerSettings>();

                if (targetSettings == null) return;

                if (myTeam != targetSettings.teamName)
                {
                    if (!enemies.Contains(col.gameObject))
                    {
                        enemies.Add(col.gameObject);
                        targetSettings.HighLightPlayer();
                    }
                }
            }

        }


        private void OnTriggerExit(Collider col)
        {
            if (!ready) return;

            if (col.tag.Contains("Player"))
            {
                var myTeam = PlayerSettings.localPlayerSettings.teamName;
                var targetSettings = col.gameObject.GetComponent<PlayerSettings>();

                if (targetSettings != null)
                    if (myTeam != targetSettings.teamName)
                    {
                        if (enemies.Contains(col.gameObject))
                        {
                            enemies.Remove(col.gameObject);
                            targetSettings.UnHighLightPlayer();
                        }
                    }
            }
        }


    }
}
