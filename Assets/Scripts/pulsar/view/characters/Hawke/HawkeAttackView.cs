﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using pulsar.view.characters.attack;
using DG.Tweening;
using pulsar.view.characters.additional;
using System;
using UnityEngine.UI;
using pulsar.view.characters.shooting;
using pulsar.networksync;
using pulsar.view.mono;

namespace pulsar.view.characters.hawke
{
	public class HawkeAttackView : NetworkBehaviour, IAttackInputListener 
	{	
		[SyncVar(hook = "ToggleShot")]
		private bool toggleShot;

        [SyncVar(hook = "ToggleSuper")]
        private bool toggleSuper;

		[SyncVar(hook="OnRangeChange")]
		private float myShotRange;

		private IAttackManager attackManager;
	
		private Tweener canvasTween;

		//Config vars
		[SerializeField]
		private float maxRange = 500;

		[SerializeField]
		private float minRange = 100;

		[SerializeField]
		private GameObject projectileObj;

		[SerializeField]
		private Transform projectileSpawnPoint;

		[SerializeField]
		private GameObject myGun;

		[SerializeField]
		private CanvasGroup playerHolderCGroup;

		[SerializeField]
		private GameObject superProjectileObj;

		[SerializeField]
		private Transform superProjectileSpawnPoint;

		[SerializeField]
		private GameObject grenadeObj;

        [SerializeField]
        private GameObject arrow;

        [SerializeField]
        private HawkeGrenadeView grenadeView;

        [SerializeField]
        private Image ammoFillBar;

        [SerializeField]
        private LayerMask layerMask;

        [SerializeField]
        private Animator anim;

        private Player_Health myHealth;

		private PlayerSettings settings;
        private bool checkForButtonUp;
        private float currentTime;
        private float maxTime = 1.25f;
        private GameObject currentArrow;
        private bool usingGrenade = false;
        private float rangeDifference;
        private float damageDifference;

        public Vector3 spawnPoint
		{
			get
			{
				return superProjectileSpawnPoint.position;
			}

			set
			{
			}
		}

        private RayForPlayersView rayForPlayersView;


        [SerializeField]
        private AudioClip[] clips;

        private AudioPlayerView audioView;
        private bool checkRemote;

        void Start ()
		{
            audioView = GetComponent<AudioPlayerView>();
            myHealth = GetComponent<Player_Health>();

            myHealth.EventDie += HandleDeath;
            myHealth.EventRespawn += HandleRespawn;

            rayForPlayersView = GetComponent<RayForPlayersView>();
            rangeDifference = maxRange - minRange;

			settings = GetComponent<PlayerSettings>();
			//   cam = Camera.main;
			myShotRange = minRange;
			attackManager = GetComponent<IAttackManager>();

            var maxDamage = attackManager.myDamage * 1.5f;
            damageDifference = maxDamage - attackManager.myDamage;
            rayForPlayersView.PlayerRange = minRange;
		}

        private void HandleRespawn()
        {
            if (isLocalPlayer)
                StartCoroutine(CreateArrow(0.1f));
        }

        private void HandleDeath()
        {
            if (isLocalPlayer)
            {
                if (currentArrow != null)
                {
                    Destroy(currentArrow);
                }
            }
        }

        public override void OnStartLocalPlayer()
        {
            base.OnStartLocalPlayer();
            StartCoroutine(CreateArrow());
        }

        private IEnumerator CreateArrow(float delay = 0)
        {
            yield return new WaitForSeconds(delay);
            if (currentArrow != null) Destroy(currentArrow);
            currentArrow = Instantiate(arrow);

            currentArrow.transform.SetParent(projectileSpawnPoint);
            currentArrow.transform.localPosition = Vector3.zero; //new Vector3(-0.003f, -0.003f, -0.001f);
            currentArrow.transform.localRotation = Quaternion.Euler(Vector3.zero);//Quaternion.Euler(new Vector3(-61.582f, -102.905f, 2.604f));
            currentArrow.GetComponent<TrailRenderer>().enabled = false;

            //currentArrow.transform.localPosition = Vector3.zero;
            //currentArrow.transform.localRotation = Quaternion.Euler(-63f, -104f, 4f);
        }

        //Handle scoping out
        public void ScopeOut()
		{			
		}

		//Handle scoping in
		public void ScopeIn()
		{
		}

		//Handle shoot called
		public void Shoot(bool isScoped)
		{
            currentTime = 0f;
            checkRemote = Input.GetAxis("Fire1j") > 0;
            checkForButtonUp = true;
            anim.SetTrigger("Draw");
            audioView.PlayClip(clips[0]);
            rayForPlayersView.PlayerRange = minRange;
            //StartCoroutine(StartShotTimer());
			//DoStandardShot ();
		}

        void Update()
        {
            if (checkForButtonUp)
            {
                currentTime += Time.deltaTime;
                if (currentTime >= maxTime)
                {
                    currentTime = maxTime;
                }
                ammoFillBar.fillAmount = currentTime / maxTime;

                var percentage = currentTime / maxTime;
                var range = minRange + (rangeDifference * percentage);

                rayForPlayersView.PlayerRange = range;

                var axis = Input.GetAxis("Fire1j");

                // if (Input.GetKeyUp(KeyCode.Mouse0))
                bool release = checkRemote ? axis == 0 : Input.GetButtonUp("Fire1");

            
                //if (Input.GetButtonUp("Fire1") || (axis > 0 && axis <= 0.05f))
                if (release)
                {
                    checkRemote = false;
                    currentTime = Mathf.Clamp(currentTime, 0.1f, maxTime);

                    ammoFillBar.fillAmount = 0;

                    checkForButtonUp = false;
                    
                    //Debug.Log("my shot range - " + range);

                    CmdUpdateShotRange(range);

                    var damage = attackManager.myDamage + (int)(damageDifference * percentage);

                    currentArrow.transform.SetParent(null);
                    var view = currentArrow.GetComponent<ProjectileMonoView>();

                    if (usingGrenade)
                    {
                        usingGrenade = false;
                        audioView.PlayClip(clips[3]);
                        view.Launch(settings.myCamera.transform, range, false, 0.2f, true, projectileCallBack);
                        StartCoroutine(CreateArrow(0.25f));
                    }
                    else
                    {
                        view.Launch(settings.myCamera.transform, range, false, 0.2f, true);
                        DoStandardShot(range, damage);
                    }

                    anim.SetTrigger("Fire");
                    currentTime = 0;
                 //   ammoFillBar.fillAmount = 0;
                }


            }
        }

        private void projectileCallBack(Vector3 obj)
        {
            grenadeView.Init(obj);
        }

        //Do hip fire shot
        private void DoStandardShot(float range, float damage)
		{
            audioView.PlayClip(clips[1], true);
            StartCoroutine(CreateArrow(0.25f));

			var cam = PlayerSettings.localPlayerSettings.myCamera;
			//Get random points to aim for
			var points = attackManager.GetShotPoints(cam.transform.forward);

			RaycastHit hit;

            foreach (Vector3 point in points)
            {
                if (Physics.Raycast(cam.transform.TransformPoint(0, 0, 0.5f), point, out hit, range, layerMask))
                {
                    if (hit.collider.tag.Contains("Player"))
                    {
                        bool isHead = hit.collider.tag.Contains("Head");

                        PlayerSettings settings; 

                        settings = hit.transform.gameObject.GetComponent<PlayerSettings>();
                        if (PlayerSettings.localPlayerSettings.teamName != settings.teamName)
                        {
                            if (isHead)
                            {
                                if ((maxRange - range) <= 1f)
                                {
                                    var health = hit.collider.gameObject.GetComponentInParent<Player_Health>();
                                    if (health == null) health = hit.collider.GetComponent<Player_Health>();

                                    if (health != null)
                                        damage = health.maxHealth;
                                }
                                else
                                {
                                    damage += 20;
                                }
                            }

                            var n = hit.transform.gameObject.name;

                            //Hit the player
                            attackManager.HitPlayer(settings, n, (int)damage, isHead);
                        }

                        break;
                    }
                }
            }

			//Tell server to spawn a projectile
			CmdSpawnShot();
		}

        [Command]
		private void CmdUpdateShotRange(float range)
		{
			myShotRange = range;
		}

		public void OnRangeChange(float range)
		{
			myShotRange = range;
		}

		[Command]
		private void CmdSpawnShot()
		{
			toggleShot = !toggleShot;
		}
			
		public void ToggleShot(bool v)
		{
            if (settings.VRHasControl) return;

            if (!isLocalPlayer)
            {
			    var shot = (GameObject)Instantiate(projectileObj, projectileSpawnPoint.position, Quaternion.Euler(-90,0,0));
                //Debug.Log("my shot range - " + myShotRange);
			    shot.GetComponent<ProjectileMonoView>().Launch(settings.myCamera.transform, myShotRange, false, 0.2f, true);
                audioView.PlayClip(clips[0], false);
            }
		}
			
		//Super management

		/// <summary>
		/// Super projectile spawning and setting authority.
		/// </summary>
		/// <param name="_isServer"></param>
		[Command]
		private void CmdSpawnSuper(bool _isServer)
		{
			//Create the super object
			var obj = (GameObject)Instantiate(superProjectileObj, superProjectileSpawnPoint.position, Quaternion.identity);

			if (_isServer)
			{
			//	obj.GetComponent<PsyonicSuperView>().Init(PlayerSettings.localPlayerSettings.gameObject.GetComponent<PsyonicAttackView>(), true);
				NetworkServer.Spawn(obj);
			}
			else
			{
				NetworkServer.SpawnWithClientAuthority(obj, base.connectionToClient);
			}
		}

		/// <summary>
		/// Executes super - called from attack manager
		/// </summary>
		public void ExecuteSuper()
		{
			StartCoroutine(DoUltArrow());
		}

		//Gun plasma shot 
		private IEnumerator DoUltArrow()
		{
            CmdUpdateUlt();
            audioView.PlayClip(clips[2]);
            SpawnUltArrow(true);
			yield return new WaitForSeconds(1.5f);
			attackManager.ResetSuper();
		}

        [Command]
        private void CmdUpdateUlt()
        {
            toggleSuper = !toggleSuper;            
        }

        public void ToggleSuper(bool v)
        {
            if (settings.VRHasControl) return;

            if (!isLocalPlayer)
            {
                audioView.PlayClip(clips[1], false);
                SpawnUltArrow(false);
            }
        }

        private void SpawnUltArrow(bool doTrigger)
        {
            GameObject obj = (GameObject)Instantiate(superProjectileObj, superProjectileSpawnPoint.position, Quaternion.Euler(-90, 0, 0));

            var endPoint = transform.position + settings.myCamera.transform.forward * 250f;

            if (doTrigger)
            {
                obj.GetComponent<HawkeUltimateView>().Init(endPoint, doTrigger, HitPlayersWithSuper);
            }
            else
            {
                obj.GetComponent<HawkeUltimateView>().Init(endPoint, doTrigger);
            }
        }

        /// <summary>
        /// Call back for super projectile 
        /// </summary>
        /// <param name="playerSettings"></param>
        public void HitPlayersWithSuper(PlayerSettings playerSettings)
		{
            if (playerSettings.isLocalPlayer) return;
			if (playerSettings.teamName == PlayerSettings.localPlayerSettings.teamName) return;

			attackManager.UpdateServerWhoWasShot(playerSettings.gameObject.name, 1000, PlayerSettings.localPlayerSettings.playerName, gameObject.name);
		}

		//Grenade Management
		/// <summary>
		/// Called from attack manager
		/// </summary>
		public void ExecuteGrenade()
		{
            usingGrenade = true;
            if (currentArrow != null)
            {
                Transform obj = currentArrow.transform.GetChild(0);
                if (obj != null)
                    obj.gameObject.SetActive(true);
            }
		}
	}
}