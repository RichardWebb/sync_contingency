﻿/*
 * Hawke Ult view
 * This class handles the ult of hawke, a arrow that travels forward and kills all enemies it hits
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using System;
using pulsar.networksync;
using System.Collections.Generic;
using DG.Tweening;

namespace pulsar.view.characters.hawke
{
    public class HawkeUltimateView : MonoBehaviour
    {
        [SerializeField]
        private float life = 5f;

        private Action<PlayerSettings> callBack;
        private bool ready = false;

        private List<string> hitList = new List<string>();

        //Init the movement and callback
        public void Init(Vector3 endPoint, bool useTrigger, Action<PlayerSettings> _callback = null)
        {
            if (_callback != null) callBack = _callback;

            if (!useTrigger)
            {
                Destroy(GetComponent<BoxCollider>());
            }
            else
            {
                ready = true;
            }
            transform.DOLookAt(endPoint, 0.1f);
            transform.DOMove(endPoint, life).SetEase(Ease.Linear).OnComplete(() => {
                Destroy(this.gameObject);
            });
        }

        //Check collisions
        private void OnTriggerEnter(Collider col)
        {
            if (!ready) return;

            if (col.tag.Contains("Player"))
            {
                var health = col.GetComponent<Player_Health>();
                if (health == null) health = col.GetComponentInParent<Player_Health>();

                if (!health.isDead)
                {
                    if (!hitList.Contains(col.gameObject.name))
                    {
                        hitList.Add(col.gameObject.name);

                        var settings = col.GetComponent<PlayerSettings>();
                        if (settings == null) settings = GetComponentInParent<PlayerSettings>();

                        if (settings != null)
                            callBack(settings);
                    }
                }
            }
        }

    }
}