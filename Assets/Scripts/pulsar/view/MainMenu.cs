﻿///
/// This is the main menu view, this is connected to the Lobby Manager. This class controls the user name input and using the lobby manager to connect / start a server.
/// Created by Richard Webb, s1308033
///

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using pulsar.view.characterselection;
using pulsar.events;
using Rancon.Extensions.BootstrapSettings.API;
using Robotlegs.Bender.Platforms.Unity.Extensions.Mediation.Impl;
using Rancon.Extensions.BootstrapSettings;
using pulsar.model;
using UnityStandardAssets.Network;
using System;

namespace pulsar.view
{
    public class MainMenu : EventView, IBootstrapSettingsRequester
    {
        private GameObject mainMenu;
        private InputField playerInputField;
        private CanvasGroup canvasGroup;

        private GameObject joinButton;
        private CanvasGroup lobbyCanvas;
        private string teamName;
        private string playerName;
        private bool pressed = false;

        protected override void Start ()
        {
            base.Start();
            //Setup the main menu 
            SetupMenu();
            StartCoroutine(GetTeam());
        }

        private IEnumerator GetTeam()
        {
            yield return new WaitForEndOfFrame();
            //Getting the team from the bootstrap file, GotBootstrapSetting to be called below when finished.
            dispatcher.Dispatch(new BootstrapSettingsEvent(BootstrapSettingsEvent.Type.GET_SETTING, "//game_settings/team"));
            dispatcher.Dispatch(new BootstrapSettingsEvent(BootstrapSettingsEvent.Type.GET_SETTING, "//game_settings/push_path"));
        }

        private void SetupMenu()
        {
            //Locking the cursor to the main screen.
            Cursor.lockState = CursorLockMode.Confined;

            //Load the main 
            mainMenu = Instantiate(Resources.Load<GameObject>("Menus/MainMenu"));
            mainMenu.transform.SetParent(transform);
            RectTransform rect = mainMenu.GetComponent<RectTransform>();

            //position the menu 
            rect.localScale = Vector3.one;
            rect.anchoredPosition = Vector3.zero;
            rect.localPosition = Vector3.zero;

            //Get the input field 
            playerInputField = mainMenu.GetComponentInChildren<InputField>();

            //Hook input for the input field
            playerInputField.onValueChanged.AddListener(delegate { ManageInputChange(); });

            RectTransform logoRect = mainMenu.transform.FindChild("Logo").GetComponent<RectTransform>();
            logoRect.localScale = new Vector3(0, 0, 0);

            canvasGroup = mainMenu.GetComponentInChildren<CanvasGroup>();
            canvasGroup.alpha = 0;

            logoRect.DOScale(1f, 1.5f).SetDelay(0.25f).SetEase(Ease.InOutExpo).OnComplete(() => canvasGroup.DOFade(1f, 0.5f));

            //Creating the lobby manager
            GameObject lobby = Instantiate(Resources.Load<GameObject>("Menus/LobbyManager")) as GameObject;

            joinButton = lobby.transform.Find("MainPanel").Find("DirectPlaySubPanel").Find("JoinButton").gameObject;
            joinButton.GetComponent<Button>().onClick.AddListener(() => PlayGame());
            joinButton.SetActive(false);

            lobbyCanvas = lobby.transform.Find("LobbyPanel").GetComponent<CanvasGroup>();
            lobbyCanvas.alpha = 0;
        }

        private void ManageInputChange()
        {
            //Check on input change - toggle the join button if user has entered a value input.
            if (joinButton != null)
            {
                joinButton.SetActive(playerInputField.text.Length > 0);
            }
        }

        private void PlayGame()
        {
            GetComponent<AudioSource>().Play();
            //Ensure only one press
            if (pressed) return;

            pressed = true;
            playerName = playerInputField.text;

            //Storing the users name and team within the user data model 
            dispatcher.Dispatch(new UpdateDetailsEvent(UpdateDetailsEvent.Type.UPDATE_USER_DETAILS, playerName, teamName));

            canvasGroup.DOFade(0, 0.75f).OnComplete(() =>{ });

            if (lobbyCanvas != null)
            {
                //Creating the character selection menu that will sit with the lobby screen 
                GameObject characterSelectionMenu = Instantiate(Resources.Load<GameObject>("Menus/CharacterSelection"));
                characterSelectionMenu.GetComponent<CharacterSelectionView>().Init();

                characterSelectionMenu.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
                lobbyCanvas.DOFade(1,0.75f).SetDelay(0.25f).OnComplete(() =>
                {
                    Destroy(mainMenu, 1f);
                });
            }

        }

        //Get settings response
        public void GotBootstrapSetting(string key, object value)
        {
            if (key.Equals("//game_settings/team"))
            {
                teamName = value.ToString();
                bool isVR = false;

                //Checking if application is targted for VR
                if (teamName.ToLower().Contains("vr"))
                {

                    string[] team = teamName.Split('-');
                    teamName = team[1];
                    isVR = true;
                }

                UserDataModel._IsTeamOne = teamName.ToLower().Equals("red");

                //Running auto connect if VR character
                if (isVR)
                {
                    dispatcher.Dispatch(new UpdateDetailsEvent(UpdateDetailsEvent.Type.UPDATE_USER_DETAILS, "VR_Sync", teamName));

                    LobbyManager.s_Singleton.networkAddress = LobbyManager.s_Singleton.targetIP;

                    LobbyManager.s_Singleton.StartClient();

                    LobbyManager.s_Singleton.backDelegate = LobbyManager.s_Singleton.StopClientClbk;
                    LobbyManager.s_Singleton.DisplayIsConnecting();

                    LobbyManager.s_Singleton.SetServerInfo("Connecting...", LobbyManager.s_Singleton.networkAddress);
                }
            }
            else if(key.Equals("//game_settings/push_path"))
            {
                GameSettings.PushPath = value.ToString();
                Debug.Log("Push path = " + value.ToString());
            }
        }
        
    }
}
