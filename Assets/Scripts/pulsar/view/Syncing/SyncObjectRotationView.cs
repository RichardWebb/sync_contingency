﻿/*
 * Sync objects rotation over the network.
 * This class handles the rotation syncing over the network.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System;

namespace pulsar.view.syncing
{
    public class SyncObjectRotationView : NetworkBehaviour
    {
        private enum SyncStates
        {
            SYNC_X,
            SYNC_Y,
            SYNC_Z
        }

        [SerializeField]
        private SyncStates syncState;

        //Object to sync over the network
        [SerializeField]
        private GameObject objectToSync;

        [SerializeField]
        private bool doLocalRotation = false;

        //Network var - syncing rotation val
        [SyncVar(hook = "OnRotationUpdated")]
        private float syncRotationVal;

        //Lerp rate settings
        private float lerpRate = 20;

        //Last rotation 
        private float lastKnownRotation;

        //Threshold checks
        private float threshold = 0.3f;
        private float isCloseEnough = 0.4f;

        //local settings
        private PlayerSettings localSettings;

        private void Awake()
        {
            localSettings = GetComponent<PlayerSettings>();
        }

        private void Update()
        {
            //if dummy then lerp to known rotation
            if (!isLocalPlayer)
            {
                LerpToKnownRotation();
            }
        }

        private void LerpToKnownRotation()
        {
            //if we can sync rotation then do so
            if (!localSettings.StopSyncing && !localSettings.VRHasControl)
            {
                //Build target rotation
                Vector3 rot = Vector3.zero;
                switch (syncState)
                {
                    case SyncStates.SYNC_X:
                        rot = new Vector3(syncRotationVal, 0, 0);
                        break;

                    case SyncStates.SYNC_Y:
                        rot = new Vector3(0, syncRotationVal, 0);
                        break;

                    case SyncStates.SYNC_Z:
                        rot = new Vector3(0, 0, syncRotationVal);
                        break;
                }
                //lerp to rotation
                if (doLocalRotation)
                    objectToSync.transform.localRotation = Quaternion.Lerp(objectToSync.transform.localRotation, Quaternion.Euler(rot), lerpRate * Time.deltaTime);
                else 
                    objectToSync.transform.rotation = Quaternion.Lerp(objectToSync.transform.rotation, Quaternion.Euler(rot), lerpRate * Time.deltaTime);
            }
        }

        private void FixedUpdate()
        {
            //if local player then send rotation to server
            if (isLocalPlayer)
            {
                SendRotationsToServer();
            }
        }

        //Client - send rotation to server
        [Client]
        private void SendRotationsToServer()
        {
            bool shouldSend = false;
            float val = 0;
            //Check threshold of rotation
            switch (syncState)
            {
                case SyncStates.SYNC_X:
                    shouldSend = CheckThreshold(objectToSync.transform.localEulerAngles.x, lastKnownRotation);
                    val = objectToSync.transform.localEulerAngles.x;
                    break;

                case SyncStates.SYNC_Y:
                    shouldSend = CheckThreshold(objectToSync.transform.localEulerAngles.y, lastKnownRotation);
                    val = objectToSync.transform.localEulerAngles.y;
                    break;

                case SyncStates.SYNC_Z:
                    shouldSend = CheckThreshold(objectToSync.transform.localEulerAngles.x, lastKnownRotation);
                    val = objectToSync.transform.localEulerAngles.z;
                    break;
            }

            //Check if we should send - if so send rotation to server
            if (shouldSend)
            {
                CmdSendRotationToServer(val);
            }
        }

        //Server side - updates rotation val
        [Command]
        private void CmdSendRotationToServer(float val)
        {
            syncRotationVal = val;
        }

        //Client updates rotation val
        [Client]
        private void OnRotationUpdated(float val)
        {
            syncRotationVal = val;
        }

        //Threshold check
        private bool CheckThreshold(float rot1, float rot2)
        {
            return Mathf.Abs(rot1 - rot2) > threshold;
        }
    }
}