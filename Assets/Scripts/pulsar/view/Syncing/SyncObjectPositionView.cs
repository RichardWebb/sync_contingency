﻿/*
 * Sync object position view
 * This class handles the syncing of objects position over the network.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System;

namespace pulsar.view.syncing
{
    [NetworkSettings(channel = 0, sendInterval = 0.1f)]
    public class SyncObjectPositionView : NetworkBehaviour
    {
        //network var - used for position over network.
        [SyncVar(hook = "OnPositionUpdate")]
        private Vector3 lastKnownPos;

        //Lerp speeds for the dummy objects.
        private float lerpRate;
        private float standardLerpRate = 16;
        private float enhancedLerpRate = 27;

        //last position known
        private Vector3 lastPos;

        //distance checks
        private float threshold = 0.1f;
        private float isCloseEnough = 0.11f;

        //Local settings
        private PlayerSettings settings;

        private void Awake()
        {
            lerpRate = standardLerpRate;
            settings = GetComponent<PlayerSettings>();
        }

        // Update is called once per frame
        void Update()
        {
            //if object is not active disbale syncing.
            if (!this.gameObject.activeSelf) return;

            //if Dummy then move to position
            if (!isLocalPlayer)
                LerpDummy();
        }

        private void LerpDummy()
        {
            //if we should sync then move to known position
            if (!settings.StopSyncing && !settings.VRHasControl)
            {
                transform.position = Vector3.Lerp(transform.position, lastKnownPos, Time.deltaTime * lerpRate);
            }
        }

        private void FixedUpdate()
        {
            //Local player - Update server of known position.
            if (isLocalPlayer)
                SendPositionToServer();
        }

        //Only client side function - sends position to server 
        [ClientCallback]
        private void SendPositionToServer()
        {
            if (Vector3.Distance(transform.position, lastPos) > threshold)
            {
                lastPos = transform.position;
                CmdSendPositionToServer(transform.position);
            }
        }

        //Server side  - updates last known position for dummies.
        [Command]
        private void CmdSendPositionToServer(Vector3 pos)
        {
            lastKnownPos = pos;
        }

        [Client]
        private void OnPositionUpdate(Vector3 latestPos)
        {
            lastKnownPos = latestPos;
        }
    }
}