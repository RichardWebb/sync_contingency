﻿/*
 * Button highlight view
 * Small script that toggles text colour when mouse over an object.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using UnityEngine.UI;

namespace pulsar.view
{
    public class ButtonHighlightView : MonoBehaviour
    {
        //Configurable vars.
        [SerializeField]
        private Color highLightColor;

        [SerializeField]
        private Color normalColor;

        private Text myText;

        void Start()
        {
            //Get the text
            myText = GetComponentInChildren<Text>();
        }

        //On mouse over display mosue over text
        public void HandleMouseOver()
        {
            myText.color = highLightColor;
        }

        //OnMouseLeave display normal text
        public void HandleMouseExit()
        {
            myText.color = normalColor;
        }
    }
}