﻿/*
 * Super listener interface
 * This interface allows views to listener for a super event.
 * Created by Richard Webb, s1308033
 */

namespace pulsar.view.interfaces
{
    public interface ISuperListener 
    {
        void ExecuteSuper();
    }
}