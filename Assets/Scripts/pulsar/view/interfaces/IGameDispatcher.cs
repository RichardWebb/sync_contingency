﻿/*
 * game dispatcher interface
 * This interface allows views to listen for game start/finish
 * Created by Richard Webb, s1308033
 */

namespace pulsar.view.interfaces
{
    public interface IGameDispatcher
    {
        void HandleGameStared();
        void HandleGameFinshed();
    }
}
