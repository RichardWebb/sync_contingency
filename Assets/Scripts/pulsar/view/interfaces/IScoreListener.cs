﻿/*
 * Score listener interface
 * This interface allows views to listener for a score event.
 * Created by Richard Webb, s1308033
 */

using pulsar.events;

namespace pulsar.view.interfaces
{
    public interface IScoreListener
    {
        void UpdateTeam1Score(ScoreEvent evt);
        void UpdateTeam2Score(ScoreEvent evt);
    }
}
