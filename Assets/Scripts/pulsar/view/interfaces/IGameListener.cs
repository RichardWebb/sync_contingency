﻿/*
 * Game Listener interface
 * This interface allows views to listen for the game start and finish.
 * Created by Richard Webb, s1308033
 */

namespace pulsar.view.interfaces
{
    public interface IGameListener
    {
        void HandleGameStart();
        void HandleGameFinished();	
    }
}
