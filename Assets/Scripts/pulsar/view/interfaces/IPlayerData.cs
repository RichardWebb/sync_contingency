﻿using UnityEngine;
using System.Collections;

namespace pulsar.view.interfaces
{
    public interface IPlayerData
    {
        void GotPlayerData(PlayerVO Player);
    }
}
