﻿/*
 * Action listener interface
 * This interface allows views listen for player and enemy deaths
 * Created by Richard Webb, s1308033
 */

namespace pulsar.view.interfaces
{
    public interface IActionListener
    {
        void YouDied(string name);
        void KilledPlayer(string name);
    }
}
