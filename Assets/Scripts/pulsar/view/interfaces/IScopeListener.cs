﻿/*
 * Scope listener interface
 * This interface allows views to listen for scope events.
 * Created by Richard Webb, s1308033
 */

namespace pulsar.view.interfaces
{
    public interface IScopeListener
    {
        void ScopeIn();
        void ScopeOut();
    }
}
