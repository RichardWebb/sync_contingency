﻿/*
 * Rift listener interface
 * This interface is the hook between users holding the rift 
 * Created by Richard Webb, s1308033
 */

namespace pulsar.view.interfaces
{
    public interface IRiftListener
    {
        bool holdingRift { get; set; }
    }
}