﻿/*
 * Settings requester interface
 * This interface allows views to listen for user details events
 * Created by Richard Webb, s1308033
 */

using pulsar.events;
using Robotlegs.Bender.Extensions.Mediation.API;

namespace pulsar.view.interfaces
{
    public interface ISettingsRequester : IEventView
    {
        void GotSettings(UpdateDetailsEvent evt);
    }

}
