﻿/*
 * Lobby view
 * This script handles grabbing data from the bootstrap model containing the target ip and if the applicaion should be the server or not.
 * Created by Richard Webb, s1308033
 */
using Rancon.Extensions.BootstrapSettings.API;
using Rancon.Extensions.BootstrapSettings;
using Robotlegs.Bender.Platforms.Unity.Extensions.Mediation.Impl;
using UnityStandardAssets.Network;

namespace pulsar.view
{
    public class LobbyView : EventView, IBootstrapSettingsRequester
    {
        public bool IsServer { get; private set; }
    
        protected override void Start()
        {
             base.Start();
         //Dispatch request for settings.
            dispatcher.Dispatch(new BootstrapSettingsEvent(BootstrapSettingsEvent.Type.GET_SETTING, "//game_settings/is_server"));
            dispatcher.Dispatch(new BootstrapSettingsEvent(BootstrapSettingsEvent.Type.GET_SETTING, "//game_settings/ip"));
        }

        public void GetCurrentCharacter()
        {
            // dispatcher.Dispatch(new UserDetailsEvent())
        }

        public void GotBootstrapSetting(string key, object value)
        {
            //Get settings back - assign their values 
            if (key.Equals("//game_settings/is_server"))
            {
                IsServer = bool.Parse(value.ToString());
            }
            else if (key.Equals("//game_settings/ip"))
            {
                LobbyManager.s_Singleton.targetIP = value.ToString();
            }
        }
    }
}


