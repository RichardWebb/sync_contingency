﻿/*
 * Player settings class
 * This class contains all the players settings ( player name, team, syncing settings and vr control)
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using pulsar.view.characterselection;
using System;
using Robotlegs.Bender.Extensions.EventManagement.API;
using Robotlegs.Bender.Extensions.Mediation.API;
using Robotlegs.Bender.Extensions.ViewManagement.API;
using pulsar.events;
using Robotlegs.Bender.Extensions.EventManagement.Impl;
using pulsar.view.game.ui;
using pulsar.model;
using pulsar.view.interfaces;
using pulsar.view.game;
using UnityStandardAssets.Characters.FirstPerson;
using pulsar.view.characters.additional;
using pulsar.view.characters.super;
using pulsar.view.characters.special;
using pulsar.networksync;
using DG.Tweening;
using pulsar.view.mono;
using UnityEngine.UI;

namespace pulsar.view
{
	public class PlayerSettings : NetworkBehaviour, ILobbyPlayer, IGameListener, ISuperListener, IPlayerData
    {
        //Network variables - hooked for player name and team syncing.
        [SyncVar(hook = "OnNameSet")]
        public string playerName;

        [SyncVar(hook = "OnTeamSet")]
        public string teamName;

        [SyncVar(hook = "OnHookSet")]
        public bool hooked = false;

        //Configurable vars - marked for death obj and player hober text 
        [SerializeField]
        public TextMesh playerNameMeshTxt;

        [SerializeField]
        private GameObject markedObj;

        [SerializeField]
        private GameObject canvas;

        [SerializeField]
        public Canvas hudCanvas;

		[SerializeField]
		private AudioClip[] clips;

		[SerializeField]
		private AudioPlayerView audioView;

        [SerializeField]
        private SkinnedMeshRenderer[] skinnedMeshRends;

        [SerializeField]
        private MeshRenderer[] meshRends;

        [SerializeField]
        public string characterName;

        [SerializeField]
        public Text riftText;

        [SerializeField]
        private Image teamImage;

        [SerializeField]
        private Sprite[] teamSprite;

        public Sprite portraitSprite;

        [SerializeField]
        private GameObject markerObject;


        //Robot legs setup
        public event Action<IView> RemoveView;
        private IEventDispatcher _dispatcher = new EventDispatcher();

        public IEventDispatcher dispatcher
        {
            get
            {
                return _dispatcher;
            }
            set
            {
                _dispatcher = value;
            }
        }

        //Local player settings - quick access
        public static PlayerSettings localPlayerSettings = null;

        //Syncing settings - true stops all rotation, movement or shooting over the network for this player.
        private bool _stopSyncing = false;

        // team spawn points
        private GameObject[] spawnPoints;
    
        public bool StopSyncing
        {
            get
            {
                return _stopSyncing;
            }
            set
            {
                _stopSyncing = value;
            }
        }

        //Vr take over settings.
        private bool _vrHasControl;

        //Local players main camera
        [SerializeField]
        public Camera myCamera;
        private bool setMarkerColor;

        public bool VRHasControl
        {
            get
            {
                return _vrHasControl;
            }
            set
            {
                _vrHasControl = value;
            }
        }

        IEnumerator Start()
        {
            ViewNotifier.RegisterView(this);
            if (isLocalPlayer)
            {
                Cursor.visible = false;
                dispatcher.Dispatch(new UpdateDetailsEvent(UpdateDetailsEvent.Type.GET_USER_DETAILS));
            }

        //    Debug.Log("Getting player data " + characterName);

            yield return new WaitForSeconds(1);
            dispatcher.Dispatch(new PlayerDataEvent(PlayerDataEvent.Type.GET_PLAYER_DATA, characterName));
        }

        public void HookPlayer(bool v)
        {
            if (!VRHasControl)
            {
                hooked = v;
            //    GetComponent<FirstPersonController>().hooked = v;
            }
        }

        [Client]
        private void OnHookSet(bool v)
        {
            GetComponent<FirstPersonController>().hooked = v;
        }


        //Vr take over mode 
        public void HandleVRTakeOver()
        {
            VRHasControl = true;
            var nT = GetComponent<NetworkTransform>();
            if (nT != null)
                nT.enabled = false;
        }

        //Vr take over finished
        public void HandleVRRelease()
        {
            VRHasControl = false;
            var nT = GetComponent<NetworkTransform>();
            if (nT != null)
                nT.enabled = true;
        }

        //Client side - setting the team name 
        public void OnTeamSet(string team)
        {
            teamName = team;

            if (isLocalPlayer)
            {
                teamImage.sprite = teamSprite[team.ToLower() == "red" ? 0 : 1];
            }
        }

        //Client side setting the player name
        public void OnNameSet(string name)
        {
            playerName = name;      
            playerNameMeshTxt.text = name;
        }

        //Server side setting the player name  
        [Command]
        public void CmdNameSet(string name)
        {
            playerName = name;
            playerNameMeshTxt.text = playerName;
        }

        //Server side setting the team name
        [Command]
        public void CmdTeamSet(string name)
        {
            teamName = name;
        }

		public void PlayClip(int index)
		{
			audioView.PlayClip (clips [index]);
		}

        //Local player start 
        public override void OnStartLocalPlayer()
        {
            base.OnStartLocalPlayer();

            //if (isLocalPlayer)
            //{
                myCamera.tag = "MainCamera";
                GameObject.FindObjectOfType<SplineView>().DOCutScene();
                //Switch layers for local player.
                gameObject.layer = LayerMask.NameToLayer("LocalPlayer");


                if (characterName == "Hawke")
                {
                    int uiLayer = LayerMask.NameToLayer("UI");
                    Transform[] children = GetComponentsInChildren<Transform>();
                    foreach (Transform o in children)
                    {
                        if (o.gameObject.layer != uiLayer)
                            o.gameObject.layer = LayerMask.NameToLayer("LocalPlayer");
                    }
                }
           

                //Update local player 
                PlayerSettings.localPlayerSettings = this;
                FindSpawnPoint();

                //Create the score screen overlay
                Instantiate(Resources.Load("Menus/ScoreScreen"));
               // StartCoroutine(EnablePlayerNames());
           // }
            
        }

        public void EnableCanvas()
        {
            //enable hud
            canvas.SetActive(true);
            canvas.GetComponent<CanvasGroup>().DOFade(1, 0.75f) ;
            canvas.transform.SetParent(null);
        }

        //Toggle player names after a delay
        private IEnumerator EnablePlayerNames()
        {
            yield return new WaitForSeconds(0.1f);
            //Get all player settings (names)
            PlayerSettings[] players = GameObject.FindObjectsOfType<PlayerSettings>();

            //Assign text to players.
            foreach (PlayerSettings _player in players)
            {
                if (_player.teamName == teamName)
                {
                    _player.playerNameMeshTxt.gameObject.SetActive(true);
                }
            }
        }

        void Update()
        {
            if (!isLocalPlayer)
            {
                GetComponent<FirstPersonController>().enabled = false;
            }
        }

        //Get spawn points 
        private void FindSpawnPoint()
        {
            //Getting spawn points based on team 
            spawnPoints = GameObject.FindGameObjectsWithTag(UserDataModel._IsTeamOne ? "RedSpawnPoint" : "BlueSpawnPoint");

            if (spawnPoints != null && spawnPoints.Length > 0)
            {
                this.gameObject.transform.position = spawnPoints[UnityEngine.Random.Range(0, spawnPoints.Length - 1)].transform.position;
            }

            if (spawnPoints != null && spawnPoints.Length > 0)
                Array.Clear(spawnPoints, 0, spawnPoints.Length);

            spawnPoints = GameObject.FindGameObjectsWithTag(UserDataModel._IsTeamOne ? "RedRespawnPoint" : "BlueRespawnPoint");
        }

        //Get a respawn point
        public Vector3 GetRespawnPoint()
        {
            return spawnPoints[UnityEngine.Random.Range(0, spawnPoints.Length - 1)].transform.position;
        }

        //User data response  - Update team name and player name.
        public void HandleDetails(UpdateDetailsEvent evt)
        {
            if (isLocalPlayer)
            {
                CmdTeamSet(evt.teamName);
                CmdNameSet(evt.userName);
                playerNameMeshTxt.text = playerName;
            }
        }

        //Toggle marked for death 
        public void SetMarked()
        {
            GetComponent<MarkedPlayerView>().MarkPlayer();
        }

        //Call to server to destroy certain object.
        public void DestroyObj(GameObject obj)
        {
            CmdDestroyNetworkObj(obj);
        }

        //Server side destroy network objs
        [Command]
        private void CmdDestroyNetworkObj(GameObject obj)
        {
            NetworkServer.Destroy(obj);
        }

        public void HandleGameStart()
        {
            if (isLocalPlayer)
            {
                StartCoroutine(EnablePlayerNames());

				audioView.PlayClip(GameSettings.GameType == GameSettings.GameTypes.DEATH_MATCH ? clips[5] : clips[6]);

            }
        }

		public void HandleNearEndOfGame() 
		{
			GameScoreManager scoreManager = GameObject.FindObjectOfType<GameScoreManager>();
			if (teamName.ToLower () == "red") {
				if (scoreManager.team1Score != scoreManager.team2Score)
					audioView.PlayClip (scoreManager.team1Score > scoreManager.team2Score ? clips [8] : clips [7]);	
			} 
			else 
			{
				if (scoreManager.team1Score != scoreManager.team2Score)
					audioView.PlayClip (scoreManager.team2Score > scoreManager.team1Score ? clips [8] : clips [7]);	
			}

		}


        //End of game management.
        public void HandleGameFinished()
        {
            if (isLocalPlayer)
            {
                //Client side working out who won
                GetComponent<FirstPersonController>().enabled = false;
                GetComponent<Player_Health>().enabled = false;

                EndOfGameScreenView endGameView = GameObject.FindObjectOfType<EndOfGameScreenView>();
                GameScoreManager scoreManager = GameObject.FindObjectOfType<GameScoreManager>();
                int t1Score = scoreManager.team1Score;
                int t2Score = scoreManager.team2Score;
                int result = 0;
				int clip = 2;
                if (teamName.ToLower() == "red")
                {
                    if (t1Score > t2Score)
                    {
                        result = 0;
						clip = 2;
                    }
                    else if (t1Score < t2Score)
                    {
                        result = 1;
						clip = 3;
                    }
                    else if (t1Score == t2Score)
                    {
                        result = 2;
						clip = 4;
                    }
                    //Displaying end of game overlay
                    endGameView.DisplayEndGame(result);
                }
                else
                {
                    if (t2Score > t1Score)
                    {
                        result = 0;
						clip = 2;
                    }
                    else if (t2Score < t1Score)
                    {
                        result = 1;
						clip = 3;
                    }
                    else if (t2Score == t1Score)
                    {
                        result = 2;
						clip = 4;
                    }
                    //Displaying end of game overlay
                    endGameView.DisplayEndGame(result);
                }

				audioView.PlayClip (clips [clip]);
            }
        }

        //Marked player killed - refresh all players cooldowns
        public void KilledMarkedPlayer()
        {
            GetComponent<UltimateView>().RefreshSuper();
            GetComponent<GrenadeView>().RefreshGrenade();
            GetComponent<Player_Health>().RefeshHealth();
        }

		public void ExecuteSuper()
		{
			if (isLocalPlayer)
				audioView.PlayClip (clips[1]);
		}

        public void HighLightPlayer()
        {
            foreach (Renderer rend in skinnedMeshRends)
            {
                rend.gameObject.layer = LayerMask.NameToLayer("ExposeLayer");
                rend.material.SetColor("_SilhouetteColor", Color.gray);
                rend.material.SetFloat("_OutlineSize", 0.005f);
            }

            foreach (Renderer rend in meshRends)
            {
                rend.gameObject.layer = LayerMask.NameToLayer("ExposeLayer");
                rend.material.SetColor("_SilhouetteColor", Color.gray);
                rend.material.SetFloat("_OutlineSize", 0.005f);
            }
        }

        public void UnHighLightPlayer()
        {
            foreach (Renderer rend in skinnedMeshRends)
            {
                rend.gameObject.layer = LayerMask.NameToLayer("Default");
                rend.material.SetColor("_SilhouetteColor", Color.black);
                rend.material.SetFloat("_OutlineSize", 0f);
            }

            foreach (Renderer rend in meshRends)
            {
                rend.gameObject.layer = LayerMask.NameToLayer("Default");
                rend.material.SetColor("_SilhouetteColor", Color.black);
                rend.material.SetFloat("_OutlineSize", 0f);
            }
        }

        public void GotPlayerData(PlayerVO Player)
        {
          //  if (Player.characterName == characterName)
           //     Debug.Log("Got health " + Player.health);
        }

        public void Toggle3DMarker(bool state)
        {
            var r = markerObject.GetComponent<Renderer>();

            if (!setMarkerColor)
            {
                setMarkerColor = true;
                r.material = new Material(r.material);
                r.material.SetColor("_GlowColor", teamName.ToLower() == "red" ? Color.red : new Color(0, 170/255, 1, 1));
            }
            markerObject.SetActive(state);
        }
    }
}
