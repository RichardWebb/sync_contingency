﻿/*
 *Player Data Event
 * Created by, Richard Webb s1308033, Ethan Gill
 */
using UnityEngine;
using System.Collections;
using System;
using Robotlegs.Bender.Extensions.EventManagement.API;

namespace pulsar.events
{
    public class PlayerDataEvent : IEvent {

        public enum Type
        {
            GET_PLAYER_DATA,
            GOT_PLAYER_DATA
        }

        protected PlayerDataEvent.Type _type;
        protected string _name;
        protected PlayerVO player;

        public string CharacterName
        {
            get
            {
                return _name;
            }
        }

        public Enum type
        {
            get
            {
                return _type;
            }
        }


        public PlayerVO Player
        {
            get
            {
                return player;
            }
        }


        //Contructor
        public PlayerDataEvent(PlayerDataEvent.Type type, string name)
        {
            _type = type;
            _name = name;
        }

        public PlayerDataEvent(PlayerDataEvent.Type type, PlayerVO player)
        {
            _type = type;
            this.player = player;
        }
    }
}