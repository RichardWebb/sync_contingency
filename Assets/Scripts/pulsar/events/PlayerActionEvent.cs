﻿/*
 * Player action event, 
 * Handles player death and killing a player messages.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using System;
using Robotlegs.Bender.Extensions.EventManagement.API;

namespace pulsar.events
{
    public class PlayerActionEvent : IEvent
    {
        public enum Type
        {
            KILLED_A_PLAYER,
            PLAYER_HAS_DIED
        }

        protected PlayerActionEvent.Type _type;
        protected string _name;

        public string PlayerName
        {
            get
            {
                return _name;
            }
        }

        public Enum type
        {
            get
            {
                return _type;
            }
        }
        //Contructor
        public PlayerActionEvent(PlayerActionEvent.Type type, string name)
        {
            _type = type;
            _name = name;
        }
    }
}
