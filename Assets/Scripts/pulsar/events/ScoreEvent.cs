﻿/*
 * Score event, used for team 1 / 2 score messages
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using Robotlegs.Bender.Extensions.EventManagement.API;
using System;

namespace pulsar.events
{
    public class ScoreEvent : IEvent
    {
        public enum Type
        {
            UPDATE_TEAM1_SCORE,
            UPDATE_TEAM2_SCORE
        }

        protected ScoreEvent.Type _type;
        protected int _amount;

        public int scoreAmount
        {
            get
            {
                return _amount;
            }
        }

        public Enum type
        {
            get
            {
                return _type;
            }
        }

        //Constructor
        public ScoreEvent(ScoreEvent.Type type, int amount)
        {
            _type = type;
            _amount = amount;
        }

    }
}
