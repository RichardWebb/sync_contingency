﻿/*
 * Update details event, used for getting and setting user detail messages.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using System;
using Robotlegs.Bender.Extensions.EventManagement.API;

namespace pulsar.events
{
    public class UpdateDetailsEvent : IEvent
    {
        public enum Type
        {
            UPDATE_USER_DETAILS,
            GET_USER_DETAILS,
            GOT_USER_DETAILS
        }

        protected UpdateDetailsEvent.Type _type;

        public string userName;
        public string teamName;
        public bool isTeamOne;

        public Enum type
        {
            get
            {
                return _type;
            }
        }

        //Constructors

        public UpdateDetailsEvent(UpdateDetailsEvent.Type type)
        {
            _type = type;
        }

        public UpdateDetailsEvent(UpdateDetailsEvent.Type type, string userName, string teamName) :this (type)
        {
            this.userName = userName;
            this.teamName = teamName;
        }

        public UpdateDetailsEvent(UpdateDetailsEvent.Type type, string userName, bool isTeamOne, string teamName) : this(type)
        {
            this.userName = userName;
            this.isTeamOne = isTeamOne;
            this.teamName = teamName;
        }

    }
}
