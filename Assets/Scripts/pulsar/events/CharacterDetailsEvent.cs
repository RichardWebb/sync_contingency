﻿/*
 * Character details event. 
 * Contains all display text for the character.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using System;
using Robotlegs.Bender.Extensions.EventManagement.API;

namespace pulsar.events
{
    public class CharacterDetailsEvent : IEvent
    {
        public enum Type
        {
            GOT_CHARACTER_DETAILS,
            GET_CHARACTER_DETAILS,
        }

        protected CharacterDetailsEvent.Type _type;

        public string name;
        public string role;
        public string tagLine;
        public string weapon;
        public string grenadeTitle;
        public string grenadeDescription;
        public string primaryAttTitle;
        public string primaryAttDescriptn;
        public string secondaryAttTitle;
        public string secondaryAttDescriptn;
        public string ultAttTitle;
        public string ultAttDescriptn;

        public string currentCharacter;

        public Enum type
        {
            get
            {
                return _type;
            }
        }

        public CharacterDetailsEvent(CharacterDetailsEvent.Type type)
        {
            _type = type;
        }

        public CharacterDetailsEvent(CharacterDetailsEvent.Type type, string selectedCharacter) : this (type)
        {
            currentCharacter = selectedCharacter;
        }
        //Character details constructor.
        public CharacterDetailsEvent(Type type, string name, string role, string tagLine, string weapon, string grenadeTitle, string grenadeDescription, string primaryAttTitle,
                                                string primaryAttDescriptn, string secondaryAttTitle, string secondaryAttDescriptn, string ultAttTitle, string ultAttDescriptn) : this(type)
        {
            this.name = name;
            this.role = role;
            this.tagLine = tagLine;
            this.weapon = weapon;
            this.grenadeTitle = grenadeTitle;
            this.grenadeDescription = grenadeDescription;
            this.primaryAttTitle = primaryAttTitle;
            this.primaryAttDescriptn = primaryAttDescriptn;
            this.secondaryAttTitle = secondaryAttTitle;
            this.secondaryAttTitle = secondaryAttDescriptn;
            this.ultAttTitle = ultAttTitle;
            this.ultAttDescriptn = ultAttDescriptn;
        }
    }
}
