﻿/*
 * Game event, used to single start and end of games.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using Robotlegs.Bender.Extensions.EventManagement.API;
using System;

namespace pulsar.events
{
    public class GameEvent : IEvent
    {
        public enum Type
        {
           START_GAME,
           END_GAME
        }

        protected GameEvent.Type _type;

        public Enum type
        {
            get
            {
                return _type;
            }
        }

        public GameEvent(GameEvent.Type type)
        {
            _type = type;
        }

    }
}
