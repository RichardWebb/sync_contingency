﻿/*
 * Super event, used for execute super messages.
 * Created by Richard Webb, s1308033
 */
using UnityEngine;
using System.Collections;
using Robotlegs.Bender.Extensions.EventManagement.API;
using System;

namespace pulsar.events
{
    public class SuperEvent : IEvent
    {
        public enum Type
        {
            EXECUTE_SUPER
        }

        protected SuperEvent.Type _type;

        public Enum type
        {
            get
            {
                return _type;
            }
        }

        //Constructor
        public SuperEvent(SuperEvent.Type type)
        {
            _type = type;
        }
    }
}

