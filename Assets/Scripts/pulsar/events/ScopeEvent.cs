﻿/*
 * Scope event, used for scoping in and out messages.
 * Created by Richard Webb, s130833
 */
using UnityEngine;
using System.Collections;
using System;
using Robotlegs.Bender.Extensions.EventManagement.API;

namespace pulsar.events 
{
    public class ScopeEvent : IEvent
    {
        public enum Type
        {
            SCOPED_IN,
            SCOPED_OUT
        }

        protected ScopeEvent.Type _type;

        public Enum type
        {
            get
            {
                return _type;
            }
        }

        //Constructor.
        public ScopeEvent(ScopeEvent.Type type)
        {
            _type = type;
        }

    }
}
