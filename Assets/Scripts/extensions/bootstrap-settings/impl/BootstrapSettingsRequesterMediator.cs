﻿using Rancon.Extensions.BootstrapSettings.API;
using Rancon.Extensions.BootstrapSettings;
using Robotlegs.Bender.Bundles.MVCS;
using System.Collections.Generic;

namespace Rancon.Extensions.BootstrapSettings.Impl
{
    public class BootstrapSettingsRequesterMediator : Mediator
    {
        [Inject]
        public IBootstrapSettingsRequester view;

        private List<string> currentRequestKeys;

        public override void Initialize()
        {
            currentRequestKeys = new List<string>();
            AddViewListener<BootstrapSettingsEvent>(BootstrapSettingsEvent.Type.GET_SETTING, HandleBoostrapSettingsRequest);
            base.Initialize();
        }

        protected void HandleBoostrapSettingsRequest(BootstrapSettingsEvent bootstrapSettingsEvent)
        {
            currentRequestKeys.Add(bootstrapSettingsEvent.Key);
            if (currentRequestKeys.Count == 1)
            {
                AddContextListener<BootstrapSettingsEvent>(BootstrapSettingsEvent.Type.GOT_SETTING, HandleBoostrapSettingsResponse);
            }
            Dispatch(bootstrapSettingsEvent);
        }

        protected void HandleBoostrapSettingsResponse(BootstrapSettingsEvent bootstrapSettingsEvent)
        {
            int indexOfRequestKey = currentRequestKeys.IndexOf(bootstrapSettingsEvent.Key);
            if(indexOfRequestKey != -1)
            {
                view.GotBootstrapSetting(bootstrapSettingsEvent.Key, bootstrapSettingsEvent.Value);
                currentRequestKeys.RemoveAt(indexOfRequestKey);
                if (currentRequestKeys.Count == 1)
                {
                    RemoveContextListener<BootstrapSettingsEvent>(BootstrapSettingsEvent.Type.GOT_SETTING, HandleBoostrapSettingsResponse);
                }
            }
        }

    }
}
