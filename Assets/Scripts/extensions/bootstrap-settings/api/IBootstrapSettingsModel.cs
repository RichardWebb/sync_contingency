﻿

using System;
namespace Rancon.Extensions.BootstrapSettings.API
{
    public interface IBootstrapSettingsModel
    {
        bool LoadBootstrapSettings(string bootstrapSettingsPath);

        bool LoadBootstrapSettingsAsync(string bootstrapSettingsPath);

        bool LoadBootstrapSettingsAsync(string bootstrapSettingsPath, Action<bool> loadCallback);

        bool LoadBootstrapSettingsAsync(string bootstrapSettingsPath, Action<bool> loadCallback, int timerInterval);

        bool HasSetting(string key);

        T GetSetting<T>();
        
        T GetSetting<T>(string key);

        object GetSetting(Type type);

        object GetSetting(string key);
        
        object GetSetting(Type type, string key);

        bool LoadCompleted { get; }

        Action BeforeLoad { get; set; }

        Action AfterLoad { get; set; }

        Action<int> OnLoadProgress { get; set; }
        
        Action OnLoadFail { get; set; }
    }
}
