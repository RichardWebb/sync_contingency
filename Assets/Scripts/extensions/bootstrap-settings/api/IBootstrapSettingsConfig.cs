using Robotlegs.Bender.Framework.API;
using System;

namespace Rancon.Extensions.BootstrapSettings.API
{
	public interface IBootstrapSettingsConfig : IConfig
	{
        string BootstrapSettingsPath { get; }
        bool LoadAsync { get; }
        Type ModelType { get; }
        int TimerInterval { get; }        
        Action BeforeLoad { get; }
        Action AfterLoad { get; }
        Action OnLoadFail { get; }
        Action<int> OnLoadProgress { get; }

        bool Softly { get; }

        IBootstrapSettingsConfig WithPath(string path);
        IBootstrapSettingsConfig WithTimerInterval(int timerInterval);
        IBootstrapSettingsConfig WithBeforeLoad(Action beforeLoad);
        IBootstrapSettingsConfig WithAfterLoad(Action beforeLoad);
        IBootstrapSettingsConfig WithOnLoadProgress(Action<int> onLoadProgress);
        IBootstrapSettingsConfig WithOnLoadFail(Action onLoadError);
        IBootstrapSettingsConfig Asynchronously();
        IBootstrapSettingsConfig Synchronously();
        IBootstrapSettingsConfig LoadMeSoftlyWithThisSong();
        IBootstrapSettingsConfig NotSoftly();

    }
}