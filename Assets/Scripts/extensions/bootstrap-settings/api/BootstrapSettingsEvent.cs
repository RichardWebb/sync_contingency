using Robotlegs.Bender.Extensions.EventManagement.Impl;

namespace Rancon.Extensions.BootstrapSettings
{
    public class BootstrapSettingsEvent : Event
    {
        public enum Type
        {
            GET_SETTING,
            GOT_SETTING
        }

        public System.Type ReturnType { get; private set; }
        public string Key { get; private set; }
        public object Value { get; private set; }

        public BootstrapSettingsEvent(BootstrapSettingsEvent.Type type, string key) : this(type, key, typeof(object)) { }
        public BootstrapSettingsEvent(BootstrapSettingsEvent.Type type, string key, System.Type returnType)
            : base(type)
        {
            this.ReturnType = returnType;
            this.Key = key;
        }

        public BootstrapSettingsEvent(BootstrapSettingsEvent.Type type, string key, object value) : this(type, key, value.GetType(), value) {}
		public BootstrapSettingsEvent(BootstrapSettingsEvent.Type type, string key, System.Type returnType, object value)
			: this(type, key, returnType)
		{
			this.Value = value;
		}
    }
}
