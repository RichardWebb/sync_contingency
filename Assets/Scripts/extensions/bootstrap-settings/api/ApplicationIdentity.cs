﻿using System.Runtime.Serialization;
namespace Rancon.Extensions.BootstrapSettings.API
{
    [DataContract(Name="application", Namespace="")]
    public class ApplicationIdentity
    {
        [DataMember(Name="project_name")]
        public string ProjectName { get; set; }

        [DataMember(Name="application_name")]
        public string ApplicationName { get; set; }

        [DataMember(Name="client", IsRequired=false)]
        public string Client { get; set; }

        public override string ToString()
        {
            return ProjectName + " - " + ApplicationName + ". Client: " + Client;
        }
    }
}
