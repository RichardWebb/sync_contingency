
Bootstrap Settings Extension
=========

Version
-------
* 1.0.0
  * Bootstrap settings xml is loaded from a configurable path
  * Settings can be retrieved and will be serialised to any requested type 


How to Setup, Install & Configure
------------------
Make sure to have a bootstrap settings xml file somewhere readable from your application.

* Install the extensions

```c#
  context.Install<BootstrapSettingsExtension> ()

```

* Configure it based on your platform / location of bootstrap settings file

```c#
  context.Configure( new UnityBootstrapSettingsConfig())
  // or any of the following to use a custom path
  context.Configure( new UnityBootstrapSettingsConfig("C:/Assets/boostrap-settings.xml"))
  context.Configure( new BootstrapSettingsConfig("C:/Assets/boostrap-settings.xml"))
```
If you dont configure the extension it will default to loading a file at path 'bootstrap-settings.xml'

How to use
---------------

The bootstrap extension will be ready and have your bootstrap file loaded by the time the context initializes.
You can either request settings through events or directly on the IBootstrapSettingsModel through injection.

## Using Events


The BootstrapSettingsEvent has a type of 'GET_SETTING' which will request any settings from the model.
The key parameter is an xpath string to the xml node you're after.
The optional return type parameter will serialise the xml to an object of this type. If this serialisation process fails the innerXml property (string) is returned.

A response event with Type 'GOT_SETTING' will be dispatched with the setting value as the 'value' payload. The key will also be stored in this responding event, be sure to check the event you heard is for the setting you requested by validating the key.
If specified a Type when making the request the Value will be this Type.

## Example
```c#
  dispatcher.Dispatch(new BootstrapSettingsEvent(BootstrapSettingsEvent.Type.GET_SETTING, "//Version", typeof(AppVersion)));
```

## Injecting the model
```c#
  [Inject] public IBootstrapSettingsModel model;
  string xPathString = "//size/x";
  Type returnType = typeof(int);
  int setting = model.GetSetting(returnType, xPathString);
```
