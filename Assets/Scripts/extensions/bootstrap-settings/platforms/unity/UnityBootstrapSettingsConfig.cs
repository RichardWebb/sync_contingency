
using System;
namespace Rancon.Extensions.BootstrapSettings
{
	public class UnityBootstrapSettingsConfig : BootstrapSettingsConfig
	{
		public UnityBootstrapSettingsConfig () : base(UnityEngine.Application.streamingAssetsPath+"/bootstrap-settings.xml")
		{
			
		}

		public UnityBootstrapSettingsConfig (string bootstrapSettingsPath) : base(bootstrapSettingsPath)
		{

		}

        public UnityBootstrapSettingsConfig(string bootstrapSettingsPath, Type modelType)
            : base(bootstrapSettingsPath, modelType)
        {

        }

	}
}