﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Linq;
using UnityEditor;

namespace Rancon.Extensions.BootstrapSettings.editor
{
    [InitializeOnLoad]
    public class UnityVSReference 
    {
        static UnityVSReference() 
        {

            Type projectFilesGeneratorType = Type.GetType("SyntaxTree.VisualStudio.Unity.Bridge.ProjectFilesGenerator, SyntaxTree.VisualStudio.Unity.Bridge, Version=1.9.9.0, Culture=neutral, PublicKeyToken=null");
            if (projectFilesGeneratorType != null)
            {
                MethodInfo callbackMethod = ((Func<String, String, String>)ProjectFilesCallback).Method;
                FieldInfo projectFileGenerationField = projectFilesGeneratorType.GetField("ProjectFileGeneration", BindingFlags.Static | BindingFlags.Public);
                projectFileGenerationField.SetValue(null,
                    Delegate.Combine(
                        (Delegate) projectFileGenerationField.GetValue(null),
                        Delegate.CreateDelegate(projectFileGenerationField.FieldType, null, callbackMethod)));
            }
        }

        private static string ProjectFilesCallback(string name, string content)
        {
            if (Path.GetExtension(Path.GetFileNameWithoutExtension(name)) != ".Editor") 
            {
                XNamespace ns = "http://schemas.microsoft.com/developer/msbuild/2003";
                XDocument document = XDocument.Parse(content);
                XElement element = document.Descendants(ns + "ItemGroup").FirstOrDefault();
                element.Add(new XElement(ns + "Reference", new XAttribute("Include", "System.Runtime.Serialization")));
                var str = new Utf8StringWriter();
                document.Save(str);
                return str.ToString();
            }
            else
                return content;
        }

        private class Utf8StringWriter : StringWriter
        {
            public override Encoding Encoding
            {
                get { return Encoding.UTF8; }
            }
        }
    }
}