Callback Timer Extension
=====================
Provides an easy way to set up a periodic callback for use where threading operations are unsafe (e.g Unity)