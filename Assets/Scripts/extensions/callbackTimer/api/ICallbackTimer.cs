﻿using System;

namespace Rancon.Extensions.CallbackTimer.API
{
    public interface ICallbackTimer
    {
        int TicksRemaining { get; }

        void StartOneOffTimer(int duration, Action tickCallback);
        
        void StartTimer(int interval, Action completeCallback);

        void StartTimer(int interval, Action tickCallback, int totalTicks, Action completeCallback);

		void StartOneOffFrameTimer(int frameInterval, Action completeCallback);
		
		void StartFrameTimer(int frameInterval, Action tickCallback);
		
		void StartFrameTimer(int frameInterval, Action tickCallback, int totalTicks, Action completeCallback);

        void UsingUpdateType(UpdateType updateType);

        void Stop();

        void Dispose();

		bool IsRunning { get; }

		bool TimeIsFrames { get; }

        
    }
}
