﻿
namespace Rancon.Extensions.CallbackTimer.API
{
    public enum UpdateType
    {
        Update,
        LateUpdate,
        EndOfFrameUpdate
    }
}
