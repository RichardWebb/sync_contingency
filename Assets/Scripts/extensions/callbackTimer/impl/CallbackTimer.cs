﻿using System;
using Rancon.Extensions.CallbackTimer.API;
using System.Timers;

namespace Rancon.Extensions.CallbackTimer.Impl
{
	public class CallbackTimer : ICallbackTimer
	{
		private const int FRAMES_PER_SECOND = 60;
		public int TicksRemaining { get; protected set; }
		private Action tickCallback;
		private Action completeCallback;
		public bool TimeIsFrames { get; protected set; }
		public virtual bool IsRunning { get; protected set; }
		private Timer timer;

		public CallbackTimer ()
		{}

		public virtual void StartOneOffTimer(int duration, Action completeCallback)
		{
			StartTimer(duration, null, 1, completeCallback);
		}

		public virtual void StartTimer(int interval, Action tickCallback)
		{
			StartTimer(interval, tickCallback, -1, null);
		}

		public virtual void StartTimer(int interval, Action tickCallback, int totalTicks, Action completeCallback)
		{
			InitTimer (interval, tickCallback, totalTicks, completeCallback, false);
		}

		public virtual void StartOneOffFrameTimer(int frameInterval, Action completeCallback)
		{
			StartFrameTimer(frameInterval, null, 1, completeCallback);
		}

		public virtual void StartFrameTimer(int frameInterval, Action tickCallback)
		{
			StartFrameTimer(frameInterval, tickCallback, -1, null);
		}

		public virtual void StartFrameTimer(int frameInterval, Action tickCallback, int totalTicks, Action completeCallback)
		{
			InitTimer ((float)frameInterval / (float)FRAMES_PER_SECOND * 1000f, tickCallback, totalTicks, completeCallback, true);
		}


		protected virtual void InitTimer(float interval, Action tickCallback, int totalTicks, Action completeCallback, bool timeIsFrames) {
			if (IsRunning)
				Stop ();

			this.tickCallback = tickCallback;
			this.TicksRemaining = totalTicks;
			this.completeCallback = completeCallback;
			this.TimeIsFrames = timeIsFrames;

			timer = new Timer (interval);
			timer.Elapsed += OnTimerTick;
			timer.Start ();
			IsRunning = true;
		}

		private void OnTimerTick(Object source, ElapsedEventArgs e) {
			if (TicksRemaining != -1)
				TicksRemaining--;

			if (TicksRemaining == 0) {
				((Timer)source).Stop ();
				completeCallback ();
			} else {
				tickCallback ();
			}
		}

		public virtual void Stop() {
			IsRunning = false;
			completeCallback = null;
			tickCallback = null;
			timer.Stop ();
		}

		public virtual void Dispose() {
			if (IsRunning) {
				Stop ();
			}
		}

        //TODO; implement this somehow....?!
        public void UsingUpdateType(UpdateType updateType)
        {
            throw new NotImplementedException();
        }
    }
}

