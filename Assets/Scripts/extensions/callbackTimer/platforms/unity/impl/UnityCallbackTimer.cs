﻿using Rancon.Extensions.CallbackTimer.API;
using System;
using System.Collections;
using UnityEngine;

namespace Rancon.Extensions.CallbackTimer.Impl
{
    public class UnityCallbackTimer : MonoBehaviour, ICallbackTimer
    {
        private float interval;
		public int TicksRemaining { get; protected set; }
        private Action tickCallback;
        private Action completeCallback;
        private Coroutine timerCoroutine;
		public bool TimeIsFrames { get; protected set; }
		public virtual bool IsRunning { get; protected set; }
		private int frameCount;
        protected UpdateType updateType = UpdateType.Update;

        public virtual void UsingUpdateType(UpdateType updateType)
        {
            this.updateType = updateType;
        }

        public virtual void StartOneOffTimer(int duration, Action completeCallback)
        {
            StartTimer(duration, null, 1, completeCallback);
        }

		public virtual void StartTimer(int interval, Action tickCallback)
        {
            StartTimer(interval, tickCallback, -1, null);
        }

		public virtual void StartTimer(int interval, Action tickCallback, int totalTicks, Action completeCallback)
		{
			InitTimer (interval * 0.001f, tickCallback, totalTicks, completeCallback, false);
            IsRunning = true;
            timerCoroutine = StartCoroutine(RunCoroutine());
        }

		public virtual void StartOneOffFrameTimer(int frameInterval, Action completeCallback)
		{
			StartFrameTimer(frameInterval, null, 1, completeCallback);
		}
		
		public virtual void StartFrameTimer(int frameInterval, Action tickCallback)
		{
			StartFrameTimer(frameInterval, tickCallback, -1, null);
		}

		public virtual void StartFrameTimer(int frameInterval, Action tickCallback, int totalTicks, Action completeCallback)
		{
			InitTimer (frameInterval, tickCallback, totalTicks, completeCallback, true);
			IsRunning = true;
		}

		protected virtual void InitTimer(float frameInterval, Action tickCallback, int totalTicks, Action completeCallback, bool timeIsFrames)
		{
			this.interval = frameInterval;
			this.tickCallback = tickCallback;
			this.TicksRemaining = totalTicks;
			this.completeCallback = completeCallback;
			this.TimeIsFrames = timeIsFrames;
		}

        protected virtual void Update()
        {
            if (updateType == UpdateType.Update)
            {
                Tick();
            }
        }

        protected virtual void LateUpdate()
        {
            if(updateType == UpdateType.EndOfFrameUpdate)
            {
                StartCoroutine(DoEndOfFrameUpdate());
            }
            else if (updateType == UpdateType.LateUpdate)
            {
                Tick();
            }
        }

        protected virtual IEnumerator DoEndOfFrameUpdate()
        {
            yield return new WaitForEndOfFrame();
            Tick();
        }

        protected virtual void Tick()
        {
            if (IsRunning && TimeIsFrames)
            {
                if (++frameCount % interval == 0)
                {
                    if (TicksRemaining != -1 && --TicksRemaining == 0)
                    {
                        TimerComplete();
                    }
                    else
                    {
                        TickComplete();
                    }
                }

            }
        }

        protected virtual IEnumerator RunCoroutine()
        {
            while (true)
            {
                float targetTime = Time.realtimeSinceStartup + interval;
                while (Time.realtimeSinceStartup < targetTime)
                {
                    yield return 0;
                }
                break;
            }
            // WaitForSeconds is effected by Time.timescale. the above approach shouldn't be..
			// TODO: Create a custom co-routine here
            //yield return new WaitForSeconds(interval);
			if (IsRunning)
            {
                if (TicksRemaining != -1 && --TicksRemaining == 0)
                {
                    TimerComplete();
                }
                else
                {
					TickComplete();
                    timerCoroutine = StartCoroutine(RunCoroutine());
                }
            }
        }

		protected virtual void TickComplete ()
		{
			if (tickCallback != null)
			{
				tickCallback();
			}
		}
        
		protected virtual void TimerComplete()
        {
			// simon: changed the order here so that if the callback starts another timer then it isn't instantly killed by the stop command
			Action currentCallback = completeCallback;

			Stop();

			if (currentCallback != null)
			{
				currentCallback();
			}
        }
		
		public virtual void Stop()
		{
			IsRunning = false;
			if (timerCoroutine != null)
			{
				StopCoroutine(timerCoroutine);
			}
			if(completeCallback!= null)
			{
				completeCallback = null;
			}
			if(tickCallback != null)
			{
				tickCallback = null;
			}
		}

		public virtual void Dispose()
		{
			if(IsRunning)
			{
				Stop();
			}
			completeCallback = null;
			tickCallback = null;
			timerCoroutine = null;
			Destroy(this);
		}
    }
}