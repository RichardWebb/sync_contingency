﻿using Rancon.Extensions.CallbackTimer.API;
using Rancon.Extensions.CallbackTimer.Impl;
using Robotlegs.Bender.DependencyProviders;
using Robotlegs.Bender.Framework.API;

namespace Rancon.Extensions.CallbackTimer.Platforms.Unity
{
    public class UnityCallbackTimerExtension : IExtension
    {
        public void Extend(IContext context)
        {
            context.injector.Map(typeof(ICallbackTimer)).ToProvider(new UnityComponentProvider(typeof(UnityCallbackTimer)));
        }
    }
}
